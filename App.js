import React from 'react';
import firebase from '@firebase/app';
import { AppRegistry } from 'react-native';
import { AppLoading,Font } from 'expo';
import ReduxThunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './src/reducers';
import Router from './src/Router';



export default class App extends React.Component {

  state = {
    isReady: false,
  };
  
  componentWillMount() {
    console.ignoredYellowBox = ['Setting a timer'];
    const config = {
      apiKey: 'AIzaSyDJCMMr0UVaeV-BqDMjId-15HS2v2mIDDU',
      authDomain: 'hashtazappfirebase-bcf55.firebaseapp.com',
      databaseURL: 'https://hashtazappfirebase-bcf55.firebaseio.com',
      projectId: 'hashtazappfirebase-bcf55',
      storageBucket: 'hashtazappfirebase-bcf55',
      messagingSenderId: '202020929339'
    };

    firebase.initializeApp(config);
  }

  async _loadAssetsAsync() {
    await Font.loadAsync({
      'coolvetica': require('./assets/fonts/OpenSans-Bold.ttf')
    })
  }
  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this._loadAssetsAsync}
          onFinish={() => this.setState({ isReady: true })}
          
          onError={console.warn}
        />
      );
    }
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}
AppRegistry.registerComponent('H18RNEXPV101', () => App);