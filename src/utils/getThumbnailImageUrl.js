
function getThumbnailUrl(imageName) {
    return new Promise(async (res, rej) => {
        var data = {
            filename: "thumb_" + imageName.toString()
        };
        fetch('https://us-central1-hashtazappfirebase-bcf55.cloudfunctions.net/getSignedUrl', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((response) => response.json())
            .then((responseJson) => {
                res(responseJson.imageUri)

            })
            .catch((error) => {
                rej(error);
            });
    });
}

export default getThumbnailUrl;