import { ImageManipulator } from 'expo';
function imageThumbnailForMessenger(image) {
    return ImageManipulator.manipulate(
      image.uri, [{ resize: { height: 300, width: 300 } }],
      {
        compress: 1,
        format: 'jpeg'
      }
    );
  }

  export default imageThumbnailForMessenger;