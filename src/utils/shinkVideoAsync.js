import { ImageManipulator } from 'expo';

function reduceVideoAsync(video){
    return ImageManipulator.manipulate(
        video.uri,[{resize:{height: 720, width: 1280}}],{
            format: 'jpeg'
        }
      
    );
  }

  export default reduceVideoAsync;