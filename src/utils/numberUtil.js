   
   
function formatNumber(n) {
    for (let i = 0; i < ranges.length; i++) {
        if (n >= ranges[i].divider) {
            return (n / ranges[i].divider).toString() + ranges[i].suffix;
        }
    }
    return n.toString();
}

export default formatNumber;