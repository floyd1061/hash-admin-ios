import firebase from '@firebase/app'
function uploadPhoto(uri, uploadUri, progressCallback) {
  return new Promise(async (res, rej) => {
    const response = await fetch(uri);
    const blob = await response.blob();
    //console.log(response)
    const ref = firebase.storage().ref(uploadUri);
    const unsubscribe = ref.put(blob).on(
      'state_changed',

      state => {
        progressCallback && progressCallback((state.bytesTransferred / state.totalBytes) * 100)
      },
      err => {
        unsubscribe();
        rej(err);
      },
      async () => {
        unsubscribe();
        const url = await ref.getDownloadURL();
        //
        res(url);
      },
    );
  });
}

export default uploadPhoto;
