import { ImageManipulator } from 'expo';
function reduceImageAsync(image) {
  if (image.height < image.width) {
    return ImageManipulator.manipulate(
      image.uri, [{ resize: { height: 1114, width: 1440 } }],
      {
        compress: 0.47,
        format: 'jpeg'
      }
    );
  } else if (image.height > image.width) {
    return ImageManipulator.manipulate(
      image.uri, [{ resize: { height: 960, width: 720 } }],
      {
        compress: 0.47,
        format: 'jpeg'
      }
    );
  }

}


export default reduceImageAsync;
