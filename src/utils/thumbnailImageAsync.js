import { ImageManipulator } from 'expo';
function imageThumbnailForMessenger(image) {
    return ImageManipulator.manipulate(
      image, [{ resize: { height: 256, width: 256 } }],
      {
        compress: 1,
        format: 'jpeg'
      }
    );
  }
  export default imageThumbnailForMessenger;