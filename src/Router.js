import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { Scene, Router, Actions } from 'react-native-router-flux'
import Loader from './components/Loader'
import Login from './components/Login/Login'
import ChatHome from './components/ChatHome/ChatHome'
import ChatListView from './components/Chat/ChatList'
import Register from './components/Register/ResigterComponent'
import Gallery from './components/Event/Gallery'
import AboutApp from './components/AppSettings/Settings/AboutApp'
import UserProfile from './components/UserProfile/Profile/UserProfile'
import NetworkItem from './components/Network/NetworkUsage'
import AccountDeleteForm from './components/AccountDelete/AccountDelete'
import AdSettings from './components/AdSettings/AdSettings'
import EventBox from './components/Event/EventBox'
import EditProfile from './components/EditProfile/EditProfile'
import PrivacyPolicy from './components/PrivacyPolicy/PrivacyPolicy'
import FavouriteEvent from './components/FavouriteEvent/FavouriteEvent'
import FollowMainView from './components/FollowingUsers/FollowMainView'
import FriendsFollowersActivity from './components/FriendsFollowersActivity/FriendsFollowersActivity'
import SearchUserAndEvent from './components/SearchUserAndEvent/SearchUserAndEvent/SearchTabView'
import MostlyLikedEvents from './components/MostlyLikedEvents/MostlyLikedEvents'
import FriendEvents from './components/FriendEvents/FriendEvents'
import UserProfileTimeline from './components/UserProfileTimeline/Profile'
import SearchNotificationList from './components/SearchNotifications/SearchNotificationList';
import Walkthrough from './components/Walkthrough/Walkthrough';
import HomeStoryList from './components/HomeStoryList';
import SegmentedProfile from './components/UserProfileTimeline/SegmentedProfile'

import { Ionicons } from '@expo/vector-icons';
//router 12/8/18

const RouterComponent = () => {
    return (
        <Router sceneStyle={{}}>
            <Scene>
                <Scene key="Intro" hideNavBar={true}>
                    <Scene key="loaderIntro" component={Loader} title="" />
                </Scene>

                <Scene key="auth" hideNavBar={true}>
                    <Scene key="login" component={Login} title="Please Login" />
                </Scene>


                <Scene key="Register" component={Register}
                    hideNavBar={false} navTransparent
                    renderBackButton={() => (
                        <TouchableOpacity onPress={() => Actions.firebase.auth()}>
                            <Ionicons name="ios-arrow-back" size={40} color="#fff" style={{ paddingLeft: 15 }} />
                        </TouchableOpacity>
                    )}
                    
                />
                <Scene key="SegmentedProfile" hideNavBar={true} >
                    <Scene key="SegmentedProfile" component={SegmentedProfile} />
                </Scene>

                <Scene key="HomeStoryList" hideNavBar={true} >
                    <Scene key="HomeStoryList" component={HomeStoryList} />
                </Scene>

                <Scene key="Event" hideNavBar={true}>
                    <Scene key="Event" component={EventBox} />
                </Scene>
                
                <Scene key="UserProfileTimeline" hideNavBar={true} panHandlers={null}>
                    <Scene key="UserProfileTimeline" component={UserProfileTimeline} panHandlers={null}/>
                </Scene>
                
                <Scene key="FriendEvents" hideNavBar={true}>
                    <Scene key="FriendEvents" component={FriendEvents} />
                </Scene>


                <Scene key="MostlyLikedEvents" hideNavBar={true}>
                    <Scene key="MostlyLikedEvents" component={MostlyLikedEvents} />
                </Scene>

                <Scene key="SearchUserAndEvent" hideNavBar={true}>
                    <Scene key="SearchUserAndEvent" component={SearchUserAndEvent} />
                </Scene>

                <Scene key="FriendsFollowersActivity" hideNavBar={true}>
                    <Scene key="FriendsFollowersActivity" component={FriendsFollowersActivity} />
                </Scene>

                <Scene key="FollowMainView" hideNavBar={true}>
                    <Scene key="FollowMainView" component={FollowMainView} />
                </Scene>

                <Scene key="FavouriteEvent" hideNavBar={true}>
                    <Scene key="FavouriteEvent" component={FavouriteEvent} />
                </Scene>

                <Scene key="SearchNotifications" hideNavBar={true}>
                    <Scene key="SearchNotifications" component={SearchNotificationList} />
                </Scene>

                <Scene key="PrivacyPolicy" hideNavBar={true}>
                    <Scene key="PrivacyPolicy" component={PrivacyPolicy} />
                </Scene>

                <Scene key="EditProfile" hideNavBar={true}>
                    <Scene key="EditProfile" component={EditProfile} />
                </Scene>

                <Scene key="AdSettings" hideNavBar={true}>
                    <Scene key="AdSettings" component={AdSettings} />
                </Scene>

                <Scene key="settings" hideNavBar={true}>
                    <Scene key="settings" component={AboutApp} />
                </Scene>

                <Scene key="AccountDeleteForm" hideNavBar={true}>
                    <Scene key="AccountDeleteForm" component={AccountDeleteForm} />
                </Scene>

                <Scene key="NetworkItem" hideNavBar={true}>
                    <Scene key="NetworkItem" component={NetworkItem} />
                </Scene>

                <Scene key="userprofile" hideNavBar={true}>
                    <Scene key="UserProfile" component={UserProfile} panHandlers={null}/>
                </Scene>

                <Scene key="Walkthrough" hideNavBar={true}>
                    <Scene key="Walkthrough" component={Walkthrough} />
                </Scene>

                <Scene key="gallery" hideNavBar={true}>
                    <Scene key="gallery" component={Gallery} title="eventBox" />
                </Scene>

                <Scene key="main" hideNavBar={true}>
                    <Scene
                        key="chatHome"
                        component={ChatHome}
                        title="Chat List"
                        initial
                    />
                </Scene>
                <Scene key="chatListView" hideNavBar={true}>
                    <Scene
                        key="chatListView"
                        component={ChatListView}
                        title=""
                        initial
                    />
                </Scene>

            </Scene>
        </Router>
    );
};

export default RouterComponent;