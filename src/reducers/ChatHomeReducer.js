// import data from './ChatHomeUserData.json';
import data from './Messages.json';

// export default () => data;

const INITIAL_STATE = {
    //allMessages: []
    conversations: {}
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'fetch_chatList':
            //console.log(action.payload)
            return { ...state, conversations: { ...state.conversations, ...action.payload } };
    }

    return state;
};