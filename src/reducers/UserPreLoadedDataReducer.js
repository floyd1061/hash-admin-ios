import {
    GET_ALL_FRIENDS_OF_CURRENT_USER,
    GET_ALL_FRIEND_REQUEST,
    GET_ALL_APP_USERS
} from '../actions/types';


INITIAL_STATE = {
    friends: null,
    fiendRequests: {},
    applicationUsers: null,
    allRelations: {}
}
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_ALL_FRIENDS_OF_CURRENT_USER:
            return { ...state, friends: { ...state.friends, [action.key]: action.payload } };
        case GET_ALL_FRIEND_REQUEST:
            return { ...state, fiendRequests: action.payload };
        case GET_ALL_APP_USERS:
            return { ...state, applicationUsers: action.payload };
        case "GET_ALL_RELATIONS":
            return { ...state, allRelations: action.payload };
        default:
            return state;
    }
}