
import {
    EVENT_TRANSCRIPT_FETCH_SUCCESS,
    FETCH_EVENT_ATTENDEE_SUCCESS,
    EVENT_FETCH_SUCCESS
} from '../actions/types'
import _ from 'lodash';


const INITIAL_STATE = {

    invited: null,
    attended: null,
    liked: null,
    shares: null,
    eventTranscript: [],
    attendee: {},
    allEventsOfTheUser: null
}

export default (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case EVENT_TRANSCRIPT_FETCH_SUCCESS:

            let index = _.findIndex(state.eventTranscript, ["eventKey", action.key]);

            if (index > 0) {
                return {
                    ...state,
                    eventTranscript: [...state.eventTranscript, Object.assign(...state.eventTranscript[index], action.payload)]
                }
            }
            return {
                ...state, eventTranscript: [...state.eventTranscript, action.payload]
            };
        case EVENT_FETCH_SUCCESS:
            return { ...state, allEventsOfTheUser: { ...state.allEventsOfTheUser, ...action.payload } }

        case FETCH_EVENT_ATTENDEE_SUCCESS:
            ///state.attendee = {};
            return { ...state, attendee: { ...state.attendee, ...action.payload } }
        default:
            return INITIAL_STATE;
    }
}