import {
    FETCH_USER_PROFILE_PHOTO,
    USER_STATUS_CHANGED,
    USER_DESCRIPTION_CHANGED,
    USER_BIRTHDAY_CHANGED,
    FETCH_USER_PROFILE_INFO
} from '../actions/types'

const INITIAL_STATE = {
    profile_picture: [],
    date_of_birth: '',
    user_profile_status: '',
    user_profile_description: ''
}


export default (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case FETCH_USER_PROFILE_PHOTO:
            return { ...state, profile_picture: action.payload };
        case USER_STATUS_CHANGED:
            return { ...state, user_profile_status: action.payload };
        case USER_DESCRIPTION_CHANGED:
            return { ...state, user_profile_description: action.payload };
        case USER_BIRTHDAY_CHANGED:
            return { ...state, date_of_birth: action.payload };
        default:
            return state;
    }
}