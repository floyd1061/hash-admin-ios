import { FETCH_USER_PROFILE_INFO } from '../actions/types'
const INITIAL_STATE = {
    userProfileInfoData: null
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case FETCH_USER_PROFILE_INFO:
            return { ...state, userProfileInfoData: action.payload };
        default:
            return state;
    }
}