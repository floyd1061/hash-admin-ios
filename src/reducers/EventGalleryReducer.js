import {
    FETCH_EVENT_GALLERY_SUCCESS
} from '../actions/types'

const INITIAL_STATE = {
    eventGallery: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case FETCH_EVENT_GALLERY_SUCCESS:
            return { ...state, eventGallery: action.payload }
        default:
            return INITIAL_STATE;
    }
};