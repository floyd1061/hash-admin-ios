import _ from 'lodash';
import {
  TIMELINE_DATA_FETCH_SUCCESS,
  EVENT_NAME_CHANGED,
  EVENT_PRIOR_VALUE_CHANGED,
  EVENT_START_TIME_CHANGED,
  EVENT_END_TIME_CHANGED,
  ADD_EVENT_SUCCESS,
  SELECTER_YEAR
} from '../actions/types';

const INITIAL_STATE = {
  timeLineData: null,
  eventName: '',
  eventPriority: 0,
  eventStartTime: '--:--',
  eventEndTime: '--:--',
  event: null,
  selectedYear: null
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TIMELINE_DATA_FETCH_SUCCESS:
    return { ...state, timeLineData: { ...state.timeLineData, [action.year]: action.payload } };
      //return { ...state, timeLineData: [...state.timeLineData, action.payload] };
    case EVENT_NAME_CHANGED:
      return { ...state, eventName: action.payload }
    case EVENT_PRIOR_VALUE_CHANGED:
      return { ...state, eventPriority: action.payload }
    case EVENT_START_TIME_CHANGED:
      return { ...state, eventStartTime: action.payload }
    case EVENT_END_TIME_CHANGED:
      return { ...state, eventEndTime: action.payload }
    case ADD_EVENT_SUCCESS:
      return { ...state, event: action.payload }
    case SELECTER_YEAR:
      return { ... state, selectedYear: action.payload}

    default:
      return state;
  }
};