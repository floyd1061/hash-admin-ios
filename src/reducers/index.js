import { combineReducers  } from 'redux';
import UserPrelodDataReducer from './UserPreLoadedDataReducer';
import ProfileReducer from './ProfileReducer';
import DimentionReducer from './DimentionReducer';
import AuthReducer from './AuthReducer';
import ChatReducer from './ChatReducer';
import ChatHomeReducer from './ChatHomeReducer';
import SearchUserReducer from './SearchUserReducer';
import SearchEventReducer from './SearchEventReducer';
import ActivityReducer from './ActivityReducer';
import ChatUsersReducer from './ChatUsersReducer';
import RegisterReducer from './RegisterReducer'
import ChatPopupReducer from './ChatPopupReducer';
import EventGalleryReducer from './EventGalleryReducer';
import EditUserProfileReducer from './EditUserProfileReducer';
import FriendsFollowersReducer from './FriendsFollowersReducer';
import SearchNotificationReducer from './SearchNotificationReducer'
import EventReducer from './EventReducer'
import EditUserProfileInfoReducer from './EditUserProfileInfoReducer'


export default combineReducers({
    timelineData: ProfileReducer,
    userPreloadData: UserPrelodDataReducer,
    window: DimentionReducer,
    auth: AuthReducer,
    dim: DimentionReducer,
    messages: ChatReducer,
    allMessages: ChatHomeReducer,
    searchUsers: SearchUserReducer,
    searchEvents: SearchEventReducer,
    activities: ActivityReducer,
    friendsfollowers: FriendsFollowersReducer,
    searchnotifications: SearchNotificationReducer,
    chatUsers: ChatUsersReducer,
    register: RegisterReducer,
    chatPopup: ChatPopupReducer,
    editUserProfile: EditUserProfileReducer,
    editUserProfileInfo: EditUserProfileInfoReducer,
    eventData: EventReducer,
    eventGallery: EventGalleryReducer
});