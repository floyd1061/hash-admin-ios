
const INITIAL_STATE = {
    // allMessages: data
    status: false,
    popup_conversationId: null
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'set_popup':
            // return action.payload;
            return { ...state, status: action.payload.status, popup_conversationId: action.payload.popup_conversationId };            
    }
    
    return state;
};

