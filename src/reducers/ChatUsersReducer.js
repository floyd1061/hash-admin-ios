const INITIAL_STATE = {
    chatUsers: null
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'fetch_chatUsers':
            return { ...state, chatUsers: action.payload};
    }
    
    return state;
};