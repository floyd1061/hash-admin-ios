import { Dimensions } from 'react-native';
import { WINDOW_STATE } from '../actions/types'

const window = Dimensions.get('window');

const INITIAL_STATE = {
    window_dimention : window
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
       /*  case WINDOW_STATE:
            return { ...state, window_dimention: action.payload } */
        default:
            return INITIAL_STATE;
    }
}