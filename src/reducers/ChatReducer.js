import _ from 'lodash';
const INITIAL_STATE = {
    messages: null,
    conversationId: null,
    friendId: null,
    receiversId: null,
    conversationMetaData: null,
    conversationData: {},
    chatGallery: [],
    unseenMsgCount: []
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'fetch_messages':
            return { ...state, messages: action.payload };
        case 'start_conversation':
            return { ...state, conversationId: action.payload.conversation_id, friendId: action.payload.friendId, receiversId: action.payload.receiversId };
        case 'START_NEW_CONVERSATION':
            return { ...state, conversation_id: action.payload };
        case 'create_conversation':
            return { ...state, friendId: action.payload };
        case 'fetch_conversation_meta':
            return { ...state, conversationMetaData: action.payload };
        case 'fetch_conversation':
            return { ...state, conversationData: action.payload };
        case 'save_message':
            return { ...state, messages: [...state.messages, action.payload] };
        case 'FETCH_CHAT_GALLERY_SUCCESS':
            return { ...state, chatGallery: action.payload }
        case 'GET_UNSEEN_MSG_COUNT':
            let index = _.findIndex(state.unseenMsgCount, ["key", action.msgKey]);

            if (index > -1) {

                state.unseenMsgCount.splice(index, 1)

            }
            return {
                ...state, unseenMsgCount: [...state.unseenMsgCount, { key: action.msgKey, count: action.payload }]
            };


    }

    return state;
};