import {
    NAME_CHANGED,    
    EMAIL_CHANGED,
    PASSWORD_CHANGED,
    CONFIRM_PASSWORD_CHANGED,
    REGISTER_USER_SUCCESS,
    REGISTER_USER_FAIL,
    REGISTER_USER
  } from '../actions/types';
  
  const INITIAL_STATE = {
    name: '',
    email: '',
    password: '',
    confirmpassword: '',
    user: '',
    error: '',
    loading: false
  };
  
  export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case NAME_CHANGED:
        return { ...state, name: action.payload };
      case EMAIL_CHANGED:
        return { ...state, email: action.payload };
      case PASSWORD_CHANGED:
        return { ...state, password: action.payload };
      case CONFIRM_PASSWORD_CHANGED:
        return { ...state, confirmpassword: action.payload };
      case REGISTER_USER:
        return { ...state, loading: true, error: '' };
      case REGISTER_USER_SUCCESS:
        return { ...state, ...INITIAL_STATE, user: action.payload };
      case REGISTER_USER_FAIL:
        return { ...state, error: 'Authentication Failed.', loading: false };
      default:
        return state;
    }
  };
  