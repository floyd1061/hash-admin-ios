import firebase from '@firebase/app'
import auth from '@firebase/auth'
import { Actions } from 'react-native-router-flux';
import {
  NAME_CHANGED,
  CONFIRM_PASSWORD_CHANGED,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_FAIL,
  REGISTER_USER
} from './types';
import { insertTimelineDataForNewYear } from '../actions';

const thisYear = new Date(Date.now()).getFullYear();

export const nameChanged = (text) => {
  return {
    type: NAME_CHANGED,
    payload: text
  };
};


export const confirmPasswordChanged = (text) => {
  return {
    type: CONFIRM_PASSWORD_CHANGED,
    payload: text
  };
};


export const registerUser = ({ name, email, password, confirmpassword }) => {
  return (dispatch) => {
    dispatch({ type: REGISTER_USER });

    firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(user => {
        firebase.auth().signInWithEmailAndPassword(email, password)
          .then(user => {
            const { currentUser } = firebase.auth();
            currentUser.updateProfile({
              displayName: name
            }).then(function () {
              let ref = firebase.database().ref(`/users/${currentUser.uid}/userProfileInfo`)
              ref.update({
                displayName: name
              })
              ref.update({
                email: email
              })
            }).catch(function (error) {
            });
            dispatch(insertTimelineDataForNewYear(thisYear)).then(() => Promise.all([
              registerUserSuccess(dispatch, currentUser)
            ]));



          })
          .catch((error) => registerUserFail(dispatch));
      })
      .catch((error) => registerUserFail(dispatch));
  };
};

const registerUserFail = (dispatch) => {
  dispatch({ type: REGISTER_USER_FAIL });
};

const registerUserSuccess = (dispatch, user) => {

  dispatch({
    type: REGISTER_USER_SUCCESS,
    payload: user
  });

  Actions.Intro();
};