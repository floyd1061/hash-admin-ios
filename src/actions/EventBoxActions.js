import firebase from '@firebase/app'
import { Actions } from 'react-native-router-flux';
import {
  COVER_VIDEO_FETCH,
  COVER_VIDEO_FETCH_SUCCESS,
  COVER_VIDEO_FETCH_FAIL,
  MASTER_EVENT_DATA_FETCH_SUCCESS,
  FETCH_EVENT_GALLERY_SUCCESS
} from './types'
export const fetchGalleryImages = (eventKey) => {
  const { currentUser } = firebase.auth();
  return (dispatch) => {
    firebase.database().ref(`warehouse/assets/${eventKey}`)
      .on('value', snapshot => {
        console.log(snapshot.val())
        dispatch({ type: FETCH_EVENT_GALLERY_SUCCESS, payload: snapshot.val() });
      });
  };

};


