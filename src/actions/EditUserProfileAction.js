import firebase from '@firebase/app'
import "firebase/auth"
import "firebase/database"
import "firebase/storage"
import { Actions } from 'react-native-router-flux';
import { saveNotification, getallfriends } from '../actions';
import {
    FETCH_USER_PROFILE_PHOTO,
    USER_STATUS_CHANGED,
    USER_DESCRIPTION_CHANGED,
    FETCH_USER_PROFILE_INFO,
    USER_BIRTHDAY_CHANGED
} from './types'


export const fetchProfilePictureFromDB = (user) => {
    //const { currentUser } = firebase.auth();
    return (dispatch) => {
        firebase.database().ref(`/users/${user}/userProfileInfo/profilepicture`)
            .limitToLast(1)
            .on('value', snapshot => {
                dispatch({ type: FETCH_USER_PROFILE_PHOTO, payload: snapshot.val() });
            });
    };
}

export const statusChanged = (text) => {
    return {
        type: USER_STATUS_CHANGED,
        payload: text
    };
};
export const updateUserStatus = (userStatus) => {
    const { currentUser } = firebase.auth();
    return (dispatch) => {
        let ref = firebase.database().ref(`/users/${currentUser.uid}/userProfileInfo`);
        ref.update({
            userStatus: userStatus
        })
    };
}
export const descriptionChanged = (text) => {
    return {
        type: USER_DESCRIPTION_CHANGED,
        payload: text
    };
};
export const updateUserDescription = (userDescription) => {
    const { currentUser } = firebase.auth();
    return (dispatch) => {
        let ref = firebase.database().ref(`/users/${currentUser.uid}/userProfileInfo`);
        ref.update({
            userDescription: userDescription
        })
    };
}

export const updateUserBirthday = (userBirthday) => {
    const { currentUser } = firebase.auth();
    return (dispatch) => {
        let ref = firebase.database().ref(`/users/${currentUser.uid}/userProfileInfo`)
        ref.update({
            userBirthday: userBirthday
        })
    };
}

export const fetchUserProfileInfo = (user) => {

    return (dispatch) => {
        firebase.database().ref(`/users/${user}/userProfileInfo`)
            .on('value', snapshot => {
                dispatch({ type: FETCH_USER_PROFILE_INFO, payload: snapshot.val() });
            });
    };
}


export const displayNameChanged = (text) => {
    return {
        type: USER_DISPLAYNAME_CHANGED,
        payload: text
    };
};
export const updateDisplayName = (text) => {
    const { currentUser } = firebase.auth();
    return (dispatch) => {
        currentUser.updateProfile({

            displayName: text

        }).then(function () {
            let email = currentUser.email
            let ref = firebase.database().ref(`/users/${currentUser.uid}/userProfileInfo`)
            ref.update({
                displayName: text
            })
            if (email != "") {
                ref.update({
                    email: currentUser.email
                })
            }

        }).catch(function (error) {

        });
    };
}

export const acceptFriendRequest = (objectID, notificationId, badge) => {

    //const { displayName, email, objectID } = user;
    const { currentUser } = firebase.auth();


    return (dispatch) => {
        var userProfileData = {
            displayName: currentUser.displayName,
            profilepicture: currentUser.photoURL,
            email: currentUser.email,
        }
        //console.log(userProfileData);

        firebase.database().ref(`/users/${currentUser.uid}/friends`).push({
            'userId': objectID,
            'status': 1
        })
        firebase.database().ref(`/users/${objectID}/friends/${badge}`).update({
            'status': 1
        })

        var data = { status: 1 }
        firebase.database().ref(`/warehouse/notification/${currentUser.uid}/${notificationId}`).update({
            data: { status: 1 }
        })
        dispatch(saveNotification(data, objectID, userProfileData, currentUser.uid, 'friendRequest', 'You are now friends with'
            , `${currentUser.displayName}`, 1))
        dispatch(getallfriends(currentUser.uid))

    }

}
export const getAllRelations = () => {
    const { currentUser } = firebase.auth();
    return (dispatch) => {
        firebase.database().ref(`/users/${currentUser.uid}/friends`).on('value', snap => {
            dispatch({ type: 'GET_ALL_RELATIONS', payload: snap.val() })
        })
    }
}

export const sendFriendRequest = (user, userProfileData) => {
    const { displayName, email, objectID } = user;
    const { currentUser } = firebase.auth();
    return (dispatch) => {
        let ref = firebase.database().ref(`/users/${currentUser.uid}/friends`);
        ref.push({
            'userId': objectID,
            'status': 0
        }).then((snap) => {
            //console.log(snap.key);
            var data = { status: 0 }
            dispatch(saveNotification(data, objectID, userProfileData, currentUser.uid, 'friendRequest', 'New friend request'
                , `${currentUser.displayName}`, snap.key))
            dispatch(getallfriends(currentUser.uid))
        })




    }

}




