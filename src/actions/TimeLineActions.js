import firebase from '@firebase/app'
import { AsyncStorage } from "react-native"
import date from 'date-and-time';
import {
    TIMELINE_DATA_FETCH_SUCCESS
    , EVENT_NAME_CHANGED
    , EVENT_PRIOR_VALUE_CHANGED,
    EVENT_START_TIME_CHANGED,
    EVENT_END_TIME_CHANGED,
    ADD_EVENT_SUCCESS,
    ADD_EVENT_FAIL,
    GLOBAL_NUMBER_OF_YEARS,
    SELECTER_YEAR,
    EVENT_FETCH_SUCCESS
} from './types';
import _ from 'lodash'

const daysOfMonth18 = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
const thisYear = new Date(Date.now()).getFullYear();

const getAllDayByYear = (year) => {
    const daysInYearArr = [];
    let dateToday = new Date(year);
    if (date.isLeapYear(dateToday)) {
        daysOfMonth18[2] = 29;
    }

    for (var m = 0; m <= 11; m++) {
        for (var dn = 1; dn <= daysOfMonth18[m]; dn++) {
            let dte = date.format(new Date(year, m, dn), 'ddd,MMM DD');
            var c = new Date(2018, m, dn);

            daysInYearArr.push(dte);
        }
    }
    return daysInYearArr;
};
const getAllUnixTimeStampsByYear = (year) => {
    const epochsInYear = [];
    let dateToday = new Date(year);
    if (date.isLeapYear(dateToday)) {
        daysOfMonth18[2] = 29;
    }

    for (var m = 0; m <= 11; m++) {
        for (var dn = 1; dn <= daysOfMonth18[m]; dn++) {

            var c = new Date(year, m, dn);
            var e = c.valueOf();
            epochsInYear.push(e);
        }
    }
    return epochsInYear;
};

const storeAsyncTimelineDataForYears = (year) => {
    //const year = new Date(Date.now()).getFullYear();

    // console.log(year)

    try {
        //console.log(daysInYearArr);
        let i = 1;
        var j = 0;
        var s = 1;
        let timelinedataAsync = [];

        let daysInYearArr = getAllDayByYear(year);
        let epochsInYear = getAllUnixTimeStampsByYear(year);

        daysInYearArr.forEach(item => {
            if (i >= 4) {
                i = 1;
            }

            if (s >= 7) {
                s = 1;
            }



            timelinedataAsync.push({
                "uid": epochsInYear[j].toString(),
                "year": year,
                "unixUtc": epochsInYear[j],
                "svgSegmentId": i,
                "svgStyleId": s,
                "hasEvent": false,
                "name": item,
            })
            i++;
            j++;
            s++;
        });
        return timelinedataAsync;
        //_storeTlData(year, timelinedataAsync);
    } catch (error) {
        //console.log(error);
    }



};

const _storeTlData = async (currentyear, data) => {
    try {

        await AsyncStorage.setItem(currentyear.toString(), JSON.stringify(data)).then((data) => {
            //console.log(data);
        })
    } catch (error) {
        // Error saving data
    }
}

export const fetchTimelineDataFromAsyncStoreAfterLogin = (year) => {

    return (dispatch) => {

        _retrieveData(year, dispatch);
        //console.log(data)
        //dispatch({ type: TIMELINE_DATA_FETCH_SUCCESS, payload: data });
    };
}

const _retrieveData = async (year, dispatch) => {

    try {
        //await AsyncStorage.removeItem(year.toString());
        const value = await AsyncStorage.getItem(year.toString());
        //console.log(value);
        let JsonVal = JSON.parse(value);

        if (JsonVal !== null) {
            //console.log(JsonVal);
            dispatch({ type: TIMELINE_DATA_FETCH_SUCCESS, year: year, payload: JsonVal });
            //console.log(JsonVal);
            //return JsonVal;
        } else {
            let data = storeAsyncTimelineDataForYears(year);
            _storeTlData(year, data);

        }
    } catch (error) {
        // Error retrieving data
    }
}
function replacer(key, value) {
    //console.log(typeof value);

    return { value };
}
const _retrieveAndMergeData = async (item, dispatch) => {

    try {
        let itemNew = { ...item, hasEvent: true }

        const value = await AsyncStorage.getItem(itemNew.year.toString());
        let JsonVal = JSON.parse(value);
        let index = _.findIndex(JsonVal, ["uid", itemNew.uid])
        JsonVal = { ...JsonVal, [index]: itemNew }
        if (JsonVal !== null) {

        }
        let timelineData = _.map(JsonVal, (val) => {

            return { ...val, uid: val.uid };
        });
        let JSONdata = JSON.stringify(timelineData);

        if (timelineData !== null) {

            AsyncStorage.setItem(itemNew.year.toString(), JSONdata, () => {
                AsyncStorage.getItem(itemNew.year.toString(), (err, result) => {
                    dispatch({ type: TIMELINE_DATA_FETCH_SUCCESS, year: itemNew.year, payload: JSON.parse(result) });
                });

            });


        }
    } catch (error) {
        //console.log(error);
        // Error retrieving data
    }
}
const addEventSuccess = (dispatch, data, item) => {

    dispatch({
        type: ADD_EVENT_SUCCESS,
        payload: data
    });
    _retrieveAndMergeData(item, dispatch);
};

export const insertTimelineDataForNewYear = (year) => {

    return (dispatch) => {
        for (let index = 0; index <= GLOBAL_NUMBER_OF_YEARS; index++) {
            let i = 1;
            var j = 0;
            var s = 1;

            let daysInYearArr = getAllDayByYear(year + index);
            let epochsInYear = getAllUnixTimeStampsByYear(year + index);
            const { currentUser } = firebase.auth();

            daysInYearArr.forEach(item => {
                if (i >= 4) {
                    i = 1;
                }

                if (s >= 7) {
                    s = 1;
                }


                firebase.database().ref(`/users/${currentUser.uid}/timelinedata/${year + index}`)
                    .push({
                        "year": year + index,
                        "unixUtc": epochsInYear[j],
                        "svgSegmentId": i,
                        "svgStyleId": s,
                        "hasEvent": false,
                        "name": item,
                    }).then(() => dispatch({ type: 'success' }))
                    .catch(error => console.log(error));
                i++;
                j++;
                s++;
            });
        }
    }


};
export const tConvert = (time) => {
    // Check correct time format and split into components
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
        time = time.slice(1);  // Remove full string match value
        time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
        time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join(''); // return adjusted time or original string
}
export const fetchTimelineData = (year) => {
    const { currentUser } = firebase.auth();
    return (dispatch) => {
        if (currentUser) {
            firebase.database().ref(`/users/${currentUser.uid}/timelinedata/${year}`)
                /*  .orderByChild('unixUtc')
                 .startAt(getDayIndex())
                 .limitToFirst(65) */
                .on('value', snapshot => {
                    if (snapshot.val() !== null) {
                        dispatch({ type: TIMELINE_DATA_FETCH_SUCCESS, year: year, payload: snapshot.val() });
                    }
                    else
                        dispatch(insertTimelineDataForNewYear(year));
                });
        }
    };
}

function getDayIndex() {


    let thisMonth = new Date(Date.now()).getMonth();
    let thisDay = new Date(Date.now()).getDate();


    let c = new Date(thisYear, (thisMonth - 1), 1);

    let epoch = c.valueOf();
    //console.log(epoch);

    return epoch;
}



export const fetchTimelineDataAfterLogin = (year) => {
    const { currentUser } = firebase.auth();
    return (dispatch) => {
        firebase.database().ref(`/users/${currentUser.uid}/timelinedata/${year}`)
            /*   .orderByChild('unixUtc')
              .startAt(getDayIndex())
              .limitToFirst(65) */
            .on('value', snapshot => {

                dispatch({ type: TIMELINE_DATA_FETCH_SUCCESS, year: year, payload: snapshot.val() });
            })
    };
}
export const addEventToTimeline = (rowData) => {
    const { currentUser } = firebase.auth();
    const { eventName, eventStartTime, eventEndTime, eventPriority } = rowData
    const { item } = rowData;
    const creationDate = Date.now();

    return (dispatch) => {
        

        firebase.database().ref(`/warehouse/events/${currentUser.uid}/${item.uid}/`).push({
            calendarId: null,
            dayId: item.uid,
            title: eventName,
            startDate: eventStartTime,
            endDate: eventEndTime,
            eventPriority: eventPriority,
            allDay: false,
            location: null,
            notes: null,
            alarms: null,
            recurrenceRule: null,
            availability: 'public',
            timeZone: '',
            creationDate: creationDate,
            lastModifiedDate: null,
            originalStartDate: item.unixUtc,
            isDetached: false,
            status: 'active',
            isHighlighted: false,
            location: null,
            organizer: currentUser.uid,
            organizerEmail: currentUser.email,
            accessLevel: 'default',
            guestsCanModify: false,
            guestsCanInviteOthers: true,
            guestsCanSeeGuests: true,
            reminder: null,
            attendee: null,
            alarm: null
        }).then(data => {
            firebase.database().ref(`/warehouse/events/${currentUser.uid}/${item.uid}/${data.key}/metrics/`)
                .set({
                    eventKey: data.key,
                    invited: 0,
                    attended: 0,
                    liked: 0,
                    shares: 0,
                }).then(() => {
                    addEventSuccess(dispatch, data, item)
                })
        }).catch(() => addEventFail(dispatch));



    }
};

export const selectYear = (year) => {
    return (dispatch) => {
        dispatch({ type: SELECTER_YEAR, payload: year });
    }
}

const addEventFail = (dispatch) => {
    dispatch({ type: ADD_EVENT_FAIL });
};

export const eventNameChanged = (text) => {
    return {
        type: EVENT_NAME_CHANGED,
        payload: text
    };
}
export const eventPriorityChanged = (text) => {
    return {
        type: EVENT_PRIOR_VALUE_CHANGED,
        payload: text
    };

}
export const eventSTChanged = (text) => {
    return {
        type: EVENT_START_TIME_CHANGED,
        payload: text
    };

}
export const eventETChanged = (text) => {
    return {
        type: EVENT_END_TIME_CHANGED,
        payload: text
    };

}


export const getEventsbyDay = (dayId) => {
    const { currentUser } = firebase.auth();
    return (dispatch) => {
        firebase.database().ref(`/warehouse/events/${currentUser.uid}/${dayId}`).on('value', snapshot => {
            if (snapshot.val() !== null) {
                dispatch({ type: EVENT_FETCH_SUCCESS, payload: snapshot.val() });
            }
        })
    }
}


