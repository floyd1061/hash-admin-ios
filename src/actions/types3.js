export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER = 'login_user';
export const USER_LOGGED_IN_SESSION='user_logged_in_session'

export const REGISTER_FORM_HEIGHT_STATE = 'window_state';

export const NAME_CHANGED = 'name_changed';
export const CONFIRM_PASSWORD_CHANGED = 'confirm_password_changed';
export const REGISTER_USER_SUCCESS = 'register_user_success';
export const REGISTER_USER_FAIL = 'register_user_fail';
export const REGISTER_USER = 'register_user';

export const TIMELINE_DATA_FETCH_SUCCESS = 'timeline_data_fetch_success';

export const FETCH_USER_PROFILE_PHOTO = 'profile_picture'

export const EVENT_NAME_CHANGED = 'event_name_changed';
export const EVENT_PRIOR_VALUE_CHANGED = 'event_prior_val_changed';
export const EVENT_START_TIME_CHANGED = 'event_st_changed';
export const EVENT_END_TIME_CHANGED = 'event_et_changed';
export const ADD_EVENT_SUCCESS = 'event_add_success';
export const ADD_EVENT_FAIL = 'event_add_fail';


export const EVENT_TRANSCRIPT_FETCH = 'EVENT_TRANSCRIPT_FETCH';
export const EVENT_TRANSCRIPT_FETCH_SUCCESS = 'EVENT_TRANSCRIPT_FETCH_SUCCESS';
export const EVENT_TRANSCRIPT_FETCH_FAIL = 'EVENT_TRANSCRIPT_FETCH_FAIL';

export const EVENT_DATA_FETCH_SUCCESS = 'EVENT_DATA_FETCH_SUCCESS';
export const EVENT_DATA_FETCH_FAIL = 'EVENT_DATA_FETCH_FAIL';