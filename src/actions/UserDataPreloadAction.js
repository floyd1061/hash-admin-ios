import firebase from '@firebase/app'
import "firebase/auth"
import "firebase/database"
import "firebase/storage"
import {
    GET_ALL_FRIENDS_OF_CURRENT_USER,
    GET_ALL_FRIEND_REQUEST,
    GET_ALL_APP_USERS
} from './types';



/* export const getallfriends = (userId) => {
    //const { currentUser } = firebase.auth();

    return (dispatch) => {
        firebase.database().ref(`/users/${userId}/friends`)
            .on('value', snapshot => {
                ////console.log(snapshot.val());
                dispatch({ type: GET_ALL_FRIENDS_OF_CURRENT_USER, payload: snapshot.val() });
            });
    }

} */


export const getallfriendsOfLoggedUser = () => {
    const { currentUser } = firebase.auth();

    return (dispatch) => {
        getAllFriendsByIds(currentUser.uid, snapshot => {
            ////console.log(snapshot.key);
            dispatch({ type: GET_ALL_FRIENDS_OF_CURRENT_USER, key: snapshot.key, payload: { ...snapshot.val().userProfileInfo } });
        })


    }
}

export const getallfriends = (userId) => {
    //const { currentUser } = firebase.auth();

    return (dispatch) => {
        getAllFriendsByIds(userId, snapshot => {
            //console.log(snapshot);
            dispatch({ type: GET_ALL_FRIENDS_OF_CURRENT_USER, key: snapshot.key, payload: { ...snapshot.val().userProfileInfo } });
        })


    }
}
function getAllFriendsByIds(userId, cb) {
    let root = firebase.database().ref();
    let userRef = root.child('users');

    userRef.child(userId).child('friends').on('child_added', snap => {
        //console.log(snap);

        userRef.child(snap.val().userId)
            .once('value', cb)


    })
}

export const loadAllfriendRequests = () => {
    const { currentUser } = firebase.auth();

    return (dispatch) => {

        firebase.database().ref(`/warehouse/notification/${currentUser.uid}`)

            .on('value', snapshot => {
                //console.log(snapshot.val())
                dispatch({ type: GET_ALL_FRIEND_REQUEST, payload: snapshot.val() });
            });
    }
}

export const getAllApplicationUsers = () => {
    return (dispatch) => {

        firebase.database().ref(`/warehouse/applicationUsers/`)
            .on('value', snapshot => {
                // //console.log(snapshot);
                dispatch({ type: GET_ALL_APP_USERS, payload: snapshot.val() });
            });
    }
}