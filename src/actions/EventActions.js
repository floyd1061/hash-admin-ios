import firebase from '@firebase/app'
import _ from 'lodash'
import { AsyncStorage } from "react-native"
import { Actions } from 'react-native-router-flux';
import {
    EVENT_TRANSCRIPT_FETCH,
    EVENT_TRANSCRIPT_FETCH_FAIL,
    EVENT_TRANSCRIPT_FETCH_SUCCESS,
    FETCH_EVENT_ATTENDEE_SUCCESS,
    EVENT_FETCH_SUCCESS,
    TIMELINE_DATA_FETCH_SUCCESS
} from './types';
import { saveNotification } from '../actions';


const checkIfAnimationneededForFavorite = (itemKey, eventToAnimate, status, animationLength, item) => {
    const { currentUser } = firebase.auth();

    return (dispatch) => {
        firebase.database().ref(`warehouse/events/${currentUser.uid}/${itemKey}/${eventToAnimate}/isHighlighted`).once('value', snapshot => {
            if (snapshot.val() != true) {
                if (status) {
                    _retrieveAndMergeData(item, animationLength, eventToAnimate, dispatch);
                }
            }
        });
    }
}

const checkIfAnimationneeded = (itemKey, eventToAnimate, animationLength, item) => {
    const { currentUser } = firebase.auth();

    return (dispatch) => {
        firebase.database().ref(`warehouse/events/${currentUser.uid}/${itemKey}/${eventToAnimate}/isHighlighted`).once('value', snapshot => {
            //console.log(!snapshot)
            if (!snapshot.val()) {
                _retrieveAndMergeData(item, animationLength, eventToAnimate, dispatch);
            }
        });
    }
}

const _retrieveAndMergeData = async (item, animationLength, eventToAnimate, dispatch) => {
    try {
        let itemNew = { ...item, animationPending: true, animationlength: animationLength, eventToAnimate: eventToAnimate }
        const value = await AsyncStorage.getItem(itemNew.year.toString());
        let JsonVal = JSON.parse(value);
        let index = _.findIndex(JsonVal, ["uid", itemNew.uid])
        JsonVal = { ...JsonVal, [index]: itemNew }
        if (JsonVal !== null) {
        }
        let timelineData = _.map(JsonVal, (val) => {

            return { ...val, uid: val.uid };
        });
        let JSONdata = JSON.stringify(timelineData);

        if (timelineData !== null) {

            AsyncStorage.setItem(itemNew.year.toString(), JSONdata, () => {
                AsyncStorage.getItem(itemNew.year.toString(), (err, result) => {
                    dispatch({ type: TIMELINE_DATA_FETCH_SUCCESS, year: itemNew.year, payload: JSON.parse(result) });
                    Actions.pop();
                });
            });
        }
    } catch (error) {
        console.log(error);
        // Error retrieving data
    }
}

export const fetchTranscript = (itemKey, eventKey) => {
    const { currentUser } = firebase.auth();
    return (dispatch) => {
        firebase.database().ref(`warehouse/events/${currentUser.uid}/${itemKey}/${eventKey}/metrics`)
            .on('value', snapshot => {
                //console.log(snapshot.val())
                dispatch({ type: EVENT_TRANSCRIPT_FETCH_SUCCESS, key: eventKey, payload: snapshot.val() });
            });
    }
};
function getAllAttendeeByIds(itemKey, eventKey, cb) {
    const { currentUser } = firebase.auth();
    let root = firebase.database().ref();
    let userRef = root.child('users');
    let attendeeRef = root.child(`warehouse/events/${currentUser.uid}/${itemKey}/${eventKey}/attendee`);

    attendeeRef.on('child_added', snap => {
        userRef.child(snap.val().userId)
            .once('value', s => {
                cb({ [snap.key]: { ...s.val().userProfileInfo, ...snap.val() } })
            })

    })
}
export const fetchAttendee = (itemKey, eventKey) => {
    return (dispatch) => {
        getAllAttendeeByIds(itemKey, eventKey, snapshot => {
            dispatch({ type: FETCH_EVENT_ATTENDEE_SUCCESS, payload: snapshot });
        })
    }
}
export const getAllEventsOftheUser = () => {

    const { currentUser } = firebase.auth();
    return (dispatch) => {

        firebase.database().ref(`/warehouse/events/${currentUser.uid}/`).on('value', snapshot => {
            if (snapshot.val() !== null) {
                dispatch({ type: EVENT_FETCH_SUCCESS, payload: snapshot.val() });
            }

            //console.log(snapShotFiltered);
        })
    }
}

export const inviteToEvent = (itemKey, eventKey, userId, userProfileData, animationLength, item, event) => {
    //const { displayName, email, objectID } = user;
    let badge = '';
    const { currentUser } = firebase.auth();
    return (dispatch) => {
        firebase.database().ref(`warehouse/events/${currentUser.uid}/${itemKey}/${eventKey}/attendee`)
            .push({
                'userId': userId,
                'invitationStatus': 0,
                'role': 'invitee'
            }).then((snap) => {
                badge = snap.key
                firebase.database().ref(`warehouse/events/${currentUser.uid}/${itemKey}/${eventKey}/metrics/invited`)
                    .once('value', snapshot => {
                        let totalInvited = parseInt(snapshot.val(), 10);
                        totalInvited = totalInvited + 1;
                        firebase.database().ref(`warehouse/events/${currentUser.uid}/${itemKey}/${eventKey}/metrics`).update({
                            "invited": totalInvited
                        })
                    });
                dispatch(saveNotification(data, userId, userProfileData, currentUser.uid, 'inviteToEvent', 'Invited To Event'
                    , `${currentUser.displayName}`, badge))
            });
        var data = event;


        dispatch(checkIfAnimationneeded(itemKey, eventKey, animationLength, item));
    }
}

export const setEventLocaiton = (itemKey, eventKey, poi, animationLength, item) => {
    const { currentUser } = firebase.auth();
    //console.log(itemKey, eventKey);
    return (dispatch) => {
        firebase.database().ref(`warehouse/events/${currentUser.uid}/${itemKey}/${eventKey}`).update({
            "location": poi,
        })
       // dispatch(checkIfAnimationneeded(itemKey, eventKey, animationLength, item));
    }
}

export const setFavouriteStatus = (itemKey, eventKey, status, animationLength, item) => {

    const { currentUser } = firebase.auth();
    //console.log(eventKey)
    return (dispatch) => {
        firebase.database().ref(`warehouse/events/${currentUser.uid}/${itemKey}/${eventKey}/`).update({
            "isFavourited": status
        })
        dispatch(checkIfAnimationneededForFavorite(itemKey, eventKey, status, animationLength, item));

    }
}

export const uploadImageToEventPhotoGallery =
    (itemKey, eventKey, remoteUriOriginal, remoteUriThumb,
        reducedImage,
        animationLength, item) => {
        const { currentUser } = firebase.auth();
        return (dispatch) => {
            var ref = firebase.database().ref(`warehouse/assets/${eventKey}`);
            ref.push({
                'uri': remoteUriOriginal,
                'thumbnail': remoteUriThumb,
                'height': reducedImage.height,
                'meta': 'image/jpeg',
                'width': reducedImage.width,
                'owner': currentUser && currentUser.uid,
                'uploadtime': new Date().getTime()
            }).then(() => dispatch(checkIfAnimationneeded(itemKey, eventKey, animationLength, item)))
        }
    }
export const uploadVideoToEventVideoGallery =
    (itemKey, eventKey, remoteUriOriginal, remoteUriThumb,
        animationLength, item) => {
        const { currentUser } = firebase.auth();
        return (dispatch) => {
            var ref = firebase.database().ref(`warehouse/assets/${eventKey}`);
            ref.push({
                'uri': remoteUriOriginal,
                'thumbnail': remoteUriThumb,
                'height': null,
                'meta': 'video/mp4',
                'width': null,
                'owner': currentUser && currentUser.uid,
                'uploadtime': new Date().getTime()
            }).then(() => {
                firebase.database().ref(`warehouse/events/${currentUser.uid}/${itemKey}/${eventKey}/`)
                    .update({
                        "coverVideo": `${remoteUriOriginal}`
                    }).then(() => {
                        dispatch(checkIfAnimationneeded(itemKey, eventKey, animationLength, item))
                    })
            })
        }
    }

export const updateEventAndTimelineAfterAnimation = (itemKey, eventToAnimate, animatedEvent, item) => {

    const { currentUser } = firebase.auth();
    return (dispatch) => {
        firebase.database().ref(`warehouse/events/${currentUser.uid}/${itemKey}/${eventToAnimate}`).update({
            "isHighlighted": true
        }).then(() => {
            _retrieveAndMergeDataAfterAnimation(item, itemKey, eventToAnimate, animatedEvent, dispatch);

        })
    }
}

const _retrieveAndMergeDataAfterAnimation = async (item, itemKey, eventToAnimate, animatedEvent, dispatch) => {
    try {
        let itemNew = { ...item, animationPending: false, animationlength: null, eventToAnimate: eventToAnimate }
        console.log(item, itemNew);
        const value = await AsyncStorage.getItem(itemNew.year.toString());
        let JsonVal = JSON.parse(value);
        let index = _.findIndex(JsonVal, ["uid", itemNew.uid])
        JsonVal = { ...JsonVal, [index]: itemNew }
        if (JsonVal !== null) {
        }
        let timelineData = _.map(JsonVal, (val) => {

            return { ...val, uid: val.uid };
        });
        let JSONdata = JSON.stringify(timelineData);

        if (timelineData !== null) {

            AsyncStorage.setItem(itemNew.year.toString(), JSONdata, () => {
                AsyncStorage.getItem(itemNew.year.toString(), (err, result) => {
                    dispatch({ type: TIMELINE_DATA_FETCH_SUCCESS, year: itemNew.year, payload: JSON.parse(result) });
                    setTimeout(() => {
                        Actions.Event({
                            event: animatedEvent, eventKey: eventToAnimate,
                            year: itemNew.year, itemKey: itemNew.uid,
                            animationLength: null, item: itemNew, backFromAnimation: true
                        })
                    }, 1000)
                });
            });
        }
    } catch (error) {
        console.log(error);
    }
}

