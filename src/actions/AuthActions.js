import firebase from '@firebase/app'
import "firebase/auth"
import "firebase/database"
import "firebase/storage"
import { Permissions, Location } from 'expo';
import { Actions } from 'react-native-router-flux';
import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
  GET_GEO_TAG,
  GLOBAL_NUMBER_OF_YEARS
} from './types';
import {
  fetchTimelineDataAfterLogin,
  fetchUserProfileInfo, storeAsyncTimelineDataForYears,
  fetchTimelineDataFromAsyncStoreAfterLogin, getallfriends, getAllEventsOftheUser,
  getAllApplicationUsers,fetchChatListFrom
} from '../actions';

export const emailChanged = (text) => {
  return {
    type: EMAIL_CHANGED,
    payload: text
  };
};

export const passwordChanged = (text) => {
  return {
    type: PASSWORD_CHANGED,
    payload: text
  };
};
const thisYear = new Date(Date.now()).getFullYear();

async function _getLocationAsync(dispatch) {
  let { status } = await Permissions.askAsync(Permissions.LOCATION);
  if (status !== 'granted') {
    //console.log('GeoPermission Denied')
  }

  let location = await Location.getCurrentPositionAsync({ enableHighAccuracy: true });
  if (location != null) {
    dispatch({ type: GET_GEO_TAG, payload: location })
  }
};

function getyearsArray() {
  let years = [];
  for (let index = 0; index <= GLOBAL_NUMBER_OF_YEARS; index++) {
    years.push({
      value: (thisYear + index).toString()
    })
  }
  return years;
}


export const autoLoggIn = () => {
  return (dispatch) => {
    dispatch({ type: LOGIN_USER });
    firebase.auth().onAuthStateChanged(user => {
      ////console.log(user);
      if (user !== null) {
        onlineOfflineStatus();

        loadDataAfterLogin(dispatch, user);
        loginUserSuccess(dispatch, user);
        //Actions.chatHome();
        Actions.UserProfileTimeline({ years: getyearsArray() })
      } else {
        loginUserFail(dispatch);
      }
    }

    )
  }
}
const onlineOfflineStatus = () => {
  // Fetch the current user's ID from Firebase Authentication.
  var uid = firebase.auth().currentUser.uid;

  // Create a reference to this user's specific status node.
  // This is where we will store data about being online/offline.
  var userStatusDatabaseRef = firebase.database().ref(`warehouse/applicationUsers/${uid}/status`);

  // We'll create two constants which we will write to 
  // the Realtime database when this device is offline
  // or online.
  var isOfflineForDatabase = {
    state: 'offline',
    last_changed: firebase.database.ServerValue.TIMESTAMP,
  };

  var isOnlineForDatabase = {
    state: 'online',
    last_changed: firebase.database.ServerValue.TIMESTAMP,
  };

  // Create a reference to the special '.info/connected' path in 
  // Realtime Database. This path returns `true` when connected
  // and `false` when disconnected.
  firebase.database().ref('.info/connected').on('value', function (snapshot) {
    // If we're not currently connected, don't do anything.
    if (snapshot.val() == false) {
      return;
    };

    // If we are currently connected, then use the 'onDisconnect()' 
    // method to add a set which will only trigger once this 
    // client has disconnected by closing the app, 
    // losing internet, or any other means.
    userStatusDatabaseRef.onDisconnect().set(isOfflineForDatabase).then(function () {
      // The promise returned from .onDisconnect().set() will
      // resolve as soon as the server acknowledges the onDisconnect() 
      // request, NOT once we've actually disconnected:
      // https://firebase.google.com/docs/reference/js/firebase.database.OnDisconnect

      // We can now safely set ourselves as 'online' knowing that the
      // server will mark us as offline once we lose connection.
      userStatusDatabaseRef.set(isOnlineForDatabase);
    });
  });
}
export const loginUser = ({ email, password }) => {
  return (dispatch) => {
    dispatch({ type: LOGIN_USER });

    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(user => {
        onlineOfflineStatus();
        loadDataAfterLoginFresh(dispatch, user);
        loginUserSuccess(dispatch, user)
        Actions.UserProfileTimeline({ years: getyearsArray() });
      })
      .catch((error) => {
        loginUserFailFromLogin(dispatch);
      });
  };
};

const loadDataAfterLoginFresh = (dispatch, user) => {
  dispatch(fetchTimelineDataFromAsyncStoreAfterLogin(thisYear));
  dispatch(getAllEventsOftheUser());
  dispatch(getallfriends(user.uid));
  _getLocationAsync(dispatch);
  dispatch(fetchChatListFrom());
  dispatch(getAllApplicationUsers());
  dispatch(fetchUserProfileInfo(user.uid));
}

const loadDataAfterLogin = (dispatch, user) => {
  dispatch(fetchTimelineDataFromAsyncStoreAfterLogin(thisYear));
  dispatch(getallfriends(user.uid));
  dispatch(getAllEventsOftheUser());
  _getLocationAsync(dispatch);
  dispatch(fetchChatListFrom());
  dispatch(getAllApplicationUsers());
  dispatch(fetchUserProfileInfo(user.uid));
}



const loginUserFail = (dispatch) => {
  dispatch({ type: LOGIN_USER_FAIL });
  Actions.auth();
};

const loginUserFailFromLogin = (dispatch) => {
  dispatch({ type: LOGIN_USER_FAIL });

  Actions.auth();
};

const loginUserSuccess = (dispatch, user) => {
  dispatch({
    type: LOGIN_USER_SUCCESS,
    payload: user
  });
};
