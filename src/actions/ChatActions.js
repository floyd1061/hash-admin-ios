import firebase from '@firebase/app'

import "firebase/auth"
import "firebase/database"
import "firebase/storage"
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';

export const setPopup = (status, popup_conversationId) => {
  return {
    type: 'set_popup',
    payload: { status, popup_conversationId }
  };
};

export const addFriendToConversation = (friendId, popup_conversationId) => {

  // //console.log('addFriendToConversation' + friendId+''+popup_conversationId);

  const { currentUser } = firebase.auth();

  return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/conversations/${popup_conversationId}/conversationMetaData/withWhom`)
      .once('value', snapshot => {

        var existed = false;
        var withWhom = [];
        snapshot.forEach(function (item) {
          withWhom.push(item.val());

          if (item.val() == friendId) {
            existed = true;
          }

        });

        if (!existed) {

          // Create conversation to the newly added member's conversations bucket
          withWhom.push(currentUser.uid);
          // //console.log('withWhomArrayForNewMember'+ withWhom);
          firebase.database().ref(`/users/${friendId}/conversations/${popup_conversationId}`)
            .set({
              'conversationMetaData': {
                'isGroup': false,
                'withWhom': withWhom
              }
            }).then(() => {
              firebase.database().ref(`/users/${friendId}/conversations/${popup_conversationId}/conversation`)
                .push({
                  "messageBody": "You have been added to the group",
                  "createdAt": Math.round((new Date()).getTime() / 1000),
                  "sender": {
                    "uid": `${currentUser.uid}`,
                    "name": `${currentUser.dispalyName}`,
                    "avatar": `${currentUser.photoURL}`
                  },
                  "sent": true,
                  "received": false,
                  "location": {
                    "latitude": 48.864601,
                    "longitude": 2.398704
                  },
                  "image": "https://lh3.googleusercontent.com/-uXipYA5hSKc/VVWKiFIvo-I/AAAAAAAAAhQ/vkjLyZNEzUA/w800-h800/1.jpg",
                  "file": "",
                  "event": {
                    "eventId": ""
                  }
                }).then(() => {
                  withWhom.push(friendId);
                  // //console.log("test"+ withWhom.length);            
                  for (var singleGrpMem = 0; singleGrpMem < withWhom.length; singleGrpMem++) {
                    firebase.database().ref(`/users/${withWhom[singleGrpMem]}/conversations/${popup_conversationId}/conversationMetaData`)
                      .set({
                        'isGroup': false,
                        'withWhom': withWhom
                      })
                  }
                })
            })
        }
      });
  };
};

export const startConversation = (receiversId, conversation_id) => {
  ////console.log("conversationId" + conversation_id);
  Actions.chatmain();
  return {
    type: 'start_conversation',
    payload: { receiversId, conversation_id }
  };
};
export const startConversationFromList = (conversation_id) => {
  //sconsole.log(withWhom);
  return (dispatch) => {
    //dispatch(detachChatHomeListener());
    Actions.chatListView({ conversation_id: conversation_id });
  }
};
export const setReceived = (conversation_id) => {
  const { currentUser } = firebase.auth();
  ////console.log(conversation_id);
  return (dispatch) => {
    firebase.database().ref(`/warehouse/conversations/${conversation_id}/conversation`).once('value', snapshot => {
      const chatList = _.map(snapshot.val(), (val, uid) => {
        return { ...val, uid }
      })

      if (chatList.length > 0) {
        for (let i = 0; i < chatList.length; i++) {

          if (chatList[i].sender.uid != currentUser.uid) {
            firebase.database().ref(`/warehouse/conversations/${conversation_id}/conversation/${chatList[i].uid}`)
              .update({
                'received': true,
                'receivedAt': Math.round((new Date()).getTime() / 1000),
              });
          }
        }
      }
    })
  };
}

export const getUnsenMessagesCount = (cb) => {
  return (dispatch) => {
    let usrMessaged = [];
    let mainCounterUnSeen = 0;
    let mainCounterSeen = 0;
    const { currentUser } = firebase.auth();
    firebase.database().ref(`/warehouse/${currentUser.uid}/conversations`).on('child_added', snap => {

      firebase.database().ref(`/warehouse/conversations/${snap.key}/conversation`)
        .on('value', convSnap => {
          if (convSnap.val() != null) {
            mainCounterUnSeen = 0;
            convSnap.forEach(z => {
              if (z.val().sender.uid != currentUser.uid) {
                if (z.val().received != true) {
                  mainCounterUnSeen++;
                } else {
                  mainCounterSeen++;
                }
              }
            })
          }
          dispatch({ type: 'GET_UNSEEN_MSG_COUNT', payload: mainCounterUnSeen, msgKey: snap.key.toString() });
        })
    })

    firebase.database().ref(`/warehouse/${currentUser.uid}/conversations`).on('child_changed', snap => {

      firebase.database().ref(`/warehouse/conversations/${snap.key}/conversation`)
        .on('value', convSnap => {
          if (convSnap.val() != null) {
            mainCounterUnSeen = 0;
            convSnap.forEach(z => {
              if (z.val().sender.uid != currentUser.uid) {
                if (z.val().received != true) {
                  mainCounterUnSeen++;
                } else {
                  mainCounterSeen++;
                }
              }
            })
          }
          dispatch({ type: 'GET_UNSEEN_MSG_COUNT', payload: mainCounterUnSeen, msgKey: snap.key.toString() });
        })
    })
  }
}
export const fetchConversationGallery = (conversationId) => {

  return (dispatch) => {
    firebase.database().ref(`/warehouse/conversations/${conversationId}/conversationAssets`)
      .on('value', snapshot => {
        dispatch({ type: 'FETCH_CHAT_GALLERY_SUCCESS', payload: snapshot.val() });
      });
  };

};

export const fetchConversationById = (conversationId) => {
  return (dispatch) => {
    firebase.database().ref(`/warehouse/conversations/${conversationId}/`)
      .on('value', snapshot => {
        //console.log(snapshot.val());
        dispatch({ type: 'fetch_conversation', payload: snapshot.val() });
      });
  };
}

export const findConversationByFriendId = (friend) => {

  const { currentUser } = firebase.auth();
  let conversation_id = null;
  let chatList = null;
  return (dispatch) => {
    firebase.database().ref(`/warehouse/${currentUser.uid}/conversations`)
      .once('value', snapshot => {
        chatList = _.map(snapshot.val(), (val, uid) => {
          return { ...val, uid };
        });
        ////console.log(chatList);
        if (chatList.length > 0) {
          chatList.forEach((element, index, array) => {

            let conv = null
            firebase.database().ref(`/warehouse/conversations/${element.uid}/conversationMetaData`).once('value', snap => {

              if (snap.val() != null) {
                conv = snap.val();
                if (!conv.isGroup) {
                  conv.withWhom.forEach((val) => {
                    if (val.uid === friend.uid) {
                      conversation_id = element.uid;
                    }
                  })
                }
              }
            })

          });
        }
      });

    Actions.chatListView({ friend: friend, conversation_id: conversation_id });
  };


};





export const fetchChatListFrom = () => {
  const currentUserId = firebase.auth().currentUser.uid;

  return (dispatch) => {
    getAllMessagesByIds(currentUserId, dispatch, snapshot => {
      dispatch({ type: 'fetch_chatList', payload: snapshot });
    })
  }
}



function getAllMessagesByIds(currentUserId, dispatch, cb) {
  let usrMessaged = [];
  let mainCounterUnSeen = 0;
  let mainCounterSeen = 0;
  firebase.database().ref(`/warehouse/${currentUserId}/conversations`).on('child_added', snap => {
    //console.log(snap.key);
    firebase.database().ref(`/warehouse/conversations/${snap.key}/`).on('value', convSnap => {
      if (convSnap.val() != null) {

        mainCounterUnSeen = 0;
        convSnap.child('conversation').forEach(z => {
          //console.log(z.val().sender.uid);
          if (z.val().sender.uid != currentUserId) {
            if (z.val().received != true) {
              mainCounterUnSeen++;
            } else {
              mainCounterSeen++;
            }
          }
        })
      }
      cb({ [snap.key]: { ...convSnap.val() } });
      dispatch({ type: 'GET_UNSEEN_MSG_COUNT', payload: mainCounterUnSeen, msgKey: snap.key.toString() });
     
    })
  })
  firebase.database().ref(`/warehouse/${currentUserId}/conversations`).on('child_changed', snap => {
    //console.log(snap.key);
    firebase.database().ref(`/warehouse/conversations/${snap.key}/`).limitToLast(18).on('value', convSnap => {
      if (convSnap.val() != null) {
        mainCounterUnSeen = 0;
        convSnap.child('conversation').forEach(z => {
          if (z.val().sender.uid != currentUserId) {
            if (z.val().received != true) {
              mainCounterUnSeen++;
            } else {
              mainCounterSeen++;
            }
          }
        })
      }
      cb({ [snap.key]: { ...convSnap.val() } });
      dispatch({ type: 'GET_UNSEEN_MSG_COUNT', payload: mainCounterUnSeen, msgKey: snap.key.toString() });
      
    })
  })
}

export const detachChatHomeListener = () => {
  return (dispatch) => {
    //console.log('detaching');
    const currentUserId = firebase.auth().currentUser.uid;
    firebase.database().ref(`/warehouse/${currentUserId}/conversations`).off('child_added', snap => {
      firebase.database().ref(`/warehouse/conversations/${snap.key}/`).off()
    });
  }
}
export const detachChatlistListener = (conversation_id) => {
  return (dispatch) => {
    firebase.database().ref(`/warehouse/conversations/${conversation_id}/`).off()
  }
}

export const saveMessageToFirebase = (conversationId, messageBody) => {
  const { currentUser } = firebase.auth();
  //consolconsole.log(conversationId);
  return (dispatch) => {

    firebase.database().ref(`/warehouse/conversations/${conversationId}/conversation`)
      .push({
        "messageBody": `${messageBody}`,
        "createdAt": Math.round((new Date()).getTime() / 1000),
        "sender": {
          "uid": `${currentUser.uid}`,
          "name": `${currentUser.displayName}`,
          "avatar": `${currentUser.photoURL}`
        },
        "sent": true,
        "received": false,
        "type": "text",
      }).then(() => {
        firebase.database().ref(`/warehouse/conversations/${conversationId}`).update({
          lastMessageSentAt: Math.round((new Date()).getTime() / 1000)
        })
      })
  }
}


export const saveLocationMessageToFirebase = (conversationId, location) => {
  const { currentUser } = firebase.auth();
  //consolconsole.log(conversationId);
  return (dispatch) => {

    firebase.database().ref(`/warehouse/conversations/${conversationId}/conversation`)
      .push({
        "messageBody": '',
        "createdAt": Math.round((new Date()).getTime() / 1000),
        "sender": {
          "uid": `${currentUser.uid}`,
          "name": `${currentUser.displayName}`,
          "avatar": `${currentUser.photoURL}`
        },
        "sent": true,
        "received": false,
        "type": "map",
        "location": {
          "latitude": location.coords.latitude,
          "longitude": location.coords.longitude,
          "latitudeDelta": 0.0322,
          "longitudeDelta": 0.0321,
        },

      }).then(() => {
        firebase.database().ref(`/warehouse/conversations/${conversationId}`).update({
          lastMessageSentAt: Math.round((new Date()).getTime() / 1000)
        })
      })
  }
}


export const saveImageMessageToFirebase = (conversationId, remoteUriOriginal, remoteUriThumb, reducedImage) => {
  const { currentUser } = firebase.auth();
  console.log('start Image upload', remoteUriOriginal);
  return (dispatch) => {

    firebase.database().ref(`/warehouse/conversations/${conversationId}/conversation`)
      .push({
        "messageBody": "",
        "createdAt": Math.round((new Date()).getTime() / 1000),
        "sender": {
          "uid": `${currentUser.uid}`,
          "name": `${currentUser.displayName}`,
          "avatar": `${currentUser.photoURL}`
        },
        "sent": true,
        "received": false,
        "type": "image",

        "image": {
          "uri": remoteUriOriginal,
          "thumbnail": remoteUriThumb,
          "height": reducedImage.height,
          "meta": "image/jpeg",
          "width": reducedImage.width,
          "owner": currentUser && currentUser.uid,
          "uploadtime": new Date().getTime()
        },

      }).then(
        firebase.database().ref(`/warehouse/conversations/${conversationId}/conversationAssets`).push({
          "uri": remoteUriOriginal,
          "thumbnail": remoteUriThumb,
          "height": reducedImage.height,
          "meta": "image/jpeg",
          "width": reducedImage.width,
          "owner": currentUser && currentUser.uid,
          "uploadtime": new Date().getTime()
        }).then(() => {
          firebase.database().ref(`/warehouse/conversations/${conversationId}`).update({
            lastMessageSentAt: Math.round((new Date()).getTime() / 1000)
          })
        })
      )
  }
}

export const saveVideoMessageToFirebase = (conversationId, remoteUri, remoteThumb) => {
  const { currentUser } = firebase.auth();
  //consolconsole.log(conversationId);
  return (dispatch) => {

    firebase.database().ref(`/warehouse/conversations/${conversationId}/conversation`)
      .push({
        "messageBody": "",
        "createdAt": Math.round((new Date()).getTime() / 1000),
        "sender": {
          "uid": `${currentUser.uid}`,
          "name": `${currentUser.displayName}`,
          "avatar": `${currentUser.photoURL}`
        },
        "sent": true,
        "received": false,
        "type": "video",
        "video": {
          'uri': remoteUri,
          'thumbnail': remoteThumb,
          'height': null,
          'meta': 'video/mp4',
          'width': null,
          'owner': currentUser && currentUser.uid,
          'uploadtime': new Date().getTime()
        },
      }).then(
        firebase.database().ref(`/warehouse/conversations/${conversationId}/conversationAssets`).push({
          'uri': remoteUri,
          'thumbnail': remoteThumb,
          'height': null,
          'meta': 'video/mp4',
          'width': null,
          'owner': currentUser && currentUser.uid,
          'uploadtime': new Date().getTime()
        }).then(() => {
          firebase.database().ref(`/warehouse/conversations/${conversationId}`).update({
            lastMessageSentAt: Math.round((new Date()).getTime() / 1000)
          })
        })
      )
  }
}

export const createNewConversationWithUser = (destinationUser, messageBody, msgType, callback) => {

  const { currentUser } = firebase.auth();

  return (dispatch) => {
    var newConversationId = null;

    var withWhom = [];
    let withWhomSourceObject = {
      uid: currentUser.uid,
    }
    if (Array.isArray(destinationUser)) {
      destinationUser.forEach((user) => {
        withWhom.push({ "uid": user.uid });
      })
    } else {
      withWhom.push({ "uid": destinationUser.uid });
    }


    withWhom.push(withWhomSourceObject);
    firebase.database().ref(`/warehouse/conversations`)
      .push({
        'conversationMetaData': {
          'isGroup': false,
          'lastMessageSentAt': Math.round((new Date()).getTime() / 1000),
          'withWhom': withWhom
        }
      }).then((conversation) => {

        newConversationId = conversation.key;
        firebase.database().ref(`/warehouse/conversations/${newConversationId}`).update({
          lastMessageSentAt: Math.round((new Date()).getTime() / 1000)
        })
        //Actions.refresh({friend: destinationUser, conversation_id: newConversationId});
        firebase.database().ref(`/warehouse/${currentUser.uid}/conversations/${newConversationId}`).set({
          "createdAt": Math.round((new Date()).getTime() / 1000)
        }).catch((err) => {
          //console.warn(err);
        });
        firebase.database().ref(`/warehouse/${destinationUser.uid}/conversations/${newConversationId}`).set({
          "createdAt": Math.round((new Date()).getTime() / 1000)
        }).catch((err) => {
          //console.warn(err);
        });
        if (msgType === 'text') {
          firebase.database().ref(`/warehouse/conversations/${newConversationId}/conversation`)
            .push({
              "messageBody": `${messageBody}`,
              "createdAt": Math.round((new Date()).getTime() / 1000),
              "sender": {
                "uid": `${currentUser.uid}`,
                "name": `${currentUser.displayName}`,
                "avatar": `${currentUser.photoURL}`
              },
              "sent": true,
              "received": false,
              "type": "text",


            }).then(callback(newConversationId))
        } else if (msgType === 'location') {
          firebase.database().ref(`/warehouse/conversations/${newConversationId}/conversation`)
            .push({
              "messageBody": '',
              "createdAt": Math.round((new Date()).getTime() / 1000),
              "sender": {
                "uid": `${currentUser.uid}`,
                "name": `${currentUser.displayName}`,
                "avatar": `${currentUser.photoURL}`
              },
              "sent": true,
              "received": false,
              "type": "map",
              "location": {
                "latitude": messageBody.coords.latitude,
                "longitude": messageBody.coords.longitude,
                "latitudeDelta": 0.0322,
                "longitudeDelta": 0.0321,
              },

            }).then(callback(newConversationId))
        } else if (msgType === 'image') {
          firebase.database().ref(`/warehouse/conversations/${newConversationId}/conversation`)
            .push({
              "messageBody": "",
              "createdAt": Math.round((new Date()).getTime() / 1000),
              "sender": {
                "uid": `${currentUser.uid}`,
                "name": `${currentUser.displayName}`,
                "avatar": `${currentUser.photoURL}`
              },
              "sent": true,
              "received": false,
              "type": "image",

              "image": {
                "uri": messageBody.remoteUriOriginal,
                "thumbnail": messageBody.remoteUriThumb,
                "height": messageBody.reducedImage.height,
                "meta": "image/jpeg",
                "width": messageBody.reducedImage.width,
                "owner": currentUser && currentUser.uid,
                "uploadtime": new Date().getTime()
              },

            }).then(
              firebase.database().ref(`/warehouse/conversations/${conversationId}/conversationAssets`).push({
                "uri": messageBody.remoteUriOriginal,
                "thumbnail": messageBody.remoteUriThumb,
                "height": messageBody.reducedImage.height,
                "meta": "image/jpeg",
                "width": messageBody.reducedImage.width,
                "owner": currentUser && currentUser.uid,
                "uploadtime": new Date().getTime()
              }).then(callback(newConversationId))
            )
        } else if (msgType === 'video') {
          firebase.database().ref(`/warehouse/conversations/${newConversationId}/conversation`)
            .push({
              "messageBody": "",
              "createdAt": Math.round((new Date()).getTime() / 1000),
              "sender": {
                "uid": `${currentUser.uid}`,
                "name": `${currentUser.displayName}`,
                "avatar": `${currentUser.photoURL}`
              },
              "sent": true,
              "received": false,
              "type": "video",
              "video": {
                'uri': messageBody.remoteUri,
                'thumbnail': messageBody.remoteThumb,
                'height': null,
                'meta': 'video/mp4',
                'width': null,
                'owner': currentUser && currentUser.uid,
                'uploadtime': new Date().getTime()
              },
            }).then(
              firebase.database().ref(`/warehouse/conversations/${newConversationId}/conversationAssets`).push({
                'uri': messageBody.remoteUri,
                'thumbnail': messageBody.remoteThumb,
                'height': null,
                'meta': 'video/mp4',
                'width': null,
                'owner': currentUser && currentUser.uid,
                'uploadtime': new Date().getTime()
              }).then(callback(newConversationId))
            )
        }
        
      })
      
  };
};



