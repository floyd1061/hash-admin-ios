import React, { Component } from 'react'
import { View, StyleSheet, Text, TouchableOpacity, Image, TextInput } from 'react-native'
import { Font } from 'expo'

export default class SearchNotifications extends Component {

    constructor(props) {
        super(props);
        this.state = { showHeaderOption: false, fontLoaded: true };
    }

    
    render() {
        return (
            <View style={ styles.container }>
                <View style = {styles.headerContainer}>
                    <View style = {styles.searchBox}>
                        <View style = {{flexDirection:'row'}}>
                            <TouchableOpacity>
                                <Image source={require('./assets/explore.png')} style={styles.exploreIcon}/>
                            </TouchableOpacity>
                            {
                                this.state.fontLoaded ? ( <TextInput
                                style={{ alignSelf: 'stretch', flex: .7, flexDirection:'row', paddingHorizontal: 10,  fontSize: 15, fontFamily: 'coolvetica' }}
                                underlineColorAndroid="transparent"
                                placeholder="Search Favourites"
                                /> ) : null
                            }
                        </View>

                        <View style = {{flexDirection:'row', justifyContent: 'space-between'}}>
                            <TouchableOpacity>
                                <Image source={require('./assets/mic.png')} style={styles.micIcon}/>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => {this.setState({showHeaderOption: !this.state.showHeaderOption})}}>
                                <Image source={require('./assets/dropdown.png')} style={styles.dropdownIcon}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                <View style = {styles.notificationBody} >

                    <TouchableOpacity>
                        <Text style = { styles.textStyle }> 54 Events /// </Text>
                    </TouchableOpacity>     

                    <View style = {{flexDirection: 'row'}}>

                        <TouchableOpacity style = {{position: 'relative', width: 140, height: 130, marginRight: 15, justifyContent: 'center', alignItems: 'center'}}>
                            <Image source={require('./assets/lock.png')} style={styles.lockIcon}/>
                            <Image source={require('./assets/eventbox.png')} style={{width: 110, height: 80, resizeMode: 'stretch', position: 'relative'}} />
                            <View style={{position: 'absolute', width: 80, right: 25}}>
                                {
                                    this.state.fontLoaded ? ( <Text style={{textAlign: 'center', color: '#fff',  fontSize: 13, fontFamily: 'coolvetica'}}>John's Party</Text> ) : null
                                }
                                {
                                    this.state.fontLoaded ? ( <Text style={{textAlign: 'center', fontSize: 6, color: '#fff', fontFamily: 'coolvetica'}}>12pm - 3pm</Text> ) : null
                                }
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity style = {{position: 'relative', width: 140, height: 130, marginRight: 15, justifyContent: 'center', alignItems: 'center'}}>
                            <Image source={require('./assets/Shape.png')} style={{width: 110, height: 80, resizeMode: 'stretch', position: 'relative'}} />
                            <View style={{position: 'absolute', width: 80, right: 25}}>
                                {
                                    this.state.fontLoaded ? ( <Text style={{textAlign: 'center', color: '#fff',  fontSize: 13, fontFamily: 'coolvetica'}}>John's Party</Text> ) : null
                                }
                                {
                                    this.state.fontLoaded ? ( <Text style={{textAlign: 'center', fontSize: 6, color: '#fff', fontFamily: 'coolvetica'}}>12pm - 3pm</Text> ) : null
                                }
                            </View>
                        </TouchableOpacity>

                    </View>

                </View>

                {/* ----dropdown function ----  */}

                {this.state.showHeaderOption ?
                    <View style = {styles.headerOptionArea}>
                        <View  style = {{position: 'relative', flex: 1}}>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}> Most Favourites </Text> ) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}> Tagged Favourites </Text> ) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity disabled = {true}>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}> Most Favourites </Text> ) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}> Latest Favourites </Text> ) : null
                                }
                            </TouchableOpacity>
                        </View>                            
                    </View> : null
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#DEDEDE',
        alignSelf: 'stretch',
    },

    headerContainer: {
        alignSelf: 'stretch',
        backgroundColor: '#FF1303',
        height: 150,
    },

    searchBox: {
        flexDirection:'row',
        backgroundColor: '#fff',
        height: 60, 
        alignSelf: 'stretch',
        alignItems: 'center',
        paddingHorizontal: 20,
        marginTop: 80, 
        justifyContent: 'space-between'          
    },

    exploreIcon: {
        height: 40,
        width:40, 
        resizeMode : 'stretch',
    },

    micIcon: {
        height: 40,
        width:35, 
        resizeMode : 'stretch',
        marginRight: 10,
    },

    dropdownIcon: {
        height: 35,
        width:35, 
        resizeMode : 'stretch',
    },

    headerOptionArea: {
        width:200, 
        height: 150, 
        backgroundColor: '#fff', 
        position: 'absolute',
        right: 22,
        top: 140,
        borderColor: '#FD0607',
        borderWidth: 2,
        borderRadius: 3,
        borderTopWidth: 0
    },

    optionText: {
        paddingVertical: 8,
        paddingHorizontal: 8,
        color:'#000',
        fontFamily: 'coolvetica'
    },

    notificationBody: {
        paddingHorizontal: 30,
        paddingVertical: 30,
    },

    textStyle: {
        fontSize: 20,
        color: '#F71300',
        marginBottom: 10
    },

    lockIcon: {
        width: 20,
        height: 25,
        resizeMode: 'stretch',
        position: 'absolute',
        top: 3, 
        right: 0
    },
});