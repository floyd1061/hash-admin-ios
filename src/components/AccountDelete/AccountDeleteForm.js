import React, { Component } from 'react'
import { StyleSheet, View, Image, TouchableOpacity, TextInput, Text, KeyboardAvoidingView } from 'react-native'
import { LinearGradient, Font } from 'expo'

export default class AccountDeleteForm extends Component {

    state = {
        fontLoaded: true,
      };

   /*  async componentWillMount() {
        await Font.loadAsync({
          'coolvetica': require('../../../assets/fonts/MyriadPro-Regular.ttf'),
        });

        this.setState({ fontLoaded: true });
    } */

    render() {
        return (
            <View style={styles.container}>

                <KeyboardAvoidingView behavior="padding">

                    <View style= { styles.Box }>
                        <View style= { styles.SectionStyletext1 }>
                            {
                                this.state.fontLoaded ? ( <Text style= { styles.text1Style }>Delete Account</Text> ) : null
                            }
                        </View>

                        <View style= { styles.SectionStyletext2 }>
                            {
                                this.state.fontLoaded ? <Text style= { styles.text2Style }>Deleting your account will</Text> : null
                            }
                        </View>

                        <View style= { styles.SectionStyletext3 }>
                            {
                                this.state.fontLoaded ? <Text style= { styles.text3Style }>
                                    Delete your account permanantly from Hashtazapp { "\n" }
                                    Delete all your media history from Hashtazapp including pics, videos & blogs. { "\n" }
                                    Remove you from all the groups on Hashtazapp you have joined.
                                </Text> : null
                            }
                        </View>

                        <View style= { styles.SectionStyletext4 }>
                            {
                                this.state.fontLoaded ? <Text style= { styles.text4Style }>
                                    To delete your account, please confirm { "\n" }
                                    your password and please verify your number.
                                </Text> : null
                            }
                        </View>

                        <View style={styles.SectionStyle1}>
                            {
                                this.state.fontLoaded ? <TextInput
                                    style={{ flex: 1, fontSize: 20, fontFamily: 'coolvetica'}}
                                    placeholder="Password"
                                    secureTextEntry
                                    returnKeyType="next"
                                    underlineColorAndroid="transparent"
                                    onSubmitEditing={() => this.confirmNumberInput.focus()}
                                /> : null
                            }
                            <Image source={require('./assets/lock.png')} style={styles.ImageStyle} />
                        </View>

                        <TouchableOpacity style = {{alignItems: 'flex-end'}}>
                            {
                                this.state.fontLoaded ? <Text style={styles.CustomLabelStyle}>Can’t Remember Password?</Text> : null
                            }
                        </TouchableOpacity>

                        <View style={styles.SectionStyle2}>
                            {
                                this.state.fontLoaded ? <TextInput
                                    style={{ flex: 1, fontSize: 20, fontFamily: 'coolvetica' }}
                                    placeholder ="Confirm Number"
                                    keyboardType="phone-pad"
                                    returnKeyType="go"
                                    ref={(input) => this.confirmNumberInput = input}
                                    underlineColorAndroid="transparent"
                                /> : null
                            }
                            <Image source={require('./assets/phoneicon.png')} style={styles.ImageStyle} />
                        </View>
                    </View>

                    <TouchableOpacity>
                        <LinearGradient
                        colors={['#ff2002', '#df1d04']}
                        style={styles.SectionStyleDeleteButton}>
                            {
                                this.state.fontLoaded ? <Text style={styles.buttonText}>Delete Account</Text> : null
                            }
                        </LinearGradient>
                    </TouchableOpacity>

                </KeyboardAvoidingView>

            </View>
        )
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      },

    Box: {
        alignSelf: 'stretch',
        backgroundColor: '#282828',
        borderWidth: 3,
        borderColor: 'rgb(255, 255, 255)',
        marginTop: 30,
        elevation: 3
      },

      // Button Style
      SectionStyleDeleteButton: {
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 2,
        borderColor: '#fff',
        height: 55,
        borderRadius: 3,
        width: 185,
        left: 55,
        marginTop:15
      },

      buttonText: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 18,
        fontFamily: 'coolvetica'
      },

      // Text Style
      SectionStyletext1: {
        borderBottomWidth: 2,
        borderBottomColor: '#4B4B4B',
        flexDirection: 'row',
        marginHorizontal: 3
      },

      text1Style: {
        color: '#B2B2B2',
        fontSize: 32,
        paddingTop: 15,
        paddingBottom: 15,
        left: 10,
        fontFamily: 'coolvetica'
      },

      SectionStyletext2: {
        flexDirection: 'row',
        marginTop: 10,
        marginHorizontal: 15
      },

      text2Style: {
        color: '#FFFFFF',
        fontSize: 12,
        fontFamily: 'coolvetica'
      },

      SectionStyletext3: {
        flexDirection: 'row',
        marginTop: 15,
        marginHorizontal: 15
      },

      text3Style: {
        color: '#F81300',
        fontSize: 8,
        lineHeight: 20,
        fontFamily: 'coolvetica'
      },

      SectionStyletext4: {
        flexDirection: 'row',
        marginTop: 15,
        marginBottom: 15,
        marginHorizontal: 15
      },

      text4Style: {
        color: '#FFFFFF',
        fontSize: 12,
        lineHeight: 25,
        fontFamily: 'coolvetica'
      },

      SectionStyle1: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        alignSelf: 'stretch',
        height: 50,
        borderRadius: 3,
        paddingHorizontal: 10,
        marginBottom: 5,
        marginHorizontal: 15
      },

      SectionStyle2: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        alignSelf: 'stretch',
        height: 50,
        borderRadius: 3,
        paddingHorizontal: 10,
        marginTop: 10,
        marginBottom: 20,
        marginHorizontal: 15
      },

      ImageStyle: {
        height: 25,
        width: 20,
        resizeMode: 'stretch',
      },

      CustomLabelStyle: {
        fontSize: 8,
        color: '#FC1600',
        fontStyle: 'italic',
        
        paddingRight: 15,
        fontFamily: 'coolvetica'
      }
});
