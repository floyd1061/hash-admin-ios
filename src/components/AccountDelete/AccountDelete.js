import React, { Component } from 'react'
import {StyleSheet} from 'react-native'
import { LinearGradient } from 'expo'
import AccountDeleteForm from './AccountDeleteForm'

export default class AccountDelete extends Component {
  
  render() {

    return (
      <LinearGradient colors={['#E60300', '#FF0906']} style={ styles.container }>

          <AccountDeleteForm />

      </LinearGradient>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
