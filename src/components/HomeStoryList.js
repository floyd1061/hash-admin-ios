import React, { Component } from 'react'
import { View, ScrollView, StyleSheet } from 'react-native'
import StoryDetail from './StoryDetail'
import Navbar from './NavbarComponent'

export default class HomeStoryList extends Component {

    state = { stories: [] };

    componentWillMount() {
        this.setState({ stories: storiesData });
    };

    renderStories() {
        const { navigate } = this.props.navigation;
        return this.state.stories.map(story =>
            <StoryDetail key={story.story_id} story={story} navigate={navigate} />
        );
    };

    render() {
        return (
            <View style={styles.container}>
                <Navbar />
                <ScrollView>
                    {this.renderStories()}
                </ScrollView>
            </View>
        )
    }
};

const storiesData = [
    {
        story_id: 2,
        user: { user_id: 1000223, user_name: "Sarah Parker", profile_pic: require('./assets/profile_pic.jpg') },
        shared_event_with: { user_id: 1000333, user_name: "John S.", profile_pic: require('./assets/forid.jpg') },
        story_title: "Amanda's New year Party",
        story_description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
        story_date: "01 January 2018 at 00:12",
        story_geolocation: "Nevermind Nightclub",
        story_like_comment_count: { likes: 500, comments: 7 },
        story_contents: [
            {
                id: 21, url: require('./assets/01.jpg'), likes: 30, comments: [
                    {
                        user: {
                            user_name: "Abhijit Majumder", profile_pic: require('./assets/abhi.jpg'), user_id: "22334843",
                        },
                        comment_text: "The party looks too sick for my taste!!",
                        key: "25",
                        time: "05 January at 16.18",
                        like_count: 3,
                        youLiked: true,
                    }
                ]
            },
            {
                id: 22, url: require('./assets/02.jpg'), likes: 40, comments: [],
            },
            {
                id: 23, url: require('./assets/03.jpg'), likes: 0, comments: [],
            },
            {
                id: 24, url: require('./assets/04.jpg'), likes: 0, comments: [],
            },
            {
                id: 25, url: require('./assets/05.jpg'), likes: 0, comments: [],
            },
        ],
        story_comment_and_replies: [
            {
                user: { user_name: "Sarah P.", profile_pic: require('./assets/profile_pic.jpg'), user_id: "22334543", },
                comment_text: "We had a great party where were you fred",
                key: "21",
                time: "03 January at 22.25",
                like_count: 5,
                youLiked: false,
                replies: [
                    {
                        user: {
                            user_name: "Shermon Khan", profile_pic: require('./assets/shermon.jpg'), user_id: "22334549",
                        },
                        comment_text: "We missed you forid vi",
                        key: "22",
                        time: "04 January at 10.15",
                        like_count: 3,
                        youLiked: true,
                    },
                    {
                        user: {
                            user_name: "Freddy", profile_pic: require('./assets/forid.jpg'), user_id: "22334599",
                        },
                        comment_text: "Great party there",
                        key: "23",
                        time: "04 January at 10.15",
                        like_count: 0,
                        youLiked: false,
                    },
                ],
            },
            {
                user: {
                    user_name: "Sarah P.", profile_pic: require('./assets/profile_pic.jpg'), user_id: "22334543",
                }, comment_text: "I wish you were here honey..",
                key: "24",
                time: "05 January at 16.19",
                like_count: 1,
                youLiked: false,
                replies: [
                    {
                        user: {
                            user_name: "Abhijit Majumder", profile_pic: require('./assets/abhi.jpg'), user_id: "22334843",
                        },
                        comment_text: "The party looks too sick for my taste!!",
                        key: "25",
                        time: "05 January at 16.18",
                        like_count: 3,
                        youLiked: true,
                    }
                ],
            },
            {
                user: {
                    user_name: "Sarah P.", profile_pic: require('./assets/profile_pic.jpg'), user_id: "22334543",
                },
                comment_text: "Hi5 !! hey are you coming at 4??",
                key: "26",
                time: "09 January at 15.01",
                like_count: 11,
                youLiked: false,
                replies: [
                    {
                        user: {
                            user_name: "Deepa", profile_pic: require('./assets/deepa.jpg'), user_id: "2334543",
                        },
                        comment_text: "Hi5 sarah!! Coming",
                        key: "27",
                        time: "09 January at 14.14",
                        like_count: 5,
                        youLiked: false,
                    }
                ],
            },
        ],
    },
    {
        story_id: 1,
        user: { user_id: 1000223, user_name: "Abhijit Majumder", profile_pic: require('./assets/abhi.jpg') },
        shared_event_with: { user_id: 1000334, user_name: "Freedy M.", profile_pic: require('./assets/forid.jpg') },
        story_title: "Christmas Eve At My Place",
        story_description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
        story_date: "25 December 2017 at 18:22",
        story_geolocation: "East Liverpool",
        story_like_comment_count: { likes: 50, comments: 2 },
        story_contents: [
            {
                id: 11, url: require('./assets/05.jpg'), likes: 0, comments: [],
            },
            {
                id: 12, url: require('./assets/03.jpg'), likes: 10, comments: [],
            },
            {
                id: 13, url: require('./assets/01.jpg'), likes: 0, comments: [],
            }
        ],
        story_comment_and_replies: [
            {
                user: { user_name: "Shermon Khan", profile_pic: require('./assets/shermon.jpg'), user_id: "22334549", }, comment_text: "Nice party", key: "12", time: "04 January at 10.15", like_count: 3, youLiked: true, replies: [],
            },
            {
                user: { user_name: "Deepa", profile_pic: require('./assets/deepa.jpg'), user_id: "2334543", }, comment_text: "Hi Mate", key: "17", time: "06 January at 14.14", like_count: 1, youLiked: false, replies: [],
            }
        ]
    }
];

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
});