import React, { Component } from 'react'
import { View, StyleSheet, ListView } from 'react-native'
import FriendsFollowerListItem from './FriendsFollowerListItem'
import { loadAllfriendRequests, acceptFriendRequest } from '../../actions'
import { connect } from 'react-redux'
import _ from 'lodash'
class FriendsFollowerList extends Component {

    
    componentWillMount() {
        this.props.loadAllfriendRequests();
        this.createDataSource(this.props.friendsfollowersdata);
    }
    componentWillReceiveProps(nextProps) {
        // nextProps are the next set of props that this component
        // will be rendered with
        // this.props is still the old set of props

        this.createDataSource(nextProps.friendsfollowersdata);
    }
    createDataSource(friendsfollowersdata) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.dataSource = ds.cloneWithRows(friendsfollowersdata);
    }
   
    renderRow(item) {
        return <FriendsFollowerListItem item={item} />;
    }

    render() {
        return (
            <View style={styles.Container}>
                <View style={styles.bodySection}>
                    <ListView
                        enableEmptySections={true}
                        dataSource={this.dataSource}
                        renderRow={this.renderRow}
                        initialListSize={10}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    Container: {
        flex: 1,
        backgroundColor: '#DEDEDE',
    },
});

const mapStateToProps = (state) => {
    const data = _.map(state.userPreloadData.fiendRequests, (val, uid) => {
        return { ...val, uid }
    })
    const friendsfollowersdata = _.filter(data, 'channelId', 'friendRequest');
    return { friendsfollowersdata };
};

export default connect(mapStateToProps, { loadAllfriendRequests, acceptFriendRequest })(FriendsFollowerList);