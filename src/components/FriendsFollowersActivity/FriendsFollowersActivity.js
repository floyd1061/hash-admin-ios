import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import HeaderNavBar from './HeaderNavBar';
import ActivityList from './ActivityList';
import FriendsFollowerList from './FriendsFollowerList';

export default class FriendsFollowersActivity extends Component {
    state = {
        index: 0,
        routes: [
            { key: 'activity', title: 'My Activity' },
            { key: 'friendsfollowers', title: 'Friends & Followers' },
        ],
    };

    _handleIndexChange = index => this.setState({ index });

    _renderHeader = props => {
        const inputRange = props.navigationState.routes.map((x, i) => i);
        return (
            <View>
                <HeaderNavBa r  />
                <TabBar style={styles.Talkbar} {...props} />
            </View>
        )
    };

    _renderScene = SceneMap({
        activity: ActivityList,
        friendsfollowers: FriendsFollowerList,
    });

    render() {
        return (
           
                
                <FriendsFollowerList />
            
        )
    }
}

const styles = StyleSheet.create({
    Talkbar: {
        backgroundColor: '#FF1004',
    }
})




