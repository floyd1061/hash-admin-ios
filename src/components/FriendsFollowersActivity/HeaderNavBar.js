import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native'
import { Actions } from 'react-native-router-flux';
import {Font} from 'expo'

export default class HeaderNavBar extends Component {

    state = {
        fontLoaded: true,
    };

  

    render(){
        return (
            <View style = {styles.Container} >
                <View style = {styles.HeaderSection} >
                    <View style = { styles.rowsec }>
                        <TouchableOpacity onPress={() => Actions.UserProfileTimeline()}>
                            {
                                this.state.fontLoaded ? ( <Text style = {styles.headerText1} > Hashtazapp </Text> ) : null
                            }
                        </TouchableOpacity>

                        <View style = {styles.headeritem} >
                            <TouchableOpacity>                   
                                <Image source={require('./assets/messagenumber.png')} style={styles.messageicon} />
                                {/* <Text style = {styles.messagecount}> {messageCount} </Text> */}
                            </TouchableOpacity>
                        
                            <TouchableOpacity>
                                <Image source={require('./assets/searchicon.png')} style={styles.searchicon} />
                            </TouchableOpacity>

                            <TouchableOpacity>
                                <Image source={require('./assets/followersnumber.png')} style={styles.followersnumber} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    Container: {
        backgroundColor: '#DEDEDE',
    },

    HeaderSection: {
        backgroundColor: '#FF1303',
        height: 110,
        paddingHorizontal: 20,
        alignSelf: 'stretch',   
    },

    rowsec: {
        flexDirection: 'row',
        marginTop: 50,
        justifyContent: 'space-between'  
    },

    headerText1: {
        fontSize: 23,
        color: '#fff',
        
        fontFamily: 'coolvetica'
    },

    headeritem: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    // messagearea: {
    //     position: 'relative',
    // },

    messageicon: {
        height: 30,
        width: 30,
        resizeMode: 'stretch',
        marginRight: 20
    },

    // messagecount: {
    //     position: 'absolute',
    //     top: -6,
    //     color: '#fff',
    //     backgroundColor: '#EB1406',
    //     borderRadius: 50,
    //     borderWidth: 2,
    //     borderColor: '#fff',
    //     width: 20,
    //     height: 20,
    //     fontSize: 8,
    //     paddingTop: 5,
    //     textAlign: 'center',
    //     left: 32,
    // },  

    searchicon: {
        height: 30,
        width: 30,
        resizeMode: 'stretch',
        marginRight: 15
    },

    followersnumber: {
        height: 30,
        width: 30,
        resizeMode: 'stretch',
    },
});