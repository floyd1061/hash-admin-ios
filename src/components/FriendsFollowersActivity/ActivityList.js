import React, { Component } from 'react'
import { View, StyleSheet, ListView } from 'react-native'
import ActivityListItem from './ActivityListItem'
import { connect } from 'react-redux'

class ActivityList extends Component {
    
    componentWillMount() {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        this.dataSource = ds.cloneWithRows(this.props.activitydata);
    }
    renderRow(item) {
        return <ActivityListItem item = {item} />;
    }

    render() {
        return (
            <View style = {styles.Container}>
                <ListView
                    dataSource={this.dataSource}
                    renderRow={this.renderRow}
                    initialListSize={10}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    Container: {
        flex: 1,
        backgroundColor: '#DEDEDE',
    },
});

const mapStateToProps = (state) => {
    return { activitydata: state.activities };
};

export default connect (mapStateToProps)(ActivityList);