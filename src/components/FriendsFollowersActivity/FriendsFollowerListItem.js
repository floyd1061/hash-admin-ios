import React, { Component } from 'react'
import firebase from '@firebase/app'
import "firebase/auth"
import "firebase/database"
import "firebase/storage"
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native'
import { Font } from 'expo'
import { connect } from 'react-redux';
import { loadAllfriendRequests, acceptFriendRequest } from '../../actions'
class FriendsFollowerListItem extends Component {

    state = {
        fontLoaded: true,
    };

    acceptFriendRequest(senderId) {
        this.props.acceptFriendRequest(senderId);

    }
    renderByFriendsFollowersType(item) {
        //console.log(item.data);
        switch (item.data.status) {
            case 3
                : return (
                    <View style={styles.rowSec}>
                        <TouchableOpacity>
                            <Image source={require('./assets/avatar.png')} style={styles.avataricon} />
                        </TouchableOpacity>

                        <View style={styles.textsec}>
                            {
                                this.state.fontLoaded ? (<Text style={styles.Text2}>You Started Following</Text>) : null
                            }
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? (<Text style={styles.Text1}>{item.from.displayName}</Text>) : null
                                }
                            </TouchableOpacity>
                        </View>

                        <TouchableOpacity style={styles.box1Button}>
                            <Image source={require('./assets/load.png')} style={styles.loadicon} />
                            <Image source={require('./assets/plus.png')} style={styles.plusicon} />
                            <View style={styles.box2Button}>
                                {
                                    this.state.fontLoaded ? (<Text style={styles.followText}>Follow</Text>) : null
                                }
                            </View>
                        </TouchableOpacity>
                    </View>
                );

            case 1
                : return (
                    <View style={styles.rowSec}>
                        <TouchableOpacity>
                            <Image source={require('./assets/avatar.png')} style={styles.avataricon} />
                        </TouchableOpacity>

                        <View style={styles.textsec}>
                            {
                                this.state.fontLoaded ? (<Text style={styles.Text2}>Now You Are Friend With</Text>) : null
                            }
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? (<Text style={styles.Text1}>{item.from.displayName}</Text>) : null
                                }
                            </TouchableOpacity>
                        </View>

                        <TouchableOpacity style={styles.box3Button}>
                            <Image source={require('./assets/likeactive.png')} style={styles.likeactiveicon} />

                            <View style={styles.box4Button}>
                                {
                                    this.state.fontLoaded ? (<Text style={styles.friendText}>Friends</Text>) : null
                                }
                            </View>
                        </TouchableOpacity>
                    </View>
                );

            case 0
                : return (
                    <View style={styles.rowSec}>
                        <TouchableOpacity>
                            <Image source={require('./assets/avatar.png')} style={styles.avataricon} />
                        </TouchableOpacity>

                        <View style={styles.textsec}>
                            {
                                this.state.fontLoaded ? (<Text style={styles.Text2}>New Friend Request From</Text>) : null
                            }
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? (<Text style={styles.Text1}>{item.from.displayName}</Text>) : null
                                }
                            </TouchableOpacity>
                        </View>

                        <TouchableOpacity style={styles.box5Button}
                            onPress={() => { this.props.acceptFriendRequest(item.senderId, item.uid, item.badge) }}>
                            <Image source={require('./assets/like.png')} style={styles.likeicon} />
                            <Image source={require('./assets/plus.png')} style={styles.plusicon2} />

                            <View style={styles.box6Button}>
                                {
                                    this.state.fontLoaded ? (<Text style={styles.ReqbuttonText}>Accept Request</Text>) : null
                                }
                            </View>
                        </TouchableOpacity>
                    </View>

                );
        }
    }

    render() {
        //console.log(this.props.item)
        const item = this.props.item;
        return (
            <View style={styles.bodyContainer}>
                {this.renderByFriendsFollowersType(item)}
            </View>
        )
    }
}
export default connect(null, { acceptFriendRequest })(FriendsFollowerListItem)
const styles = StyleSheet.create({
    bodyContainer: {
        flex: 1,
    },

    rowSec: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'stretch',
        paddingHorizontal: 15,
        paddingTop: 25,
        paddingBottom: 5,
        marginBottom: 1,
        justifyContent: 'space-between',
    },

    avataricon: {
        height: 60,
        width: 60,
        resizeMode: 'stretch',
    },

    textsec: {
        width: 140,
        paddingRight: 1
    },

    Text1: {
        fontSize: 12,
        color: '#FF0408',

        fontFamily: 'coolvetica'
    },

    Text2: {
        fontSize: 11,
        color: '#000',

        fontFamily: 'coolvetica'
    },

    box1Button: {
        width: 124,
        height: 40,
        backgroundColor: '#FFFFFF',
        borderRadius: 3,
        justifyContent: 'center',
    },

    box2Button: {
        width: 86,
        height: 36,
        justifyContent: 'center',
        backgroundColor: '#7C7C7C',
        borderRadius: 3,
        bottom: 14,
        left: 36,
    },

    followText: {
        fontSize: 15,
        color: '#FFFFFF',
        textAlign: 'center',

        fontFamily: 'coolvetica'
    },

    loadicon: {
        height: 20,
        width: 20,
        resizeMode: 'stretch',
        left: 8,
        top: 22
    },

    plusicon: {
        height: 8,
        width: 8,
        resizeMode: 'stretch',
        left: 23,
        top: 12
    },

    Text3: {
        fontSize: 11,
        color: '#000',

        top: 12,
        fontFamily: 'coolvetica'
    },

    likeactiveicon: {
        height: 20,
        width: 20,
        resizeMode: 'stretch',
        left: 8,
        top: 17
    },

    box3Button: {
        width: 124,
        height: 40,
        backgroundColor: '#FFFFFF',
        borderRadius: 3,
        justifyContent: 'center',
    },

    box4Button: {
        width: 86,
        height: 36,
        justifyContent: 'center',
        backgroundColor: '#FF1303',
        borderRadius: 3,
        bottom: 10,
        left: 36,
    },

    friendText: {
        fontSize: 15,
        color: '#FFFFFF',
        textAlign: 'center',

        fontFamily: 'coolvetica'
    },

    box5Button: {
        width: 124,
        height: 40,
        backgroundColor: '#FFFFFF',
        borderRadius: 3,
        justifyContent: 'center',
    },

    box6Button: {
        width: 86,
        height: 36,
        justifyContent: 'center',
        backgroundColor: '#7C7C7C',
        borderRadius: 3,
        bottom: 14,
        left: 36,
    },

    ReqbuttonText: {
        fontSize: 12,
        color: '#FFFFFF',
        textAlign: 'center',

        fontFamily: 'coolvetica'
    },

    likeicon: {
        height: 20,
        width: 20,
        resizeMode: 'stretch',
        left: 8,
        top: 22
    },

    plusicon2: {
        height: 8,
        width: 8,
        resizeMode: 'stretch',
        left: 22,
        top: 4
    },
});