import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native'
import {Font} from 'expo'

export default class ActivityListItem extends Component {

    state = {
        fontLoaded: true,
    };

   

    renderByActivityType(item) {
        switch (item.activityType) {
            case 'followUser'
                : return (
                    <View style={styles.rowSec}>
                        <TouchableOpacity>
                            <Image source={require('./assets/avatar.png')} style={styles.avataricon} />
                        </TouchableOpacity>

                        <View style={styles.textsec1}>
                            {
                                this.state.fontLoaded ? ( <Text style={styles.Text2}>You Started Following</Text> ) : null
                            }
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style={styles.Text1}>{item.followedUser.firstName} {item.followedUser.lastName}</Text> ) : null
                                }
                            </TouchableOpacity>
                        </View>

                        <TouchableOpacity style={styles.box1Button}>
                            <Image source={require('./assets/load.png')} style={styles.loadicon} />
                            <Image source={require('./assets/plus.png')} style={styles.plusicon} />
                            <View style={styles.box2Button}>
                                {
                                    this.state.fontLoaded ? ( <Text style={styles.followText}>Follow</Text> ) : null
                                }
                            </View>
                        </TouchableOpacity>
                    </View>
                );

            case 'favoriteEvent' 
                : return (
                    <View style={styles.rowSec}>
                        <TouchableOpacity>
                            <Image source={require('./assets/avatar.png')} style={styles.avataricon} />
                        </TouchableOpacity>

                        <View style={styles.textsec2}>
                            {
                                this.state.fontLoaded ? ( <Text style={styles.Text3}>You Favouritised an Event Day</Text> ) : null
                            }
                        </View>
    
                        {this.styleForEventBubble(item)}
                        
                    </View>
                );

            case 'friendsUser' 
                : return (
                    <View style= { styles.rowSec }>
                        <TouchableOpacity>
                            <Image source={require('./assets/avatar.png')} style={styles.avataricon} />
                        </TouchableOpacity>

                        <View style= { styles.textsec1 }>
                            {
                                this.state.fontLoaded ? ( <Text style = {styles.Text2}>Now You Are Friend With</Text> ) : null
                            }
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.Text1}>{item.friendedUser.firstName} {item.friendedUser.lastName}</Text> ) : null
                                }
                            </TouchableOpacity>
                        </View>

                        <TouchableOpacity style= { styles.box3Button }>
                            <Image source={require('./assets/likeactive.png')} style={styles.likeactiveicon} />
                            
                            <View style= { styles.box4Button }>
                                {
                                    this.state.fontLoaded ? ( <Text style= { styles.friendText }>Friends</Text> ) : null
                                }
                            </View>
                        </TouchableOpacity>
                    </View>
                );

            case 'friendRequestUser' 
                : return (
                    <View style= { styles.rowSec }>
                        <TouchableOpacity>
                            <Image source={require('./assets/avatar.png')} style={styles.avataricon} />
                        </TouchableOpacity>

                        <View style= { styles.textsec1 }>
                            {
                                this.state.fontLoaded ? ( <Text style = {styles.Text2}>Friend Request Sent</Text> ) : null
                            }
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.Text1}>{item.friendRequestedUser.firstName} {item.friendRequestedUser.lastName}</Text> ) : null
                                }
                            </TouchableOpacity>
                        </View>

                        <TouchableOpacity style= { styles.box5Button }>
                            <Image source={require('./assets/like.png')} style={styles.likeicon} />
                            <Image source={require('./assets/plus.png')} style={styles.plusicon2} />
                            
                            <View style= { styles.box6Button }>
                                {
                                    this.state.fontLoaded ? ( <Text style= { styles.ReqbuttonText }>Friend Request</Text> ) : null
                                }
                            </View>
                        </TouchableOpacity>
                    </View>
                );
        }
    }

    styleForEventBubble(item){
        if(item.favouritedEvent.isHighlighted)
            return (
                <TouchableOpacity style = {{width: 110, height: 80, justifyContent: 'center', alignItems: 'center'}}>

                    <Image source={require('./assets/eventbox.png')} style={{width: 110, height: 80, resizeMode: 'stretch', position: 'relative'}} />
                    <View style={{position: 'absolute', width: 80, right: 10}}>
                        {
                            this.state.fontLoaded ? ( <Text style={{textAlign: 'center', color: '#fff',  fontSize: 13, fontFamily: 'coolvetica'}}>{item.favouritedEvent.eventName}</Text> ) : null
                        }
                        {
                            this.state.fontLoaded ? ( <Text style={{textAlign: 'center', fontSize: 6, color: '#fff', fontFamily: 'coolvetica'}}>{item.favouritedEvent.eventStartTime} - {item.favouritedEvent.eventEndTime}</Text> ) : null
                        }
                    </View>

                </TouchableOpacity>
            )
            return (
                <TouchableOpacity style = {{width: 110, height: 80, justifyContent: 'center', alignItems: 'center'}}>

                    <Image source={require('./assets/Shape.png')} style={{width: 110, height: 80, resizeMode: 'stretch', position: 'relative'}} />
                    <View style={{position: 'absolute', width: 80, right: 10}}>
                        {
                            this.state.fontLoaded ? ( <Text style={{textAlign: 'center', color: '#fff',  fontSize: 13, fontFamily: 'coolvetica'}}>{item.favouritedEvent.eventName}</Text> ) : null
                        }
                        {
                            this.state.fontLoaded ? ( <Text style={{textAlign: 'center', fontSize: 6, color: '#fff', fontFamily: 'coolvetica'}}>{item.favouritedEvent.eventStartTime} - {item.favouritedEvent.eventEndTime}</Text> ) : null
                        }
                    </View>

                </TouchableOpacity>
            )
    }

    render() {
        const item = this.props.item;
        return (
            <View style={styles.bodyContainer}>
                <View style={styles.datetimeSec}>
                    {
                        this.state.fontLoaded ? ( <Text style={styles.datetimeText}>{item.dateTime }</Text> ) : null
                    }
                    <Image source={require('./assets/time.png')} style={styles.timeicon} />
                </View>
                {this.renderByActivityType(item)}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    bodyContainer: {
        flex: 1,
        paddingHorizontal: 15,
        paddingBottom: 5
    },

    datetimeSec: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: 15,
    },

    datetimeText: {
        fontSize: 10,
        color: '#7C7C7C',
        top: 7,
        
        right: 5,
        fontFamily: 'coolvetica'
    },

    timeicon: {
        height: 20,
        width: 20,
        resizeMode: 'stretch',
    },

    rowSec: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 10,
        alignItems: 'center',
        alignSelf: 'stretch',
        justifyContent: 'space-between',
    },

    avataricon: {
        height: 60,
        width: 60,
        resizeMode: 'stretch',
    },

    textsec1: {
        width: 140,
        paddingRight: 1,
    },

    Text1: {
        fontSize: 12,
        color: '#FF0408',
        
        fontFamily: 'coolvetica'
    },

    Text2: {
        fontSize: 11,
        color: '#000',
        
        fontFamily: 'coolvetica'
    },

    box1Button: {
        width: 124,
        height: 40,
        backgroundColor: '#FFFFFF',
        borderRadius: 3,
        justifyContent:'center',
    },

    box2Button: {
        width: 86,
        height: 36,
        justifyContent: 'center',
        backgroundColor: '#7C7C7C',
        borderRadius: 3,
        bottom: 14,
        left: 36,
    },

    followText: {
        fontSize: 15,
        color: '#FFFFFF',
        textAlign: 'center',
        
        fontFamily: 'coolvetica'
    },

    loadicon: {
        height: 20,
        width: 20,
        resizeMode: 'stretch',
        left: 8,
        top: 22
    },

    plusicon: {
        height: 8,
        width: 8,
        resizeMode: 'stretch',
        left: 23,
        top: 12
    },
    
    textsec2: {
        width: 155,
        paddingRight: 1,
    },

    Text3: {
        fontSize: 11,
        color: '#000',
        
        fontFamily: 'coolvetica'
    },

    likeactiveicon: {
        height: 20,
        width: 20,
        resizeMode: 'stretch',
        left: 8,
        top: 17
    },

    box3Button: {
        width: 124,
        height: 40,
        backgroundColor: '#FFFFFF',
        borderRadius: 3,
        justifyContent:'center', 
    },

    box4Button: {
        width: 86,
        height: 36,
        justifyContent: 'center',
        backgroundColor: '#FF1303',
        borderRadius: 3,
        bottom: 10,
        left: 36,
    },

    friendText: {
        fontSize: 15,
        color: '#FFFFFF',
        textAlign: 'center',
        
        fontFamily: 'coolvetica'
    },

    box5Button: {
        width: 124,
        height: 40,
        backgroundColor: '#FFFFFF',
        borderRadius: 3,
        justifyContent:'center',
    },

    box6Button: {
        width: 86,
        height: 36,
        justifyContent: 'center',
        backgroundColor: '#7C7C7C',
        borderRadius: 3,
        bottom: 14,
        left: 36,
    },

    ReqbuttonText: {
        fontSize: 12,
        color: '#FFFFFF',
        textAlign: 'center',
        
        fontFamily: 'coolvetica'
    },

    likeicon: {
        height: 20,
        width: 20,
        resizeMode: 'stretch',
        left: 8,
        top: 22
    },

    plusicon2: {
        height: 8,
        width: 8,
        resizeMode: 'stretch',
        left: 22,
        top: 4
    },
});