import React, { Component } from 'react'
/* import Svg, {
    Svg.Path,
} from 'react-native-svg' */
import {Svg} from 'expo'
import firebase from '@firebase/app'
import { connect } from 'react-redux'
import { View, Animated, Easing } from 'react-native'
import { svgPathProperties } from 'svg-path-properties';
import _ from 'lodash';
import { Actions } from 'react-native-router-flux';
import { updateEventAndTimelineAfterAnimation } from '../../actions'

import {
    d1, d2, d3,
    oneEvOne, oneEvTwo, oneEvThree,
    twoEvOne, twoEvTwo, twoEvThree,
    threeEvOne, threeEvTwo, threeEvThree,
    lgDefTwo, lgDefThree, lgDefFour,
    lgDefTwoOne, lgDefTwoTwo, lgDefTwoThree,
    lgDefThreeOne, lgDefThreeTwo, lgDefThreeThree,
    lgDefFourOne, lgDefFourTwo, lgDefFourThree,
    lgDefTwoR,
    lgDefThreeR,
    lgDefFourR,
    lgDefTwoROne, lgDefTwoRTwo, lgDefTwoRThree,
    lgDefThreeROne, lgDefThreeRTwo, lgDefThreeRThree,
    lgDefFourROne, lgDefFourRTwo, lgDefFourRThree

} from './SvgPoints/SvgPoints'

const AnimatedPath = Animated.createAnimatedComponent(Svg.Path);
const widthSvg = 170.4683;
class helperSvgPoints extends Component {

    constructor(props) {
        super(props);
        this.currentScene = '';
        this.animLength = 150;
        this.animatedEvent = null;
        const properties1 = svgPathProperties(d1);
        this.length1 = properties1.getTotalLength();
        this.strokeDashoffset1 = new Animated.Value(this.length1);

        const properties2 = svgPathProperties(d2);
        this.length2 = properties2.getTotalLength();
        this.strokeDashoffset2 = new Animated.Value(this.length2);

        const properties3 = svgPathProperties(d3);
        this.length3 = properties3.getTotalLength();
        this.strokeDashoffset3 = new Animated.Value(this.length3);
    }

    state = {
        renderAnim: false
    }

    renderSecTionOneDefs(events) {

        try {

            let el = events.length;
            if (el > 0) {
                if (el === 3) {
                    if (events[2].isHighlighted) {
                        return lgDefTwoThree;
                    }
                    else if (events[1].isHighlighted) {
                        return lgDefTwoTwo;
                    }
                    else if (events[0].isHighlighted) {
                        return lgDefTwoOne;
                    } else {
                        return lgDefTwo;
                    }
                } else if (el === 2) {
                    if (events[1].isHighlighted) {
                        return lgDefTwoTwo;
                    } else if (events[0].isHighlighted) {
                        return lgDefTwoOne;
                    } else {
                        return lgDefTwo;
                    }
                } else if (el === 1) {
                    if (events[0].isHighlighted) {
                        return lgDefTwoOne;
                    } else {
                        return lgDefTwo;
                    }
                }
            } else {
                return lgDefTwo;
            }



        } catch (error) {
            //console.log(error);
        }
    }
    renderSecTionTwoDefs(events) {

        try {

            let el = events.length;
            if (el > 0) {
                if (el === 3) {
                    if (events[2].isHighlighted) {
                        return lgDefThreeThree;
                    }
                    else if (events[1].isHighlighted) {
                        return lgDefThreeTwo;
                    }
                    else if (events[0].isHighlighted) {
                        return lgDefThreeOne;
                    } else {
                        return lgDefThree;
                    }
                } else if (el === 2) {
                    if (events[1].isHighlighted) {
                        return lgDefThreeTwo;
                    } else if (events[0].isHighlighted) {
                        return lgDefThreeOne;
                    } else {
                        return lgDefThree;
                    }
                } else if (el === 1) {
                    if (events[0].isHighlighted) {
                        return lgDefThreeOne;
                    } else {
                        return lgDefThree;
                    }
                }
            } else {
                return lgDefThree;
            }



        } catch (error) {
            //console.log(error);
        }
    }
    renderSecTionThreeDefs(events) {

        try {

            let el = events.length;
            if (el > 0) {
                if (el === 3) {
                    if (events[2].isHighlighted) {
                        return lgDefFourThree;
                    }
                    else if (events[1].isHighlighted) {
                        return lgDefFourTwo;
                    }
                    else if (events[0].isHighlighted) {
                        return lgDefFourOne;
                    } else {
                        return lgDefFour;
                    }
                } else if (el === 2) {
                    if (events[1].isHighlighted) {
                        return lgDefFourTwo;
                    } else if (events[0].isHighlighted) {
                        return lgDefFourOne;
                    } else {
                        return lgDefFour;
                    }
                } else if (el === 1) {
                    if (events[0].isHighlighted) {
                        return lgDefFourOne;
                    } else {
                        return lgDefFour;
                    }
                }
            } else {
                return lgDefFour;
            }



        } catch (error) {
            //console.log(error);
        }
    }
    renderSecTionFourDefs(events) {

        try {

            let el = events.length;
            if (el > 0) {
                if (el === 3) {
                    if (events[2].isHighlighted) {
                        return lgDefTwoRThree;
                    }
                    else if (events[1].isHighlighted) {
                        return lgDefTwoRTwo;
                    }
                    else if (events[0].isHighlighted) {
                        return lgDefTwoROne;
                    } else {
                        return lgDefTwoR;
                    }
                } else if (el === 2) {
                    if (events[1].isHighlighted) {
                        return lgDefTwoRTwo;
                    } else if (events[0].isHighlighted) {
                        return lgDefTwoROne;
                    } else {
                        return lgDefTwoR;
                    }
                } else if (el === 1) {
                    if (events[0].isHighlighted) {
                        return lgDefTwoROne;
                    } else {
                        return lgDefTwoR;
                    }
                }
            } else {
                return lgDefTwoR;
            }



        } catch (error) {
            //console.log(error);
        }
    }
    renderSecTionFiveDefs(events) {

        try {

            let el = events.length;
            if (el > 0) {
                if (el === 3) {
                    if (events[2].isHighlighted) {
                        return lgDefThreeRThree;
                    }
                    else if (events[1].isHighlighted) {
                        return lgDefThreeRTwo;
                    }
                    else if (events[0].isHighlighted) {
                        return lgDefThreeROne;
                    } else {
                        return lgDefThreeR;
                    }
                } else if (el === 2) {
                    if (events[1].isHighlighted) {
                        return lgDefThreeRTwo;
                    } else if (events[0].isHighlighted) {
                        return lgDefThreeROne;
                    } else {
                        return lgDefThreeR;
                    }
                } else if (el === 1) {
                    if (events[0].isHighlighted) {
                        return lgDefThreeROne;
                    } else {
                        return lgDefThreeR;
                    }
                }
            } else {
                return lgDefThreeR;
            }



        } catch (error) {
            //console.log(error);
        }
    }
    renderSecTionSixDefs(events) {

        try {

            let el = events.length;
            if (el > 0) {
                if (el === 3) {
                    if (events[2].isHighlighted) {
                        return lgDefFourRThree;
                    }
                    else if (events[1].isHighlighted) {
                        return lgDefFourRTwo;
                    }
                    else if (events[0].isHighlighted) {
                        return lgDefFourROne;
                    } else {
                        return lgDefFourR;
                    }
                } else if (el === 2) {
                    if (events[1].isHighlighted) {
                        return lgDefFourRTwo;
                    } else if (events[0].isHighlighted) {
                        return lgDefFourROne;
                    } else {
                        return lgDefFourR;
                    }
                } else if (el === 1) {
                    if (events[0].isHighlighted) {
                        return lgDefFourROne;
                    } else {
                        return lgDefFourR;
                    }
                }
            } else {
                return lgDefFourR;
            }



        } catch (error) {
            //console.log(error);
        }
    }


    animate = (valueAnimateLength, animationPending, eventToAnimate, svgSegmentId, itemKey, year, animatedEvent, item) => {


        if (animationPending) {
            if (svgSegmentId == 1) {
                this.strokeDashoffset1.setValue(this.length1);
                Animated.sequence([
                    Animated.delay(2000),
                    Animated.timing(this.strokeDashoffset1, {
                        toValue: valueAnimateLength,
                        easing: Easing.elastic(-1, -2),
                        duration: 5000,

                    })
                ]).start(({ finished }) => {
                    if (finished) {
                        this.props.updateEventAndTimelineAfterAnimation(itemKey, eventToAnimate, animatedEvent, item)

                    } else {
                        //console.log('Animation was aborted')
                    }
                });
            } else if (svgSegmentId == 2) {

                this.strokeDashoffset2.setValue(this.length2);
                Animated.sequence([
                    Animated.delay(2000),
                    Animated.timing(this.strokeDashoffset2, {
                        toValue: valueAnimateLength,
                        easing: Easing.elastic(-1, -2),
                        duration: 5000,
                    })
                ]).start(({ finished }) => {
                    if (finished) {
                        this.props.updateEventAndTimelineAfterAnimation(itemKey, eventToAnimate, animatedEvent, item)

                    } else {
                        //console.log('Animation was aborted')
                    }
                });
            } else if (svgSegmentId == 3) {

                this.strokeDashoffset3.setValue(this.length3);
                Animated.sequence([
                    Animated.delay(2000),
                    Animated.timing(this.strokeDashoffset3, {
                        toValue: valueAnimateLength,
                        easing: Easing.elastic(-1, -2),
                        duration: 5000,
                    })
                ]).start(({ finished }) => {
                    if (finished) {
                        this.props.updateEventAndTimelineAfterAnimation(itemKey, eventToAnimate, animatedEvent, item)
                    } else {

                    }
                });
            }
        }

    };

    renderSection(secId, svgStyleId, day, events, animationPending) {

        switch (svgStyleId) {
            case 0:
                return (
                    null
                );
            case 1:
                return (
                    <Svg
                        height={278.1393}
                        width={widthSvg}
                    >
                        {animationPending ? lgDefTwo : this.renderSecTionOneDefs(events)}
                        <Svg.Path fill="none"
                            stroke="url(#df0272b4-af27-4498-a012-8e6b05c8c6c0)"
                            strokeMiterlimit={10}
                            strokeWidth={28.125}
                            d={d1}
                        />
                        <Svg.Path fill="none"
                            stroke="url(#0629d1b3-c525-43f9-b7a3-ad319f4252fc)"
                            strokeMiterlimit={10}
                            strokeWidth={19.6875}
                            d={d1}
                        />

                        <AnimatedPath fill="none"
                            stroke="url(#df0272b4-af27-4498-a012-8e6b05c8c6c01)"
                            strokeMiterlimit={10}
                            strokeWidth={28.125}
                            strokeDashoffset={this.strokeDashoffset1}
                            strokeDasharray={[this.length1, this.length1]}
                            d={d1}
                        />
                        <AnimatedPath fill="none"
                            stroke="url(#0629d1b3-c525-43f9-b7a3-ad319f4252fc1)"
                            strokeMiterlimit={10}
                            strokeWidth={19.6875}
                            strokeDashoffset={this.strokeDashoffset1}
                            strokeDasharray={[this.length1, this.length1]}
                            d={d1}
                        />
                    </Svg>
                );
            case 2:
                return (
                    <Svg
                        height={281.4884}
                        width={widthSvg}
                    >
                        {animationPending ? lgDefThree : this.renderSecTionTwoDefs(events)}
                        <Svg.Path
                            fill="none"
                            stroke="url(#f5bfdeee-16ad-4168-9edd-48c395e44d65)"
                            strokeMiterlimit={10}
                            strokeWidth={28.125}
                            d={d2}
                        />
                        <Svg.Path
                            fill="none"
                            stroke="url(#686a64e9-a46d-4086-98cb-9ddc3a5017df)"
                            strokeMiterlimit={10}
                            strokeWidth={19.6875}
                            d={d2} />

                        <AnimatedPath fill="none"
                            stroke="url(#f5bfdeee-16ad-4168-9edd-48c395e44d66)"
                            strokeMiterlimit={10}
                            strokeWidth={28.125}
                            strokeDashoffset={this.strokeDashoffset2}
                            strokeDasharray={[this.length2, this.length2]}
                            d={d2}
                        />
                        <AnimatedPath fill="none"
                            stroke="url(#686a64e9-a46d-4086-98cb-9ddc3a5017dg)"
                            strokeMiterlimit={10}
                            strokeWidth={19.6875}
                            strokeDashoffset={this.strokeDashoffset2}
                            strokeDasharray={[this.length2, this.length2]}
                            d={d2}
                        />

                    </Svg>
                );
            case 3:
                return (
                    <Svg
                        height={268.2128}
                        width={widthSvg}
                    >
                        {animationPending ? lgDefFour : this.renderSecTionThreeDefs(events)}
                        <Svg.Path
                            fill="none"
                            stroke="url(#551a812d-0e06-4ac7-abb0-e4d828b82b87)"
                            strokeWidth={28.125}
                            d={d3} />
                        <Svg.Path
                            fill="none"
                            stroke="url(#551a812d-0e06-4ac7-abb0-e4d828b82b88)"
                            strokeWidth={19.6875}
                            d={d3} />
                        <AnimatedPath fill="none"
                            stroke="url(#551a812d-0e06-4ac7-abb0-e4d828b82b89)"
                            strokeMiterlimit={10}
                            strokeWidth={28.125}
                            strokeDashoffset={this.strokeDashoffset3}
                            strokeDasharray={[this.length3, this.length3]}
                            d={d3}
                        />
                        <AnimatedPath fill="none"
                            stroke="url(#551a812d-0e06-4ac7-abb0-e4d828b82b90)"
                            strokeMiterlimit={10}
                            strokeWidth={19.6875}
                            strokeDashoffset={this.strokeDashoffset3}
                            strokeDasharray={[this.length3, this.length3]}
                            d={d3}
                        />
                    </Svg>
                );
            case 4:
                return (
                    <Svg
                        height={278.1393}
                        width={widthSvg}
                    >
                        {animationPending ? lgDefTwoR : this.renderSecTionFourDefs(events)}

                        <Svg.Path fill="none"
                            stroke="url(#df0272b4-af27-4498-a012-8e6b05c8c6c0)"
                            strokeMiterlimit={10}
                            strokeWidth={28.125}
                            d={d1}
                        />
                        <Svg.Path fill="none"
                            stroke="url(#0629d1b3-c525-43f9-b7a3-ad319f4252fc)"
                            strokeMiterlimit={10}
                            strokeWidth={19.6875}
                            d={d1}
                        />
                        <AnimatedPath fill="none"
                            stroke="url(#551a812d-0e06-4ac7-abb0-e4d828b82b89)"
                            strokeMiterlimit={10}
                            strokeWidth={28.125}
                            strokeDashoffset={this.strokeDashoffset1}
                            strokeDasharray={[this.length1, this.length1]}
                            d={d1}
                        />
                        <AnimatedPath fill="none"
                            stroke="url(#551a812d-0e06-4ac7-abb0-e4d828b82b90)"
                            strokeMiterlimit={10}
                            strokeWidth={19.6875}
                            strokeDashoffset={this.strokeDashoffset1}
                            strokeDasharray={[this.length1, this.length1]}
                            d={d1}
                        />
                    </Svg>
                );
            case 5:
                return (
                    <Svg
                        height={281.4884}
                        width={widthSvg}
                    >
                        {animationPending ? lgDefThreeR : this.renderSecTionFiveDefs(events)}
                        <Svg.Path
                            fill="none"
                            stroke="url(#f5bfdeee-16ad-4168-9edd-48c395e44d65)"
                            strokeMiterlimit={10}
                            strokeWidth={28.125}
                            d={d2} />
                        <Svg.Path
                            fill="none"
                            stroke="url(#686a64e9-a46d-4086-98cb-9ddc3a5017df)"
                            strokeMiterlimit={10}
                            strokeWidth={19.6875}
                            d={d2} />
                        <AnimatedPath fill="none"
                            stroke="url(#f5bfdeee-16ad-4168-9edd-48c395e44d66)"
                            strokeMiterlimit={10}
                            strokeWidth={28.125}
                            strokeDashoffset={this.strokeDashoffset2}
                            strokeDasharray={[this.length2, this.length2]}
                            d={d2}
                        />
                        <AnimatedPath fill="none"
                            stroke="url(#686a64e9-a46d-4086-98cb-9ddc3a5017dg)"
                            strokeMiterlimit={10}
                            strokeWidth={19.6875}
                            strokeDashoffset={this.strokeDashoffset2}
                            strokeDasharray={[this.length2, this.length2]}
                            d={d2}
                        />
                    </Svg>
                );
            case 6:
                return (
                    <Svg
                        height={267.2128}
                        width={widthSvg}
                    >
                        {animationPending ? lgDefFourR : this.renderSecTionSixDefs(events)}
                        <Svg.Path
                            fill="none"
                            stroke="url(#551a812d-0e06-4ac7-abb0-e4d828b82b87)"
                            strokeWidth={28.125}
                            d={d3} />
                        <Svg.Path
                            fill="none"
                            stroke="url(#551a812d-0e06-4ac7-abb0-e4d828b82b88)"
                            strokeWidth={19.6875}
                            d={d3} />
                        <AnimatedPath fill="none"
                            stroke="url(#df0272b4-af27-4498-a012-8e6b05c8c6c01)"
                            strokeMiterlimit={10}
                            strokeWidth={28.125}
                            strokeDashoffset={this.strokeDashoffset3}
                            strokeDasharray={[this.length3, this.length3]}
                            d={d3}
                        />
                        <AnimatedPath fill="none"
                            stroke="url(#0629d1b3-c525-43f9-b7a3-ad319f4252fc1)"
                            strokeMiterlimit={10}
                            strokeWidth={19.6875}
                            strokeDashoffset={this.strokeDashoffset3}
                            strokeDasharray={[this.length3, this.length3]}
                            d={d3}
                        />
                    </Svg>
                );
        }

    }
    getActualAnimationLength(animationLength) {
        switch (animationLength) {
            case '1e1': return oneEvOne;
            case '1e2': return oneEvTwo;
            case '1e3': return oneEvThree;
            case '2e1': return twoEvOne;
            case '2e2': return twoEvTwo;
            case '2e3': return twoEvThree;
            case '3e1': return threeEvOne;
            case '3e2': return threeEvTwo;
            case '3e3': return threeEvThree;
            default: return 150;
        }
    }
    getAnimatedEvent(events, eventToAnimate) {
        for (let i = 0; i < events.length; i++) {
            if (events[i].uid === eventToAnimate) {
                return events[i];
            }
        }
    }
    render() {
        const { svgSegmentId, svgStyleId, name, uid, animationPending, eventToAnimate, year, events } = this.props.item;
        // let eventsArr = _.map(events, (val, uid) => {
        //     return { ...val, uid };
        // });
        //console.log(eventsArr);
        let eventsArr = this.props.events
        let topThreeEvents = []
        //eventsArr = _.orderBy(eventsArr, ['eventPriority'], ['desc']);
        if (eventsArr.length > 3) {
            for (let ind = 0; ind <= 2; ind++) {
                topThreeEvents.push(eventsArr[ind]);
            }
        }

        if (_.has(this.props.item, 'animationlength') && _.has(this.props.item, 'eventToAnimate')) {
            let animationlength = this.props.item.animationlength;
            let al = this.getActualAnimationLength(animationlength)
            this.animLength = al;

            this.animatedEvent = this.getAnimatedEvent(eventsArr, eventToAnimate);

        }


        return (
            <View>
                {eventsArr.length > 3 ?
                    this.renderSection(svgSegmentId, svgStyleId, name, topThreeEvents, animationPending)
                    : this.renderSection(svgSegmentId, svgStyleId, name, eventsArr, animationPending)
                }

                {this.animate(this.animLength, animationPending, eventToAnimate, svgSegmentId, uid, year, this.animatedEvent, this.props.item)}
            </View>
        )
    }
}


export default connect(null, { updateEventAndTimelineAfterAnimation })(helperSvgPoints)
