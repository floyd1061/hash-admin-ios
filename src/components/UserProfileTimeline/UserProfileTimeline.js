import React, { Component } from 'react'
import { View, Text, TextInput, Image, TouchableOpacity, StyleSheet } from 'react-native'
import { LinearGradient } from 'expo'
import Slider from 'react-native-slider'

const messageCount = 10;
const notificationCount = 10;
const likeCount = 45;

export default class HeaderNavBar extends Component {

    constructor() {
        super();
        this.state = {

        }
    }

    render() {

        return (

            <View style={styles.Container} >

                <LinearGradient style={styles.HeaderSection} colors={['#FF1900', '#FF3200']}>

                    <View style={styles.rowsec}>

                        <Text style={styles.headerText1} > Hashtazapp </Text>

                        <TouchableOpacity style={styles.iconstyle}>
                            <Image source={require('./assets/avater.png')} style={styles.avatericon} />
                        </TouchableOpacity>

                        <View style={styles.headeritem} >

                            <TouchableOpacity onPress={this.message}>
                                <View style={styles.messagearea}>
                                    <Image source={require('./assets/message.png')} style={styles.messageicon} />
                                    <Text style={styles.messageCount}>{messageCount}</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity>
                                <Image source={require('./assets/search.png')} style={styles.searchicon} />
                            </TouchableOpacity>

                            <TouchableOpacity>
                                <Image source={require('./assets/followersactivityicon.png')} style={styles.followersnumber} />
                            </TouchableOpacity>

                        </View>

                        <View style={styles.textstyle}>
                            <Text style={styles.textstyle1}>Can’t Wait For Bungee Jumping!! {'\n'}</Text>

                            <Text style={styles.textstyle2}>I’m A Proffesional Chess Player {'\n'}</Text>

                            <Text style={styles.textstyle3}>www.chessmaster.com</Text>
                        </View>

                    </View>

                    {/* <TouchableOpacity onPress={() => {this.setState({showHeaderOption: !this.state.showHeaderOption})}}> */}
                    {/* <View style = {styles.notificationArea}>
                            <Text style = {styles.notificationCount}>{notificationCount}</Text>
                        </View> */}
                    {/* </TouchableOpacity> */}

                    {/* <View style = {styles.notificationstyle}>
                    
                    </View> */}

                </LinearGradient>

                <View style={styles.bodySection}>

                    {/* --------------------------------------------------------------------------------------


                    TimeLine Design Set & Style


                    -------------------------------------------------------------------------------------- */}

                    {/* HappyNewYear Image Design */}
                    <Image source={require('./assets/happynewyear.png')} style={styles.happynewyearicon} />

                    {/* Instant Live Share Design */}
                    <TouchableOpacity style={styles.instantliveshareStyle}>
                        <Image source={require('./assets/worldshare.png')} style={styles.worldshareicon} />
                    </TouchableOpacity>

                    <Text style={{ fontSize: 17, fontWeight: 'bold', color: '#000', left: 230, top: 20 }}> Jan 1st </Text>
                    <Text style={styles.dateCount1}> ? </Text>

                    <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#000', left: 15, top: 100 }}> 2015 </Text>

                    <View style={{ left: 240, top: -5, position: 'relative' }}>
                        <View style={[styles.talkBubbleSquare, { backgroundColor: '#ff0000' }]} >
                            <Text style={{ textAlign: 'center', color: '#fff', fontWeight: 'bold', fontSize: 12 }}>John's Party!</Text>
                            <Text style={{ textAlign: 'center', fontSize: 8, color: '#fff' }}>12pm - 3pm</Text>
                        </View>
                        <View style={styles.boxTriangle} />
                        <View style={[styles.talkBubbleTriangle, { borderRightColor: '#ff0000' }]} />
                    </View>
                    <Image source={require('./assets/DeleteEvent.png')} style={{ position: 'absolute', top: 118, right: 82, height: 16, width: 16, resizeMode: 'stretch' }} />
                    <Image source={require('./assets/lock1.png')} style={{ position: 'absolute', top: 105, right: 62, height: 22, width: 18, resizeMode: 'stretch' }} />


                    <Image source={require('./assets/transcriptdropdown1.png')} style={{ position: 'absolute', top: 200, right: 120, height: 7, width: 14, resizeMode: 'stretch' }} />


                    <View style={{ padding: 3, flexDirection: 'row', height: 40, width: 160, backgroundColor: '#EBEBEB', borderWidth: 1, borderColor: '#B2AFA4', borderRadius: 3, left: 210 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ textAlign: 'center', fontSize: 10, fontWeight: 'bold', color: '#494949', marginTop: 2, marginRight: 2 }}> 1000 {'\n'} Invited</Text>
                            <Text style={{ textAlign: 'center', fontSize: 10, fontWeight: 'bold', color: '#494949', marginTop: 2, marginRight: 2 }}> 634 {'\n'} Attended</Text>
                            <Text style={{ textAlign: 'center', fontSize: 10, fontWeight: 'bold', color: '#494949', marginTop: 2, marginRight: 2 }}> 400 {'\n'} Liked</Text>
                            <Text style={{ textAlign: 'center', fontSize: 10, fontWeight: 'bold', color: '#494949', marginTop: 2 }}> 43 {'\n'} Shares</Text>

                        </View>
                    </View>


                    <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#000', left: 220, top: 38 }}> Jan 2nd </Text>
                    <Text style={styles.dateCount2}> ? </Text>


                    <View style={{ left: 80, top: 20, position: 'relative' }}>
                        <View style={[styles.talkBubbleSquare1, { backgroundColor: '#8F8F8F' }]} >
                            <Text style={{ textAlign: 'center', color: '#fff', fontWeight: 'bold', fontSize: 12 }}>Tim's BBQ</Text>
                            <Text style={{ textAlign: 'center', fontSize: 8, color: '#fff' }}>4pm - 7pm</Text>
                        </View>
                        <View style={styles.boxTriangle1} />
                        <View style={[styles.talkBubbleTriangle1, { borderRightColor: '#8F8F8F' }]} />
                    </View>

                    <Image source={require('./assets/timer.png')} style={{ position: 'absolute', top: 285, right: 335, height: 22, width: 22, resizeMode: 'stretch' }} />
                    <Image source={require('./assets/lock.png')} style={{ position: 'absolute', top: 310, right: 338, height: 26, width: 20, resizeMode: 'stretch' }} />
                    <Image source={require('./assets/camera.png')} style={{ position: 'absolute', top: 320, right: 360, height: 22, width: 22, resizeMode: 'stretch' }} />
                    <Image source={require('./assets/mike.png')} style={{ position: 'absolute', top: 340, right: 338, height: 22, width: 22, resizeMode: 'stretch' }} />


                    {/* SideBar Design */}
                    {/* <LinearGradient style={styles.leftmenu} colors={['#FF0209', '#FF2900']}>
                        <TouchableOpacity style={styles.leftmenuItem}>
                            <Image source={require('./assets/myprofile.png')} style={[styles.leftmenuIcon, { width: 55, height: 60 }]} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.leftmenuItem}>
                            <Image source={require('./assets/editprofile.png')} style={[styles.leftmenuIcon, { width: 60, height: 55 }]} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.leftmenuItem}>
                            <Image source={require('./assets/notifications.png')} style={[styles.leftmenuIcon, { width: 70, height: 50 }]} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.leftmenuItem}>
                            <Image source={require('./assets/activity.png')} style={[styles.leftmenuIcon, { width: 50, height: 55 }]} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.leftmenuItem}>
                            <Image source={require('./assets/settings.png')} style={[styles.leftmenuIcon, { width: 50, height: 55 }]} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.leftmenuItem}>
                            <Image source={require('./assets/logout.png')} style={[styles.leftmenuIcon, { width: 45, height: 55 }]} />
                        </TouchableOpacity>
                    </LinearGradient> */}

                    {/* Event Create Design */}
                    {/* <LinearGradient style = {styles.eventCreate} colors = { ['#F0F0F0', '#E4E4E4'] }>

                        <LinearGradient style = {styles.headerTitle} colors = { ['#EC0900', '#FF0D05'] }>
                            <Text style = {styles.headerTextTitle}> Add Event Title </Text>

                            <TouchableOpacity style = {styles.timeSelectorStart}>
                                <Text style = {styles.timeSelectorStartText}>Starts 12:00PM </Text>
                            </TouchableOpacity>

                            <TouchableOpacity style = {styles.timeSelectorEnd}>
                                <Text style = {styles.timeSelectorEndText}>Ends ??:??AM </Text>
                            </TouchableOpacity>
                        </LinearGradient>

                        <View style = {styles.eventBody}>
                            <Text style = {styles.eventBodyText}> Update your event name... </Text>

                            <Slider 
                                step= {1}
                                thumbStyle = {{borderWidth: 3, borderColor: '#FFFFFF'}}
                                trackStyle = {{ height: 15, borderWidth: 3, borderColor: '#726358', borderRadius: 10 }}
                                minimumTrackTintColor= '#F50D00'
                                maximumTrackTintColor= '#5C534F'
                                thumbTintColor= '#FD1700'
                                maximumValue= '100'
                                style= {{ width: 135, marginLeft: 10, marginTop: 6 }}
                                value={this.state.value}
                                onValueChange={value => this.setState({ value })}
                            />
                        </View>

                        <View style = {styles.textInputStyle}>
                            <TextInput
                                style={{ flex: 1 }}
                                placeholder="Enter event name"
                                underlineColorAndroid="transparent"
                                style = {{height: 30, width: 250, borderRadius: 2, borderWidth: 2, borderColor: '#E90600', marginLeft: 8, paddingLeft: 10}}   
                            />
                        </View>

                        <View style = {styles.buttonStyle}>
                            <TouchableOpacity>
                                <Text style = {styles.button1Style}> Cancel </Text>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Text style = {styles.button2Style}> OK </Text>
                            </TouchableOpacity>
                        </View>

                    </LinearGradient> */}

                    {/* Invite Event Create Design */}
                    {/* <LinearGradient style = {styles.inviteEventCreate} colors = { ['#F0F0F0', '#E4E4E4'] }>

                        <LinearGradient style = {styles.inviteEventheaderTitle} colors = { ['#EC0900', '#FF0D05'] }>
                            <Text style = {styles.inviteEventheaderTextTitle}> Invite Friends to the Event </Text>
                            <TouchableOpacity>
                                <Image source={require('./assets/fb.png')} style={{width: 25, height: 25, resizeMode: 'stretch', marginTop: 7, marginLeft: 15}}/>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Image source={require('./assets/twitter.png')} style={{width: 25, height: 25, resizeMode: 'stretch', marginTop: 7, marginLeft: 5}}/>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Image source={require('./assets/instagram.png')} style={{width: 25, height: 25, resizeMode: 'stretch', marginTop: 7, marginLeft: 5}}/>
                            </TouchableOpacity>
                        </LinearGradient>


                        <View style = {styles.inviteEventBody}>

                            <View style = {styles.inviteEventBody1}>
                                <TouchableOpacity style = {styles.inviteEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 55, height: 55, resizeMode: 'stretch'}}/>
                                    <Image source={require('./assets/HostAdd.png')} style={{position: 'absolute' ,width: 25, height: 25, resizeMode: 'stretch', top: 5, right: 3}}/>
                                    <Text style = {{fontSize: 8, fontWeight: 'bold', color: '#fff', color: '#9F9F9F', textAlign: 'center'}}> John Smith </Text>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.inviteEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 55, height: 55, resizeMode: 'stretch'}}/>
                                    <Image source={require('./assets/HostAdd.png')} style={{position: 'absolute' ,width: 25, height: 25, resizeMode: 'stretch', top: 5, right: 3}}/>
                                    <Text style = {{fontSize: 8, fontWeight: 'bold', color: '#fff', color: '#9F9F9F', textAlign: 'center'}}> Heyley Burn </Text>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.inviteEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 55, height: 55, resizeMode: 'stretch'}}/>
                                    <Image source={require('./assets/HostAdd.png')} style={{position: 'absolute' ,width: 25, height: 25, resizeMode: 'stretch', top: 5, right: 3}}/>
                                    <Text style = {{fontSize: 8, fontWeight: 'bold', color: '#fff', color: '#9F9F9F', textAlign: 'center'}}> Joey Rice </Text>
                                </TouchableOpacity>
                            </View>

                            <View style = {styles.inviteEventBody1}>
                                <TouchableOpacity style = {styles.inviteEventUser}>
                                    <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 55, height: 55, resizeMode: 'stretch'}}/>
                                    <Image source={require('./assets/HostAdd1.png')} style={{position: 'absolute' ,width: 25, height: 25, resizeMode: 'stretch', top: 5, right: 3}}/>
                                    <Text style = {{fontSize: 8, fontWeight: 'bold', color: '#fff', color: '#9F9F9F', textAlign: 'center'}}> David Corn </Text>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.inviteEventUser}>
                                    <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 55, height: 55, resizeMode: 'stretch'}}/>
                                    <Image source={require('./assets/HostAdd1.png')} style={{position: 'absolute' ,width: 25, height: 25, resizeMode: 'stretch', top: 5, right: 3}}/>
                                    <Text style = {{fontSize: 8, fontWeight: 'bold', color: '#fff', color: '#9F9F9F', textAlign: 'center'}}> Stephen Cain </Text>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.inviteEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 55, height: 55, resizeMode: 'stretch'}}/>
                                    <Image source={require('./assets/HostAdd.png')} style={{position: 'absolute' ,width: 25, height: 25, resizeMode: 'stretch', top: 5, right: 3}}/>
                                    <Text style = {{fontSize: 8, fontWeight: 'bold', color: '#fff', color: '#9F9F9F', textAlign: 'center'}}> Joey Rice </Text>
                                </TouchableOpacity>
                            </View>

                        </View>

                        <View style = {styles.buttonStyle}>
                            <TouchableOpacity>
                                <Text style = {styles.button1Style}> Send Invitation </Text>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Text style = {styles.button2Style}> Cancel </Text>
                            </TouchableOpacity>
                        </View>

                    </LinearGradient> */}

                    {/* MayBe Event Create Design */}
                    {/* <LinearGradient style = {styles.maybeEventCreate} colors = { ['#F0F0F0', '#E4E4E4'] }>

                        <LinearGradient style = {styles.maybeEventheaderTitle} colors = { ['#EC0900', '#FF0D05'] }>
                            <Text style = {styles.maybeEventheaderTextTitle}> Maybe </Text>
                        </LinearGradient>

                        <View style = {styles.maybeEventBody}>

                            <View style = {styles.maybeEventBody1}>
                                <TouchableOpacity style = {styles.maybeEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.maybeEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.maybeEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.maybeEventUser}>
                                    <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.maybeEventUser}>
                                    <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                            </View>

                            <View style = {styles.maybeEventBody1}>
                                <TouchableOpacity style = {styles.maybeEventUser}>
                                    <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.maybeEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.maybeEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.maybeEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.maybeEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                            </View>

                            <View style = {styles.maybeEventBody1}>
                                <TouchableOpacity style = {styles.maybeEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.maybeEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.maybeEventUser}>
                                    <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.maybeEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.maybeEventUser}>
                                    <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                            </View>

                        </View>


                    </LinearGradient> */}

                    {/* Share Event Create Design */}
                    {/* <LinearGradient style = {styles.shareEventCreate} colors = { ['#F0F0F0', '#E4E4E4'] }>

                        <LinearGradient style = {styles.shareEventheaderTitle} colors = { ['#EC0900', '#FF0D05'] }>
                            <Text style = {styles.shareEventheaderTextTitle}> Shares </Text>
                        </LinearGradient>

                        <View style = {styles.shareEventBody}>

                            <View style = {styles.shareEventBody1}>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                            </View>

                            <View style = {styles.shareEventBody1}>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                            </View>

                            <View style = {styles.shareEventBody1}>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                            </View>

                        </View>

                    </LinearGradient> */}

                    {/* invite Event Day Create Design */}
                    {/* <LinearGradient style = {styles.shareEventCreate} colors = { ['#F0F0F0', '#E4E4E4'] }>

                        <LinearGradient style = {styles.shareEventDayheaderTitle} colors = { ['#EC0900', '#FF0D05'] }>
                            <Text style = {styles.shareEventDayheaderTextTitle}> Share Event Day </Text>

                             <TouchableOpacity>
                                <Image source={require('./assets/favouriteicon.png')} style={{width: 18, height: 18, resizeMode: 'stretch', marginTop: 10, marginLeft: 5}}/>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.like} style = {styles.likearea}>
                                <Image source={require('./assets/likeicon.png')} style={{width: 18, height: 18, resizeMode: 'stretch', marginTop: 10, marginLeft: 5, marginRight: 10}}/>
                                <Text style = {styles.likeCount}>{likeCount}</Text>
                            </TouchableOpacity>

                            <TouchableOpacity>
                                <Image source={require('./assets/fb.png')} style={{width: 25, height: 25, resizeMode: 'stretch', marginTop: 7, marginLeft: 20}}/>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Image source={require('./assets/twitter.png')} style={{width: 25, height: 25, resizeMode: 'stretch', marginTop: 7, marginLeft: 5}}/>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Image source={require('./assets/instagram.png')} style={{width: 25, height: 25, resizeMode: 'stretch', marginTop: 7, marginLeft: 5}}/>
                            </TouchableOpacity>
                        </LinearGradient>

                        <View style = {styles.shareEventBody}>

                            <View style = {styles.shareEventBody1}>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                            </View>

                            <View style = {styles.shareEventBody1}>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                            </View>

                            <View style = {styles.shareEventBody1}>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.shareEventUser}>
                                    <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                            </View>

                        </View>

                        <View style = {styles.buttonStyle}>
                            <TouchableOpacity>
                                <Text style = {styles.button1Style}> Send Event </Text>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Text style = {styles.button2Style}> Cancel </Text>
                            </TouchableOpacity>
                        </View>

                    </LinearGradient> */}



                </View>

                <LinearGradient style={styles.bottomSection} colors={['#2B2B2B', '#272727']}>
                    <TouchableOpacity style={styles.bottomItem}>
                        <Image source={require('./assets/home.png')} style={styles.bottomIcon} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.bottomItem}>
                        <Image source={require('./assets/mostpopulartimeline.png')} style={[styles.bottomIcon, { width: 45 }]} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.bottomItem}>
                        <Image source={require('./assets/mostlikedevent.png')} style={styles.bottomIcon} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.bottomItem}>
                        <Image source={require('./assets/populareventbox.png')} style={[styles.bottomIcon, { width: 45, height: 35 }]} />
                    </TouchableOpacity>
                </LinearGradient>



            </View>
        )
    }
}

const styles = StyleSheet.create({

    Container: {
        flex: 1,
        backgroundColor: '#fff'
    },

    notificationstyle: {
        height: 140,
        width: 80,
        backgroundColor: '#fff',
        flexDirection: 'column',
        top: 35,
        left: 335,
        borderRadius: 3,
        position: 'absolute',
    },

    // --------- Header Section Style ----------------

    HeaderSection: {
        backgroundColor: '#F40C00',
        height: 150,
        paddingLeft: 20,
        paddingRight: 20,
        // position: 'relative',
    },

    rowsec: {
        flexDirection: 'row',
        padding: 10,
        top: 30,
        // backgroundColor: '#000',   
    },

    headerText1: {
        fontSize: 18,
        color: '#fff',
        fontWeight: 'bold',
        // backgroundColor: '#fff'
    },

    headeritem: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        bottom: 40,
        // backgroundColor: '#000',
    },

    messagearea: {
        position: 'relative',
        marginTop: 10
    },

    messageicon: {
        height: 30,
        width: 30,
        resizeMode: 'stretch',
        marginRight: 15,
    },

    messageCount: {
        position: 'absolute',
        top: -8,
        color: '#fff',
        backgroundColor: '#EB1406',
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#fff',
        width: 15,
        height: 15,
        fontSize: 7,
        paddingTop: 2,
        textAlign: 'center',
        left: 21,
    },

    searchicon: {
        height: 30,
        width: 30,
        resizeMode: 'stretch',
        marginRight: 15
    },

    followersnumber: {
        height: 30,
        width: 30,
        resizeMode: 'stretch',
    },

    textstyle: {
        top: 35,
        right: 120,
        // backgroundColor: '#000'
    },

    textstyle1: {
        fontSize: 8,
        color: '#fff',
    },

    textstyle2: {
        // top: 30,
        fontSize: 8,
        color: '#282828',
    },

    textstyle3: {
        fontSize: 8,
        color: '#282828'
    },

    iconstyle: {
        top: 30,
        marginLeft: 15,
        marginRight: 25,
        // backgroundColor: '#fff'
    },

    avatericon: {
        height: 70,
        width: 70,
        resizeMode: 'stretch',
    },

    notificationArea: {
        position: 'relative',
    },

    notificationCount: {
        position: 'absolute',
        right: 30,
        top: 25,
        color: '#fff',
        backgroundColor: '#FF2400',
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#fff',
        width: 20,
        height: 20,
        fontSize: 10,
        paddingTop: 2,
        textAlign: 'center',
    },

    // headerOptionArea: {
    //     width:200, 
    //     height: 150, 
    //     backgroundColor: '#fff', 
    //     position: 'absolute',
    //     right: 22,
    //     top: 120,
    //     borderColor: '#FD0607',
    //     borderWidth: 2,
    //     borderRadius: 3,
    //     borderTopWidth: 0,
    //     alignItems: 'center',

    // },



    // -------------------------------------------------

    // ------------ Body Section Style ------------------

    bodySection: {
        position: 'relative',
        // height: 465,
        flex: 1,
        backgroundColor: '#DEDEDE',
    },

    leftmenu: {
        flexDirection: 'column',
        flex: 1,
        width: 104,
        height: 464,
        borderWidth: 2,
        borderColor: '#fff',
        position: 'absolute'
    },

    leftmenuItem: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: '#fff'
    },

    leftmenuIcon: {
        width: 50,
        height: 50,
        resizeMode: 'stretch',
    },

    happynewyearicon: {
        width: 75,
        height: 65,
        resizeMode: 'stretch',
        top: 20,
        left: 105,
    },

    instantliveshareStyle: {
        height: 60,
        width: 70,
        position: 'absolute',
        top: 20,
        left: 350,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FF0C05',
        borderWidth: 2,
        borderColor: '#FF0C05',
        borderRadius: 10
    },

    worldshareicon: {
        height: 40,
        width: 30,
        resizeMode: 'stretch',
    },

    dateCount1: {
        position: 'absolute',
        right: 95,
        top: 87,
        color: '#fff',
        backgroundColor: '#8F8F8F',
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#fff',
        width: 20,
        height: 20,
        fontSize: 12,
        fontWeight: 'bold',
        paddingTop: 2,
        textAlign: 'center',
    },

    dateCount2: {
        position: 'absolute',
        right: 112,
        top: 290,
        color: '#fff',
        backgroundColor: '#FD1400',
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#fff',
        width: 20,
        height: 20,
        fontSize: 12,
        fontWeight: 'bold',
        paddingTop: 2,
        textAlign: 'center',
    },

    //Event Create Design
    talkBubbleSquare: {
        width: 83,
        height: 70,
        backgroundColor: '#565656',
        borderRadius: 10,
        borderWidth: 4,
        borderColor: '#fff',
        justifyContent: 'center',
        top: 8
    },

    boxTriangle: {
        position: 'relative',
        left: -16,
        bottom: 37,
        width: 0,
        height: 0,
        borderTopColor: 'transparent',
        borderTopWidth: 10,
        borderRightWidth: 20,
        borderRightColor: '#fff',
        borderBottomWidth: 10,
        borderBottomColor: 'transparent',
    },

    talkBubbleTriangle: {
        position: 'absolute',
        left: -8,
        top: 34,
        width: 0,
        height: 0,
        borderTopColor: 'transparent',
        borderTopWidth: 9,
        borderRightWidth: 16,
        borderRightColor: '#565656',
        borderBottomWidth: 9,
        borderBottomColor: 'transparent',
    },

    talkBubbleSquare1: {
        width: 83,
        height: 70,
        backgroundColor: '#565656',
        borderRadius: 10,
        borderWidth: 4,
        borderColor: '#fff',
        justifyContent: 'center',
        top: 8
    },

    boxTriangle1: {
        position: 'relative',
        left: 83,
        bottom: 37,
        width: 0,
        height: 0,
        borderTopColor: 'transparent',
        borderTopWidth: 10,
        borderLeftWidth: 20,
        borderLeftColor: '#fff',
        borderBottomWidth: 10,
        borderBottomColor: 'transparent',
    },

    talkBubbleTriangle1: {
        position: 'absolute',
        left: 79,
        top: 34,
        width: 0,
        height: 0,
        borderTopColor: 'transparent',
        borderTopWidth: 9,
        borderLeftWidth: 16,
        borderLeftColor: '#8F8F8F',
        borderBottomWidth: 9,
        borderBottomColor: 'transparent',
    },

    eventCreate: {
        flex: 1,
        height: 150,
        width: 270,
        position: 'absolute',
        // backgroundColor: '#F1F1F1',
        left: 110,
        top: 100,
        borderWidth: 1,
        borderColor: '#B7B7B7',
        borderRadius: 5
    },

    headerTitle: {
        flexDirection: 'row',
        height: 35,
        width: 268,
        borderRadius: 3
    },

    headerTextTitle: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#fff',
        marginLeft: 5,
        marginTop: 8
    },

    timeSelectorStart: {
        height: 25,
        width: 72,
        backgroundColor: '#fff',
        borderRadius: 2,
        marginTop: 5,
        marginLeft: 15
    },

    timeSelectorStartText: {
        fontSize: 9,
        fontWeight: 'bold',
        color: '#FF1105',
        textAlign: 'center',
        marginTop: 5
    },

    timeSelectorEnd: {
        height: 25,
        width: 72,
        backgroundColor: '#fff',
        borderRadius: 2,
        marginTop: 5,
        marginLeft: 10
    },

    timeSelectorEndText: {
        fontSize: 9,
        fontWeight: 'bold',
        color: '#FF1105',
        textAlign: 'center',
        marginTop: 5
    },

    eventBody: {
        flexDirection: 'row',
    },

    eventBodyText: {
        fontSize: 9,
        fontWeight: 'bold',
        color: '#544D48',
        marginLeft: 3,
        marginTop: 20
    },

    textInputStyle: {

    },

    buttonStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        // backgroundColor: '#000',    
    },

    button1Style: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#F30F00',
        paddingVertical: 5,
        marginRight: 5
    },

    button2Style: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#F30F00',
        paddingVertical: 5,
        marginRight: 10
    },

    inviteEventCreate: {
        height: 300,
        width: 270,
        position: 'absolute',
        // backgroundColor: '#F1F1F1',
        left: 110,
        top: 70,
        borderWidth: 1,
        borderColor: '#B7B7B7',
        borderRadius: 5
    },

    inviteEventheaderTitle: {
        flexDirection: 'row',
        height: 40,
        width: 268,
        borderRadius: 3
    },

    inviteEventheaderTextTitle: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#fff',
        marginLeft: 5,
        marginTop: 11
    },

    inviteEventBody: {
        height: 225,
        width: 270,
        padding: 5,
        // backgroundColor: '#ccc'
    },

    inviteEventBody1: {
        flexDirection: 'row',
        padding: 5,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: '#000'
    },

    inviteEventUser: {
        height: 90,
        width: 75,
        // backgroundColor: '#ccc',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 5
    },

    maybeEventCreate: {
        height: 270,
        width: 270,
        position: 'absolute',
        // backgroundColor: '#F1F1F1',
        left: 110,
        top: 70,
        borderWidth: 1,
        borderColor: '#B7B7B7',
        borderRadius: 5
    },

    maybeEventheaderTitle: {
        flexDirection: 'row',
        height: 40,
        width: 268,
        borderRadius: 3
    },

    maybeEventheaderTextTitle: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#fff',
        marginLeft: 5,
        marginTop: 10
    },

    maybeEventBody: {
        height: 210,
        width: 270,
        padding: 5,
        // backgroundColor: '#ccc'
    },

    maybeEventBody1: {
        flexDirection: 'row',
        padding: 3,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: '#000'
    },

    maybeEventUser: {
        height: 50,
        width: 50,
        // backgroundColor: '#ccc',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 1
    },


    shareEventCreate: {
        height: 270,
        width: 270,
        position: 'absolute',
        // backgroundColor: '#F1F1F1',
        left: 110,
        top: 70,
        borderWidth: 1,
        borderColor: '#B7B7B7',
        borderRadius: 5
    },

    shareEventheaderTitle: {
        flexDirection: 'row',
        height: 40,
        width: 268,
        borderRadius: 3
    },

    shareEventheaderTextTitle: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#fff',
        marginLeft: 5,
        marginTop: 10
    },

    shareEventBody: {
        height: 195,
        width: 270,
        padding: 5,
        // backgroundColor: '#ccc'
    },

    shareEventBody1: {
        flexDirection: 'row',
        padding: 3,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: '#000'
    },

    shareEventUser: {
        height: 50,
        width: 50,
        // backgroundColor: '#ccc',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 1
    },

    shareEventDayheaderTitle: {
        flexDirection: 'row',
        height: 40,
        width: 268,
        borderRadius: 3
    },

    shareEventDayheaderTextTitle: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#fff',
        marginLeft: 5,
        marginTop: 11
    },

    likearea: {
        position: 'relative',
    },

    likeCount: {
        position: 'absolute',
        top: 8,
        color: '#fff',
        backgroundColor: '#EB1406',
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#fff',
        width: 13,
        height: 13,
        fontSize: 5,
        paddingTop: 3,
        textAlign: 'center',
        left: 18,
    },




    // ------------------------------------------------

    // -------- Bottom Footer Section Style ---------

    bottomSection: {
        flexDirection: 'row',
        height: 70,
    },

    bottomItem: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRightWidth: 1,
        borderColor: '#7E7E7E'
    },

    bottomIcon: {
        width: 40,
        height: 40,
        resizeMode: 'stretch',
    }

    // ------------------------------------------------   

});
