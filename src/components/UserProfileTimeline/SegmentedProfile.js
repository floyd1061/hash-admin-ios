import React, { Component } from 'react'
import _ from 'lodash';
import firebase from '@firebase/app'
import auth from '@firebase/auth'
import {
    Platform, FlatList, DrawerLayoutAndroid, Dimensions, TouchableOpacity, View, RefreshControl, Image
} from 'react-native'
import dt from 'date-and-time';
import { connect } from 'react-redux'
import uuid from 'uuid/v4'
import NavBar from './TimelineNav'
import ListItemSvg from './SegmentedListItem'
import BottomDrawer from './TimelineBDrawer'
import { Dropdown } from 'react-native-material-dropdown'
import { svgOneHeight, svgTwoHeight, svgThreeHeight } from './SvgPoints/SvgPoints'
import { Entypo, Ionicons } from '@expo/vector-icons';
import { navigationLeftView } from '../drawer/LeftMenu'
import { navigationNotificationView } from '../drawer/NotificationMenu'
import { fetchTimelineDataAfterLogin, selectYear } from '../../actions';
const { width, height } = Dimensions.get('window');
const thisYear = new Date(Date.now()).getFullYear();

export default class SegmentedProfile extends Component {
    constructor(props) {
        super(props);
        this.horizontal = false;
        this.calendarWidth = width;
        this.calendarHeight = 360;
        this.calendarHeightSectionOne = 278.139;
        this.calendarHeightSectionTwo = 281.488;
        this.calendarHeightSectionThree = 268.212;
        this.pastScrollRange = 50;
        this.futureScrollRange = 50;
        this.showScrollIndicator = false;
        this.scrollEnabled = true;
        this.scrollsToTop = false;
        this.removeClippedSubviews = Platform.OS === 'android' ? false : true;

    }
    state = {
        isModalVisible: false,
        data: [],
        selected: [],
        selectedItemId: '',
        openDate: null,
        tlData: null,
        btMenuButton: 72,
        bottomDrowerOpen: true,
        btDrawerButtonIcon: 'ios-arrow-down',

    }

    tConvert(time) {
        // Check correct time format and split into components
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
        //console.log(time);
        if (time.length > 1) { // If time format correct
            time = time.slice(1);  // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original string
    }

    getItemLayout(data, index) {
        return {
            length:
                280,
            offset:
                280 * index, index
        };
    }
    renderItem(item) {
        return <ListItemSvg item={item.item} key={item.item.uid} />
    }
    _keyExtractor = (item, index) => item.uid;

    getBottomMenuPosition() {
        return this.state.btMenuButton;
    }
    reRenderFlatList(selectedyear) {
        const { currentUser } = firebase.auth();


        this.props.selectYear(selectedyear.toString());
        this.props.fetchTimelineDataAfterLogin(selectedyear, currentUser.uid);

        if (thisYear.toString() === selectedyear) {
            this.flatlist.scrollToIndex({ animated: true, index: this.getDayIndex() });
        }


    }
    giveDataFortheday() {
        let timelineData = [];
        let numberOfSvgElements = Math.ceil(this.props.events.length / 3)
        //console.log(numberOfSvgElements);
        let startSlice = 0;
        let endSlice = 3;
        let svgSegmentId = 1;
        let svgStyleId = 1;
        for (let index = 0; index < numberOfSvgElements; index++) {
            let evT = [];
            evT = this.props.events.slice(startSlice, endSlice);
            //console.log(evT);
            var tld = {
                'uid': uuid().toString(),

                'events': evT,
                'hasEvent': evT.length > 0 ? true : false,
                'name': evT.length > 0 ? '' + this.tConvert(evT[0].startDate) + '-' + this.tConvert(evT[evT.length - 1].endDate)
                    : '',
                'svgSegmentId': svgSegmentId,
                'svgStyleId': svgStyleId,
                'unixUtc': this.props.item.unixUtc,
                'year': this.props.item.year

            }
            timelineData.push(tld);
            startSlice += 3;
            endSlice += 3;
            svgSegmentId += 1;
            svgStyleId += 1;

            if (svgSegmentId > 3) {
                svgSegmentId = 1;
            }
            if (svgStyleId > 6) {
                svgStyleId = 1;
            }

        }
        return timelineData;
    }


    render() {
        let timelineData = this.giveDataFortheday();
        //let timelineData =[]
        //console.log(this.props.events);
        return (
            <DrawerLayoutAndroid
                drawerWidth={103}
                drawerBackgroundColor='transparent'
                drawerPosition={DrawerLayoutAndroid.positions.Left}
                renderNavigationView={() => navigationLeftView}>
                <DrawerLayoutAndroid
                    drawerWidth={100}
                    drawerBackgroundColor='transparent'
                    statusBarBackgroundColor={null}
                    drawerPosition={DrawerLayoutAndroid.positions.Right}
                    renderNavigationView={() => navigationNotificationView}>
                    <NavBar />
                    <FlatList
                        ref={(ref) => this.flatlist = ref}
                        initialNumToRender={10}
                        data={timelineData}

                        removeClippedSubviews={this.removeClippedSubviews}
                        pageSize={1}

                        horizontal={false}
                        pagingEnabled={true}
                        renderItem={this.renderItem}
                        showsVerticalScrollIndicator={this.showScrollIndicator}
                        showsHorizontalScrollIndicator={this.showScrollIndicator}
                        scrollEnabled={this.scrollingEnabled}
                        keyExtractor={this._keyExtractor}
                        getItemLayout={this.getItemLayout}
                        scrollsToTop={this.scrollsToTop}
                    />


                    {/* <View style={{
                        position: 'absolute',
                        top: (height - 140) / 2,
                        left: 15,
                        zIndex: 20,
                        height: 100,
                        width: 60
                    }}>
                        <Dropdown
                            inputContainerStyle={{ borderBottomColor: 'transparent' }}
                            dropdownOffset={{ top: 3, left: 3 }}
                            dropdownPosition={0}
                            label={thisYear.toString()}
                            fontSize={17}
                            data={this.state.years}
                            onChangeText={(val) => { this.reRenderFlatList(val) }}
                        />
                    </View>
 */}
                    <TouchableOpacity onPress={() => {
                        this.setState({
                            bottomDrowerOpen: !this.state.bottomDrowerOpen
                        })
                    }} style={{
                        position: 'absolute',
                        bottom: this.state.bottomDrowerOpen ? 80 : 20,
                        right: 15,
                        zIndex: 20,
                        height: 25,
                        width: 50,
                        alignItems: 'center',
                        backgroundColor: 'transparent'
                    }}>
                        <Entypo name={this.state.bottomDrowerOpen ? 'chevron-down' : 'chevron-up'} size={35} color="#2B2B2B" />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {

                    }} style={{
                        position: 'absolute',

                        right: 0,
                        zIndex: 200,
                        //height: 60,
                        //width: 72,
                        top: 145,
                        alignItems: 'center',
                        backgroundColor: 'transparent'
                    }}>
                        <Image
                            source={require('./assets/Instant_live_share.png')}
                            style={{ width: 60, height: 50 }}
                        />
                    </TouchableOpacity>

                    {this.state.bottomDrowerOpen ? <BottomDrawer /> : null}

                </DrawerLayoutAndroid>
            </DrawerLayoutAndroid>
        )
    }
}

const styles = {
    Dropdownstyles1: {
        flex: 1,
        height: 100,
        backgroundColor: '#FFFFFF',
    },
}
