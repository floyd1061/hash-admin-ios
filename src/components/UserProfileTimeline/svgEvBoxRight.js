import React, { Component } from 'react'
import { Text, View } from 'react-native'
import Svg, {
    LinearGradient,
    Path,
    Defs,
    Stop
} from 'react-native-svg'

export default class svgEvBoxRight extends Component {
    render() {
        return (
            <View>
                <Svg
                    height={100}
                    width={100}
                    
                >
                    <Defs>

                    </Defs>

                    <Path stroke="red" strokeWidth={4} d="M254.48,203.94q-45.12,0-90.24,0L74,203.84c-11.3,0-20.3-3.31-26.77-9.8s-9.72-15.49-9.75-26.79v-5.79q0-4,0-8c0-5.43,0-9.84.07-13.89a2.52,2.52,0,0,0-.17-1.13A5.61,5.61,0,0,0,35.76,137c-4.15-3.06-7.94-6-11.57-8.87-4-3.18-7.77-6.36-11.26-9.46C10,116,6.1,111.88,5.77,105.8c-.4-7.27,4.7-12.14,6.38-13.74,3.5-3.35,7.26-6.68,11.19-9.89,3.74-3.06,7.71-6.09,11.8-9a7.09,7.09,0,0,0,2.06-2,3.87,3.87,0,0,0,.32-2.08c-.09-4.29-.14-8.94-.13-14.22,0-4.69,0-9.39.08-14.08.09-10.19,3.47-18.89,9.77-25.18S62.29,6,72.49,6q19,0,38.06,0l63.45,0v.11h42.94l37.56,0c10.82,0,19.9,3.38,26.27,9.73s9.75,15.43,9.76,26.23q0,31.5,0,63t0,63c0,10.79-3.38,19.85-9.78,26.22s-15.49,9.71-26.31,9.71Z"/>
                </Svg>
            </View>
        )
    }
}