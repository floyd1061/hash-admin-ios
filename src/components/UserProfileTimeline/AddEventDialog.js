import React, { Component } from "react";
import { Text, TouchableOpacity, View, TextInput, Image, StyleSheet } from "react-native";
import Modal from "react-native-modal";
import { LinearGradient } from 'expo'
import Slider from 'react-native-slider'
import DatePicker from 'react-native-datepicker';
import styles from './TimelineStyles'



export default class AddEventDialog extends Component {
    state = {
        isModalVisible: true,
        startingtime: '--:--',
        endingtime: '--:--',
    };

    sliderValueChange(value) {
        if (value >= 4) {
            this.setState({
                eventPriority: 'High'
            })
        }
        if (1 <= value && value <= 3) {
            this.setState({
                eventPriority: 'Medium'
            })
        }
        this.setState({
            eventPriorityValue: value
        })
        this.props.eventPriorityChanged(value);
    }
    onEventNameChanged(eventName) {
        this.props.eventNameChanged(eventName);
    }
    onchangeStartTime(st) {
        //console.log(st);
        //console.log(this.tConvert(st));

        this.setState({
            startTimeMills: this.timetoMill(st)
        })

        /* if (this.compareTime()) {
            st == '--:--';

        } */
        ////console.log(st);
        this.props.eventSTChanged(st);
    }
    timetoMill(time) {
        var time = time.toString();
        var timeParts = time.split(":");
        return ((+timeParts[0] * (60000 * 60)) + (+timeParts[1] * 60000));
    }
    onchangeEndTime(et) {
        /* if (this.compareTime()) {
            et == '--:--';
        } */
        this.setState({
            endTimeMills: this.timetoMill(et)
        })
        ////console.log(st);
        //console.log(this.compareEndTimetoStartTime());
        /* if (this.compareEndTimetoStartTime() == false) {
            et = '--:--';
        } */
        //console.log(et)
        this.props.eventETChanged(et);
    }

    compareEndTimetoStartTime() {
        if (this.state.startTimeMills >= this.state.endTimeMills) {
            this.setState({
                errorText: 'End time is smaller or you went to next date'
            })

            return false;
        }
        this.setState({
            errorText: ''
        })
        return true;

    }

    compareTime() {
        ////console.log(this.props.eventStartTime < this.props.eventEndTime);
        if (this.props.eventStartTime === '--.--' || this.props.eventEndTime === '--.--')
            return true;
        else if (this.props.eventStartTime < this.props.eventEndTime)
            return true;
        else
            return false;
    }
    tConvert(time) {
        // Check correct time format and split into components
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
        //console.log(time);
        if (time.length > 1) { // If time format correct
            time = time.slice(1);  // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original string
    }
    submitEventCreation() {
        if (this.validateEventProps(this.props)) {
            this.toggleModal(0)
            this.props.addEventToTimeline(this.props)
        }

    }
    renderAddEventModal() {
        return (
            <LinearGradient id='eventCreate' style={styles.eventCreate} colors={['#F0F0F0', '#E4E4E4']}>
                <LinearGradient id='eventCreateHeader' style={styles.headerTitle} colors={['#EC0900', '#FF0D05']}>
                    {
                        this.state.fontLoaded ? <Text style={styles.headerTextTitle}>Add Event</Text> : null
                    }

                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity style={styles.timeSelectorStart} onPress={() => {
                            this.refs.datepickerEventStart.onPressDate();
                        }}>
                            {
                                this.state.fontLoaded ? <Text style={styles.timeSelectorStartText}>Starts {this.tConvert(this.props.eventStartTime)}</Text> : null
                            }
                            <DatePicker
                                ref="datepickerEventStart"
                                mode="time"
                                format="HH:mm"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                is24Hour={false}
                                minuteInterval={10}
                                customStyles={{
                                    dateInput: {
                                        borderWidth: 0,
                                    }
                                }}
                                onDateChange={(startingtime) => { this.onchangeStartTime(startingtime) }}
                            //onDateChange={(startingtime) => { this.setState({ startingtime: startingtime }); }}
                            />
                        </TouchableOpacity >
                        <TouchableOpacity style={styles.timeSelectorEnd} onPress={() => {
                            this.refs.datepickerEventEnd.onPressDate()
                        }}>
                            {
                                this.state.fontLoaded ? <Text style={styles.timeSelectorEndText}>Ends {this.tConvert(this.props.eventEndTime)}</Text> : null
                            }
                            <DatePicker
                                ref="datepickerEventEnd"
                                mode="time"
                                format="HH:mm"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                is24Hour={false}
                                minuteInterval={10}
                                customStyles={{
                                    dateInput: {
                                        borderWidth: 0,
                                    }
                                }}
                                onDateChange={(endingtime) => { this.onchangeEndTime(endingtime) }}
                            //onDateChange={(endingtime) => { this.setState({ endingtime: endingtime }); }}

                            />
                        </TouchableOpacity>
                    </View>
                </LinearGradient>
                <View style={styles.eventBody}>
                    <View style={{ width: 100 }}>
                        {
                            this.state.fontLoaded ? <Text style={styles.eventBodyText}>{this.state.eventPriority} priority...</Text> : null
                        }
                    </View>
                    <Slider
                        step={1}
                        thumbStyle={{ borderWidth: 3, borderColor: '#FFFFFF' }}
                        trackStyle={{ height: 15, borderWidth: 3, borderColor: '#726358', borderRadius: 10 }}
                        minimumTrackTintColor='#F50D00'
                        maximumTrackTintColor='#5C534F'
                        thumbTintColor='#FD1700'
                        maximumValue={0}
                        maximumValue={5}
                        style={{ width: 145, marginTop: 5 }}
                        value={this.state.eventPriorityValue}
                        onValueChange={(value) => this.sliderValueChange(value)}
                    />
                </View>
                <View style={styles.textInputStyle}>
                    <TextInput
                        placeholder="Enter Event Name"
                        underlineColorAndroid="transparent"
                        onChangeText={this.onEventNameChanged.bind(this)}
                        style={{ height: 30, borderRadius: 2, borderWidth: 2, borderColor: '#E90600', marginLeft: 8, marginHorizontal: 10, paddingHorizontal: 10 }}
                    />
                </View>
                <View style={styles.bottomPanelContainer}>
                    <View style={styles.errorStyle}>
                        <Text style={styles.errorTextStyle}>{this.state.errorText}</Text>
                    </View>
                    <View style={styles.buttonStyle}>

                        <TouchableOpacity onPress={() => {

                            this.toggleModal(0);
                        }}>
                            {
                                this.state.fontLoaded ? <Text style={styles.button1Style}>Cancel</Text> : null
                            }
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.submitEventCreation.bind(this)}>
                            {
                                this.state.fontLoaded ? <Text style={styles.button2Style}>OK</Text> : null
                            }
                        </TouchableOpacity>
                    </View>
                </View>

            </LinearGradient>
        )
    }

    _toggleModal = () =>
        this.setState({ isModalVisible: !this.state.isModalVisible });

    render() {
        return (
            <View >
                <Modal ref='addEventModal' isVisible={true} style={styles.modalStyle}>
                    <View>
                        {this.renderAddEventModal()}
                    </View>
                </Modal>
            </View>
        );
    }
}