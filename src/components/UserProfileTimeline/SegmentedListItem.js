import React, { PureComponent } from 'react';
import firebase from '@firebase/app'
import "firebase/auth"
import "firebase/database"
import "firebase/storage"
import { Text, View, TextInput, TouchableOpacity, StyleSheet, Image, Animated, Easing } from 'react-native';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';
import { connect } from 'react-redux'
import Modal from "react-native-modal"
import Slider from 'react-native-slider'
import DatePicker from 'react-native-datepicker'
import { Entypo } from '@expo/vector-icons'
import { CardSection } from '../../components/common'
import { eventNameChanged, eventPriorityChanged, eventETChanged, eventSTChanged, addEventToTimeline, fetchTranscript } from '../../actions';
import SvgHelper from './helperSvgSegmented'
import TranscriptBoxTopRight from './TranscriptTopRight'
import TranscriptBoxTopLeft from './TransscriptTopLeft'
import TranscriptBoxBottomRight from './TranscriptBottomRight'
import styles from './TimelineStyles'

export default class SegmentedListItem extends PureComponent {
    

    state = {
        isModalVisible: false,
        startingtime: '--:--',
        endingtime: '--:--',
        eventDeleting: false,
        showTranscriptTR: false,
        showTranscriptTL: false,
        showTranscriptBR: false,
        renderAnimation: false,
        fontLoaded: true,
        eventPriority: 'low'
    }


    toggleModal = () => {
        this.props.eventSTChanged('--:--');
        this.props.eventETChanged('--:--');
        this.setState({ isModalVisible: !this.state.isModalVisible });
    }

    renderTimeLine(item, events) {
        return <SvgHelper item={item} ref={(svgHelper) => { this.svgHelper = svgHelper }} key={item.uid} events={events} />;
    }

    tConvert(time) {
        // Check correct time format and split into components
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
        //console.log(time);
        if (time.length > 1) { // If time format correct
            time = time.slice(1);  // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original string
    }

    doDetach(itemKey, year, eventKey) {
        firebase.database().ref(`/users/${currentUser.uid}/timelinedata/${year}/${itemKey}/events/${eventKey}`).update({
            "isDetached": true
        })
    }

    rendenEventBox(e, svgSegId) {
        //console.log(e.length)

        //console.log('grtr');
        let el = e.length;
        if (el > 0) {
            switch (el) {
                case 1:
                    return [
                        <CardSection key={e[0].uid} style={[(svgSegId === 1) ? styles.eventOneSegOne :
                            (svgSegId === 2) ? styles.eventOneSegTwo : styles.eventOneSegThree
                            , { marginVertical: 20 }]}>
                            <View style={{ position: 'relative' }}>


                                <TouchableOpacity
                                    onPress={() => {
                                        Actions.Event({
                                            event: e[0], eventKey: e[0].uid,
                                            year: this.props.item.year, itemKey: this.props.item.uid,
                                            animationLength: `${this.props.item.svgSegmentId}e1`
                                        })
                                    }}
                                    onLongPress={() => this.setState({ EventLongPressedOne: !this.state.EventLongPressedOne, eventDeletingOne: !this.state.eventDeletingOne })}
                                    style={{ width: 110, height: 80, justifyContent: 'center', alignItems: 'center' }}
                                >

                                    {e[0].isHighlighted ?
                                        <View key={"_animate" + e[0].uid} style={{ position: 'absolute' }}>
                                            <Image source={require('./assets/eventboxred24.png')} style={{ width: 90, height: 70, resizeMode: 'stretch', position: 'relative' }} />
                                        </View> :
                                        <View key={"_animate" + e[0].uid} style={{ position: 'absolute' }}>
                                            <Image source={require('./assets/eventboxgray24.png')} style={{ width: 90, height: 70, resizeMode: 'stretch', position: 'relative' }} />
                                        </View>
                                    }
                                    <View style={{ position: 'absolute', width: 70, alignItems: 'center', justifyContent: 'center', left: 15 }}>
                                        {
                                            this.state.fontLoaded ? <Text style={{ textAlign: 'center', color: '#fff', fontSize: 13, fontFamily: 'coolvetica' }}>{e[0].title}</Text> : null
                                        }
                                        {
                                            this.state.fontLoaded ? <Text style={{ textAlign: 'center', fontSize: 6, color: '#fff', fontFamily: 'coolvetica', paddingTop: 10 }}>{this.tConvert(e[0].startDate)}-{this.tConvert(e[0].endDate)}</Text> : null
                                        }
                                    </View>
                                </TouchableOpacity>

                                <TouchableOpacity style={{ position: 'absolute', left: 5 }}>
                                    {this.state.eventDeletingOne ?

                                        <Image source={require('./assets/DeleteEvent.png')} style={{ height: 16, width: 16, resizeMode: 'stretch' }} /> : null
                                    }

                                </TouchableOpacity>
                                {e[0].isHighlighted && this.state.EventLongPressedOne ?
                                    [<TouchableOpacity style={{ position: 'absolute', top: 10, left: -25 }}
                                    >
                                        <Image source={require('./assets/lock1.png')} style={{ height: 22, width: 18, resizeMode: 'stretch' }} />
                                    </TouchableOpacity>] :
                                    null

                                }

                                {this.state.EventLongPressedOne && !e[0].isHighlighted ?
                                    [
                                        <TouchableOpacity style={{ position: 'absolute', top: -5, left: -20 }} onPress={() => { this.concatAnimateLength("e1") }}>
                                            <Image source={require('./assets/timer.png')} style={{ height: 22, width: 22, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                        <TouchableOpacity style={{ position: 'absolute', top: 25, left: -20 }}>
                                            <Image source={require('./assets/lock.png')} style={{ height: 26, width: 20, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                        <TouchableOpacity style={{ position: 'absolute', top: 30, left: -45 }}>
                                            <Image source={require('./assets/camera.png')} style={{ height: 22, width: 22, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                        <TouchableOpacity style={{ position: 'absolute', top: 55, left: -20 }}>
                                            <Image source={require('./assets/mike.png')} style={{ height: 22, width: 22, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                    ] : null
                                }

                                <TouchableOpacity style={{ left: 45, paddingTop: 5 }} onPress={() => { this.setState({ showTranscriptTR: !this.state.showTranscriptTR }) }}>
                                    {
                                        e[0].isHighlighted ? <Image source={require('./assets/transcriptdropdown1.png')} style={{ height: 7, width: 14, resizeMode: 'stretch' }} /> :
                                            <Image source={require('./assets/transcriptdropdown.png')} style={{ height: 7, width: 14, resizeMode: 'stretch' }} />
                                    }

                                </TouchableOpacity>


                            </View>
                            {this.state.showTranscriptTR ?
                                <TranscriptBoxTopRight eventKey={e[0].uid} event={e[0]}/>
                                : null
                            }

                        </CardSection>
                    ]
                case 2:
                    return [
                        <CardSection key={e[0].uid} style={[(svgSegId === 1) ? styles.eventOneSegOne :
                            (svgSegId === 2) ? styles.eventOneSegTwo : styles.eventOneSegThree
                            , { marginVertical: 20 }]}>
                            <View style={{ position: 'relative' }}>
                                <TouchableOpacity
                                    onPress={() => {
                                        Actions.Event({
                                            event: e[0], eventKey: e[0].uid,
                                            year: this.props.item.year, itemKey: this.props.item.uid,
                                            animationLength: `${this.props.item.svgSegmentId}e1`
                                        })
                                    }}
                                    onLongPress={() => this.setState({ EventLongPressedOne: !this.state.EventLongPressedOne, eventDeletingOne: !this.state.eventDeletingOne })}
                                    style={{ width: 110, height: 80, justifyContent: 'center', alignItems: 'center' }}
                                >

                                    {e[0].isHighlighted ?
                                        <View key={"_animate" + e[0].uid} style={{ position: 'absolute' }}>
                                            <Image source={require('./assets/eventboxred24.png')} style={{ width: 90, height: 70, resizeMode: 'stretch', position: 'relative' }} />
                                        </View> :
                                        <View key={"_animate" + e[0].uid} style={{ position: 'absolute' }}>
                                            <Image source={require('./assets/eventboxgray24.png')} style={{ width: 90, height: 70, resizeMode: 'stretch', position: 'relative' }} />
                                        </View>
                                    }
                                    <View style={{ position: 'absolute', width: 70, alignItems: 'center', justifyContent: 'center', left: 15 }}>
                                        {
                                            this.state.fontLoaded ? <Text style={{ textAlign: 'center', color: '#fff', fontSize: 13, fontFamily: 'coolvetica' }}>{e[0].title}</Text> : null
                                        }
                                        {
                                            this.state.fontLoaded ? <Text style={{ textAlign: 'center', fontSize: 6, color: '#fff', fontFamily: 'coolvetica', paddingTop: 10 }}>{this.tConvert(e[0].startDate)}-{this.tConvert(e[0].endDate)}</Text> : null
                                        }
                                    </View>
                                </TouchableOpacity>

                                <TouchableOpacity style={{ position: 'absolute', left: 5 }}>
                                    {this.state.eventDeletingOne ?

                                        <Image source={require('./assets/DeleteEvent.png')} style={{ height: 16, width: 16, resizeMode: 'stretch' }} /> : null
                                    }

                                </TouchableOpacity>
                                {e[0].isHighlighted && this.state.EventLongPressedOne ?
                                    [<TouchableOpacity style={{ position: 'absolute', top: 10, left: -25 }}
                                    >
                                        <Image source={require('./assets/lock1.png')} style={{ height: 22, width: 18, resizeMode: 'stretch' }} />
                                    </TouchableOpacity>] :
                                    null

                                }

                                {this.state.EventLongPressedOne && !e[0].isHighlighted ?
                                    [
                                        <TouchableOpacity style={{ position: 'absolute', top: -5, left: -20 }} onPress={() => { this.concatAnimateLength("e1") }}>
                                            <Image source={require('./assets/timer.png')} style={{ height: 22, width: 22, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                        <TouchableOpacity style={{ position: 'absolute', top: 25, left: -20 }}>
                                            <Image source={require('./assets/lock.png')} style={{ height: 26, width: 20, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                        <TouchableOpacity style={{ position: 'absolute', top: 30, left: -45 }}>
                                            <Image source={require('./assets/camera.png')} style={{ height: 22, width: 22, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                        <TouchableOpacity style={{ position: 'absolute', top: 55, left: -20 }}>
                                            <Image source={require('./assets/mike.png')} style={{ height: 22, width: 22, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                    ] : null
                                }

                                <TouchableOpacity style={{ left: 45, paddingTop: 5 }} onPress={() => { this.setState({ showTranscriptTR: !this.state.showTranscriptTR }) }}>
                                    {
                                        e[0].isHighlighted ? <Image source={require('./assets/transcriptdropdown1.png')} style={{ height: 7, width: 14, resizeMode: 'stretch' }} /> :
                                            <Image source={require('./assets/transcriptdropdown.png')} style={{ height: 7, width: 14, resizeMode: 'stretch' }} />
                                    }

                                </TouchableOpacity>


                            </View>
                            {this.state.showTranscriptTR ?
                                <TranscriptBoxTopRight eventKey={e[0].uid} event={e[0]} />
                                : null
                            }

                        </CardSection>,

                        <CardSection key={e[1].uid} style={(svgSegId === 1) ? styles.eventTwoSegOne :
                            (svgSegId === 2) ? styles.eventTwoSegTwo : styles.eventTwoSegThree}>
                            <View style={{ position: 'relative' }}>

                                <TouchableOpacity
                                    onPress={() => {
                                        Actions.Event({
                                            event: e[1], eventKey: e[1].uid,
                                            year: this.props.item.year, itemKey: this.props.item.uid,
                                            animationLength: `${this.props.item.svgSegmentId}e2`
                                        })
                                    }}
                                    onLongPress={() => this.setState({ EventLongPressedTwo: !this.state.EventLongPressedTwo, eventDeletingTwo: !this.state.eventDeletingTwo })}

                                    style={{ width: 110, height: 80, justifyContent: 'center', alignItems: 'center' }}
                                >

                                    {e[1].isHighlighted ?
                                        <View key={"_animate" + e[1].uid} style={{ position: 'absolute' }}>
                                            <Image source={require('./assets/eventboxred13.png')} style={{ width: 90, height: 70, resizeMode: 'stretch', position: 'relative' }} />
                                        </View>
                                        :
                                        <View key={"_animate" + e[1].uid} style={{ position: 'absolute' }} >
                                            <Image source={require('./assets/eventboxgray13.png')} style={{ width: 90, height: 70, resizeMode: 'stretch', position: 'relative' }} />
                                        </View>

                                    }
                                    <View style={{ position: 'absolute', width: 72, right: 14 }}>
                                        {
                                            this.state.fontLoaded ? <Text style={{ textAlign: 'center', color: '#fff', fontSize: 13, fontFamily: 'coolvetica' }}>{e[1].title}</Text> : null
                                        }
                                        {
                                            this.state.fontLoaded ? <Text style={{ textAlign: 'center', fontSize: 6, color: '#fff', fontFamily: 'coolvetica', paddingTop: 10 }}>{this.tConvert(e[1].startDate)}-{this.tConvert(e[1].endDate)}</Text> : null
                                        }

                                    </View>
                                </TouchableOpacity>

                                <TouchableOpacity style={{ position: 'absolute', right: 5 }}>
                                    {this.state.eventDeletingTwo ?

                                        <Image source={require('./assets/DeleteEvent.png')} style={{ height: 16, width: 16, resizeMode: 'stretch' }} /> : null
                                    }

                                </TouchableOpacity>
                                {e[1].isHighlighted && this.state.EventLongPressedTwo ?
                                    [<TouchableOpacity style={{ position: 'absolute', top: 10, right: -25 }}>
                                        <Image source={require('./assets/lock1.png')} style={{ height: 22, width: 18, resizeMode: 'stretch' }} />
                                    </TouchableOpacity>] :
                                    null

                                }
                                {!e[1].isHighlighted && this.state.EventLongPressedTwo ?
                                    [
                                        <TouchableOpacity style={{ position: 'absolute', top: -5, right: -20 }}>
                                            <Image source={require('./assets/timer.png')} style={{ height: 22, width: 22, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                        <TouchableOpacity style={{ position: 'absolute', top: 25, right: -20 }}>
                                            <Image source={require('./assets/lock.png')} style={{ height: 26, width: 20, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                        <TouchableOpacity style={{ position: 'absolute', top: 30, right: -45 }}>
                                            <Image source={require('./assets/camera.png')} style={{ height: 22, width: 22, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                        <TouchableOpacity style={{ position: 'absolute', top: 55, right: -20 }}>
                                            <Image source={require('./assets/mike.png')} style={{ height: 22, width: 22, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,

                                    ] : null
                                }

                                <TouchableOpacity style={{ left: 55, paddingTop: 5 }} onPress={() => { this.setState({ showTranscriptTL: !this.state.showTranscriptTL }) }}>
                                    {
                                        e[1].isHighlighted ? <Image source={require('./assets/transcriptdropdown1.png')} style={{ height: 7, width: 14, resizeMode: 'stretch' }} /> :
                                            <Image source={require('./assets/transcriptdropdown.png')} style={{ height: 7, width: 14, resizeMode: 'stretch' }} />
                                    }
                                </TouchableOpacity>

                                {/* <TouchableOpacity style={{ position: 'absolute', top: 83, right: 38 }} onPress={() => { this.setState({ showTranscriptTL: !this.state.showTranscriptTL }) }}>
                                        {e[1].isHighlighted ?
                                            <Image source={require('./assets/transcriptdropdown1.png')} style={{ height: 7, width: 14, resizeMode: 'stretch' }} />
                                            : <Image source={require('./assets/transcriptdropdown.png')} style={{ height: 7, width: 14, resizeMode: 'stretch' }} />
                                        }
                                    </TouchableOpacity> */}
                            </View>
                            {this.state.showTranscriptTL ?
                                <TranscriptBoxTopLeft eventKey={e[1].uid} event={e[1]} />
                                : null
                            }

                        </CardSection>
                    ]
                case 3:
                    return [
                        <CardSection key={e[0].uid} style={[(svgSegId === 1) ? styles.eventOneSegOne :
                            (svgSegId === 2) ? styles.eventOneSegTwo : styles.eventOneSegThree
                            , { marginVertical: 20 }]}>
                            <View style={{ position: 'relative' }}>


                                <TouchableOpacity
                                    onPress={() => {
                                        Actions.Event({
                                            event: e[0], eventKey: e[0].uid,
                                            year: this.props.item.year, itemKey: this.props.item.uid,
                                            animationLength: `${this.props.item.svgSegmentId}e1`
                                        })
                                    }}
                                    onLongPress={() => this.setState({ EventLongPressedOne: !this.state.EventLongPressedOne, eventDeletingOne: !this.state.eventDeletingOne })}
                                    style={{ width: 110, height: 80, justifyContent: 'center', alignItems: 'center' }}
                                >

                                    {e[0].isHighlighted ?
                                        <View key={"_animate" + e[0].uid} style={{ position: 'absolute' }}>
                                            <Image source={require('./assets/eventboxred24.png')} style={{ width: 90, height: 70, resizeMode: 'stretch', position: 'relative' }} />
                                        </View> :
                                        <View key={"_animate" + e[0].uid} style={{ position: 'absolute' }}>
                                            <Image source={require('./assets/eventboxgray24.png')} style={{ width: 90, height: 70, resizeMode: 'stretch', position: 'relative' }} />
                                        </View>
                                    }
                                    <View style={{ position: 'absolute', width: 70, alignItems: 'center', justifyContent: 'center', left: 15 }}>
                                        {
                                            this.state.fontLoaded ? <Text style={{ textAlign: 'center', color: '#fff', fontSize: 13, fontFamily: 'coolvetica' }}>{e[0].title}</Text> : null
                                        }
                                        {
                                            this.state.fontLoaded ? <Text style={{ textAlign: 'center', fontSize: 6, color: '#fff', fontFamily: 'coolvetica', paddingTop: 10 }}>{this.tConvert(e[0].startDate)}-{this.tConvert(e[0].endDate)}</Text> : null
                                        }
                                    </View>
                                </TouchableOpacity>

                                <TouchableOpacity style={{ position: 'absolute', left: 5 }}>
                                    {this.state.eventDeletingOne ?

                                        <Image source={require('./assets/DeleteEvent.png')} style={{ height: 16, width: 16, resizeMode: 'stretch' }} /> : null
                                    }

                                </TouchableOpacity>
                                {e[0].isHighlighted && this.state.EventLongPressedOne ?
                                    [<TouchableOpacity style={{ position: 'absolute', top: 10, left: -25 }}
                                    >
                                        <Image source={require('./assets/lock1.png')} style={{ height: 22, width: 18, resizeMode: 'stretch' }} />
                                    </TouchableOpacity>] :
                                    null

                                }

                                {this.state.EventLongPressedOne && !e[0].isHighlighted ?
                                    [
                                        <TouchableOpacity style={{ position: 'absolute', top: -5, left: -20 }} onPress={() => { this.concatAnimateLength("e1") }}>
                                            <Image source={require('./assets/timer.png')} style={{ height: 22, width: 22, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                        <TouchableOpacity style={{ position: 'absolute', top: 25, left: -20 }}>
                                            <Image source={require('./assets/lock.png')} style={{ height: 26, width: 20, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                        <TouchableOpacity style={{ position: 'absolute', top: 30, left: -45 }}>
                                            <Image source={require('./assets/camera.png')} style={{ height: 22, width: 22, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                        <TouchableOpacity style={{ position: 'absolute', top: 55, left: -20 }}>
                                            <Image source={require('./assets/mike.png')} style={{ height: 22, width: 22, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                    ] : null
                                }

                                <TouchableOpacity style={{ left: 45, paddingTop: 5 }} onPress={() => { this.setState({ showTranscriptTR: !this.state.showTranscriptTR }) }}>
                                    {
                                        e[0].isHighlighted ? <Image source={require('./assets/transcriptdropdown1.png')} style={{ height: 7, width: 14, resizeMode: 'stretch' }} /> :
                                            <Image source={require('./assets/transcriptdropdown.png')} style={{ height: 7, width: 14, resizeMode: 'stretch' }} />
                                    }
                                </TouchableOpacity>


                            </View>
                            {this.state.showTranscriptTR ?
                                <TranscriptBoxTopRight eventKey={e[0].uid} event={e[0]} />
                                : null
                            }

                        </CardSection>,

                        <CardSection key={e[1].uid} style={(svgSegId === 1) ? styles.eventTwoSegOne :
                            (svgSegId === 2) ? styles.eventTwoSegTwo : styles.eventTwoSegThree}>
                            <View style={{ position: 'relative' }}>

                                <TouchableOpacity
                                    onPress={() => {
                                        Actions.Event({
                                            event: e[1], eventKey: e[1].uid,
                                            year: this.props.item.year, itemKey: this.props.item.uid,
                                            animationLength: `${this.props.item.svgSegmentId}e2`
                                        })
                                    }}
                                    onLongPress={() => this.setState({ EventLongPressedTwo: !this.state.EventLongPressedTwo, eventDeletingTwo: !this.state.eventDeletingTwo })}

                                    style={{ width: 110, height: 80, justifyContent: 'center', alignItems: 'center' }}
                                >

                                    {e[1].isHighlighted ?
                                        <View key={"_animate" + e[1].uid} style={{ position: 'absolute' }}>
                                            <Image source={require('./assets/eventboxred13.png')} style={{ width: 90, height: 70, resizeMode: 'stretch', position: 'relative' }} />
                                        </View>
                                        :
                                        <View key={"_animate" + e[1].uid} style={{ position: 'absolute' }} >
                                            <Image source={require('./assets/eventboxgray13.png')} style={{ width: 90, height: 70, resizeMode: 'stretch', position: 'relative' }} />
                                        </View>

                                    }
                                    <View style={{ position: 'absolute', width: 72, right: 14 }}>
                                        {
                                            this.state.fontLoaded ? <Text style={{ textAlign: 'center', color: '#fff', fontSize: 13, fontFamily: 'coolvetica' }}>{e[1].title}</Text> : null
                                        }
                                        {
                                            this.state.fontLoaded ? <Text style={{ textAlign: 'center', fontSize: 6, color: '#fff', fontFamily: 'coolvetica', paddingTop: 10 }}>{this.tConvert(e[1].startDate)}-{this.tConvert(e[1].endDate)}</Text> : null
                                        }

                                    </View>
                                </TouchableOpacity>

                                <TouchableOpacity style={{ position: 'absolute', right: 5 }}>
                                    {this.state.eventDeletingTwo ?

                                        <Image source={require('./assets/DeleteEvent.png')} style={{ height: 16, width: 16, resizeMode: 'stretch' }} /> : null
                                    }

                                </TouchableOpacity>
                                {e[1].isHighlighted && this.state.EventLongPressedTwo ?
                                    [<TouchableOpacity style={{ position: 'absolute', top: 10, right: -25 }}>
                                        <Image source={require('./assets/lock1.png')} style={{ height: 22, width: 18, resizeMode: 'stretch' }} />
                                    </TouchableOpacity>] :
                                    null

                                }
                                {!e[1].isHighlighted && this.state.EventLongPressedTwo ?
                                    [
                                        <TouchableOpacity style={{ position: 'absolute', top: -5, right: -20 }}>
                                            <Image source={require('./assets/timer.png')} style={{ height: 22, width: 22, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                        <TouchableOpacity style={{ position: 'absolute', top: 25, right: -20 }}>
                                            <Image source={require('./assets/lock.png')} style={{ height: 26, width: 20, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                        <TouchableOpacity style={{ position: 'absolute', top: 30, right: -45 }}>
                                            <Image source={require('./assets/camera.png')} style={{ height: 22, width: 22, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                        <TouchableOpacity style={{ position: 'absolute', top: 55, right: -20 }}>
                                            <Image source={require('./assets/mike.png')} style={{ height: 22, width: 22, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,

                                    ] : null
                                }

                                <TouchableOpacity style={{ left: 55, paddingTop: 5 }} onPress={() => { this.setState({ showTranscriptTL: !this.state.showTranscriptTL }) }}>
                                    {
                                        e[1].isHighlighted ? <Image source={require('./assets/transcriptdropdown1.png')} style={{ height: 7, width: 14, resizeMode: 'stretch' }} /> :
                                            <Image source={require('./assets/transcriptdropdown.png')} style={{ height: 7, width: 14, resizeMode: 'stretch' }} />
                                    }
                                </TouchableOpacity>


                            </View>
                            {this.state.showTranscriptTL ?
                                <TranscriptBoxTopLeft eventKey={e[1].uid} event={e[1]} />
                                : null
                            }

                        </CardSection>,

                        <CardSection key={e[2].uid} style={[(svgSegId === 1) ? styles.eventThreeSegOne :
                            (svgSegId === 2) ? styles.eventThreeSegTwo : styles.eventThreeSegThree, { marginVertical: 20 }]}>
                            <View style={{ position: 'relative' }}>

                                <TouchableOpacity
                                    onPress={() => {
                                        Actions.Event({
                                            event: e[2], eventKey: e[2].uid,
                                            year: this.props.item.year, itemKey: this.props.item.uid,
                                            animationLength: `${this.props.item.svgSegmentId}e3`
                                        })
                                    }}
                                    onLongPress={() => this.setState({ EventLongPressedThree: !this.state.EventLongPressedThree, eventDeletingThree: !this.state.eventDeletingThree })}

                                    style={{ width: 110, height: 80, justifyContent: 'center', alignItems: 'center' }}
                                >
                                    {e[2].isHighlighted ?
                                        <View key={"_animate" + e[2].uid} style={{ position: 'absolute' }}>
                                            <Image source={require('./assets/eventboxred24.png')} style={{ width: 90, height: 70, resizeMode: 'stretch', position: 'relative' }} />
                                        </View>
                                        :
                                        <View key={"_animate" + e[2].uid} style={{ position: 'absolute' }}>
                                            <Image source={require('./assets/eventboxgray24.png')} style={{ width: 90, height: 70, resizeMode: 'stretch', position: 'relative' }} />
                                        </View>
                                    }
                                    <View style={{ position: 'absolute', width: 70, alignItems: 'center', justifyContent: 'center', left: 15 }}>
                                        {
                                            this.state.fontLoaded ? <Text style={{ textAlign: 'center', color: '#fff', fontSize: 13, fontFamily: 'coolvetica' }}>{e[2].title}</Text> : null
                                        }
                                        {
                                            this.state.fontLoaded ? <Text style={{ textAlign: 'center', fontSize: 6, color: '#fff', fontFamily: 'coolvetica', paddingTop: 10 }}>{this.tConvert(e[2].startDate)}-{this.tConvert(e[2].endDate)}</Text> : null
                                        }
                                    </View>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    style={{ position: 'absolute', left: 5 }}>
                                    {this.state.eventDeletingThree ?

                                        <Image source={require('./assets/DeleteEvent.png')} style={{ height: 16, width: 16, resizeMode: 'stretch' }} /> : null
                                    }

                                </TouchableOpacity>
                                {e[2].isHighlighted && this.state.EventLongPressedThree ?
                                    [<TouchableOpacity style={{ position: 'absolute', top: 10, left: -25 }}>
                                        <Image source={require('./assets/lock1.png')} style={{ height: 22, width: 18, resizeMode: 'stretch' }} />
                                    </TouchableOpacity>] :
                                    null

                                }
                                {this.state.EventLongPressedThree && !e[2].isHighlighted ?
                                    [
                                        <TouchableOpacity style={{ position: 'absolute', top: -5, left: -20 }}>
                                            <Image source={require('./assets/timer.png')} style={{ height: 22, width: 22, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                        <TouchableOpacity style={{ position: 'absolute', top: 25, left: -20 }}>
                                            <Image source={require('./assets/lock.png')} style={{ height: 26, width: 20, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                        <TouchableOpacity style={{ position: 'absolute', top: 30, left: -45 }}>
                                            <Image source={require('./assets/camera.png')} style={{ height: 22, width: 22, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                        <TouchableOpacity style={{ position: 'absolute', top: 55, left: -20 }}>
                                            <Image source={require('./assets/mike.png')} style={{ height: 22, width: 22, resizeMode: 'stretch' }} />
                                        </TouchableOpacity>,
                                    ] : null
                                }

                                <TouchableOpacity style={{ left: 45, paddingTop: 5 }} onPress={() => { this.setState({ showTranscriptBR: !this.state.showTranscriptBR }) }}>
                                    {
                                        e[2].isHighlighted ? <Image source={require('./assets/transcriptdropdown1.png')} style={{ height: 7, width: 14, resizeMode: 'stretch' }} /> :
                                            <Image source={require('./assets/transcriptdropdown.png')} style={{ height: 7, width: 14, resizeMode: 'stretch' }} />
                                    }
                                </TouchableOpacity>


                            </View>
                            {this.state.showTranscriptBR ?
                                <TranscriptBoxBottomRight eventKey={e[2].uid} event={e[2]} />
                                : null
                            }

                        </CardSection>
                    ]
            }
        }
    }




    getEvArrTimeSorted(events) {
        /* var e = _.map(events, (val, uid) => {
            return { ...val, uid };
        }); */
        var e = _.orderBy(events, ['creationDate'], ['asc']);
        return e;
    }

    render() {
        let e = this.props.item.events;
        //console.log(events);
        let animationLength = 250;
        let svgSegId = this.props.item.svgSegmentId;
        return (
            <View style={styles.container}>

                <View style={styles.svgBox}>

                    <TouchableOpacity onPress={() => { this.toggleModal(); }}>
                        <CardSection style={styles.profileCardStyle}>
                            {this.renderTimeLine(this.props.item, e)}


                        </CardSection>
                    </TouchableOpacity>

                    <View style={[styles.eventNameBox,
                    this.props.item.svgSegmentId == 1 ? styles.evNameSegOne :
                        this.props.item.svgSegmentId == 2 ? styles.evNameSegTwo :
                            this.props.item.svgSegmentId == 3 ? styles.evNameSegThree :
                                null

                    ]}>
                        {
                            this.state.fontLoaded ? <Text style={{ fontSize: 12, color: '#2B2B2B', fontFamily: 'coolvetica' }}>{this.props.item.name}</Text> : null
                        }
                    </View>
                    {e.length > 3 ?
                        <TouchableOpacity
                            onPress={() => {
                                this.onPressmoreEvents()
                            }}
                            style={{ position: 'absolute', bottom: 15, right: 15 }}><Entypo name="dots-three-horizontal" size={20} /></TouchableOpacity> : null}
                    {this.rendenEventBox(e, svgSegId)}

                </View>

                

            </View>
        )
    }
}