import React, { PureComponent } from 'react'
import { View, Animated, Easing } from 'react-native';
import { svgPathProperties } from 'svg-Svg.Path-properties';
/* import Svg, {
    Svg.LinearGradient,
    Svg.Path,
    Svg.Defs,
    Svg.Stop
} from 'react-native-svg' */
import {Svg} from 'expo'
import svgOne from '../SvgPoints/VivusHi';

const AnimatedPath = Animated.createAnimatedComponent(Svg.Path);

export default class helperSvgPoints extends PureComponent {
    constructor(props) {
        super(props);
        
        this.def = defOne
        this.d = svgOne
        this.svgAnimationLength = this.props.svgAnimationLength;

        const properties = svgPathProperties(this.d);
        this.length = properties.getTotalLength();
        this.strokeDashoffset = new Animated.Value(this.length);

        
    }


    componentDidMount() {
        ////console.log(this.props);
        this.animate();
    }

    animate = () => {
        this.strokeDashoffset.setValue(this.length);
        Animated.sequence([
            Animated.delay(1000),
            Animated.timing(this.strokeDashoffset, {
                toValue: this.svgAnimationLength,
                easing: Easing.bounce,
                duration: 4000
            })
        ]).start();
    };
    
    render() {
        return (
            <Svg
                height={278.1393}
                width={170.4683}
            >
                {this.def}
                <AnimatedPath fill="none"
                    stroke="url(#df0272b4-af27-4498-a012-8e6b05c8c6c0)"
                   
                    strokeWidth={28.125}
                    strokeDashoffset={this.strokeDashoffset}
                    strokeDasharray={[this.length, this.length]}
                    d={this.d}
                />
                <AnimatedPath fill="none"
                    stroke="url(#0629d1b3-c525-43f9-b7a3-ad319f4252fc)"
                    
                    strokeWidth={19.6875}
                    strokeDashoffset={this.strokeDashoffset}
                    strokeDasharray={[this.length, this.length]}
                    d={this.d}
                />
            </Svg>
        )
    }

}

const defOne = (
    <Svg.Defs>
        <Svg.LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c0" x1="99.005" x2="99.005" y2="279.1393" gradientUnits="userSpaceOnUse">
            <Svg.Stop offset="0" stopColor="#EF4C23" />
            <Svg.Stop offset="1" stopColor="#EF4923" />
        </Svg.LinearGradient>
        <Svg.LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc" x1="99.005" x2="99.005" y2="279.1393" gradientUnits="userSpaceOnUse">
            <Svg.Stop offset="0" stopColor="#E21F26" />
            <Svg.Stop offset="1" stopColor="#EF4323" />
        </Svg.LinearGradient>

    </Svg.Defs>
);

const defTwo = (
    <Svg.Defs>
          <Svg.LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c0" x1="98.4425" y1="281.4884" x2="98.4425" y2="-0.5703" gradientUnits="userSpaceOnUse">
            <Svg.Stop offset="0" stopColor="#EF4723" />
            <Svg.Stop offset="1" stopColor="#EF4923" />
          </Svg.LinearGradient>
          <Svg.LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc" x1="98.4425" y1="281.4884" x2="98.4425" y2="-0.5703" gradientUnits="userSpaceOnUse">
            <Svg.Stop offset="0" stopColor="#F04023" />
            <Svg.Stop offset="1" stopColor="#F04E23" />
          </Svg.LinearGradient>

        </Svg.Defs>
)

const defThree = (
    <Svg.Defs>
          <Svg.LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c0" x1="48.1024" y1="134.1382" x2="161.4439" y2="134.1382" gradientUnits="userSpaceOnUse">
            <Svg.Stop offset="0" stopColor="#ED4323" />
            <Svg.Stop offset="1" stopColor="#E32626" />
          </Svg.LinearGradient>
          <Svg.LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc" x1="48.1024" y1="134.1382" x2="161.4439" y2="134.1382" gradientUnits="userSpaceOnUse">
            <Svg.Stop offset="0" stopColor="#EF4923" />
            <Svg.Stop offset="1" stopColor="#EF4723" />
          </Svg.LinearGradient>

        </Svg.Defs>
)