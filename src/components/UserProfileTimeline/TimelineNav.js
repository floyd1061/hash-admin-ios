import React, { PureComponent } from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native'
import { LinearGradient } from 'expo'
import firebase from '@firebase/app'
import "firebase/auth"
import "firebase/database"
import "firebase/storage"
import _ from 'lodash';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import { getUnsenMessagesCount } from '../../actions'

const messageCount = 0;

class TimelineNav extends PureComponent {

    state = {
        profilepicURL: null,
        fontLoaded: true,
        msgCounter: 0
    }

    componentDidMount() {
        this.props.getUnsenMessagesCount(cb => {
            this.setState({
                msgCounter: cb
            })
        });
        this.stateUpdate();
    }

    stateUpdate() {
        this.setState({
            status: this.props.userStatus,
            description: this.props.userDescription
        });
    }

    render() {

       
        return (
            <LinearGradient style={[styles.HeaderSection, { paddingTop: 15, paddingHorizontal: 15 }]} colors={['#FF1900', '#FF3200']}>


                <View style={styles.title}>
                    {
                        this.state.fontLoaded ? <Text style={styles.headerText1}>Hashtazapp</Text> : null
                    }
                </View>


                <TouchableOpacity onPress={() => { Actions.userprofile() }}>
                    <View style={styles.iconstyle}>
                        {firebase.auth().currentUser.photoURL ?
                            <Image style={styles.avatericon} source={{ uri: firebase.auth().currentUser.photoURL }} /> :
                            <Image style={styles.avatericon} source={require('./assets/avatar.png')} />
                        }
                    </View>
                </TouchableOpacity>

                <View style={[styles.headeritem]} >
                    <View style={{ flexDirection: 'row', paddingTop: 15, justifyContent: 'space-between' }}>
                        <TouchableOpacity onPress={() => { Actions.main() }}>
                            <View style={styles.messagearea}>
                                <Image source={require('./assets/message.png')} style={styles.messageicon} />
                                {
                                    this.state.fontLoaded ? <Text style={styles.messageCount}>{this.props.unseenMessageCount}</Text> : null
                                }
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => { Actions.SearchUserAndEvent() }}>
                            <Image source={require('./assets/search.png')} style={[styles.searchicon, { marginRight: 20 }]} />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => { Actions.FriendsFollowersActivity() }}>
                            <Image source={require('./assets/followersactivityicon.png')} style={styles.followersnumber} />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.textstyle}>
                        {
                            this.state.fontLoaded ? <Text style={styles.textstyle1}>{this.props.userStatus}{'\n'}</Text> : null
                        }

                        {
                            this.state.fontLoaded ? <Text style={styles.textstyle2}>{this.props.userDescription}{'\n'}</Text> : null
                        }

                        {/* {
                            this.state.fontLoaded ? <Text style={styles.textstyle3}>www.chessmaster.com</Text> : null
                        } */}
                    </View>
                </View>
            </LinearGradient>
        )
    }
}

const mapStateToProps = (state) => {
    let unseenMessageCount = _.sumBy(state.messages.unseenMsgCount, 'count');

    let userProfileData = state.editUserProfileInfo.userProfileInfoData;
    let userStatus = (userProfileData !== null) ? userProfileData.userStatus : "";
    let userDescription = (userProfileData !== null) ? userProfileData.userDescription : "";
    return { userStatus, userDescription, unseenMessageCount };
};

export default connect(mapStateToProps, { getUnsenMessagesCount })(TimelineNav);

const styles = StyleSheet.create({

    HeaderSection: {
        backgroundColor: '#F40C00',
        alignSelf: 'stretch',
        height: 140,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    title: {
        paddingTop: 25,
        left: 10
    },

    headerText1: {
        fontSize: 20,
        color: '#fff',
        fontFamily: 'coolvetica'
    },

    iconstyle: {
        alignItems: 'stretch',
        top: 20,
        alignSelf: 'center',
        marginLeft: 25
    },

    avatericon: {
        height: 90,
        width: 90,
        borderRadius: 90 / 2,
        borderWidth: 3,
        borderColor: '#FF3C00'
    },

    headeritem: {
        flexDirection: 'column',
        // backgroundColor:'#eee'
    },

    messagearea: {
        position: 'relative',
        marginTop: 10,
        marginRight: 20,
    },

    messageicon: {
        height: 30,
        width: 30,
        resizeMode: 'stretch',
        marginRight: 8,
    },

    messageCount: {
        position: 'absolute',
        top: -8,
        color: '#fff',
        backgroundColor: '#FE2800',
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#fff',
        width: 15,
        height: 15,
        fontSize: 7,
        paddingTop: 3,
        textAlign: 'center',
        left: 21,
        fontFamily: 'coolvetica'
    },

    searchicon: {
        height: 30,
        width: 30,
        resizeMode: 'stretch',
        top: 10
    },

    followersnumber: {
        height: 30,
        width: 30,
        resizeMode: 'stretch',
        top: 10,
        marginRight: 7
    },

    textstyle: {
        marginTop: 10,
        marginLeft: 5,
        width: 120,
        flexDirection: 'column',
    },

    textstyle1: {
        fontSize: 9,
        color: '#fff',
        textAlign: 'left',
        fontFamily: 'coolvetica'
    },

    textstyle2: {
        fontSize: 9,
        color: '#282828',
        textAlign: 'left',
        fontFamily: 'coolvetica'
    },

    textstyle3: {
        fontSize: 9,
        color: '#282828',
        textAlign: 'center',
        fontFamily: 'coolvetica'
    },
});