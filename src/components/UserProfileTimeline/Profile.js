import React, { PureComponent } from 'react'
import _ from 'lodash';
import firebase from '@firebase/app'
import auth from '@firebase/auth'
import {
    Platform, FlatList, Dimensions, TouchableOpacity, View, ActivityIndicator, Image
} from 'react-native'
import DrawerLayoutAndroid from 'react-native-drawer-layout';
import dt from 'date-and-time';
import { connect } from 'react-redux'
import NavBar from './TimelineNav'
import ListItemSvg from './TimeLineListItem'
import BottomDrawer from './TimelineBDrawer'
import { Dropdown } from 'react-native-material-dropdown'
import { Entypo, Ionicons } from '@expo/vector-icons';
import { navigationLeftView } from '../drawer/LeftMenu'
import { navigationNotificationView } from '../drawer/NotificationMenu'
import { fetchTimelineDataAfterLogin, selectYear, getAllEventsOftheUser } from '../../actions';
const { width, height } = Dimensions.get('window');
const thisYear = new Date(Date.now()).getFullYear();
const thisMonth = new Date(Date.now()).getMonth();
const thisDay = new Date(Date.now()).getDate();
const epoch = new Date(thisYear, thisMonth, thisDay).valueOf();
class Profile extends PureComponent {
    constructor(props) {
        super(props);
        this.horizontal = false;
        this.calendarWidth = width;
        this.calendarHeight = 360;
        this.calendarHeightSectionOne = 278.139;
        this.calendarHeightSectionTwo = 281.488;
        this.calendarHeightSectionThree = 268.212;
        this.pastScrollRange = 50;
        this.futureScrollRange = 50;
        this.showScrollIndicator = false;
        this.scrollEnabled = true;
        this.scrollsToTop = false;
        this.removeClippedSubviews = Platform.OS === 'android' ? false : true;
        this.state = {
            isModalVisible: false,
            data: [],
            selected: [],
            selectedyear: thisYear,
            years: (this.props.years.length > 0) ? this.props.years : [thisYear],
            selectedItemId: '',
            openDate: null,
            tlData: null,
            btMenuButton: 72,
            bottomDrowerOpen: true,
            btDrawerButtonIcon: 'ios-arrow-down',
            loading: false
        }
    }

    componentDidMount() {
        this.props.selectYear(thisYear.toString());
        setTimeout(() => {
            this.setState({loading: true})
            this.flatlist.scrollToIndex({ animated: false, index: this.props.dayIndex });
        }, 5000)
    }
    getItemLayout = (data, index) => {
        return {
            length:
                280,
            offset:
                280 * index, index
        };
    }
    _getEventsbyDay = (item) => {

        let events = []
        let gallery = []
        if (item.hasEvent) {
            const { currentUser } = firebase.auth();
            firebase.database().ref(`/warehouse/events/${currentUser.uid}/${item.uid}`)
                .orderByChild('eventPriority')
                .limitToLast(5)
                .on('value', snapshot => {

                    if (snapshot.val() !== null) {
                        events = _.map(snapshot.val(), (val, uid) => {
                            return { ...val, uid }
                        })
                    }
                })
        }
        return { events: events };
    }

    renderItem = ({ item }) => {
        return <ListItemSvg item={item} key={item.uid} events={this._getEventsbyDay(item).events} />
    }

    _keyExtractor = (item, index) => item.uid;

    getDayIndex = () => {
        let thisMonth = new Date(Date.now()).getMonth();
        let thisDay = new Date(Date.now()).getDate();
        let epoch = new Date(thisYear, thisMonth, thisDay).valueOf();
        let index = _.findIndex(this.props.timelineData, ["unixUtc", epoch])

        return 0;
    }
    getBottomMenuPosition() {
        return this.state.btMenuButton;
    }
    reRenderFlatList = (selectedyear) => {
        const { currentUser } = firebase.auth();


        this.props.selectYear(selectedyear.toString());
        this.props.fetchTimelineDataAfterLogin(selectedyear, currentUser.uid);

        if (thisYear.toString() === selectedyear) {
            this.flatlist.scrollToIndex({ animated: true, index: this.props.dayIndex });
        }


    }
    render() {

        return (
            <DrawerLayoutAndroid
                drawerWidth={103}
                drawerBackgroundColor='transparent'
                drawerPosition={DrawerLayoutAndroid.positions.Left}
                renderNavigationView={() => navigationLeftView}>
                <DrawerLayoutAndroid
                    drawerWidth={100}
                    drawerBackgroundColor='transparent'
                    statusBarBackgroundColor={null}
                    drawerPosition={DrawerLayoutAndroid.positions.Right}
                    renderNavigationView={() => navigationNotificationView}>
                    <NavBar />
                    {!this.state.loading ? <ActivityIndicator style={{position:'absolute', top: 150, left: 15, zIndex: 5000}} size='large' color="#EC0900" /> :
                        null
                    }
                    <FlatList
                        ref={(ref) => this.flatlist = ref}
                        initialNumToRender={10}
                        data={this.props.timelineData}

                        //removeClippedSubviews={this.removeClippedSubviews}
                        pageSize={1}

                        horizontal={false}
                        pagingEnabled={true}
                        renderItem={this.renderItem}
                        showsVerticalScrollIndicator={this.showScrollIndicator}
                        showsHorizontalScrollIndicator={this.showScrollIndicator}
                        scrollEnabled={this.scrollingEnabled}
                        keyExtractor={this._keyExtractor}
                        // initialScrollIndex={this.getDayIndex()}
                        getItemLayout={this.getItemLayout}
                    //scrollsToTop={this.scrollsToTop}
                    />
                    <View style={{
                        position: 'absolute',
                        top: (height - 140) / 2,
                        left: 15,
                        zIndex: 20,
                        height: 100,
                        width: 60
                    }}>
                        <Dropdown
                            inputContainerStyle={{ borderBottomColor: 'transparent' }}
                            dropdownOffset={{ top: 3, left: 3 }}
                            dropdownPosition={0}
                            label={thisYear.toString()}
                            fontSize={17}
                            data={this.state.years}
                            onChangeText={(val) => { this.reRenderFlatList(val) }}
                        />
                    </View>

                    <TouchableOpacity onPress={() => {
                        this.setState({
                            bottomDrowerOpen: !this.state.bottomDrowerOpen
                        })
                    }} style={{
                        position: 'absolute',
                        bottom: this.state.bottomDrowerOpen ? 80 : 20,
                        right: 15,
                        zIndex: 20,
                        height: 25,
                        width: 50,
                        alignItems: 'center',
                        backgroundColor: 'transparent'
                    }}>
                        <Entypo name={this.state.bottomDrowerOpen ? 'chevron-down' : 'chevron-up'} size={35} color="#2B2B2B" />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {

                    }} style={{
                        position: 'absolute',

                        right: 0,
                        zIndex: 200,
                        //height: 60,
                        //width: 72,
                        top: 145,
                        alignItems: 'center',
                        backgroundColor: 'transparent'
                    }}>
                        <Image
                            source={require('./assets/Instant_live_share.png')}
                            style={{ width: 60, height: 50 }}
                        />
                    </TouchableOpacity>

                    {this.state.bottomDrowerOpen ? <BottomDrawer /> : null}

                </DrawerLayoutAndroid>
            </DrawerLayoutAndroid>
        )
    }
}

const styles = {
    Dropdownstyles1: {
        flex: 1,
        height: 100,
        backgroundColor: '#FFFFFF',
    },
}

const mapStateToProps = (state) => {
    let selectedYear = state.timelineData.selectedYear;
    let timelineData = []
    let timelineDataByYear = _.map(state.timelineData.timeLineData, (val, year) => {
        return { ...val, year };
    });

    timelineData = _.find(timelineDataByYear, ["year", selectedYear])
    timelineData = _.omit(timelineData, ['year'])
    timelineData = _.map(timelineData, (val, uid) => {
        return { ...val };
    });


    const dayIndex = _.findIndex(timelineData, ["unixUtc", epoch])
    return { timelineData: timelineData, dayIndex: dayIndex };
};

export default connect(mapStateToProps, { selectYear, fetchTimelineDataAfterLogin, getAllEventsOftheUser })(Profile);