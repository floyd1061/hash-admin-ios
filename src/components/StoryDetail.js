import React, { Component } from 'react'
import { Text, View, Image, Dimensions, TouchableOpacity, StyleSheet } from 'react-native'
import { Card, CardSection } from './common'

const widthWindow = Dimensions.get('window').width;

const StoryDetail = ({ story, navigate }) => {

    const { user, shared_event_with, story_title,
        story_date, story_contents, story_geolocation,
        story_like_comment_count, story_description
    } = story;
    return (
        <Card>
            <CardSection style={{flexDirection:'row' , marginTop: 10, marginBottom: 10}}>
                <View style={styles.thumbnailContainerStyle}>
                    <Image
                        style={styles.thumbnailStyle}
                        source={user.profile_pic}
                    />
                </View>
                <View style={styles.headerContentStyle}>
                    <View style={styles.headerTextContentStyle}>
                        <Text style={styles.headerUserTextStyle}>{user.user_name}</Text>
                        <Text style={styles.headerSharedTextStyle}>Shared With</Text>
                        <Text style={styles.headerSharedUserTextStyle}>{shared_event_with.user_name}</Text>
                    </View>
                    <View style={styles.headerTextContentStyle}>
                        <Text style={styles.headerStoryTextStyle}>{story_title}</Text>
                        <Text style={styles.headerStoryTimeTextStyle}>{story_date}</Text>
                    </View>
                </View>
            </CardSection>
            <CardSection style={{marginBottom: 5}}>
                {RenderImageFlex({ story, navigate })}
            </CardSection>
            <CardSection style={{marginLeft:10, marginRight:10, flexDirection: 'row'}}>
                <View style={styles.footTextHeadContainer}>
                    <Text style={styles.footStoryText}>{story_title}</Text>
                    <Image style={styles.geoIconStyle} source={require('./assets/Location.png')} />
                    <Text style={styles.footStoryText}>{story_geolocation}</Text>
                    <TouchableOpacity onPress={() => navigate('LikeCommentList', { story: story })}>
                        <Text style={styles.likeCommentCounterTextStyle}>{story_like_comment_count.comments} Comment</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigate('LikeCommentList', { story: story })}>
                        <Text style={styles.likeCommentCounterTextStyle}>{story_like_comment_count.likes} likes</Text>
                    </TouchableOpacity>
                </View>
            </CardSection>
            <CardSection style={{marginLeft:10, marginRight:10}}>
                <Text style={styles.storyDescriptionTextStyle}>{story_description}</Text>
            </CardSection>
            <CardSection style={{marginLeft:10, marginRight:10, flexDirection: 'row'}}>
                <View style={styles.LikeCommentShareButtonContainer}>
                    <TouchableOpacity>
                        <Image style={styles.commentIconStyle} source={require('./assets/comment.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image style={styles.LikeIconStyle} source={require('./assets/like.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image style={styles.ShareIconStyle} source={require('./assets/share.png')} />
                    </TouchableOpacity>
                </View>
            </CardSection>

        </Card>
    );
};

const RenderImageFlex = ({ story, navigate }) => {
    ////console.log(navigate);
    const story_contents = story.story_contents;
    const numberOfImages = story_contents.length;

    if (numberOfImages === 1) {
        return (
            <View style={styles.imageStyleSingle}>
                <TouchableOpacity onPress={() => navigate('ImageGallery', { story: story })}>
                    <Image
                        source={story_contents[0].url}
                    />
                </TouchableOpacity>
            </View>
        );

    }
    if (numberOfImages === 2) {
        return (
            <View style={styles.imageContainerMultiple}>
                <TouchableOpacity onPress={() => navigate('ImageGallery', { story: story })}>
                    <Image
                        style={styles.imageStyleDouble}
                        source={story_contents[0].url}
                    />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigate('ImageGallery', { story: story })}>
                    <Image
                        style={styles.imageStyleDouble}
                        source={story_contents[1].url}
                    />
                </TouchableOpacity>
            </View>
        );
    }
    if (numberOfImages === 3) {
        return (
            <View style={styles.imageContainerMultiple}>
                <TouchableOpacity onPress={() => navigate('ImageGallery', { story: story })}>
                    <Image
                        style={styles.imageStyleTriple_1}
                        source={story_contents[0].url}
                    />
                </TouchableOpacity>
                <View style={styles.imageContainerMultipleColumn}>
                    <TouchableOpacity onPress={() => navigate('ImageGallery', { story: story })}>
                        <Image
                            style={styles.imageStyleTriple_2}
                            source={story_contents[1].url}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigate('ImageGallery', { story: story })}>
                        <Image
                            style={styles.imageStyleTriple_3}
                            source={story_contents[2].url}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
    if (numberOfImages === 4) {
        return (
            <View style={styles.imageContainerMultiple}>
                <TouchableOpacity onPress={() => navigate('ImageGallery', { story: story })}>
                    <Image
                        style={styles.imageStyleTriple_1}
                        source={story_contents[0].url}
                    />
                </TouchableOpacity>
                <View style={styles.imageContainerMultipleColumn}>
                    <TouchableOpacity onPress={() => navigate('ImageGallery', { story: story })}>
                        <Image
                            style={styles.imageStyleTriple_2}
                            source={story_contents[1].url}
                        />
                    </TouchableOpacity>
                    <View style={styles.imageContainerMultipleColumnRow}>
                        <TouchableOpacity onPress={() => navigate('ImageGallery', { story: story })}>
                            <Image
                                style={styles.imageStyleQuad_3}
                                source={story_contents[2].url}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigate('ImageGallery', { story: story })}>
                            <Image
                                style={styles.imageStyleQuad_4}
                                source={story_contents[3].url}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
    if (numberOfImages >= 4) {
        return (
            <View style={styles.imageContainerMultiple}>
                <TouchableOpacity onPress={() => navigate('ImageGallery', { story: story })}>
                    <Image
                        style={styles.imageStyleTriple_1}
                        source={story_contents[0].url}
                    />
                </TouchableOpacity>
                <View style={styles.imageContainerMultipleColumn}>
                    <TouchableOpacity onPress={() => navigate('ImageGallery', { story: story })}>
                        <Image
                            style={styles.imageStyleTriple_2}
                            source={story_contents[1].url}
                        />
                    </TouchableOpacity>
                    <View style={styles.imageContainerMultipleColumnRow}>
                        <TouchableOpacity onPress={() => navigate('ImageGallery', { story: story })}>
                            <Image
                                style={styles.imageStyleQuad_3}
                                source={story_contents[2].url}
                            />
                        </TouchableOpacity>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity onPress={() => navigate('ImageGallery', { story: story })}>
                                <Image
                                    style={styles.imageStyleQuad_4_more}
                                    source={story_contents[3].url}
                                />
                            </TouchableOpacity>
                            <Text style={{ position: 'absolute', color: '#fff', fontWeight: '500' }}>{numberOfImages - 4} more..</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
    return (
        <Text style={styles.headerSharedUserTextStyle}></Text>
    );
};

const RenderImageModal = ({ story_contents }) => {

};

const styles = {
    headerContentStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',

    },

    headerTextContentStyle: {
        flexDirection: 'row',
        alignItems: 'flex-end',
    },

    headerUserTextStyle: {
        fontSize: 14,
        fontFamily: 'coolvetica',
        marginRight: 6
    },

    headerSharedTextStyle: {
        fontSize: 9,
        fontFamily: 'coolvetica',
        marginRight: 6
    },

    headerSharedUserTextStyle: {
        fontSize: 9,
        fontFamily: 'coolvetica',
        color: '#fc211a',
        marginRight: 6
    },

    headerStoryTextStyle: {
        fontSize: 9,
        fontFamily: 'coolvetica',
        marginRight: 6
    },

    headerStoryTimeTextStyle: {
        fontSize: 8,
        fontFamily: 'coolvetica',
        color: '#fc211a',
        marginRight: 6
    },

    thumbnailStyle: {
        height: 50,
        width: 50,
        borderRadius: 50,
    },

    thumbnailContainerStyle: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginLeft: 10,
        marginRight: 10,
        width: 60
    },

    imageStyleSingle: {
        height: 200,
        flex: 1,
        width: null,
        marginLeft: 3,
        marginRight: 3,
    },

    imageStyleDouble: {
        height: 200,
        width: (widthWindow / 2) - 15,
        marginRight: 5
    },

    imageContainerMultiple: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5
    },

    imageContainerMultipleColumn: {
        flexDirection: 'column',
        justifyContent: 'center',
    },

    imageContainerMultipleColumnRow: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },

    imageStyleTriple_1: {
        height: 200,
        width: (widthWindow / 2) - 20,
        marginRight: 5
    },

    imageStyleTriple_2: {
        height: 97.5,
        width: (widthWindow / 2) - 10,
        marginRight: 5,
        marginBottom: 5
    },

    imageStyleTriple_3: {
        height: 97.5,
        width: (widthWindow / 2) - 10,
        marginRight: 5,
    },

    imageStyleQuad_3: {
        height: 97.5,
        width: (widthWindow / 4) - 7.5,
        marginRight: 5,
    },

    imageStyleQuad_4: {
        height: 97.5,
        width: (widthWindow / 4) - 7.5,
    },

    imageStyleQuad_4_more: {
        height: 97.5,
        width: (widthWindow / 4) - 7.5,
        opacity: .6,
        backgroundColor: '#000'
    },

    footTextHeadContainer: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5
    },

    footStoryText: {
        fontSize: 8,
        fontFamily: 'coolvetica',
        color: '#fc211a',
        marginRight: 15
    },

    geoIconStyle: {
        height: 14,
        width: 10,
        marginRight: 3
    },

    likeCommentCounterTextStyle: {
        fontSize: 9,
        fontFamily: 'coolvetica',
        marginRight: 6
    },

    storyDescriptionTextStyle: {
        fontSize: 10,
        marginLeft: 5,
        marginRight: 5,
        fontFamily: 'coolvetica',
    },

    LikeCommentShareButtonContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5
    },

    commentIconStyle: {
        height: 15,
        width: 17,
        marginRight: 25,
        marginTop: 3
    },

    LikeIconStyle: {
        height: 15,
        width: 15,
        marginRight: 25,
    },

    ShareIconStyle: {
        height: 15,
        width: 15,
        marginTop: 1
    }
};

export default StoryDetail;