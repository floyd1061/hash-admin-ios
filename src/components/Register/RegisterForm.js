import React, { Component } from 'react'
import { Text, TextInput, Image, TouchableOpacity } from 'react-native'
import { Card, CardSection } from '../common'
import { connect } from 'react-redux';
import { Font } from 'expo'
import { nameChanged, emailChanged, passwordChanged, confirmPasswordChanged, registerUser } from '../../actions';

class RegisterForm extends Component {

    onNameChanged(name) {
        this.props.nameChanged(name);
    }

    onEmailChanged(email) {
        this.props.emailChanged(email);
    }

    onPasswordChange(password) {
        this.props.passwordChanged(password);
    }

    onConfirmPasswordChange(confirmpassword) {
        this.props.confirmPasswordChanged(confirmpassword);
    }

    onRegisterPress() {
        const { name, email, password, confirmpassword } = this.props;
        this.props.registerUser({ name: name, email: email, password: password, confirmpassword });
    }

    state = {
        fontLoaded: true,
    };



    render() {
        return (
            <Card style={styles.container}>
                <CardSection style={styles.SectionStyle}>
                    {
                        this.state.fontLoaded ? (<TextInput
                            style={{ flex: 1, fontFamily: 'coolvetica' }}
                            placeholder="Name"
                            returnKeyType="next"
                            onChangeText={this.onNameChanged.bind(this)}
                            underlineColorAndroid="transparent"
                            onSubmitEditing={() => this.emailInput.focus()}
                        />) : null
                    }
                    <Image source={require('./assets/Forma.png')} style={styles.ImageStyle} />
                </CardSection>
                <CardSection style={styles.SectionStyle}>
                    {
                        this.state.fontLoaded ? (<TextInput
                            style={{ flex: 1, fontFamily: 'coolvetica' }}
                            placeholder="Email"
                            returnKeyType="next"
                            onChangeText={this.onEmailChanged.bind(this)}
                            keyboardType="email-address"
                            underlineColorAndroid="transparent"
                            ref={(input) => this.emailInput = input}
                            onSubmitEditing={() => this.passwordInput.focus()}
                        />) : null
                    }
                    <Image source={require('./assets/Email.png')} style={styles.ImageStyleEmail} />
                </CardSection>
                <CardSection style={styles.SectionStyle}>
                    {
                        this.state.fontLoaded ? (<TextInput
                            style={{ flex: 1, fontFamily: 'coolvetica' }}
                            placeholder="Password"
                            returnKeyType="next"
                            secureTextEntry
                            onChangeText={this.onPasswordChange.bind(this)}
                            underlineColorAndroid="transparent"
                            ref={(input) => this.passwordInput = input}
                            onSubmitEditing={() => this.confirmPasswordInput.focus()}
                        />) : null
                    }
                    <Image source={require('./assets/padlock.png')} style={styles.ImageStyleLock} />
                </CardSection>
                <CardSection style={styles.SectionStyleLastInput}>
                    {
                        this.state.fontLoaded ? (<TextInput
                            style={{ flex: 1, fontFamily: 'coolvetica' }}
                            placeholder="Confirm Password"
                            secureTextEntry
                            onChangeText={this.onConfirmPasswordChange.bind(this)}
                            returnKeyType="go"
                            underlineColorAndroid="transparent"
                            ref={(input) => this.confirmPasswordInput = input}
                        />) : null
                    }
                    <Image source={require('./assets/padlock.png')} style={styles.ImageStyleLock} />
                </CardSection>

                <CardSection style={{ backgroundColor: null, marginTop: 25 }} >

                    <TouchableOpacity style={styles.SectionStyleButton} onPress={this.onRegisterPress.bind(this)}>
                        {
                            this.state.fontLoaded ? (<Text style={styles.buttonText}>Register</Text>) : null
                        }
                    </TouchableOpacity>
                </CardSection>

            </Card>
        );
    }
};

const styles = {

    container: {
        flex: 1,
        margin: 10,
    },

    customCheckboxStyle: {
        width: 15,
        height: 15
    },
    CustomLabelStyle: {
        fontSize: 8,
        color: '#fff',
        fontFamily: 'coolvetica'
    },

    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderWidth: .5,
        borderColor: '#fff',
        height: 40,
        borderRadius: 3,
        paddingHorizontal: 10,
        marginBottom: 18
    },

    SectionStyleLastInput: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderWidth: .5,
        borderColor: '#fff',
        height: 40,
        borderRadius: 3,
        paddingHorizontal: 10,
        marginBottom: 18
    },

    SectionStyleKeepLoggedIn: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 20,
        marginBottom: 10
    },

    orImageStyle: {
        height: 25,
        width: 27
    },

    SectionStyleOrBox: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 20,
        marginBottom: 15
    },

    SectionStyleButton: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#8c8c8c',
        borderWidth: 3,
        borderColor: '#fff',
        height: 40,
        borderRadius: 3,
        paddingHorizontal: 10,
        marginBottom: 5
    },

    SectionStyleButtonFaceBook: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#3B5998',
        height: 40,
        borderRadius: 3,
        paddingHorizontal: 10,
        marginBottom: 5
    },

    ImageStyle: {
        height: 20,
        width: 20,
        resizeMode: 'stretch',
        alignItems: 'center'
    },

    ImageStyleEmail: {
        height: 20,
        width: 25,
        resizeMode: 'stretch',
        alignItems: 'center'
    },

    ImageStyleLock: {
        height: 20,
        width: 18,
        resizeMode: 'stretch',
        alignItems: 'center'
    },

    buttonText: {
        textAlign: 'center',
        color: '#fff',
        fontFamily: 'coolvetica'
    }

};

const mapStateToProps = (state) => {
    return {
        name: state.register.name,
        email: state.register.email,
        password: state.register.password,
        confirmpassword: state.register.confirmpassword,
    }
}

export default connect(mapStateToProps, { nameChanged, emailChanged, passwordChanged, confirmPasswordChanged, registerUser })(RegisterForm);