import React, { Component } from 'react'
import { StyleSheet, ImageBackground, KeyboardAvoidingView, ScrollView } from 'react-native'
import { connect } from 'react-redux'
import { REGISTER_FORM_WIDTH_TO_HEIGHT_RATIO, REGISTER_FORM_WIDTH_PERCENT } from '../DimentionConstants'
import RegisterForm from './RegisterForm'

class RegisterComponent extends Component {

    calculateFromRatio = (a, b) => {
        return  a * (b/100);
    }

    componentWillMount() {
        // //console.log(this.props);
    }

    render() {
        let formWidth = this.calculateFromRatio(this.props.window.window_dimention.width, REGISTER_FORM_WIDTH_PERCENT);
        let formHeight = (formWidth * REGISTER_FORM_WIDTH_TO_HEIGHT_RATIO);
        return (
            <ImageBackground source={require('./assets/background.png')} style={styles.container}>
                <ScrollView style = {{flex: 1, paddingTop: 60}}>
                    <KeyboardAvoidingView behavior="position">        
                        <ImageBackground style={{height: 500, width: formWidth, paddingTop: 140, paddingHorizontal: 20}} source={require('./assets/Register.png')}>
                            <RegisterForm/>
                        </ImageBackground>
                    </KeyboardAvoidingView>
                </ScrollView>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    loginBox: {
        width: 250,
        height: 370,
        paddingHorizontal: 10,
        paddingVertical: 90,
        marginBottom: 20
    },

    SectionStyleLoginButton: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 2,
        borderColor: '#fff',
        height: 35,
        borderRadius: 3,
        paddingHorizontal: 15,
        marginBottom: 5,
        width: 130
    },

    buttonText: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 12
    }
});

const mapStateToProps = state => {
    return {
        window: state.window
    }
};

export default connect(mapStateToProps)(RegisterComponent);