import React, { Component } from 'react'
import { Text, View, StyleSheet, ImageBackground, Image, TouchableOpacity } from 'react-native'
import { LinearGradient, Font } from 'expo'
import Icon from '@expo/vector-icons'

export default class Walkthrough extends Component {

    state = {
        fontLoaded: true,
    };

   

    render() {
        return (
            <ImageBackground source={require('./assets/background.png')} style={styles.container}>
                <View style={styles.WalkthroughContainer}>
                    {
                        this.state.fontLoaded ? (<Text style={styles.WalkthroughHeaderText}>Walkthrough</Text>) : null
                    }
                    {
                        this.state.fontLoaded ? (<Text style={styles.WalkthroughText}>Welcome! Have a look around</Text>) : null
                    }
                    <View style={styles.WalkthroughLogoContainer}>
                        <Image style={{ height: 155, width: 147 }} source={require('./assets/Logo.png')} />
                    </View>
                    {
                        this.state.fontLoaded ? (<Text style={{ fontSize: 14, marginBottom: 3, color: '#e30000', fontFamily: 'coolvetica' }}>Explore</Text>) : null
                    }
                    {
                        this.state.fontLoaded ? (<Text style={styles.textinfo}>Explore Share memories with your friends.</Text>) : null
                    }
                    {
                        this.state.fontLoaded ? (<Text style={styles.textinfo}>Connect the pieces to your life.</Text>) : null
                    }
                    {
                        this.state.fontLoaded ? (<Text style={[styles.textinfo, styles.textinfoLastLine]}>Leave your legacy.</Text>) : null
                    }
                    <TouchableOpacity>
                        <LinearGradient
                            colors={['#ff0209', '#e30000']}
                            style={styles.SectionStyleSkipIntroButton}>
                            {
                                this.state.fontLoaded ? (<Text
                                    style={styles.buttonText}>
                                    Skip Walkthrough
                                </Text>) : null
                            }
                            <Icon name="angle-right" size={24} color="#fff" fontWeight='bold' style={{ marginLeft: 7 }} />
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    WalkthroughContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },

    WalkthroughLogoContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
        marginBottom: 7
    },

    WalkthroughHeaderText: {
        fontSize: 20,
        fontFamily: 'coolvetica',
        color: '#ff1402',
    },

    WalkthroughText: {
        fontSize: 11,
        fontFamily: 'coolvetica',
        color: '#fff',
        marginBottom: 20
    },
    textinfo: {
        fontSize: 13,
        fontFamily: 'coolvetica',
        color: '#fff',
        marginBottom: 5
    },

    textinfoLastLine: {
        marginBottom: 60
    },

    SectionStyleSkipIntroButton: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 2,
        borderColor: '#fff',
        height: 35,
        borderRadius: 3,
        paddingHorizontal: 15,
        marginBottom: 5
    },

    buttonText: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 11,
        fontFamily: 'coolvetica'
    }
});