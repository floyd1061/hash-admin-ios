import React, { Component } from 'react'
import { StyleSheet, TouchableOpacity, View, TextInput , Image, Text } from 'react-native'
import {Font} from 'expo'

export default class SearchNavBar extends Component{

    constructor(props) {
        super(props);
        this.state = { showHeaderOption: false, fontLoaded: true };
    }
    
    render(){
       return(
           <View style = {styles.background}>
                <View style={styles.headerContaineer}>    
                    <View style = {styles.headerBox}>
                        <View style = {{flexDirection: 'row'}}> 
                            <TouchableOpacity>
                                <Image source={require('../assets/search.png')} style={styles.searchIcon}/>
                            </TouchableOpacity>
                            {
                                this.state.fontLoaded ? ( <TextInput
                                    style={{ flex: .7, flexDirection: 'row', paddingLeft: 10, fontFamily: 'coolvetica' }}
                                    underlineColorAndroid="transparent"
                                    placeholder="Search For User & Events"
                                /> ) : null
                            }
                        </View>

                        <View style = {{flexDirection: 'row', justifyContent: 'space-between'}}> 
                            <TouchableOpacity> 
                                <Image source={require('../assets/micIcon.png')} style={styles.microPhn}/>
                            </TouchableOpacity>  
                            
                            <TouchableOpacity onPress={() => {this.setState({showHeaderOption: !this.state.showHeaderOption})}}>
                                <Image source={require('../assets/dropdownButton.png')} style={styles.dropDrown}/>
                            </TouchableOpacity>    
                        </View>
                    </View>                           
                </View>
                {this.state.showHeaderOption ?
                    <View style = {styles.headerOptionArea}>
                        <View  style = {{position: 'relative', flex: 1}}>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}>View most searched events</Text> ) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}>View most searched profiles</Text> ) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity disabled = {true}>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}>Go to most searched days</Text> ) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}>Go to most searched days</Text> ) : null
                                }
                            </TouchableOpacity>
                        </View>                            
                    </View> : null
                }
            </View>
        )
    } 
}

const styles = StyleSheet.create({
    backgroundContainer:{
        flex: 1,
        backgroundColor: '#DEDEDE',
    },

    headerContaineer:{
        backgroundColor: '#FF1004', 
        alignItems: 'center',
        height: 130,
        paddingTop: 70,  
    },

    headerBox:{
        backgroundColor: '#fff',
        alignSelf: 'stretch',
        height: 60, 
        alignItems: 'center',
        flexDirection:'row',
        paddingHorizontal: 15,          
    },

    searchIcon:{
        height: 40,
        width: 40, 
        resizeMode : 'stretch',
    },

    microPhn:{
        height: 40,
        width: 30, 
        resizeMode : 'stretch',
    },

    dropDrown:{
        marginLeft: 10,
        height: 35,
        width: 35, 
        resizeMode : 'stretch',
    },

    headerOptionArea: {
        width: 200, 
        height: 150, 
        backgroundColor: '#fff', 
        position: 'absolute',
        right: 22,
        top: 130,
        borderColor: '#FD0607',
        borderWidth: 2,
        borderRadius: 3,
        borderTopWidth: 0,
        alignItems: 'center',

    },

    optionText: {
        paddingVertical: 8,
        paddingHorizontal: 8,
        color:'#000',
        
        fontFamily: 'coolvetica'
    },
})