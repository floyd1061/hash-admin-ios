import React, { Component } from 'react'
import { ListView , View, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import SearchUserListItem from './SearchUserListItem'
   
class SearchUserList extends Component{

    componentWillMount(){
        const ds = new ListView.DataSource({
            rowHasChanged: (r1 , r2) => r1 !== r2
        })
        this.dataSource = ds.cloneWithRows(this.props.searchuserdata);
    }    
    
    renderRow(item) {
        return  <SearchUserListItem item ={item} />;
    }

    render(){     
        return( 
            <View style = {styles.container}>                  
                <ListView style={{flex:1}}   
                    dataSource={this.dataSource}
                    renderRow={this.renderRow}   
                    initialListSize={10}    
                />
            </View>    
        )
    }
}
 
const styles = StyleSheet.create({
    container:{
       flex:1
    },
})

const mapStateToProps = (state ) => {
    return { searchuserdata: state.searchUsers };
};

export default connect (mapStateToProps)(SearchUserList) ;   