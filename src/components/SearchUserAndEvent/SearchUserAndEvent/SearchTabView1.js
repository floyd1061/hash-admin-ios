import React , {Component} from 'react'
import { View , StyleSheet } from 'react-native'   
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import SearchNavBar from './SearchNavBar';
import SearchUserList from './SearchUserList';
import SearchEventList from './SearchEventList';

export default class SearchTabView extends Component{
    state = {
        index: 0,
        routes: [
          { key: 'user', title: 'User' },
          { key: 'events', title: 'Events' },
        ],
    };
    
    _handleIndexChange = index => this.setState({ index });

    _renderHeader  = props  => {
        const inputRange = props.navigationState.routes.map((x, i) => i);
        return(
            <View style={styles.container}>
                <SearchNavBar/>
                <TabBar style ={styles.Talkbar } {...props}/>
            </View>
        )
    };
   
    _renderScene = SceneMap({        
        user: SearchUserList,
        events: SearchEventList,
    });
      
    render() {
        return (
            <TabViewAnimated
                navigationState={this.state}
                renderScene={this._renderScene}
                renderHeader={this._renderHeader}
                onIndexChange={this._handleIndexChange}
            />
        )
    }
}

const styles = StyleSheet.create({
    Talkbar: {
        backgroundColor:'#FF1004',
    }     
})




 