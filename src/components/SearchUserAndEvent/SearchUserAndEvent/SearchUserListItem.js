import React, { Component } from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'
import firebase from '@firebase/app'
import auth from '@firebase/auth'
import _ from 'lodash'
import { connect } from 'react-redux';
import { sendFriendRequest, getAllRelations } from '../../../actions'


class SearchUserListItem extends Component {
    state = {
        fontLoaded: true,
        isFriend: false,
        status: 0,
        self: false
    };

    componentDidMount() {
        this.props.getAllRelations();
        const { currentUser } = firebase.auth();
        const { objectID } = this.props.item;
        ////console.log(this.props.friends);
        if (currentUser.uid === objectID) {
            this.setState({ self: true })
        }

        if (this.props.friends.length > 0) {
            this.props.friends.forEach(element => {
                if (element.uid === objectID) {
                    this.setState({
                        isFriend: true,
                        status: element.status
                    })
                }
            });
        }
    }
    componentWillReceiveProps() {
        const { objectID } = this.props.item;
        if (this.props.friends.length > 0) {
            this.props.friends.forEach(element => {
                if (element.uid === objectID) {
                    this.setState({
                        isFriend: true,
                        status: element.status
                    })
                }
            });
        }


    }

    sendFriendRequest(user, userProfileData) {

        this.setState({
            isFriend: true,
            status: 0
        })
        this.props.sendFriendRequest(user, userProfileData);
    }

    /*    renderRequestBox() {
          // console.log(this.state.status);
           if (this.state.isFriend) {
               return (
   
                   <View style={styles.RequestBox}>
                       <Image source={require('../assets/likeRed.png')} style={styles.likeicon} />
                       <View style={styles.box2Button}>
                           {
                               (this.state.status == 0) ?
                                   <Text style={[styles.frienshipStatus, { fontSize: 12, fontFamily: 'coolvetica' }]}> Requested </Text>
   
                                   : <Text style={[styles.frienshipStatus, { fontSize: 12, fontFamily: 'coolvetica' }]}> Friends </Text>
                           }
                       </View>
                   </View>
               )
           } else {
               return (
                   <TouchableOpacity onPress={() => { this.sendFriendRequest(this.props.item, this.props.userProfileData) }}>
                       <View style={styles.RequestBox}>
                           <Image source={require('../assets/like.png')} style={styles.likeicon} />
                           <View style={[styles.box2Button, { backgroundColor: '#565656' }]}>
                               <Text style={[styles.frienshipStatus, { fontSize: 12, fontFamily: 'coolvetica' }]}> Friend Request </Text>
                           </View>
                       </View>
                   </TouchableOpacity>
               )
           }
   
   
   
       } */


    renderRequestBox(item, relations) {
        ///console.log(allRelations);


        let x = _.filter(relations, ["userId", item.objectID]);

        //console.log(x)

        if (x.length > 0) {
            return (

                <View style={styles.RequestBox}>
                    <Image source={require('../assets/likeRed.png')} style={styles.likeicon} />
                    <View style={styles.box2Button}>
                        {
                            (x[0].status == 0) ?
                                <Text style={[styles.frienshipStatus, { fontSize: 12, fontFamily: 'coolvetica' }]}> Requested </Text> :
                                (x[0].status == 1) ?
                                    <Text style={[styles.frienshipStatus, { fontSize: 12, fontFamily: 'coolvetica' }]}> Friends </Text> :
                                    <Text style={[styles.frienshipStatus, { fontSize: 12, fontFamily: 'coolvetica' }]}> Friend Request </Text>
                        }
                    </View>
                </View>
            )
        } else {
            return (
                <TouchableOpacity onPress={() => { this.sendFriendRequest(this.props.item, this.props.userProfileData) }}>
                    <View style={styles.RequestBox}>
                        <Image source={require('../assets/like.png')} style={styles.likeicon} />
                        <View style={[styles.box2Button, { backgroundColor: '#565656' }]}>
                            <Text style={[styles.frienshipStatus, { fontSize: 12, fontFamily: 'coolvetica' }]}> Friend Request </Text>
                        </View>
                    </View>
                </TouchableOpacity>
            )
        }

    }

    render() {
        //console.log(this.props.relations)
        return (
            <View>
                {!this.state.self ?
                    <View style={styles.componentContainer}>
                        <View style={styles.userBox}>
                            <TouchableOpacity>
                                {
                                    (this.props.item.profilepicture != "") ?
                                        <Image source={{ uri: this.props.item.profilepicture }} style={styles.avatar} /> :
                                        <Image source={require('../assets/avatar.png')} style={styles.avatar} />
                                }
                            </TouchableOpacity>
                            <View style={styles.showName}>
                                <TouchableOpacity>
                                    {
                                        this.state.fontLoaded ? (<Text style={{ fontSize: 15, color: '#FF1004', fontFamily: 'coolvetica' }}>{this.props.item.displayName}</Text>) : null
                                    }
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    {
                                        this.state.fontLoaded ? (<Text style={{ fontSize: 10, color: '#000', fontFamily: 'coolvetica' }}>{this.props.item.userStatus}</Text>) : null
                                    }
                                </TouchableOpacity>
                            </View>
                            {this.renderRequestBox(this.props.item, this.props.relations)}
                        </View>
                    </View> : null
                }
            </View>
        )
    }
};

const mapStateToProps = (state, ownProps) => {



    //console.log(frnds)
    ///console.log(ownProps.item);
    var friends = _.map(state.userPreloadData.friends, (val, uid) => {
        return { ...val, uid };
    });
    friends = _.uniqBy(friends, "uid")
    const relations = _.map(state.userPreloadData.allRelations, (val, uid) => {
        return { ...val, uid };
    });
    //console.log(relations);
    let userProfileData = state.editUserProfileInfo.userProfileInfoData;
    return { friends, userProfileData, relations };
}


const styles = StyleSheet.create({

    componentContainer: {
        flex: 1,
        backgroundColor: '#eee',
    },
    avatarContainer: {
        paddingHorizontal: 16,
        paddingVertical: 8,
    },
    avatar: {
        width: 55,
        height: 55,
        borderRadius: 27.5,
        borderColor: 'gray',
        borderWidth: 1,
    },
    userBox: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'stretch',
        paddingHorizontal: 15,
        paddingVertical: 10,
        justifyContent: 'space-between'

    },

    profilePhoto: {
        width: 60,
        height: 60,
        borderRadius: 30,
        borderColor: 'gray',
        borderWidth: 1,
    },

    showName: {
        flexDirection: 'column',
        width: 150,
    },

    RequestBox: {
        width: 124,
        height: 40,
        backgroundColor: '#FFFFFF',
        borderRadius: 3,
        justifyContent: 'center',
    },

    box2Button: {
        width: 86,
        height: 36,
        borderRadius: 3,
        backgroundColor: '#FF0408',
        bottom: 11,
        alignSelf: 'flex-end',
        right: 2,
        justifyContent: 'center'
    },

    likeicon: {
        height: 22,
        width: 22,
        resizeMode: 'stretch',
        alignSelf: 'flex-start',
        left: 10,
        top: 18,
    },

    frienshipStatus: {
        fontSize: 15,
        textAlign: 'center',
        color: '#fff',
    }
});

export default connect(mapStateToProps, { sendFriendRequest, getAllRelations })(SearchUserListItem);   