import React , {Component} from 'react'
import {StyleSheet ,View , Text, Image ,TouchableOpacity} from 'react-native'
import {Font} from 'expo'

export default class SearchEventListItem extends Component{

    state = {
        fontLoaded: true,
    };

    
    rendertalkBubble(){
        if(this.props.item.isHighlighted === true) 
            return (
                <TouchableOpacity style = {{width: 110, height: 80, justifyContent: 'center', alignItems: 'center'}}>

                    <Image source={require('../assets/eventbox.png')} style={{width: 110, height: 80, resizeMode: 'stretch', position: 'relative'}} />
                    <View style={{position: 'absolute', width: 80, right: 10}}>
                        {
                            this.state.fontLoaded ? ( <Text style={{textAlign: 'center', color: '#fff',  fontSize: 13, fontFamily: 'coolvetica'}}>{this.props.item.eventName}</Text> ) : null
                        }
                        {
                            this.state.fontLoaded ? ( <Text style={{textAlign: 'center', fontSize: 6, color: '#fff', fontFamily: 'coolvetica'}}>{this.props.item.eventStartTime}-{this.props.item.eventEndTime}</Text> ) : null
                        }
                    </View>

                </TouchableOpacity>
            )
            return (
                <TouchableOpacity style = {{width: 110, height: 80, justifyContent: 'center', alignItems: 'center'}}>

                    <Image source={require('../assets/Shape.png')} style={{width: 110, height: 80, resizeMode: 'stretch', position: 'relative'}} />
                    <View style={{position: 'absolute', width: 80, right: 10}}>
                        {
                            this.state.fontLoaded ? ( <Text style={{textAlign: 'center', color: '#fff',  fontSize: 13, fontFamily: 'coolvetica'}}>{this.props.item.eventName}</Text> ) : null
                        }
                        {
                            this.state.fontLoaded ? ( <Text style={{textAlign: 'center', fontSize: 6, color: '#fff', fontFamily: 'coolvetica'}}>{this.props.item.eventStartTime}-{this.props.item.eventEndTime}</Text> ) : null
                        }
                    </View>

                </TouchableOpacity>
            )  
        }

    render (){
        return(
            <View style ={styles.componentContainer}>
                <View style ={styles.componentsBox}>
                    <TouchableOpacity>
                        <Image source={require('../assets/avatar.png')} style={styles.profilePhoto}/>      
                    </TouchableOpacity>
               
                    <View style = {styles.showName}>
                        <TouchableOpacity>                       
                            {
                                this.state.fontLoaded ? ( <Text style = {{fontSize: 16, color: '#FF1004',  flexDirection:'row', fontFamily: 'coolvetica'}}>{this.props.item.createdBy.firstName} {this.props.item.createdBy.lastName}</Text> ) : null
                            }
                        </TouchableOpacity>
                        <TouchableOpacity>
                            {
                                this.state.fontLoaded ? ( <Text style = {{fontSize: 8, color: '#000', fontFamily: 'coolvetica'}}>You are tag in Jon's Party</Text> ) : null
                            }
                        </TouchableOpacity>
                    </View> 
                                                        
                    {this.rendertalkBubble()}                                                                                
                </View>                                                      
            </View>  
         
        )
    }
}

const styles = StyleSheet.create({
    componentContainer:{
        flex: 1,   
        backgroundColor: '#DEDEDE',
        paddingHorizontal: 15,
        paddingBottom: 5 
    },

    componentsBox:{
        height: 120,
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'stretch',
        justifyContent: 'space-between',   
    },

    profilePhoto :{
        width: 60,
        height: 60,
        resizeMode: 'stretch',
    },
    
    showName:{
        flexDirection: 'column',
        width: 130,
    },   
})