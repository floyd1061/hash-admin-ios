import React, { Component } from 'react';
import {
    View,
    TextInput,
    Text,
    StyleSheet,
    FlatList,
    Image,
    TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';
import { Ionicons } from '@expo/vector-icons';
import layout from '../../layout';
import { InstantSearch } from 'react-instantsearch/native';
import { connectSearchBox, connectInfiniteHits } from 'react-instantsearch/connectors';
import ListItem from './SearchUserListItem';
import { Constants } from 'expo';
import { Actions } from 'react-native-router-flux';

export default class SearchTabView extends Component {
    render() {
        return (
            <View style={styles.mainContainer}>
                <InstantSearch
                    appId="5IM80J1Q51"
                    apiKey="d714a548cce5aedf245dc58f062c8215"
                    indexName="users"
                >
                    <View style={styles.searchContainer}>
                        <ConnectedSearchBar />
                    </View>
                    <ConnectedHits />
                </InstantSearch>
            </View>
        )
    }
}

class SearchBar extends Component {
    _textInput: TextInput;

    state = {
        isFocused: false,
        showHeaderOption: false, fontLoaded: true
    }

    render() {
        const { isFocused } = this.state;

        return (
            <View style={styles.background}>
                <View style={styles.headerContaineer}>
                    
                    <View style={styles.headerBox}>
                        <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => Actions.pop()} style={{ flexDirection: 'row', height: 40, alignContent: 'flex-start' }}>
                            <Ionicons name="ios-arrow-back" size={40}  style={{ paddingRight: 15 }} />
                        </TouchableOpacity>
                            <TouchableOpacity>
                                <Image source={require('../assets/search.png')} style={styles.searchIcon} />
                            </TouchableOpacity>
                            {
                                this.state.fontLoaded ? (<TextInput
                                    returnKeyType='done'
                                    clearButtonMode='while-editing'
                                    onFocus={this._onFocus}
                                    onBlur={this._onBlur}
                                    style={styles.textInput}
                                    placeholder="Search Users & Events"
                                    onChangeText={text => this.props.refine(text)}
                                    value={this.props.currentRefinement}
                                    underlineColorAndroid="transparent"
                                    autoCorrect={false}
                                    autoFocus
                                />) : null
                            }
                        </View>

                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end' }}>
                            <TouchableOpacity>
                                <Image source={require('../assets/micIcon.png')} style={styles.microPhn} />
                            </TouchableOpacity>

                            {/* <TouchableOpacity onPress={() => { this.setState({ showHeaderOption: !this.state.showHeaderOption }) }}>
                                <Image source={require('../assets/dropdownButton.png')} style={styles.dropDrown} />
                            </TouchableOpacity> */}
                        </View>
                    </View>
                </View>
                {this.state.showHeaderOption ?
                    <View style={styles.headerOptionArea}>
                        <View style={{ position: 'relative', flex: 1 }}>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? (<Text style={styles.optionText}>View most searched events</Text>) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? (<Text style={styles.optionText}>View most searched profiles</Text>) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity disabled={true}>
                                {
                                    this.state.fontLoaded ? (<Text style={styles.optionText}>Go to most searched days</Text>) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? (<Text style={styles.optionText}>Go to most searched days</Text>) : null
                                }
                            </TouchableOpacity>
                        </View>
                    </View> : null
                }
            </View>

        )
    }

    focus() {
        this._textInput && this._textInput.focus();
    }

    blur() {
        this._textInput && this._textInput.blur();
    }

    _onFocus = () => {
        this.setState({ isFocused: true });
        this.props.onFocus && this.props.onFocus();
    }

    _onBlur = () => {
        this.setState({ isFocused: false });
        this.props.onBlur && this.props.onBlur();
    };
}

SearchBar.propTypes = {
    refine: PropTypes.func.isRequired,
    currentRefinement: PropTypes.string,
}

const ConnectedSearchBar = connectSearchBox(SearchBar)

class Hits extends Component {
    renderItem({ item }) {
        return <ListItem item={item} />
    }
    render() {
        ///console.log(this.props.hits);
        const hits = this.props.hits.length > 0 ?
       
            <FlatList
                data={this.props.hits}
                renderItem={this.renderItem}
                keyExtractor={item => item.objectID}
                initialNumToRender={20}
            /> : null;
        return hits;
    };
};

Hits.propTypes = {
    hits: PropTypes.array.isRequired,
    refine: PropTypes.func.isRequired,
    hasMore: PropTypes.bool.isRequired,
};

const ConnectedHits = connectInfiniteHits(Hits);

const styles = StyleSheet.create({

    mainContainer: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        paddingTop: Constants.statusBarHeight
    },

    searchContainer: {
        width: layout.window.width,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },

    logo: {
        height: 20,
        width: 20
    },

    textInput: {
        height: 40,
        fontSize: 15,
        width: layout.window.width / 2,
        marginLeft: 10,
        fontFamily: 'coolvetica'
    },

    headerContaineer: {
        width: layout.window.width,
        backgroundColor: '#FF1004',
        alignItems: 'center',

    },

    headerBox: {
        backgroundColor: '#fff',
        height: 60,
        width: layout.window.width,
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 20,
        borderColor: 'gray',
        borderBottomWidth: 1
    },

    searchIcon: {
        height: 40,
        width: 40,
        resizeMode: 'stretch',
    },

    microPhn: {
        height: 40,
        width: 30,
        resizeMode: 'stretch',
    },

    dropDrown: {
        marginLeft: 10,
        height: 35,
        width: 35,
        resizeMode: 'stretch',
    },

    headerOptionArea: {
        width: 200,
        height: 150,
        backgroundColor: '#fff',
        position: 'absolute',
        right: 22,
        top: 130,
        borderColor: '#FD0607',
        borderWidth: 2,
        borderRadius: 3,
        borderTopWidth: 0,
        alignItems: 'center',

    },

    optionText: {
        paddingVertical: 8,
        paddingHorizontal: 8,
        color: '#000',
        fontFamily: 'coolvetica'
    }
})

