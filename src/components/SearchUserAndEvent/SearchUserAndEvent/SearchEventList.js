import React, { Component } from 'react'
import { ListView , View , StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import SearchEventListItem from './SearchEventListItem'

class  SearchEventList extends Component{
    componentWillMount(){
        const ds = new ListView.DataSource({
            rowHasChanged: (r1 , r2) => r1 !== r2
        })
        this.dataSource = ds.cloneWithRows(this.props.searcheventdata);
    }

    renderRow(item) {
        return  <SearchEventListItem item = {item} />;
    }

    render(){     
        return( 
            <View style = {styles.container}> 
                <ListView style={{flex:1}}   
                    dataSource={this.dataSource}
                    renderRow={this.renderRow}   
                    initialListSize={10}    
                />
            </View>
        )
    }    
}

const styles = StyleSheet.create({
    container:{
       flex:1,
    }
})

const mapStateToProps = (state ) => {
    return { searcheventdata: state.searchEvents };
};

export default connect (mapStateToProps)(SearchEventList) ; 
