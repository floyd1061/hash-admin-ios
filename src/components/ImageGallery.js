import React, { Component } from 'react'
import { Text, View, ScrollView, StyleSheet } from 'react-native'
import ImageGalleryItem from './ImageGalleryItem'
import { StackNavigator } from 'react-navigation';

export default class ImageGallery extends Component {

  renderImages() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    return params.story.story_contents.map(story_content =>
      <ImageGalleryItem key={story_content.id} story_content={story_content} navigate={navigate} />
    );
  };
  render() {

    return (
      <View style={styles.container}>
        <ScrollView>
          {this.renderImages()}
        </ScrollView>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 15
  },
});