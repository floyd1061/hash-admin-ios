import React, { Component, PureComponent } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import { CardSection } from './common'
import AddEventModal from './UserProfileTimeline/AddEventModal'

import Svg, {
    LinearGradient,
    Path,
    Defs,
    Stop
} from 'react-native-svg'

import Modal from "react-native-modal";

const widthSvg = 170.4683;
export default class ListItemSvg extends React.PureComponent {
    state = { isModalVisible: false }
    _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });

    renderEventBox(item) {
        const { svgSegmentId } = item;
        switch (svgSegmentId) {
            case 0:
                return this.renderSectionZero()
            case 1:
                return this.renderSectionOne()
            case 2:
                return this.renderSectionTwo()
            case 3:
                return this.renderSectionThree()

        }
    }
    renderSectionZero() {
        return (

            <Svg
                height={134.1965}
                width={widthSvg}
            >
                <Defs>
                    <LinearGradient id="1bba36d6-cdb9-4192-aec6-0ea45e3d9d71" x1="39.542" y1="67.312" x2="123.2739" y2="67.312" gradientUnits="userSpaceOnUse">
                        <Stop offset="0" stopColor="#eaeaea" />
                        <Stop offset="1" stopColor="#eaeaea" />
                    </LinearGradient>
                </Defs>

                <Path fill="none"
                    stroke="url(#1bba36d6-cdb9-4192-aec6-0ea45e3d9d71)"
                    strokeMiterlimit={10}
                    strokeWidth={28.125}
                    d="M109.2117,134.759c-.0477-7.3824-5.4817-14.37-12.4589-19.8618-6.9415-5.5369-15.432-10.1508-21.6281-13.8414C68.91,97.396,61.0906,90.161,56.1984,78.2022,51.3658,66.2192,54.6918,24.7819,55.09,0" />
                <Path fill="none"
                    stroke="#ccc"
                    strokeMiterlimit={10}
                    strokeWidth={19.6875}
                    d="M109.2117,134.759c-.0477-7.3824-5.4817-14.37-12.4589-19.8618-6.9415-5.5369-15.432-10.1508-21.6281-13.8414C68.91,97.396,61.0906,90.161,56.1984,78.2022,51.3658,66.2192,54.6918,24.7819,55.09,0" />
            </Svg>

        );
    }
    renderSectionOne() {


        return (
            <Svg
                height={279.1393}
                width={widthSvg}
            >
                <Defs>
                    <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c0" x1="99.005" x2="99.005" y2="279.1393" gradientUnits="userSpaceOnUse">
                        <Stop offset="0" stopColor="#eaeaea" />
                        <Stop offset="1" stopColor="#eaeaea" />
                    </LinearGradient>
                    <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc" x1="99.005" x2="99.005" y2="279.1393" gradientUnits="userSpaceOnUse">
                        <Stop offset="0" stopColor="#cecfd0" />
                        <Stop offset="1" stopColor="#eaeaea" />
                    </LinearGradient>

                </Defs>
                <Path fill="none"
                    stroke="url(#df0272b4-af27-4498-a012-8e6b05c8c6c0)"
                    strokeMiterlimit={10}
                    strokeWidth={28.125}
                    d="M118.6155,279.1393c0-5.754-2.3026-11.2582-6.6108-16.3436s-10.6217-10.3145-18.6439-16.6435c-5.8057-4.58-12.1193-9.5382-16.9815-15.0427s-8.273-11.5557-8.273-18.3229,3.6725-12.9841,9.3731-18.5171,13.43-10.3822,21.5422-14.4143c10.9943-5.4644,21.1179-10.7478,28.4929-16.5108s12.0014-12.0054,12.0014-19.3879c0-7.4547-3.6742-13.6561-11.3391-18.4379h-.0333c-7.5292-5.2292-18.7776-9.9336-33.6547-15.2894-11.1312-4.0074-20.13-9.1462-26.3458-14.9386S58.4941,83.052,58.4941,76.43a33.8539,33.8539,0,0,1,5.4971-18.9718c3.9369-6.1067,10.25-12.0778,19.7575-18.1573S99.6232,27.087,103.6144,20.6273A37.9725,37.9725,0,0,0,109.22,0"
                />
                <Path fill="none"
                    stroke="url(#0629d1b3-c525-43f9-b7a3-ad319f4252fc)"
                    strokeMiterlimit={10}
                    strokeWidth={19.6875}
                    d="M118.6155,279.1393c0-5.754-2.3026-11.2582-6.6108-16.3436s-10.6217-10.3145-18.6439-16.6435c-5.8057-4.58-12.1193-9.5382-16.9815-15.0427s-8.273-11.5557-8.273-18.3229,3.6725-12.9841,9.3731-18.5171,13.43-10.3822,21.5422-14.4143c10.9943-5.4644,21.1179-10.7478,28.4929-16.5108s12.0014-12.0054,12.0014-19.3879c0-7.4547-3.6742-13.6561-11.3391-18.4379h-.0333c-7.5292-5.2292-18.7776-9.9336-33.6547-15.2894-11.1312-4.0074-20.13-9.1462-26.3458-14.9386S58.4941,83.052,58.4941,76.43a33.8539,33.8539,0,0,1,5.4971-18.9718c3.9369-6.1067,10.25-12.0778,19.7575-18.1573S99.6232,27.087,103.6144,20.6273A37.9725,37.9725,0,0,0,109.22,0"
                />
            </Svg>
        );
    }
    renderSectionTwo() {


        return (
            <Svg
                height={280.4884}
                width={widthSvg}
            >
                <Defs>
                    <LinearGradient id="f5bfdeee-16ad-4168-9edd-48c395e44d65" x1="98.4425" y1="281.4884" x2="98.4425" y2="-0.5703" gradientUnits="userSpaceOnUse">
                        <Stop offset="0" stopColor="#cecfd0" />
                        <Stop offset="1" stopColor="#eaeaea" />
                    </LinearGradient>
                    <LinearGradient id="686a64e9-a46d-4086-98cb-9ddc3a5017df" x1="35.4411" y1="140.4602" x2="161.4439" y2="140.4602" gradientUnits="userSpaceOnUse">
                        <Stop offset="0" stopColor="#e4e4e4" />
                        <Stop offset="1" stopColor="#eaeaea" />
                    </LinearGradient>

                </Defs>
                <Path
                    fill="none"
                    stroke="url(#f5bfdeee-16ad-4168-9edd-48c395e44d65)"
                    strokeMiterlimit={10}
                    strokeWidth={28.125}
                    d="M151.6,281.4884c0-6.9844-2.7758-11.9966-8.781-16.7826s-15.24-9.3457-28.1573-15.4253c-11.1329-5.24-21.6012-10.8126-29.2862-16.7856S72.7888,220.1491,72.7888,213.31s2.3042-12.322,6.7855-17.6869,11.14-10.6122,19.848-16.9815c6.19-4.5268,11.6324-9.376,15.5277-14.9753a33.2293,33.2293,0,0,0,6.2432-19.4757c0-7.5272-3.0117-13.0731-8.663-17.8635s-13.9423-8.8253-24.501-13.3308c-11.3187-4.83-22.0048-9.9321-29.8612-16.478S45.2848,81.9827,45.2848,71.3794c0-6.8033,3.302-12.9192,9.58-18.7506S70.3957,41.25,82.2986,35.5841c9.66-4.5986,18.7664-9.0756,25.4584-14.6827S118.6117,6.2764,118.6155-.5625" />
                <Path
                    fill="none"
                    stroke="url(#686a64e9-a46d-4086-98cb-9ddc3a5017df)"
                    strokeMiterlimit={10}
                    strokeWidth={19.6875}
                    d="M151.6,281.4884c0-6.9844-2.7758-11.9966-8.781-16.7826s-15.24-9.3457-28.1573-15.4253c-11.1329-5.24-21.6012-10.8126-29.2862-16.7856S72.7888,220.1491,72.7888,213.31s2.3042-12.322,6.7855-17.6869,11.14-10.6122,19.848-16.9815c6.19-4.5268,11.6324-9.376,15.5277-14.9753a33.2293,33.2293,0,0,0,6.2432-19.4757c0-7.5272-3.0117-13.0731-8.663-17.8635s-13.9423-8.8253-24.501-13.3308c-11.3187-4.83-22.0048-9.9321-29.8612-16.478S45.2848,81.9827,45.2848,71.3794c0-6.8033,3.302-12.9192,9.58-18.7506S70.3957,41.25,82.2986,35.5841c9.66-4.5986,18.7664-9.0756,25.4584-14.6827S118.6117,6.2764,118.6155-.5625" />

            </Svg>
        );
    }
    renderSectionThree() {

        return (
            <Svg
                height={268.2128}
                width={widthSvg}
            >
                <Defs>
                    <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b88" x1="48.1024" y1="134.1382" x2="161.4439" y2="134.1382" gradientUnits="userSpaceOnUse">
                        <Stop offset="0" stopColor="#eaeaea" />
                        <Stop offset="1" stopColor="#eaeaea" />
                    </LinearGradient>
                </Defs>
                <Path
                    fill="none"
                    stroke="#ccc"
                    strokeWidth={28.125}
                    d="M151.6,0c0,6.9843-3.194,13.47-9.7166,19.7885S125.51,32.2591,112.1949,38.5764c-14.95,7.0928-24.9281,12.8106-31.1691,18.51s-9,11.7053-8.7445,18.9641l-.0525-.5625c0,7.0206,3.4658,13.6217,8.8495,19.4714s12.6855,10.948,20.3573,14.963c9.49,4.9661,15.8314,8.8786,19.8,13.6485s5.5654,10.3972,5.5654,18.7929-2.1046,15.5247-7.82,21.722-15.04,11.4626-29.4817,16.131c-9.3437,3.02-17.9977,6.8926-24.3157,11.8869h0a20.7694,20.7694,0,0,0-6.3328,9.7326,23.3523,23.3523,0,0,0-.8722,5.8275,9.4383,9.4383,0,0,1-.0332,1.1541h0c0,5.8374,2.785,11.3849,6.8185,16.4516a27.96,27.96,0,0,0,4.3976,4.365l4.7187,3.7868h0s3.0063,1.8889,3.8457,2.3827c6.0568,3.5627,13.9007,7.9326,20.3905,13.1092,6.9772,5.492,11.0552,11.9169,11.1029,19.2993" />
                <Path
                    fill="none"
                    stroke="url(#551a812d-0e06-4ac7-abb0-e4d828b82b88)"
                    strokeWidth={19.6875}
                    d="M151.6,0c0,6.9843-3.194,13.47-9.7166,19.7885S125.51,32.2591,112.1949,38.5764c-14.95,7.0928-24.9281,12.8106-31.1691,18.51s-9,11.7053-8.7445,18.9641l-.0525-.5625c0,7.0206,3.4658,13.6217,8.8495,19.4714s12.6855,10.948,20.3573,14.963c9.49,4.9661,15.8314,8.8786,19.8,13.6485s5.5654,10.3972,5.5654,18.7929-2.1046,15.5247-7.82,21.722-15.04,11.4626-29.4817,16.131c-9.3437,3.02-17.9977,6.8926-24.3157,11.8869h0a20.7694,20.7694,0,0,0-6.3328,9.7326,23.3523,23.3523,0,0,0-.8722,5.8275,9.4383,9.4383,0,0,1-.0332,1.1541h0c0,5.8374,2.785,11.3849,6.8185,16.4516a27.96,27.96,0,0,0,4.3976,4.365l4.7187,3.7868h0s3.0063,1.8889,3.8457,2.3827c6.0568,3.5627,13.9007,7.9326,20.3905,13.1092,6.9772,5.492,11.0552,11.9169,11.1029,19.2993" />
            </Svg>
        );
    }
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={this._toggleModal}
                >
                    <CardSection style={styles.profileCardStyle}>
                        {this.renderEventBox(this.props.item)}
                    </CardSection>
                </TouchableOpacity>
                <Modal isVisible={this.state.isModalVisible}>
                    <View style={{ flex: 1 }}>
                        <AddEventModal />
                    </View>
                </Modal>

            </View>

        )
    }
    callAddEventDialog() {
        return (
            <AddEventDialog isModalVisible={true} />
        )
    }

}

const styles = {
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    profileCardStyle: {
        borderBottomWidth: 0,
        padding: 0,
        justifyContent: 'center',
        alignItems: 'center'
    }
}
