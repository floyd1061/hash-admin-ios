import React , {Component} from 'react'
import { View , StyleSheet } from 'react-native'   
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import FollowingUsers from './FollowingUsers';
import FollowerUsers from './FollowerUsers';
import SearchNavBar from './SearchNavBar';
  
export default class FollowMainView extends Component{
    state = {
        index: 0,
        routes: [
          { key: 'following', title: 'Following' },
          { key: 'follower', title: 'Follower' },
        ],
    };
    
    _handleIndexChange = index => this.setState({ index });

    _renderHeader  = props  => {
        const inputRange = props.navigationState.routes.map((x, i) => i);
        return(
            <View style={styles.container}>
                <SearchNavBar />
                <TabBar style ={styles.Talkbar } {...props}/>
            </View>
        )
    };
    
    _renderScene = SceneMap({        
        following: FollowingUsers,
        follower: FollowerUsers,
    });
      
    render() {
        return (
            <TabViewAnimated
                    navigationState={this.state}
                    renderScene={this._renderScene}
                    renderHeader={this._renderHeader}
                    onIndexChange={this._handleIndexChange}
                >
            </TabViewAnimated>
        )
    }
}

const styles= StyleSheet.create({
    Talkbar :{
        backgroundColor:'#FF1004',
    }
})
