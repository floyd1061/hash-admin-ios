import React, { Component } from 'react'
import { View } from 'react-native'
import SingleUserItem from './SingleUserItem'

export default class FollowingUsers extends Component{
    
    render(){
        return(
            <View style = {[styles.container,{position: 'relative'}]}>
                <View style = {{flex: 1, flexDirection: 'row', paddingTop: 20, flexWrap:'wrap', justifyContent: 'space-around'}}>
                    <SingleUserItem/>
                    <SingleUserItem/>
                    <SingleUserItem/>
                    <SingleUserItem/>
                </View>
            </View>
        )
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#DDDDDD',
    }
}