import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import {Font} from 'expo'

class SingleUserItem extends Component {

    state = {
        fontLoaded: true,
    };

   

    renderOnlineSign(){
        return <View style = {styles.onlineSign}></View>
    }

    render() {
        return (
            <TouchableOpacity style = {styles.singleUser}>
                <View style = {[styles.imgWrapper, {width: 120, height: 120, borderRadius: 60}]}>
                    <Image source={require('./assets/avatar.png')} style={{width: 120, height: 120, resizeMode:'stretch'}} />
                    {this.renderOnlineSign()}
                </View>
                <TouchableOpacity>
                    {
                        this.state.fontLoaded ? ( <Text style = {{fontSize: 18, marginTop: 10, color: '#566573', fontFamily: 'coolvetica'}}>Jhon Smith</Text> ) : null
                    }
                </TouchableOpacity>
                <TouchableOpacity>
                    {
                        this.state.fontLoaded ? ( <Text style = {{fontSize: 18, marginTop: 0, color: '#FA1801', fontFamily: 'coolvetica'}}>Descirption</Text> ) : null
                    }
                </TouchableOpacity>
            </TouchableOpacity>
        )
    }
}

const styles = {

    singleUser: {
        position: 'relative',
        width: 160,      
        alignItems: 'center',
        marginBottom: 20,
        padding: 5
    },

    imgWrapper: {
        position: 'relative',
    },

    onlineSign: {
        width: 20,
        height: 20,
        backgroundColor: '#FA1801',
        borderRadius: 20,
        borderWidth: 2,
        borderColor: '#fff',
        position: 'absolute',
        right: 7,
        bottom: 5
    },
}

export default SingleUserItem;