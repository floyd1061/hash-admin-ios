import React, { Component } from 'react'
import { View } from 'react-native'
import SingleUserItem from './SingleUserItem'

export default class FollowerUsers extends Component{
    
    render(){
        return(
            <View style = {styles.container}>
                <View style = {{flex: 1, paddingTop: 20, flexDirection: 'row', flexWrap:'wrap', justifyContent: 'space-around'}}>
                    <SingleUserItem/>
                    <SingleUserItem/>
                    <SingleUserItem/>
                    <SingleUserItem/>
                </View>
            </View>
        )
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#DDDDDD',
    }  
}