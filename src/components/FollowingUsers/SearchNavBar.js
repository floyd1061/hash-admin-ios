import React, { Component } from 'react';
import { StyleSheet, View, TextInput, Image, TouchableOpacity, Text } from 'react-native';
import { Font } from 'expo'

export default class SearchNavBar extends Component {

    constructor(props) {
        super(props);
        this.state = { showHeaderOption: false, fontLoaded: true };
    }


   
    render() {
        return (
            <View style={styles.headerContainer}>
                <View style={styles.searchBox}>
                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity>
                            <Image source={require('./assets/explore.png')} style={styles.exploreIcon} />
                        </TouchableOpacity>

                        {
                            this.state.fontLoaded ? (<TextInput
                                style={{ alignSelf: 'stretch', flex: .7, flexDirection: 'row', paddingHorizontal: 10, fontSize: 15, fontFamily: 'coolvetica' }}
                                underlineColorAndroid="transparent"
                                placeholder="Search Users"
                            />) : null
                        }
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <TouchableOpacity>
                            <Image source={require('./assets/mic.png')} style={styles.micIcon} />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => { this.setState({ showHeaderOption: !this.state.showHeaderOption }) }}>
                            <Image source={require('./assets/dropdown.png')} style={styles.dropdownIcon} />
                        </TouchableOpacity>
                    </View>
                </View>

                {this.state.showHeaderOption ?
                    <View style={styles.headerOptionArea}>
                        <View style={{ position: 'relative', flex: 1 }}>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? (<Text style={styles.optionText}> Most Favourites </Text>) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? (<Text style={styles.optionText}> Tagged Favourites </Text>) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity disabled={true}>
                                {
                                    this.state.fontLoaded ? (<Text style={styles.optionText}> Most Favourites </Text>) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? (<Text style={styles.optionText}> Latest Favourites </Text>) : null
                                }
                            </TouchableOpacity>
                        </View>
                    </View> : null
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#DEDEDE',
        alignSelf: 'stretch',
    },

    headerContainer: {
        alignSelf: 'stretch',
        backgroundColor: '#FF1303',
        height: 150,
    },

    searchBox: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        height: 60,
        alignSelf: 'stretch',
        alignItems: 'center',
        paddingHorizontal: 20,
        marginTop: 90,
        justifyContent: 'space-between'
    },

    exploreIcon: {
        height: 40,
        width: 40,
        resizeMode: 'stretch',
        alignItems: 'flex-start',
    },

    micIcon: {
        height: 40,
        width: 35,
        resizeMode: 'stretch',
        alignItems: 'flex-end',
        marginRight: 10,
    },

    dropdownIcon: {
        height: 35,
        width: 35,
        resizeMode: 'stretch',
        alignItems: 'flex-end',
    },

    headerOptionArea: {
        position: 'absolute',
        zIndex: 9999,
        width: 200,
        height: 150,
        backgroundColor: '#fff',
        position: 'absolute',
        right: 22,
        top: 150,
        borderColor: '#FD0607',
        borderWidth: 2,
        borderRadius: 3,
        borderTopWidth: 0
    },

    optionText: {
        paddingVertical: 8,
        paddingHorizontal: 8,
        color: '#000',
        fontFamily: 'coolvetica'
    }
})