import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, StyleSheet } from 'react-native'
import Card from './Card'
import CardSection from './CardSection'
import ReplyItem from './ReplyItem'

const CommentItem = ({ story_comment_and_reply, navigate }) => {
    return (
        <Card>
            <CardSection>
                <View style={styles.thumbnailContainerStyle}>
                    <Image
                        style={styles.thumbnailStyle}
                        source={story_comment_and_reply.user.profile_pic}
                    />
                </View>
                <View style={styles.headerContentStyle}>
                    <Text style={styles.headerUserTextStyle}>{story_comment_and_reply.user.user_name}</Text>
                    <Text style={styles.headerUserCommentStyle}>{story_comment_and_reply.comment_text}</Text>
                </View>
            </CardSection>
            <CardSection>
                <View style={styles.likeCommentButtonContainer}>
                    <TouchableOpacity>
                        <Text style={styles.likeCommentButtonTextStyle}>Like</Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={styles.likeCommentButtonTextStyle}>Reply</Text>
                    </TouchableOpacity>
                </View>
            </CardSection>
            {RenderReplies(story_comment_and_reply.replies)}
        </Card>
    )
}
const RenderReplies = (replies) => {
    return replies.map(reply =>
        <ReplyItem key={reply.key} reply={reply} />
    );
};
const styles = {
    
    headerContentStyle: {
        flexDirection: 'column',
        justifyContent: 'center',
        paddingBottom: -3,
    },
    thumbnailStyle: {
        height: 20,
        width: 20,
        borderRadius: 20,
    },
    thumbnailContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    headerTextContentStyle: {
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    likeCommentButtonContainer: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        marginLeft: 40,
        marginRight: 40,
        marginTop: -5
    },
    likeCommentButtonTextStyle: {
        fontSize: 6,
        fontWeight: '500',
        marginRight: 6,
        color: '#4d4f51'
    },
    headerUserTextStyle: {
        fontSize: 8,
        fontWeight: '500',
        marginRight: 6,
        color: '#4d4f51'
    },
    headerUserCommentStyle: {
        fontSize: 8,
        fontWeight: '200',
        marginRight: 6
    },
}

export default CommentItem;
