import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import Triangle from '../../components/Triangle';
import _ from 'lodash';
import { startConversationFromList, fetchEntireConversationFromDb, setPopup } from '../../actions';
import { connect } from 'react-redux'
import firebase from '@firebase/app'
import auth from '@firebase/auth'
class SingleUserItem extends Component {
    constructor(props) {
        super(props);
        this.state = { userName: '', profilePic: null, isUserOnline: false };
    }
    componentDidMount() {

        this.props.fetchEntireConversationFromDb(this.props.item.uid);


    }

    renderUserInfo() {
        // //console.log('renderUserinfo');   
    }

    resizedMessage(msg, endIndex) {
        return msg.length > endIndex ? msg.substring(0, endIndex) + '...' : msg;
    }

    getUnseenMessageCounter(reversedArray) {
        let counter = 0;
        reversedArray.forEach((element, index, array) => {
            if (typeof element.received !== 'undefined' || element.received !== null) {
                if (!element.received)
                    counter++;
                else
                    return;
            }
        });

        return counter;
    }


    renderUnseenMessage(reversedArray) {
        const { currentUser } = firebase.auth();
        if (typeof reversedArray[0].received !== 'undefined' || reversedArray[0].received !== null) {

            if (!reversedArray[0].received) {
                if (reversedArray[0].sender.uid === currentUser.uid) {
                    return null
                }
                return (
                    <View style={{ position: 'absolute', padding: 5, top: 0, right: 0, width: 160 }}>

                        <View style={{ paddingBottom: 3, paddingTop: 9, paddingRight: 6, position: 'relative' }}>

                            <View style={{ position: 'relative', backgroundColor: '#fff', borderWidth: 2, borderColor: '#FE2613', borderRadius: 3, padding: 4, height: 50 }}>
                                <Text style={{ color: '#FE2613', fontSize: 10, fontFamily: 'coolvetica' }}>{this.resizedMessage(reversedArray[0].messageBody, 45)}</Text>
                                <Text style={{ position: 'absolute', bottom: 5, right: 5, fontSize: 8, textAlign: 'right', fontFamily: 'coolvetica' }}>Sent {this.getDateFromUtcEpoch(reversedArray[0].createdAt)} at {this.getLocalTimeFromUtcEpoch(reversedArray[0].createdAt)}</Text>
                            </View>

                            <Triangle style={{ position: 'absolute', bottom: -6, left: 36 }} width={18} height={10} color={'#F91700'} direction={'down'} />

                            <View style={{ position: 'absolute', backgroundColor: '#FE1800', right: 0, top: 0, width: 20, height: 20, borderRadius: 50, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#fff', fontSize: 12, fontFamily: 'coolvetica' }}>{this.getUnseenMessageCounter(reversedArray)}</Text>
                            </View>

                        </View>

                    </View>
                )
            }
        }
    }
    getDateFromUtcEpoch(epoch) {
        var today = new Date();
        var yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
        d.setUTCSeconds(epoch);


        if (yesterday.toDateString() === d.toDateString()) {
            return 'yesterday';
        }

        if (today.toDateString() === d.toDateString()) {
            return 'today';
        }

        return d.toDateString();
    }
    getLocalTimeFromUtcEpoch(epoch) {
        var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
        d.setUTCSeconds(epoch);
        return this.tConvert(d.toLocaleTimeString());
    }
    tConvert(time) {
        // Check correct time format and split into components
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice(1);  // Remove full string match value
            time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original string
    }

    isObject(value) {
        return value && typeof value === 'object' && value.constructor === Object;
    }
    getProfilePicAndStatus(profilepictures, uid) {
        ////console.log(uid);
        if (this.isObject(profilepictures)) {
            let pps = _.map(profilepictures, (val, uid) => {
                return { ...val, uid }
            })
            if (pps.length > 0) {
                return pps[pps.length - 1].URL
            }

        } else {
            return profilepictures;
        }
        return '../assets/avatar.png';
    }
    getUsersOnlineStatus(uid) {
         let status = _.findLast(this.props.appUsers, ["uid", uid])
        
         if(status.status.state != null && status.status.state === 'online'){
            return true;
         }
         return false;
    }

    render() {

        const result = (this.props.conversation != null) ? _.map(this.props.conversation.conversation, (val, uid) => {
            return { ...val, uid };
        }) : [];

        let reversedArray = result.length > 0 ? result.reverse() : [];
        return (
            <TouchableOpacity style={[styles.singleUser]} onPress={() => {
                this.props.startConversationFromList(this.props.withWhom, this.props.item.uid, this.props.displayNameStr)
            }}>
                {reversedArray.length > 0 ? this.renderUnseenMessage(reversedArray) : null}

                <View style={styles.imgWrapper}>
                    {(this.props.withWhom.length > 0) ?

                        <Image source={{
                            uri:
                                this.getProfilePicAndStatus(this.props.withWhom[this.props.withWhom.length - 1].profilepicture, this.props.withWhom[this.props.withWhom.length - 1].uid)

                        }} style={{ width: 120, height: 120, borderRadius: 120, resizeMode: 'stretch' }} />


                        : <Image source={require('./assets/profileselected.png')} style={{ width: 120, height: 120, resizeMode: 'stretch' }} />
                    }
                    {(this.props.withWhom.length > 0) ?
                        this.getUsersOnlineStatus(this.props.withWhom[this.props.withWhom.length - 1].uid) ?
                            <View style={styles.onlineSign}></View>
                            : <View style={styles.offlineSign}></View>
                        : <View style={styles.offlineSign}></View>}

                </View>

                <Text style={{ fontSize: 14, marginTop: 10, fontFamily: 'coolvetica' }}>
                    {this.resizedMessage(this.props.displayNameStr, 12)}</Text>

                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                    <TouchableOpacity>
                        <Image source={require('./assets/callIconSmall.png')} style={{ width: 40, height: 40, resizeMode: 'stretch', marginRight: 5 }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { this.props.setPopup(!this.props.popupStatus, this.props.item.uid) }}>
                        <Image source={require('./assets/groupIconSmall.png')} style={{ width: 40, height: 40, resizeMode: 'stretch' }} />
                    </TouchableOpacity>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = {

    singleUser: {
        position: 'relative',
        width: 160,
        height: 270,
        alignItems: 'center',
        paddingTop: 67
    },

    imgWrapper: {
        position: 'relative',
    },
    offlineSign: {
        width: 20,
        height: 20,
        backgroundColor: 'gray',
        borderRadius: 20,
        borderWidth: 2,
        borderColor: '#fff',
        position: 'absolute',
        right: 7,
        bottom: 5
    },
    onlineSign: {
        width: 20,
        height: 20,
        backgroundColor: '#FA1801',
        borderRadius: 20,
        borderWidth: 2,
        borderColor: '#fff',
        position: 'absolute',
        right: 7,
        bottom: 5
    },
}

const mapStateToProps = (state, ownProps) => {
    const { currentUser } = firebase.auth();
    let conversations = _.map(state.messages.conversationData, (val, uid) => {
        return { ...val, uid }
    });
    console.log(conversations);
    let withWhom = [];
    const appUsers = _.map(state.userPreloadData.applicationUsers, (val, uid) => {
        return { ...val, uid }
    })
    let conversation = _.find(conversations, ["uid", ownProps.item.uid])
    let conversationWithWhom = null;
    let displayNameStr = "";
    if (conversation != null) {
        conversationWithWhom = _.map(conversation.conversationMetaData.withWhom);
        conversationWithWhom.forEach(element => {
            if (element.uid !== currentUser.uid) {
                displayNameStr = displayNameStr + ',' + element.displayName;
                withWhom.push(element);
            }

        });

        displayNameStr = displayNameStr.substr(1);
    }
    return {
        conversation: conversation,
        displayNameStr: displayNameStr,
        popupStatus: state.chatPopup.status,
        withWhom: withWhom,
        appUsers: appUsers

    }
}

export default connect(mapStateToProps, { startConversationFromList, fetchEntireConversationFromDb, setPopup })(SingleUserItem);
