import React, { PureComponent } from "react";
import { View, Text, FlatList, ActivityIndicator, TouchableOpacity } from "react-native";
import { List, ListItem, SearchBar } from "react-native-elements";
import _ from 'lodash';
import firebase from '@firebase/app'
import auth from '@firebase/auth'
import { inviteToEvent, findConversationByFriendId, getallfriends } from '../../../actions'
import { connect } from 'react-redux'
class UserList extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      //data: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false
    };
  }

  componentDidMount() {
    const { currentUser } = firebase.auth()
    this.props.getallfriends(currentUser.uid);
    this.makeRemoteRequest();
  }

  makeRemoteRequest = () => {

    this.setState({ loading: true });
    this.setState({
      error: null,
      loading: false,
      refreshing: false
    });
  };

  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        seed: this.state.seed + 1,
        refreshing: true
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  handleLoadMore = () => {
    this.setState(
      {
        page: this.state.page + 1
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 0.4,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    );
  };

  renderHeader = () => {
    return <SearchBar fontFamily='coolvetica' placeholder="Search..." lightTheme round />;
  };

  renderFooter = () => {
    if (!this.state.loading) return (
      <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
        <TouchableOpacity style={{ padding: 10, }} onPress={this.props.shareMsg}>
          <Text style={{ color: 'red', fontFamily: 'coolvetica' }}>Cancel</Text>
        </TouchableOpacity>
      </View>
    );

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
  findConversationbyUserId(friend) {
    this.props.shareMsg();
    this.props.findConversationByFriendId(friend);
  }
  getProfilePic(profilepictures) {
    let pps = _.map(profilepictures, (val, uid) => {
      return { ...val, uid }
    })
    if (pps.length > 0) {
      return pps[pps.length - 1].URL
    }
    return '../assets/avatar.png';
  }

  render() {

    return (
      <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
        <FlatList
          data={this.props.data}
          renderItem={({ item }) => (

            < ListItem

              fontFamily='coolvetica'
              roundAvatar
              rightIcon={{ name: 'chat', color: "#FF3C00" }}
              onPressRightIcon={() => this.findConversationbyUserId(item)}
              title={`${item.displayName}`}
              subtitle={item.email}
              avatar={{ uri: this.getProfilePic(item.profilepicture) }}
              containerStyle={{ borderBottomWidth: 0 }}

            />
          )}
          keyExtractor={item => item.uid}
          ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={this.renderHeader}
          ListFooterComponent={this.renderFooter}
          onRefresh={this.handleRefresh}
          refreshing={this.state.refreshing}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={50}
        />

      </List>
    );
  }
}
const mapStateToProps = (state) => {
  const userProfileData = state.editUserProfileInfo.userProfileInfoData;
  let friends = _.map(state.userPreloadData.friends, (val, uid) => {
    return { ...val, uid };
  });

  friends = _.without(friends, undefined);
  friends = _.uniqBy(friends, "uid");

  let comparedAttendeeFriends = friends;

  return { userProfileData: userProfileData, friends: friends, data: comparedAttendeeFriends };
};
export default connect(mapStateToProps, { findConversationByFriendId, inviteToEvent, getallfriends })(UserList);