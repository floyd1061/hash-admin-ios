import React, { PureComponent } from 'react'
import { View, Text, Image, TouchableOpacity, Dimensions } from 'react-native'
import Triangle from '../../components/Triangle';
import _ from 'lodash';
import { Actions } from 'react-native-router-flux'
import { startConversationFromList, setPopup } from '../../actions';
import { connect } from 'react-redux'
import firebase from '@firebase/app'
import auth from '@firebase/auth'
const numColumns = 3;
const { width } = Dimensions.get('window');

class SingleUserItem extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { userName: '', profilePic: null, isUserOnline: false, withWhom: [], displayNameString: "" };

  }

  /* shouldComponentUpdate(nexProps, nextState) {
    console.log(this.props.item.conversationMetaData, nexProps.item.conversationMetaData)
    return true;
  } */

  renderUserInfo() {
    // //console.log('renderUserinfo');   
  }

  resizedMessage(msg, endIndex) {
    return msg.length > endIndex ? msg.substring(0, endIndex) + '...' : msg;
  }

  getUnseenMessageCounter(reversedArray) {
    let counter = 0;
    reversedArray.forEach((element, index, array) => {
      if (typeof element.received !== 'undefined' || element.received !== null) {
        if (!element.received)
          counter++;
        else
          return;
      }
    });

    return counter;
  }
  getProfilePicAndStatus(reversedArray) {
    const { currentUser } = firebase.auth();
    let status = false;
    //console.log(reversedArray);
    let pp = '../assets/avatar.png';
    let displayNameStr = "";
    let userStatus = "";
    let user = _.findLast(this.props.friends, ["uid", this.props.withWhom[0].uid]);

    let pps = _.map(user.profilepicture, (val, uid) => { return { ...val, uid } })
    //console.log(pps);
    pp = pps[pps.length - 1].URL;
    let onOff = _.findLast(this.props.appUsers, ["uid", user.uid])
    if (onOff.status.state != null && onOff.status.state === 'online') {
      status = true;
    }
    displayNameStr = user.displayName;
    userStatus = user.userStatus
    for (let i = 0; i <= reversedArray.length - 1; i++) {
      //console.log(reversedArray[i].sender.uid, currentUser.uid);
      if (reversedArray[i].sender.uid === currentUser.uid) {

      } else {
        pp = reversedArray[i].sender.avatar;
        let onOff = _.findLast(this.props.appUsers, ["uid", reversedArray[i].sender.uid])
        if (onOff.status.state != null && onOff.status.state === 'online') {
          status = true;
        }
      }

      /* pp = reversedArray[i].sender.avatar;
      let onOff = _.findLast(this.props.appUsers, ["uid", reversedArray[i].sender.uid])
      if (onOff.status.state != null && onOff.status.state === 'online') {
        status = true;
      } */
    }

    return { profilePicture: pp, status: status, displayName: displayNameStr, userStatus: userStatus };
  }


  renderUnseenMessage(reversedArray) {
    const { currentUser } = firebase.auth();
    if (typeof reversedArray[0].received !== 'undefined' || reversedArray[0].received !== null) {

      if (!reversedArray[0].received) {
        if (reversedArray[0].sender.uid === currentUser.uid) {
          return null
        }
        return (
          <View style={{ position: 'absolute', padding: 5, top: 0, right: 0, zIndex: 300, width: width / numColumns - 20 }}>

            <View style={{ paddingBottom: 3, paddingTop: 9, paddingRight: 6, position: 'relative' }}>

              <View style={{ position: 'relative', backgroundColor: '#fff', borderWidth: 2, borderColor: '#FE2613', borderRadius: 3, padding: 4, height: 50 }}>
                {(reversedArray[0].type === "text") ?
                  <Text style={{ color: '#FE2613', fontSize: 10, fontFamily: 'coolvetica' }}>{this.resizedMessage(reversedArray[0].messageBody, 30)}</Text>
                  : (reversedArray[0].type === "map") ?
                    <Text style={{ color: '#FE2613', fontSize: 10, fontFamily: 'coolvetica' }}>Sent current Location</Text>
                    : (reversedArray[0].type === "image") ?
                      <Text style={{ color: '#FE2613', fontSize: 10, fontFamily: 'coolvetica' }}>Sent an image</Text> :
                      <Text style={{ color: '#FE2613', fontSize: 10, fontFamily: 'coolvetica' }}>Sent a video</Text>
                }
                <Text style={{ position: 'absolute', bottom: 5, right: 5, fontSize: 8, textAlign: 'right', fontFamily: 'coolvetica' }}>Sent {this.getDateFromUtcEpoch(reversedArray[0].createdAt)} at {this.getLocalTimeFromUtcEpoch(reversedArray[0].createdAt)}</Text>
              </View>

              <Triangle style={{ position: 'absolute', bottom: -6, left: 36 }} width={18} height={10} color={'#F91700'} direction={'down'} />

              <View style={{ position: 'absolute', backgroundColor: '#FE1800', right: 0, top: 0, width: 20, height: 20, borderRadius: 50, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#fff', fontSize: 12, fontFamily: 'coolvetica' }}>{this.getUnseenMessageCounter(reversedArray)}</Text>
              </View>

            </View>

          </View>
        )
      }
    }
  }
  getDateFromUtcEpoch(epoch) {
    var today = new Date();
    var yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
    d.setUTCSeconds(epoch);


    if (yesterday.toDateString() === d.toDateString()) {
      return 'yesterday';
    }

    if (today.toDateString() === d.toDateString()) {
      return 'today';
    }

    return d.toDateString();
  }
  getLocalTimeFromUtcEpoch(epoch) {
    var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
    d.setUTCSeconds(epoch);
    return this.tConvert(d.toLocaleTimeString());
  }
  tConvert(time) {
    // Check correct time format and split into components
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
      time = time.slice(1);  // Remove full string match value
      time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join(''); // return adjusted time or original string
  }

  isObject(value) {
    return value && typeof value === 'object' && value.constructor === Object;
  }

  render() {

    const result = (this.props.item.conversation != null) ? _.map(this.props.item.conversation, (val, uid) => {
      return { ...val, uid };
    }) : [];

    let reversedArray = result.length > 0 ? result.reverse() : [];
    //console.log(this.props.item);

    return (
      <TouchableOpacity style={[styles.singleUser]} onPress={() => {
        //this.props.startConversationFromList(this.props.item.uid)conversation_id
        Actions.chatListView({
          conversation_id: this.props.item.uid,
          conversationMetaData: this.props.item.conversationMetaData
        });
      }}>
        {reversedArray.length > 0 ? this.renderUnseenMessage(reversedArray) : null}

        <View style={styles.imgWrapper}>
          {(this.props.withWhom.length > 0) ?

            <Image source={{
              uri:
                this.getProfilePicAndStatus(reversedArray).profilePicture

            }} style={{ width: 120, height: 120, borderRadius: 120, resizeMode: 'stretch' }} />


            : <Image source={require('./assets/profileselected.png')} style={{ width: 120, height: 120, resizeMode: 'stretch' }} />
          }
          {(this.props.withWhom.length > 0) ?
            this.getProfilePicAndStatus(reversedArray).status ?
              <View style={styles.onlineSign}></View>
              : <View style={styles.offlineSign}></View>
            : <View style={styles.offlineSign}></View>}

        </View>

        <Text style={{ fontSize: 14, marginTop: 10, fontFamily: 'coolvetica' }}>
          {this.resizedMessage(this.getProfilePicAndStatus(reversedArray).displayName, 12)}</Text>

        <View style={{ flexDirection: 'row', marginTop: 5 }}>
          <TouchableOpacity>
            <Image source={require('./assets/callIconSmall.png')} style={{ width: 40, height: 40, resizeMode: 'stretch', marginRight: 5 }} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => { this.props.setPopup(!this.props.popupStatus, this.props.item.uid) }}>
            <Image source={require('./assets/groupIconSmall.png')} style={{ width: 40, height: 40, resizeMode: 'stretch' }} />
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = {


  singleUser: {
    //backgroundColor: '#4D243D',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    height: width / numColumns + 140, // approximate a square
  },
  imgWrapper: {
    position: 'relative',
  },
  offlineSign: {
    width: 20,
    height: 20,
    backgroundColor: 'gray',
    borderRadius: 20,
    borderWidth: 2,
    borderColor: '#fff',
    position: 'absolute',
    right: 7,
    bottom: 5
  },
  onlineSign: {
    width: 20,
    height: 20,
    backgroundColor: '#FA1801',
    borderRadius: 20,
    borderWidth: 2,
    borderColor: '#fff',
    position: 'absolute',
    right: 7,
    bottom: 5
  },
}

const mapStateToProps = (state, ownProps) => {
  //console.log(ownProps.item.conversation)
  const { currentUser } = firebase.auth();
  const appUsers = _.map(state.userPreloadData.applicationUsers, (val, uid) => {
    return { ...val, uid }
  })
  const friends = _.map(state.userPreloadData.friends, (val, uid) => { return { ...val, uid } })

  let withWhom = [];

  let conversationWithWhom = null;

  conversationWithWhom = _.map(ownProps.item.conversationMetaData.withWhom);
  conversationWithWhom.forEach(element => {
    if (element.uid !== currentUser.uid) {
      withWhom.push(element);
    }
  });
  return {
    popupStatus: state.chatPopup.status,
    withWhom: withWhom,
    appUsers: appUsers,
    friends: friends
  }
}

export default connect(mapStateToProps, { startConversationFromList, setPopup })(SingleUserItem);
