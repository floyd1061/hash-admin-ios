import React, { Component } from 'react'
import { View, Text, ListView, Image, TouchableOpacity } from 'react-native'
import Modal from "react-native-modal";
import { connect } from 'react-redux'
import { LinearGradient } from 'expo'
import {Actions} from 'react-native-router-flux'
import SingleUserItem from './SingleUserItem'
import { fetchChatListFrom } from '../../actions';
import _ from 'lodash';
import firebase from '@firebase/app'
import auth from '@firebase/auth'
import { Ionicons } from '@expo/vector-icons';
import UserList from './UserPopup/UserList';

class ChatHome extends Component {
    constructor(props) {
        super(props);
        this.shareMsg = this.shareMsg.bind(this);
        this.state = {
            searchUserModal: false
        }
    }
    componentWillMount() {
        const { currentUser } = firebase.auth();
        this.props.fetchChatListFrom(currentUser.uid);
        this.createDataSource(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this.createDataSource(nextProps);
    }

    createDataSource({ chatList }) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.dataSource = ds.cloneWithRows(chatList);
    }

    renderRow(item) {
        return <SingleUserItem item={item} key={item.uid}/>
        //return 'kk';
    }
    shareMsg = () => {
        this.setState({ searchUserModal: !this.state.searchUserModal });
    }

    render() {
        return (
            <View style={styles.chatHomeContainer}>
                <Modal isVisible={this.state.searchUserModal}>
                    <UserList shareMsg={this.shareMsg} />
                </Modal>
                <LinearGradient style={styles.chatHeader} colors={['#FF3C00', '#FF0409']}>
                    <View style={styles.headerTop}>
                        <TouchableOpacity onPress={() => Actions.UserProfileTimeline()}>
                            <Text style={styles.headerText}>Hashtazapp</Text>
                        </TouchableOpacity>
                        <View style={styles.topIconArea}>
                            <TouchableOpacity style={[styles.messageIconArea, { paddingHorizontal: 10 }]}>
                                <Image source={require('./assets/messageIcon.png')} style={styles.messageIcon} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({
                                searchUserModal: true
                            })} style={{ paddingHorizontal: 10 }}>
                                <Image source={require('./assets/search_icon.png')} style={styles.searchIcon} />
                            </TouchableOpacity>
                            <TouchableOpacity style={{ paddingHorizontal: 10 }}>
                                <Image source={require('./assets/userIcon.png')} style={styles.userIcon} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.tabArea}>
                        <TouchableOpacity style={styles.tab}>
                            <View style={styles.tabIconWrapper}>
                                <Image source={require('./assets/tabIconcall.png')} style={{ width: 50, height: 50, resizeMode: 'stretch' }} />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.tab}>
                            <View style={styles.tabIconWrapper}>
                                <Image source={require('./assets/tabIconmessage.png')} style={{ width: 60, height: 55, resizeMode: 'stretch' }} />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.tab}>
                            <View style={styles.tabIconWrapper}>
                                <Image source={require('./assets/tabIcongroupchat.png')} style={{ width: 60, height: 40, resizeMode: 'stretch' }} />
                            </View>
                        </TouchableOpacity>
                    </View>
                </LinearGradient>

                <ListView
                    enableEmptySections={true}
                    contentContainerStyle={{ backgroundColor: '#DEDEDE', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-around' }}
                    dataSource={this.dataSource}
                    renderRow={this.renderRow}
                    initialListSize={10}
                />

                <View style={{ flexDirection: 'row', justifyContent: 'center', alignSelf: 'center', alignItems: 'center', position: 'absolute', bottom: 10, right: 10 }}>
                    <Text style={styles.startConversationText}>Start New Conversation</Text>
                    <TouchableOpacity onPress={() => this.setState({
                        searchUserModal: true
                    })}>
                        <Ionicons name="ios-add-circle" size={34} color="#FF3C00" />
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}

const styles = {
    chatHomeContainer: {
        flex: 1,
        backgroundColor: '#DEDEDE',
        position: 'relative'
    },

    chatHeader: {
        height: 140
    },

    headerTop: {
        flexDirection: 'row',
        paddingTop: 35,
        paddingHorizontal: 15,
        alignItems: 'flex-end'
    },

    headerText: {
        flex: 1,
        color: '#fff',
        fontSize: 18,
        fontFamily: 'coolvetica'
    },

    topIconArea: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },

    messageIcon: {
        width: 30,
        height: 30,
        resizeMode: 'stretch'
    },

    searchIcon: {
        width: 30,
        height: 30
    },

    userIcon: {
        width: 30,
        height: 30
    },

    tabArea: {
        flexDirection: 'row',
        flex: 1
    },

    tab: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center'
    },

    // Single User Component

    singleUser: {
        position: 'relative',
        width: 160,
        // backgroundColor:'#000',        
        alignItems: 'center',
        paddingTop: 67
    },
    imgWrapper: {
        position: 'relative',
    },
    startConversationText: {
        color: '#FF3C00',
        fontSize: 10,
        fontFamily: 'coolvetica',
        paddingRight: 5
    },
    onlineSign: {
        width: 20,
        height: 20,
        backgroundColor: '#FA1801',
        borderRadius: 20,
        borderWidth: 2,
        borderColor: '#fff',
        position: 'absolute',
        right: 7,
        bottom: 5
    },
}
const mapStateToProps = (state) => {
    ////console.log(state.allMessages);
    const chatList = _.map(state.allMessages, (data, uid) => {
        return { ...data, uid };
    });

    //console.log(chatList);
    return { chatList, chatPopup: state.chatPopup.status };
};
export default connect(mapStateToProps, { fetchChatListFrom })(ChatHome);