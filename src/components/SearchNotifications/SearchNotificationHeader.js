import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, Image, TextInput } from 'react-native'
import { Font } from 'expo'

class SearchNotificationHeader extends Component {

    constructor(props) {
        super(props);
        this.state = { showHeaderOption: false, fontLoaded: true };
    }
   

    render() {
        return (
            <View style = {styles.Container}>
                <View style = {styles.headerContainer}>
                    <View style = {styles.searchBox}>
                        <View style = {{flexDirection:'row'}}>
                            <TouchableOpacity>
                                <Image source={require('./assets/explore.png')} style={styles.exploreIcon}/>
                            </TouchableOpacity>
                            {
                                this.state.fontLoaded ? ( <TextInput
                                style={{ alignSelf: 'stretch', flex: .7, flexDirection: 'row', paddingHorizontal: 10,  fontSize: 15, fontFamily: 'coolvetica' }}
                                underlineColorAndroid="transparent"
                                placeholder="Search Notifications"
                                /> ) : null
                            }
                        </View>

                        <View style = {{flexDirection:'row', justifyContent: 'space-between'}}>
                            <TouchableOpacity>
                                <Image source={require('./assets/mic.png')} style={styles.micIcon}/>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => {this.setState({showHeaderOption: !this.state.showHeaderOption})}}>
                                <Image source={require('./assets/dropdown.png')} style={styles.dropdownIcon}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                {/* {this.state.showHeaderOption ?
                    <View style = {styles.headerOptionArea}>
                        <View  style = {{position: 'relative', flex: 1}}>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}>Invite friend to chat group</Text> ) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}>View contact</Text> ) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity disabled = {true}>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}>Call frined</Text> ) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}>Delete chat</Text> ) : null
                                }
                            </TouchableOpacity>
                        </View>                            
                    </View> : null
                }  */}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    Container: {
        backgroundColor: '#DEDEDE',
    },

    headerContainer: {
        alignSelf: 'stretch',
        backgroundColor: '#FF1303',
        height: 150,
    },

    searchBox: {
        flexDirection:'row',
        backgroundColor: '#fff',
        height: 60, 
        alignSelf: 'stretch',
        alignItems: 'center',
        paddingHorizontal: 20,
        marginTop: 80, 
        justifyContent: 'space-between'          
    },

    exploreIcon: {
        height: 40,
        width:40, 
        resizeMode : 'stretch',
    },

    micIcon: {
        height: 40,
        width:35, 
        resizeMode : 'stretch',
        marginRight: 10,
    },

    dropdownIcon: {
        height: 35,
        width:35, 
        resizeMode : 'stretch',
    },

    headerOptionArea: {
        width:200, 
        height: 150, 
        backgroundColor: '#fff', 
        position: 'absolute',
        right: 22,
        top: 140,
        borderColor: '#FD0607',
        borderWidth: 2,
        borderRadius: 3,
        borderTopWidth: 0
    },

    optionText: {
        paddingVertical: 8,
        paddingHorizontal: 8,
        color:'#000',
        
        fontFamily: 'coolvetica'
    },
});

export default SearchNotificationHeader;