import React, { Component } from 'react'
import { View, StyleSheet, Text, TouchableOpacity, Image, TextInput } from 'react-native'
import { Font } from 'expo'


export default class SearchNotifications extends Component {

    constructor(props) {
        super(props);
        this.state = { showHeaderOption: false };
    }

    state = {
        fontLoaded: true,
    };

  
    render() {
  
        return (

            <View style={ styles.container }>

                <View style = {styles.headerContainer}>
                    <View style = {styles.searchBox}>

                        <TouchableOpacity>
                            <Image source={require('./assets/explore.png')} style={styles.exploreIcon}/>
                        </TouchableOpacity>

                        {
                            this.state.fontLoaded ? ( <TextInput style={{ flex: .9, flexDirection:'row', paddingLeft: 10, paddingRight: 10,  fontSize: 15 }}
                            underlineColorAndroid="transparent"
                            placeholder="Search Notifications"
                            /> ) : null
                        }

                        <TouchableOpacity>
                            <Image source={require('./assets/mic.png')} style={styles.micIcon}/>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => {this.setState({showHeaderOption: !this.state.showHeaderOption})}}>
                            <Image source={require('./assets/dropdown.png')} style={styles.dropdownIcon}/>
                        </TouchableOpacity>

                    </View>
                </View>

                <View style = {styles.notificationBody} > 

                    <View style={styles.datetimeSec}>

                        {
                            this.state.fontLoaded ? ( <Text style={styles.datetimeText}> Yesterday at 12.34pm </Text> ) : null
                        }
                        <Image source={require('./assets/time.png')} style= {styles.timeicon} />

                    </View>

                    <View style={styles.rowSec}>
                        <TouchableOpacity>
                            <Image source={require('./assets/avatar.png')} style={styles.avataricon} />
                        </TouchableOpacity>

                        <View style={styles.textsec}>
                            {
                                this.state.fontLoaded ? ( <Text style={styles.Text2}> You Started Following </Text> ) : null
                            }
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style={styles.Text1}> Sarah Hobbs </Text> ) : null
                                }
                            </TouchableOpacity>
                        </View>

                        <View style = {styles.boxButton}>
                            <View style={styles.box1Button}>
                                <Image source={require('./assets/load.png')} style={styles.loadicon} />
                                <Image source={require('./assets/plus.png')} style={styles.plusicon} />
                                <View style={styles.box2Button}>
                                    {
                                        this.state.fontLoaded ? ( <Text style={styles.followText}> Follow </Text> ) : null
                                    }
                                </View>
                            </View>
                        </View>

                    </View>

                    <View style={styles.datetimeSec}>

                        {
                            this.state.fontLoaded ? ( <Text style={styles.datetimeText}> 24th Septmeber at 12.34pm </Text> ) : null
                        }
                        <Image source={require('./assets/time.png')} style= {styles.timeicon} />

                    </View>


                    <View style={styles.rowSec}>
                        <TouchableOpacity>
                            <Image source={require('./assets/avatar.png')} style={styles.avataricon} />
                        </TouchableOpacity>

                        <View style={styles.textsec}>
                            {
                                this.state.fontLoaded ? ( <Text style={styles.Text3}> You Favouritised an Event Day </Text> ) : null
                            }
                        </View>
    
                        {/* {//console.log(item)} */}
                        {/* {this.styleForEventBubble(item)} */}

                        <View>
                            <View style={[styles.talkBubbleSquare,{backgroundColor: '#ff0000'}]} >    
                                {
                                    this.state.fontLoaded ? ( <Text style={{textAlign:'center', color:'#fff',  fontSize: 15}}> John's Party </Text> ) : null
                                }
                                {
                                    this.state.fontLoaded ? ( <Text style={{textAlign:'center', fontSize:8, color:'#fff' }}> 12pm - 3pm</Text> ) : null
                                }
                            </View>
                            <View style={styles.boxTriangle}/>
                            <View style={[styles.talkBubbleTriangle, {borderRightColor: '#ff0000'}]}/>
                        </View>
                        
                    </View>


                    <View style={styles.datetimeSec}>

                        {
                            this.state.fontLoaded ? ( <Text style={styles.datetimeText}> 3 days ago </Text> ) : null
                        }
                        <Image source={require('./assets/time.png')} style= {styles.timeicon} />

                    </View>

                    <View style= { styles.rowSec }>
                        <TouchableOpacity>
                            <Image source={require('./assets/avatar.png')} style={styles.avataricon} />
                        </TouchableOpacity>

                        <View style= { styles.textsec }>
                            {
                                this.state.fontLoaded ? ( <Text style = {styles.Text2}> Now You Are Friend With </Text> ) : null
                            }
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.Text1}> Sarah Hobbs </Text> ) : null
                                }
                            </TouchableOpacity>
                        </View>

                        <View style = {styles.boxButton}>
                            <View style= { styles.box3Button }>
                                <Image source={require('./assets/likeactive.png')} style={styles.likeactiveicon} />
                                
                                <View style= { styles.box4Button }>
                                    {
                                        this.state.fontLoaded ? ( <Text style= { styles.friendText }>Friends</Text> ) : null
                                    }
                                </View>
                            </View>
                        </View>
                    </View>




                </View>

                {/* ----dropdown function ----  */}

                {this.state.showHeaderOption ?
                    <View style = {styles.headerOptionArea}>
                        <View  style = {{position: 'relative', flex: 1}}>
                            <TouchableOpacity>
                                <Text style = {styles.optionText}>Invite friend to chat group</Text>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Text style = {styles.optionText}>View contact</Text>
                            </TouchableOpacity>
                            <TouchableOpacity disabled = {true}>
                                <Text style = {styles.optionText}>Call frined</Text>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Text style = {styles.optionText}>Delete chat</Text>
                            </TouchableOpacity>
                        </View>                            
                    </View> : null
                }

                
            </View>

        )
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#DEDEDE',
    },

    headerContainer: {
        backgroundColor: '#FF1303',
        height: 150,
    },

    searchBox: {
        flexDirection:'row',
        backgroundColor: '#fff',
        height: 60, 
        width: 412,
        alignItems: 'center',
        paddingLeft: 30,
        marginTop: 80,           
    },

    exploreIcon: {
        height: 40,
        width:40, 
        resizeMode : 'stretch',
        alignItems: 'flex-start',
    },

    micIcon: {
        height: 40,
        width:35, 
        resizeMode : 'stretch',
        alignItems: 'flex-end',
        marginRight: 10,
    },

    dropdownIcon: {
        height: 35,
        width:35, 
        resizeMode : 'stretch',
        alignItems: 'flex-end',
    },

    headerOptionArea: {
        width:200, 
        height: 150, 
        backgroundColor: '#fff', 
        position: 'absolute',
        right: 22,
        top: 140,
        borderColor: '#FD0607',
        borderWidth: 2,
        borderRadius: 3,
        borderTopWidth: 0
    },

    optionText: {
        paddingVertical: 8,
        paddingHorizontal: 8,
        color:'#000',
        
    },

    notificationBody: {
        paddingHorizontal: 20,

    },

    datetimeSec: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: 20,
    },

    datetimeText: {
        fontSize: 14,
        color: '#7C7C7C',
        top: 8,
        
    },

    timeicon: {
        height: 25,
        width: 25,
        resizeMode: 'stretch',
    },

    rowSec: {
        flexDirection: 'row',
        marginTop: 20,
        marginBottom: 30,
        // backgroundColor: '#eee'
    },

    avataricon: {
        height: 80,
        width: 80,
        resizeMode: 'stretch',
        marginRight: 5,
    },

    textsec: {
        flex: 1,
        justifyContent: 'center',
        marginRight: 15,
        // width: 150
    },

    Text1: {
        fontSize: 12,
        color: '#FF0408',
        
    },

    Text2: {
        fontSize: 12,
        color: '#000',
        
    },

    boxButton : {
        justifyContent: 'center'

    },

    box1Button: {
        width: 126,
        height: 44,
        backgroundColor: '#FFFFFF',
        borderRadius: 2,
        // marginTop: 20,
    },

    box2Button: {
        width: 78,
        height: 39,
        backgroundColor: '#7C7C7C',
        bottom: 26,
        left: 45
    },

    followText: {
        fontSize: 16,
        color: '#FFFFFF',
        textAlign: 'center',
        top: 6,
        
    },

    loadicon: {
        height: 20,
        width: 20,
        resizeMode: 'stretch',
        left: 15,
        top: 12
    },

    plusicon: {
        height: 8,
        width: 8,
        resizeMode: 'stretch',
        left: 32,
        top: 3
    },

    Text3: {
        fontSize: 12,
        color: '#000',
        
    },

    //eventbox style
    talkBubbleSquare: {
        width: 83,
        height: 70,
        backgroundColor: '#565656',
        borderRadius: 10,
        borderWidth: 4,
        borderColor: '#fff',
        justifyContent:'center',
        top: 8
        // lineHeight:5 
      },

      boxTriangle:{
        position: 'relative',
        left: -16,
        bottom: 37,
        width: 0,
        height: 0,     
        borderTopColor: 'transparent',
        borderTopWidth: 10,
        borderRightWidth: 20,
        borderRightColor: '#fff',
        borderBottomWidth: 10,
        borderBottomColor: 'transparent',      
      },

      talkBubbleTriangle: {
        position: 'absolute',
        left: -8,    
        top: 34,
        width: 0,
        height: 0,     
        borderTopColor: 'transparent',
        borderTopWidth: 9,
        borderRightWidth: 16,  
        borderRightColor: '#565656',
        borderBottomWidth: 9,
        borderBottomColor: 'transparent',
      },

      likeactiveicon: {
        height: 20,
        width: 20,
        resizeMode: 'stretch',
        left: 15,
        top: 10
    },

      box3Button: {
        width: 126,
        height: 44,
        backgroundColor: '#FFFFFF',
        borderRadius: 2,
    },

    box4Button: {
        width: 78,
        height: 39,
        backgroundColor: '#7C7C7C',
        bottom: 18,
        left: 45
    },

    friendText: {
        fontSize: 16,
        color: '#FFFFFF',
        textAlign: 'center',
        top: 6,
        
    },


});