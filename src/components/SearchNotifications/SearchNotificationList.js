import React, { Component } from 'react'
import { View, StyleSheet, ListView } from 'react-native'
import SearchNotificationHeader from './SearchNotificationHeader'
import SearchNotificationListItem from './SearchNotificationListItem'
import { connect } from 'react-redux'

class SearchNotificationList extends Component {
    
    componentWillMount() {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        this.dataSource = ds.cloneWithRows(this.props.searchnotificationdata);
    }
    renderRow(item) {
        return <SearchNotificationListItem item = {item} />;
    }

    render() {
        return (
            <View style = {styles.Container}>
                <SearchNotificationHeader />
                <ListView
                    dataSource={this.dataSource}
                    renderRow={this.renderRow}
                    initialListSize={10}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    Container: {
        flex: 1,
        backgroundColor: '#DEDEDE',
    },
});

const mapStateToProps = (state) => {
    return { searchnotificationdata: state.searchnotifications };
};

export default connect (mapStateToProps)(SearchNotificationList);