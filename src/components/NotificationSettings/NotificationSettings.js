import React, { Component } from 'react'
import { View, StyleSheet, Image, TouchableOpacity, Text, ImageBackground } from 'react-native'
import { LinearGradient } from 'expo'
import ToggleSwitch from 'toggle-switch-react-native'
import Slider from 'react-native-slider'
import { Dropdown } from 'react-native-material-dropdown'
import CheckBox from 'react-native-check-box'
import { Font } from 'expo';
import { Ionicons } from '@expo/vector-icons';
import { Actions } from 'react-native-router-flux';
export default class NotificationSettings extends Component {





  onToggle(isOn) {
    alert('Changed to ' + isOn)
  }

  constructor() {
    super();
    this.goBack = this.goBack.bind(this);
    this.state = {
      check: false,
      fontLoaded: true,
    }
  }

  onClick(data) {
    //console.log("Checked")
  }
  goBack() {
    Actions.pop();
  }
  render() {

    let data = [
      {
        value: 'Yes',
      },
      {
        value: 'No',
      }
    ];

    return (
      <LinearGradient colors={['#E60300', '#FF0906']} style={styles.container}>
        <TouchableOpacity onPress={this.goBack} style={styles.BackBotton}>
          <Ionicons name="ios-arrow-back" size={40} color="#fff" />
        </TouchableOpacity>
        <ImageBackground style={styles.logo} source={require('./assets/notificationsbox.png')} >

          <View style={styles.BoxSection1}>
            <View style={styles.box1}>
              {
                this.state.fontLoaded ? <Text style={styles.textStyle1}>Conversation Tone</Text> : null
              }
              {
                this.state.fontLoaded ? <Text style={styles.textStyle2}>Play sounds for incoming or outgoing messages.</Text> : null
              }
            </View>
            <View style={styles.box2}>

              <ToggleSwitch onToggle={this.onToggle} onColor="#FF341C" size='default' />

            </View>
          </View>

          <View style={styles.BoxSection2}>
            <View style={styles.box3}>
              {
                this.state.fontLoaded ? <Text style={styles.textStyle3}>Vibrate</Text> : null
              }
            </View>

            <View style={styles.box4}>

              <ToggleSwitch onToggle={this.onToggle} onColor="#FF341C" size='default' />

            </View>
          </View>

          <View style={styles.BoxSection3}>
            <View style={styles.box5}>
              {
                this.state.fontLoaded ? <Text style={styles.textStyle4}>Text Size</Text> : null
              }
            </View>

            <View style={styles.box6}>
              <Slider
                step={1}
                thumbStyle={{ borderWidth: 3, borderColor: '#D3D3D3' }}
                trackStyle={{ height: 15, borderWidth: 3, borderColor: '#D3D3D3', borderRadius: 10 }}
                minimumTrackTintColor='#FE2305'
                maximumTrackTintColor='#DDDDDD'
                maximumValue={30}
                thumbTintColor='#FD1700'
                style={{ width: 160 }}
                value={this.state.value}
                onValueChange={value => this.setState({ value })}
              />
            </View>
          </View>

          <View style={styles.BoxSection4}>
            <View style={styles.box7}>
              {
                this.state.fontLoaded ? <Text style={styles.textStyle5}>Chats & Calls</Text> : null
              }
              {
                this.state.fontLoaded ? <Text style={styles.textStyle6}>Play sounds for incoming or outgoing calls.</Text> : null
              }
            </View>

            <View style={styles.box8}>
              <ToggleSwitch onToggle={this.onToggle} onColor="#FF341C" size='default' />
            </View>
          </View>

          <View style={{ flexDirection: 'row', marginBottom: 10, marginHorizontal: 10 }}>

            <View style={{ flex: 1 }}>
              {
                this.state.fontLoaded ? <CheckBox
                  onClick={() => this.onClick(data)}
                  isChecked={data.checked}
                  rightText={"Default"}
                  rightTextStyle={{ color: '#FFFFFF', fontSize: 12, fontFamily: 'coolvetica' }}
                  checkedImage={<Image source={require('./assets/checkImage.png')} style={{ height: 16, width: 16, resizeMode: 'stretch' }} />}
                  unCheckedImage={<Image source={require('./assets/uncheckImage.png')} style={{ height: 16, width: 16, resizeMode: 'stretch' }} />}
                /> : null
              }
              {
                this.state.fontLoaded ? <CheckBox
                  onClick={() => this.onClick(data)}
                  isChecked={data.checked}
                  rightText={"Select Gender"}
                  rightTextStyle={{ color: '#FFFFFF', fontSize: 12, fontFamily: 'coolvetica' }}
                  checkedImage={<Image source={require('./assets/checkImage.png')} style={{ height: 16, width: 16, resizeMode: 'stretch' }} />}
                  unCheckedImage={<Image source={require('./assets/uncheckImage.png')} style={{ height: 16, width: 16, resizeMode: 'stretch' }} />}
                /> : null
              }
              {
                this.state.fontLoaded ? <CheckBox
                  onClick={() => this.onClick(data)}
                  isChecked={data.checked}
                  rightText={"Default"}
                  rightTextStyle={{ color: '#FFFFFF', fontSize: 12, fontFamily: 'coolvetica' }}
                  checkedImage={<Image source={require('./assets/checkImage.png')} style={{ height: 16, width: 16, resizeMode: 'stretch' }} />}
                  unCheckedImage={<Image source={require('./assets/uncheckImage.png')} style={{ height: 16, width: 16, resizeMode: 'stretch' }} />}
                /> : null
              }
              {
                this.state.fontLoaded ? <CheckBox
                  onClick={() => this.onClick(data)}
                  isChecked={data.checked}
                  rightText={"Select Promotion"}
                  rightTextStyle={{ color: '#FFFFFF', fontSize: 12, fontFamily: 'coolvetica' }}
                  checkedImage={<Image source={require('./assets/checkImage.png')} style={{ height: 16, width: 16, resizeMode: 'stretch' }} />}
                  unCheckedImage={<Image source={require('./assets/uncheckImage.png')} style={{ height: 16, width: 16, resizeMode: 'stretch' }} />}
                /> : null
              }
            </View>

            <View style={{ flex: 1 }}>
              <View style={[styles.box9, { flex: 1, marginBottom: 10, alignSelf: 'stretch', padding: 3 }]}>
                <Dropdown
                  inputContainerStyle={{ borderBottomColor: 'transparent' }}
                  dropdownOffset={{ top: 3, left: 0 }}
                  dropdownPosition={0}
                  selectedItemColor='#6E6E6E'
                  pickerStyle={styles.Dropdownstyles1}
                  label='Please Select'
                  fontSize={10}
                  data={data}
                />
              </View>

              <View style={[styles.box9, { flex: 1, alignSelf: 'stretch', padding: 3 }]}>
                <Dropdown
                  inputContainerStyle={{ borderBottomColor: 'transparent' }}
                  dropdownOffset={{ top: 3, left: 0 }}
                  dropdownPosition={0}
                  selectedItemColor='#6E6E6E'
                  pickerStyle={styles.Dropdownstyles1}
                  label='Please Select'
                  fontSize={10}
                  data={data}
                />
              </View>
            </View>

          </View>

          <View style={styles.itemBox}>
            <Dropdown
              inputContainerStyle={{ borderBottomColor: 'transparent' }}
              dropdownOffset={{ top: 0, left: 0 }}
              dropdownPosition={0}
              pickerStyle={styles.Dropdownstyles2}
              label='Birthday Alert'
              selectedItemColor='#6E6E6E'
              fontSize={20}
              data={data}
            />
          </View>

        </ImageBackground>

        <TouchableOpacity>
          <LinearGradient colors={['#ff2002', '#df1d04']} style={styles.buttonStyle}>
            {
              this.state.fontLoaded ? <Text style={styles.buttonText}>Apply Now</Text> : null
            }
          </LinearGradient>
        </TouchableOpacity>
      </LinearGradient>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  logo: {
    width: 300,
    height: 480,
    paddingHorizontal: 8,
    paddingVertical: 60,
    marginBottom: 20,
  },

  buttonStyle: {
    borderWidth: 2,
    borderColor: '#fff',
    borderRadius: 3,
    paddingHorizontal: 30,
    paddingVertical: 10
  },

  buttonText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 20,
    fontFamily: 'coolvetica'
  },

  BoxSection1: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    borderRadius: 3,
    borderColor: '#FFFFFF',
    marginHorizontal: 10,
    marginBottom: 8,
    padding: 7,
    marginTop: 50,
  },

  box1: {
    justifyContent: 'center',
  },

  box2: {
    justifyContent: 'center',
  },

  textStyle1: {
    fontSize: 15,
    color: '#919191',
    lineHeight: 20,
    fontFamily: 'coolvetica'
  },

  textStyle2: {
    fontSize: 8,
    color: '#FB1700',
    fontFamily: 'coolvetica'
  },

  BoxSection2: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    borderRadius: 3,
    borderColor: '#FFFFFF',
    marginHorizontal: 10,
    marginBottom: 8,
    padding: 7,
  },

  box3: {
    justifyContent: 'center',
  },

  box4: {
    justifyContent: 'center',
  },

  textStyle3: {
    fontSize: 15,
    color: '#919191',
    fontFamily: 'coolvetica'
  },

  BoxSection3: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    borderRadius: 3,
    borderColor: '#FFFFFF',
    marginHorizontal: 10,
    marginBottom: 8,
    padding: 7,
  },

  box5: {
    justifyContent: 'center',
  },

  box6: {
    justifyContent: 'center',
  },

  textStyle4: {
    fontSize: 15,
    color: '#919191',
    fontFamily: 'coolvetica'
  },

  BoxSection4: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    borderRadius: 3,
    borderColor: '#FFFFFF',
    marginHorizontal: 10,
    marginBottom: 10,
    padding: 7,
  },

  box7: {
    justifyContent: 'center',
  },

  box8: {
    justifyContent: 'center',
  },

  textStyle5: {
    fontSize: 15,
    color: '#919191',
    lineHeight: 20,
    fontFamily: 'coolvetica'
  },

  textStyle6: {
    fontSize: 8,
    color: '#FB1700',
    fontFamily: 'coolvetica'
  },

  box9: {
    backgroundColor: '#FFFFFF',
    borderColor: '#FFFFFF',
    borderRadius: 3,
  },

  Dropdownstyles1: {
    flex: 1,
    marginTop: 39,
    height: 100,
    backgroundColor: '#FFFFFF',
    borderRadius: 3,
    paddingBottom: 0,
    alignSelf: 'stretch'
  },
  BackBotton: {
    position: 'absolute',
    top: 30,
    left: 20,
    zIndex: 20
  },
  itemBox: {
    backgroundColor: '#FFFFFF',
    marginHorizontal: 10,
    borderColor: '#fff',
    borderRadius: 3,
    padding: 15,
    paddingBottom: 0,
    alignSelf: 'stretch',
  },

  Dropdownstyles2: {
    flex: 1,
    marginTop: 61,
    height: 100,
    backgroundColor: '#FFFFFF',
    borderRadius: 3,
    alignSelf: 'stretch'
  },
});