import React, { Component } from 'react'
import { StyleSheet, View, Image, TouchableOpacity, TextInput, Text} from 'react-native'
import { LinearGradient } from 'expo'
import ToggleSwitch from 'toggle-switch-react-native'
import Slider from 'react-native-slider' 
import { Dropdown } from 'react-native-material-dropdown'
import { Font } from 'expo';


export default class NotificationForm extends Component {

    state = {
        fontLoaded: true,
      };
  
    onToggle(isOn){
        alert('Changed to ' + isOn)
      }
    
    constructor() {
        super();

        this.state = {
            check: false,
                      
        }
    }
    
    onClick(data){
        //console.log("aaaaa")
    }

    render() {

        let data = [{
            value: 'Banana',
          }, {
            value: 'Mango',
          }, {
            value: 'Pear',
          }, 
        ];

        return (

            <View style={ styles.container }>

                {/* <Image style= { [styles.logo, {position: 'absolute', top: -30, zIndex: 3}] } source= {require('./assets/NotificationsLogo.png')} />                         */}
                
                <View style= { [styles.Box, {position: 'relative'}] }>

                    <Image style= { [styles.logo, {position: 'absolute', top: -33, zIndex: 1}] } source= {require('./assets/NotificationsLogo.png')} />

                    <View style= { styles.BoxStyle }>

                        <View style= { styles.BoxSection1 }>

                            <View style= {styles.box1}>
                            {
                                this.state.fontLoaded ? <Text style= { styles.textStyle1 }>Conversation Tone</Text> :null
                            }
                            {
                                this.state.fontLoaded ? <Text style= { styles.textStyle2 }>Play sounds for incoming or outgoing messages.</Text> :null
                            }
                            </View> 

                            <View style= {styles.box2}>

                                <ToggleSwitch onToggle={this.onToggle} onColor="#FF341C" size= 'default'/>

                            </View>

                        </View>

                        <View style= { styles.BoxSection2 }>

                            <View style= { styles.box3 }>
                            {
                                this.state.fontLoaded ? <Text style= { styles.textStyle3 }>Vibrate</Text> :null
                            }
                            </View>

                            <View style= {styles.box4}>

                                <ToggleSwitch onToggle={this.onToggle} onColor="#FF341C" size= 'default'/>

                            </View>

                        </View>

                        <View style= { styles.BoxSection3 }>

                            <View style= { styles.box5 }>
                            {
                                this.state.fontLoaded ? <Text style= { styles.textStyle4 }>Text Size</Text> :null
                            }
                            </View>

                            <View style= {styles.box6}>
                        
                                <Slider 
                                    step= {1}
                                    thumbStyle = {{borderWidth: 3, borderColor: '#D3D3D3'}}
                                    trackStyle = {{ height: 15, borderWidth: 3, borderColor: '#D3D3D3', borderRadius: 10 }}
                                    minimumTrackTintColor= '#FE2305'
                                    maximumTrackTintColor= '#DDDDDD'
                                    thumbTintColor= '#FD1700'
                                    style= {{ width: 180 }}
                                    value={this.state.value}
                                    onValueChange={value => this.setState({ value })}
                                    />

                            </View>

                        </View>

                        <View style= { styles.BoxSection4 }>

                            <View style= {styles.box7}>
                            {                                
                                this.state.fontLoaded ? <Text style= { styles.textStyle5 }>Chats & Calls</Text> :null
                            }
                            {
                                this.state.fontLoaded ? <Text style= { styles.textStyle6 }>Play sounds for incoming or outgoing calls.</Text> :null
                            }
                            </View>

                            <View style= {styles.box8}>

                                <ToggleSwitch onToggle={this.onToggle} onColor="#FF341C" size= 'default'/>

                            </View>

                        </View>

                        <View style= {{ flexDirection: 'row', marginBottom: 20  }}>

                            <View style={{flex:1}}>
                                
                                <CheckBox
                                    //style={{flex: 1, padding: 5}}
                                    onClick={()=>this.onClick(data)}
                                    isChecked={data.checked}
                                    rightText={"Default"}
                                    rightTextStyle= {{ color: '#FFFFFF' }}
                                    checkedImage={<Image source={require('./assets/checkImage.png')} style={{height: 16, width: 16, resizeMode: 'stretch'}}/>}
                                    unCheckedImage={<Image source={require('./assets/uncheckImage.png')} style={{height: 16, width: 16, resizeMode: 'stretch' }}/>}
                                />

                                <CheckBox
                                    // style={{flex: 1, padding: 5}}
                                    onClick={()=>this.onClick(data)}
                                    isChecked={data.checked}
                                    rightText={"Select Gender"}
                                    rightTextStyle= {{ color: '#FFFFFF' }}
                                    checkedImage={<Image source={require('./assets/checkImage.png')} style={{height: 16, width: 16, resizeMode: 'stretch'}}/>}
                                    unCheckedImage={<Image source={require('./assets/uncheckImage.png')} style={{height: 16, width: 16, resizeMode: 'stretch' }}/>}
                                />

                                <CheckBox
                                    // style={{flex: 1, padding: 5}}
                                    onClick={()=>this.onClick(data)}
                                    isChecked={data.checked}
                                    rightText={"Default"}
                                    rightTextStyle= {{ color: '#FFFFFF' }}
                                    checkedImage={<Image source={require('./assets/checkImage.png')} style={{height: 16, width: 16, resizeMode: 'stretch'}}/>}
                                    unCheckedImage={<Image source={require('./assets/uncheckImage.png')} style={{height: 16, width: 16, resizeMode: 'stretch' }}/>}
                                />

                                <CheckBox
                                    // style={{flex: 1, padding: 5}}
                                    onClick={()=>this.onClick(data)}
                                    isChecked={data.checked}
                                    rightText={"Select Promotion"}
                                    rightTextStyle= {{ color: '#FFFFFF' }}
                                    checkedImage={<Image source={require('./assets/checkImage.png')} style={{height: 16, width: 16, resizeMode: 'stretch'}}/>}
                                    unCheckedImage={<Image source={require('./assets/uncheckImage.png')} style={{height: 16, width: 16, resizeMode: 'stretch' }}/>}
                                />

                            </View>

                            <View style = {{flex: 1}}>
                                <View style= {[styles.box9, { flex: 1, marginBottom: 10}]}>
  
                                    <Dropdown
                                        // style = {{position: 'relative'}}
                                        inputContainerStyle={{ borderBottomColor: 'transparent' }}
                                        dropdownOffset= {{ top: 2, left: 0 }}
                                        dropdownPosition = {0}
                                        selectedItemColor= '#6E6E6E'
                                        //textColor= '#FB1600'
                                        pickerStyle= {styles.Dropdownstyles1}
                                        label='Please Select'
                                        fontSize = {10}
                                        data={data}
                                    />

                                </View>

                                <View style= {[styles.box9, {flex: 1}]}>

                                    <Dropdown
                                        // style = {{position: 'relative'}}
                                        inputContainerStyle={{ borderBottomColor: 'transparent' }}
                                        dropdownOffset= {{ top: 2, left: 0 }}
                                        dropdownPosition = {0}
                                        selectedItemColor= '#6E6E6E'
                                        //textColor= '#FB1600'
                                        pickerStyle= {styles.Dropdownstyles1}
                                        label='Please Select'
                                        fontSize = {10}
                                        data={data}
                                    />

                                </View>
                            </View>

                        </View>

                        <View style={styles.SectionStyle}>

                            <View style={ styles.itemBox }>
                                <Dropdown
                                    inputContainerStyle={{ borderBottomColor: 'transparent' }}
                                    dropdownOffset={{ top: 2, left: 0 }}
                                    dropdownPosition={0}
                                    pickerStyle={styles.Dropdownstyles2}
                                    label='Birthday Alert'
                                    // textColor='#6E6E6E'
                                    //baseColor= '#FF341D'
                                    selectedItemColor= '#6E6E6E'
                                    //label= 'Alert me for my birthday and other users too.'
                                    fontSize={20}
                                    data={data}
                                />

                            </View>

                        </View>

                    </View>

                </View>

                <View style={styles.buttonCenter}>
                    <TouchableOpacity>
                        <LinearGradient
                        colors={['#ff2002', '#df1d04']}
                        style={styles.SectionStyleDeleteButton}>
                        {
                         this.state.fontLoaded ? <Text style={styles.buttonText}>Apply Now</Text> :null
                        }
                        </LinearGradient>
                    </TouchableOpacity>
                </View>

            </View>
        
        )
    }
}


const styles = StyleSheet.create({

    container: {
        flex: 1,
        marginTop: 70,
    },
    
    Box: {
        width: 330,
        height: 510,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#282828',
        borderWidth: 3,
        borderColor: 'rgb(255, 255, 255)',
        elevation: 3,
        padding: 15,
    },

    //logo_wrapper: { alignItems: 'center', position: 'absolute', left: 0, right: 0, zIndex: 1, top: -50},

    logo: {
        height: 65,
        width: 80,
        resizeMode: 'stretch',
    },
    
    
      // Button Style
    
    buttonCenter: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    
    },
    
    SectionStyleDeleteButton: {
        borderWidth: 2,
        borderColor: '#fff',
        height: 55,
        borderRadius: 3,
        width: 170,
        marginTop: 20
    
    },
    
    buttonText: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 18,
        top: 12,
        fontFamily: 'coolvetica'
    },
    
    
    BoxStyle: {
        marginTop: 30
    
    },
    
    BoxSection1: {
        height: 55,
        backgroundColor: '#FFFFFF',
        borderRadius: 2,
        borderWidth: 2,
        borderColor: '#FFFFFF',
        marginBottom: 7,
        padding: 10,
     
    },

    box1: {
        justifyContent:'center',
    
    },
    
    box2: {
        alignItems: 'flex-end',
        bottom: 45, 
    
    },
    
    BoxSection2: {
        height: 55,
        backgroundColor: '#FFFFFF',
        borderRadius: 2,
        borderWidth: 2,
        borderColor: '#FFFFFF',
        marginBottom: 7,
        padding: 10
    
    },

    box3: {
        justifyContent:'center',
    
    },
    
    box4: {
        alignItems: 'flex-end',
        bottom: 43, 
    
    },

    BoxSection3: {
        height: 55,
        backgroundColor: '#FFFFFF',
        borderRadius: 2,
        borderWidth: 2,
        borderColor: '#FFFFFF',
        marginBottom: 7,
        padding: 10
    
    },

    box5: {
        justifyContent:'center',
    
    },
    
    box6: {
        alignItems: 'flex-end',
        bottom: 49, 
    
    },

    BoxSection4: {
        height: 55,
        backgroundColor: '#FFFFFF',
        borderRadius: 2,
        borderWidth: 2,
        borderColor: '#FFFFFF',
        marginBottom: 15,
        padding: 10
    
    },

    box7: {
        justifyContent:'center',
    
    },
    
    box8: {
        alignItems: 'flex-end',
        bottom: 45, 
    
    },
      
    // Text Style

    textStyle1: {
        
        fontSize: 18,
        paddingTop: 35,
        color: '#919191',
        lineHeight: 20,
        bottom: 25,
        fontFamily: 'coolvetica'
    
    },
    
    textStyle2: {
        
        fontSize:9,
        color: '#FB1700',
        bottom: 25,
        fontFamily: 'coolvetica'
    
    },

    textStyle3: {
        fontSize: 18,
        color: '#919191',
        paddingTop: 35,
        bottom: 25,
        fontFamily: 'coolvetica'
    
    },

    textStyle4: {
        fontSize: 18,
        color: '#919191',
        paddingTop: 35,
        bottom: 25,
        fontFamily: 'coolvetica'
    
    },
    
    textStyle5: {
        fontSize: 18,
        paddingTop: 35,
        color: '#919191',
        lineHeight: 20,
        bottom: 25,
        fontFamily: 'coolvetica'
    
    },
    
    textStyle6: {
        fontSize: 9,
        color: '#FB1700',
        bottom: 25,
        fontFamily: 'coolvetica'
    
    },

    box9: { 
        height: 45, 
        width: 148, 
        backgroundColor: '#D6D2C5', 
        borderColor: '#eef', 
        borderRadius: 2,  
        padding: 10,
    },

    Dropdownstyles1: {
        marginTop: 39,
        width: 155,
        height: 100,
        backgroundColor: '#FFFFFF',
        borderRadius: 3
    },

    itemBox: {
        height: 70, 
        width: 296, 
        backgroundColor: '#FFFFFF', 
        borderColor: '#eef', 
        borderRadius: 4, 
        padding: 15,
        paddingBottom: 0,
        bottom: 5,
        //justifyContent: 'center',

    },
    
    Dropdownstyles2: {
        marginTop: 70,
        width: 300,
        height: 100,
        backgroundColor: '#FFFFFF',
        borderRadius: 3,
    },
});    