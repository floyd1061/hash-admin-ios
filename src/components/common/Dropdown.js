import React, { Component } from 'react'
import { View, Text, TextInput, Image, TouchableOpacity, Picker } from 'react-native'
import { Card, CardSection } from '../common'

const Dropdown = (props) => {
   // //console.log(props);

    this.state = {
        language: 'C++'
    }

    return (
        <View style = {{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Picker
                mode = 'dropdown'
                selectedValue={this.state.language}
                style={{ height: 50, width: 400, }}
                onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
                <Picker.Item style = {{backgroundColor: '#000'}} label="Java" value="java"  />
                <Picker.Item label="JavaScript" value="js" />
            </Picker>
        </View>
    );
};

export default Dropdown ;
