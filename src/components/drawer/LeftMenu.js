import React from 'react'
import { LinearGradient } from 'expo'
import {
     Image, TouchableOpacity, Dimensions
} from 'react-native'
import { Actions } from 'react-native-router-flux';

const { height } = Dimensions.get('window');

export const navigationLeftView = (
    <LinearGradient style={{height: height - 210, top: 140, borderWidth: 1, borderColor: 'transparent', alignSelf: 'stretch'}} colors={['#FF2000', '#FF1E00']}>
        <TouchableOpacity onPress={() => { Actions.userprofile(); }} style={{flex: 1, alignSelf: 'stretch', paddingVertical: 10, alignItems: 'center', justifyContent: 'center', borderBottomWidth: 1, borderColor: 'transparent'}}>
            <Image source={require('./assets/myprofile.png')} style={{ width: 55, height: 60, resizeMode: 'stretch', }} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => { Actions.EditProfile(); }} style={{flex: 1, alignSelf: 'stretch', paddingVertical: 10, alignItems: 'center', justifyContent: 'center', borderBottomWidth: 1, borderColor: 'transparent'}}>
            <Image source={require('./assets/editprofile.png')} style={{ width: 60, height: 55, resizeMode: 'stretch', }} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => { Actions.NotificationSettings(); }} style={{flex: 1, alignSelf: 'stretch', paddingVertical: 10, alignItems: 'center', justifyContent: 'center', borderBottomWidth: 1, borderColor: 'transparent'}}>
            <Image source={require('./assets/notifications.png')} style={{ width: 70, height: 50, resizeMode: 'stretch', }} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => { Actions.FriendsFollowersActivity(); }} style={{flex: 1, alignSelf: 'stretch', paddingVertical: 10, alignItems: 'center', justifyContent: 'center', borderBottomWidth: 1, borderColor: 'transparent'}}>
            <Image source={require('./assets/activity.png')} style={{ width: 50, height: 55, resizeMode: 'stretch', }} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => { Actions.settings(); }} style={{flex: 1, alignSelf: 'stretch', paddingVertical: 10, alignItems: 'center', justifyContent: 'center', borderBottomWidth: 1, borderColor: 'transparent'}}>
            <Image source={require('./assets/settings.png')} style={{ width: 50, height: 55, resizeMode: 'stretch', }} />
        </TouchableOpacity>
        <TouchableOpacity style={{flex: 1, alignSelf: 'stretch', paddingVertical: 10, alignItems: 'center', justifyContent: 'center', borderBottomWidth: 1, borderColor: 'transparent'}}>
            <Image source={require('./assets/logout.png')} style={{ width: 45, height: 55, resizeMode: 'stretch', }} />
        </TouchableOpacity>
    </LinearGradient>
);