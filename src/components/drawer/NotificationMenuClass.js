import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

export default class notificationMenuClass extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (

            <View style={{
                alignSelf: 'stretch', alignItems: 'center', justifyContent: 'center', flexDirection: 'column', position: 'absolute',
                height: 270, width: 100, backgroundColor: '#EBEBEB', borderWidth: 1, borderColor: '#B2AFA4', borderRadius: 3, top: 25, right: -2
            }}>
                <View style={{ flexDirection: 'column' }}>
                    <TouchableOpacity>
                        <View style={{ height: 50, width: 100, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 12, color: 'red', fontFamily: 'coolvetica' }}>{this.props.notificationCount}</Text>
                            <Text style={{ fontSize: 10, color: '#494949', fontFamily: 'coolvetica' }}> Notifications </Text>
                        </View>

                    </TouchableOpacity>

                    <TouchableOpacity>
                        <View style={{ height: 50, width: 100, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 12, color: 'red', fontFamily: 'coolvetica' }}> 80 </Text>
                            <Text style={{ fontSize: 10, color: '#494949', fontFamily: 'coolvetica' }}> Events </Text>
                        </View>

                    </TouchableOpacity>

                    <TouchableOpacity>
                        <View style={{ height: 50, width: 100, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 12, color: 'red', fontFamily: 'coolvetica' }}> 400 </Text>
                            <Text style={{ fontSize: 10, color: '#494949', fontFamily: 'coolvetica' }}> Friends </Text>
                        </View>

                    </TouchableOpacity>

                    <TouchableOpacity>
                        <View style={{ height: 50, width: 100, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 12, color: 'red', fontFamily: 'coolvetica' }}> 1000 </Text>
                            <Text style={{ fontSize: 10, color: '#494949', fontFamily: 'coolvetica' }}> Followers </Text>
                        </View>

                    </TouchableOpacity>

                    <TouchableOpacity>
                        <View style={{ height: 50, width: 100, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 12, color: 'red', fontFamily: 'coolvetica' }}> 55 </Text>
                            <Text style={{ fontSize: 10, color: '#494949', fontFamily: 'coolvetica' }}> Favourites </Text>
                        </View>

                    </TouchableOpacity>
                </View>

            </View>

        );
    }
}
