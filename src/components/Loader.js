import { connect } from 'react-redux';
import React, { Component } from 'react'
import { Text, View, ActivityIndicator, StyleSheet } from 'react-native'
import { AppLoading } from 'expo';
import { autoLoggIn, insertTimelineDataForNewYear } from '../actions'


class Loader extends Component {
    componentWillMount() {
        this.props.autoLoggIn();
    }
    state = {
        isReady: false,
    };
    
    render() {
        return (
            <View style={[styles.container, styles.horizontal]}>
                
                {!this.props.loading? <ActivityIndicator size='large' color="#EC0900" /> : null}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    }
})

const mapStateToProps = (state) => {
    
    let loading = state.auth.loading
    return {
        loading: loading
    };
};
export default connect(null, { autoLoggIn, insertTimelineDataForNewYear })(Loader);