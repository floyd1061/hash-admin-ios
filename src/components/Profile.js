import React, { Component } from 'react'
import { Text, View, ListView, FlatList, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import ListItem from './ListItem'

class Profile extends React.PureComponent {

    /* randomArray = (length, max) => [...new Array(length)]
        .map(() => Math.round(Math.random() * max / 2))
        .filter((elem, pos, arr) => {
            return arr.indexOf(elem) == pos;
        }).sort((a, b) => { return a - b }); */
    
    
    componentWillMount() {
        /* var dataOdd = this.giveOddPoints();
        var dataEven = this.giveEvenPoints();
        var tmpGradientColorFrd = this.generateColor('#e30000', '#ff4200',  366);
        var tmpGradientColorBcrd = this.generateColor('#ff4200', '#e30000',  366);
        ////console.log(tmpGradientColor);
        
            for (var i = 1; i <= 365; i++) {
                var day = {};
                
                //day["name"] = key + ' ' + i;
                //dayList.fill(key + ' ' + i);
                day["year"] = '2018';
                day["key"] = '2018' + i;
                if (i % 2) {
                    day["points"] = dataOdd;
                } else {
                    day["points"] = dataEven;
                }
                day["colorFrd"] = tmpGradientColorFrd[i];
                day["colorBcrd"] = tmpGradientColorBcrd[i];
                dayList.push(day);
            }
            var j = 0;
            for(key in monthDetail){
                var numberOfDays = monthDetail[key].days;
                var name = monthDetail[key].name;
                for(d=1;d<=numberOfDays;d++){ 
                    dayList[j]["name"] = name + ' ' + d;
                    j++;
                }
            }
        //console.log(dayList); */
        //dayList = require('./ProfileData.json');
      

        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.dataSource = ds.cloneWithRows(this.props.data);
    }
    renderRow(item) {
        ////console.log(item);
        return <ListItem item={item} key={item.key} />;
    }
    _keyExtractor = (item, index) => item.key;
    /* saveKey() {
        var fs = require('fs');
        var json = JSON.stringify(dayList);
        fs.writeFile('./ProfileData.json', json, 'utf8', callback);
    } */
    render() {
        //this.saveKey();
        return (
            <View>
                {/* <FlatList
                    data={this.props.data}
                    renderItem={this.renderItem}
                    initialNumToRender={10}
                //keyExtractor={(item, index) => index}
                /> */}
                <ListView
                    dataSource={this.dataSource}
                    renderRow={this.renderRow}
                    initialListSize={10}
                />
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return { data: state.profiles };
};

export default connect(mapStateToProps)(Profile);

const dayList = [];
/* const monthDetail = {
    "Jan": {
        "name": "January",
        "short": "Jan",
        "number": 1,
        "days": 31
    } ,
    "Feb": {
        "name": "February",
        "short": "Feb",
        "number": 2,
        "days": 28
    },
    "Mar": {
        "name": "March",
        "short": "Mar",
        "number": 3,
        "days": 31
    },
    "Apr": {
        "name": "April",
        "short": "Apr",
        "number": 4,
        "days": 30
    },
    "May": {
        "name": "May",
        "short": "May",
        "number": 5,
        "days": 31
    },
    "Jun": {
        "name": "June",
        "short": "Jun",
        "number": 6,
        "days": 30
    },
    "Jul": {
        "name": "July",
        "short": "Jul",
        "number": 7,
        "days": 31
    },
    "Aug": {
        "name": "August",
        "short": "Aug",
        "number": 8,
        "days": 31
    },
    "Sep": {
        "name": "September",
        "short": "Sep",
        "number": 9,
        "days": 30
    },
    "Oct": {
        "name": "October",
        "short": "Oct",
        "number": 10,
        "days": 31
    },
    "Nov": {
        "name": "November",
        "short": "Nov",
        "number": 11,
        "days": 30
    },
    "Dec": {
        "name": "December",
        "short": "Dec",
        "number": 12,
        "days": 31
    }
} */