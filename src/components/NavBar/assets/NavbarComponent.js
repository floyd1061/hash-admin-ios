import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { LinearGradient } from 'expo'

export default class NavbarComponent extends Component {
    render() {
        return (
            <View style={styles.container}>
                <LinearGradient
                    colors={['#ff4200', '#e30000']}
                    >
                    <Text>
                        Skip Walkthrough
                    </Text>
                    
                </LinearGradient>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 10,
        paddingHorizontal: 30,
        height: 90,
    },
});