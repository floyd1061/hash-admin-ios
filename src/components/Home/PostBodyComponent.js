import React, { Component } from 'react'
import { Text, View, StyleSheet, Dimensions } from 'react-native'
var width = Dimensions.get('window').width;
export default class PostBodyComponent extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={{ fontSize: 7,}}>Lorem Ipsum is simply dummy text of the printing
            and typesetting industry. Lorem Ipsum has been the
            industry's standard dummy text ever since the 1500s,
            when an unknown printer took a galley of type and
            scrambled it to make a type specimen book. It has
            survived not only five centuries, but also the leap
            into electronic typesetting, remaining essentially unchanged.
            </Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flexWrap:'wrap',
        width: (width - 60),
        paddingLeft: 5
    },
});