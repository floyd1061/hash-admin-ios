import React, { Component } from 'react'
import {StyleSheet, View, Image } from 'react-native'

export default class ProfilePicComponent extends Component {
  render() {
    return (
      <View>
        <Image style={styles.image} source={require('./assets/profile_pic.jpg')}/>
      </View>
    )
  }
}
const styles = StyleSheet.create({
    image: {
      height: 50,
      borderRadius: 50,
      width: 50
    }
  });