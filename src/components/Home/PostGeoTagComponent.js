import React, { Component } from 'react'
import { Text, View, Image, StyleSheet, Dimensions } from 'react-native'
var width = Dimensions.get('window').width;
export default class PostGeoTagComponent extends Component {
    render() {
        return (
            <View style={{ alignContent: 'center', flexDirection: 'row' }}>
                <View style={{marginLeft: 10}}>
                    <Image source={require('./assets/Location.png')} style={{height: 9.27, width: 7}}/>
                </View>
                <View>
                    <Text style={{ fontSize: 7, fontWeight: 'bold', color: '#fc211a', alignContent: 'center', textAlignVertical: 'bottom', }}> Nevermind Nightclub
                        </Text>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexWrap:'wrap',
        width: ((width * .25) - 80)
    },
})