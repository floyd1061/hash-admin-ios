import React, { Component } from 'react'
import { Text, View, StyleSheet, Image } from 'react-native'
import { LinearGradient } from 'expo'

export default class NavbarComponent extends Component {
    render() {
        return (
            <View style={{ marginBottom: 15 }}>
                <LinearGradient
                    colors={['#ff4200', '#e30000']}>
                    <View style={styles.toolbar}>
                        <Text style={styles.toolbarTitle}>Hashtazapp                 </Text>
                            <View>
                                <Image source={require('./assets/messeanger.png')} style={{ height: 30, width: 32, }} />
                            </View>
                            <View>
                                <Image source={require('./assets/search.png')} style={{ height: 30, width: 32, marginLeft: 10 }} />
                            </View>
                            <View>
                                <Image source={require('./assets/followers.png')} style={{ height: 30, width: 32, marginLeft: 10 }} />
                            </View>
                    </View>
                </LinearGradient>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    toolbar: {
        paddingTop: 30,
        paddingBottom: 10,
        flexDirection: 'row',
        height: 75,
        paddingHorizontal: 20,
        justifyContent: 'center'    //Step 1
    },
    toolbarButton: {
        width: 50,            //Step 2
        color: '#fff',
        textAlign: 'center',
        flex: 1
    },
    toolbarTitle: {
        color: '#fff',
        textAlign: 'center',
        fontWeight: 'bold',

        fontSize: 20             //Step 3
    }
});