import React, { Component } from 'react'
import { Text, View } from 'react-native'

export default class CommentLikeCountComponent extends Component {
  render() {
    return (
      <View style={{marginLeft: 10}}>
        <Text style={{fontSize: 7, fontWeight:'500'}}> 1 Comment  54 Likes </Text>
      </View>
    )
  }
}