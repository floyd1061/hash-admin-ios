import React, { Component } from 'react'
import { Text, View, Dimensions, StyleSheet } from 'react-native'
var width = Dimensions.get('window').width;
export default class PostheaderComponent extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={{ fontSize: 7, fontWeight: 'bold', color: '#fc211a', 
                alignContent: 'center', textAlignVertical: 'bottom', }}>
                The night I lost it at Amanda's party 
                        </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexWrap:'wrap',
        width: ((width * .5) - 50),
        paddingLeft: 5
    },
});