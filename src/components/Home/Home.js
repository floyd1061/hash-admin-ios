import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';

import ProfilePicComponent from './ProfilePicComponent'
import NavbarComponent from './NavbarComponent'
import HeaderTextComponent from './HeaderTextComponent'
import MediaContainerComponent from './MediaContainerComponent'
import PostheaderComponent from './PostheaderComponent'
import PostGeoTagComponent from './PostGeoTagComponent'
import CommentLikeCountComponent from './CommentLikeCountComponent'
import PostBodyComponent from './PostBodyComponent'
import CommentLikeShareButtonComponent from './CommentLikeShareButtonComponent'
import CommentSectionListComponent from './CommentSectionListComponent'
export default class Home extends Component {
    render() {
        return (
            <View style={styles.container}>
                <NavbarComponent />
                <View style={styles.storyContainer}>
                    <KeyboardAvoidingView behavior="position">
                        <View style={styles.headerContainer}>
                            <ProfilePicComponent />
                            <HeaderTextComponent />
                        </View>

                        <View style={styles.mediaContainer}>
                            <MediaContainerComponent />
                        </View>

                        <View style={styles.postInfoHeader}>
                            <PostheaderComponent />
                            <PostGeoTagComponent />
                            <CommentLikeCountComponent />
                        </View>

                        <View style={styles.postBody}>
                            <PostBodyComponent />
                        </View>
                        <View style={styles.coometLikeUserInput}>
                            <CommentLikeShareButtonComponent />
                        </View>
                        <ScrollView >
                            <CommentSectionListComponent />
                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#dedede'

    },
    headerContainer: {
        flexDirection: 'row',
        marginBottom: 10,
        alignContent: 'center'
    },
    postInfoHeader: {
        flexDirection: 'row',
        marginBottom: 5,
    },
    postBody: {
        flexDirection: 'row',
        marginBottom: 7,
    },
    mediaContainer: {
        flexDirection: 'row',
        marginBottom: 8,
    },
    storyContainer: {
        flex: 1,
        flexDirection: 'column',
        paddingHorizontal: 30,
    },
    navbar: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 90
    },
    coometLikeUserInput: {
        marginBottom: 7
    },
    commectReplySectionList: {
        flexDirection: 'row'
    }
});