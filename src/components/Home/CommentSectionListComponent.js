import React, { Component } from 'react'
import { Text, View, StyleSheet, SectionList, FlatList, Image, TouchableOpacity } from 'react-native'


export default class CommentSectionListComponent extends Component {
    renderLikeButton(item) {
        if (item.youLiked) {
            return <Image
                source={require('./assets/likeRed.png')} />
        } else {
            return <Image
                source={require('./assets/like_gray.png')} />
        }
    }
    render() {
        const comments =
            [
                {
                    key: "1", userName: "Sarah P.", commentText: "We had a great party where were you fred", profilePic: require('./assets/profile_pic.jpg'), time: "03 January at 22.25", likeCount: 5, youLiked: false,
                    data: [{ key: "2", userName: "Shermon Khan", commentText: "We missed you forid vi", profilePic: require('./assets/shermon.jpg'), time: "04 January at 10.15", likeCount: 3, youLiked: true, }
                        ,
                    {
                        key: "3", userName: "Freddy", commentText: "Great party there", profilePic: require('./assets/forid.jpg'), time: "05 January at 22.18", likeCount: 0, youLiked: false,
                    }]
                },
                {
                    key: "4", userName: "Sarah P.", commentText: "I wish you were here honey..", profilePic: require('./assets/profile_pic.jpg'), time: "05 January at 16.19", likeCount: 1, youLiked: false,
                    data: [{
                        key: "5", userName: "Abhijit Majumder", commentText: "The party looks too sick for my taste!!", profilePic: require('./assets/abhi.jpg'), time: "05 January at 16.18", likeCount: 3, youLiked: true,
                    }]
                },
                {
                    key: "6", userName: "Sarah P.", commentText: "Hi5 !! hey are you coming at 4??", profilePic: require('./assets/profile_pic.jpg'), time: "09 January at 15.01", likeCount: 11, youLiked: false,
                    data: [{
                        key: "7", userName: "Deepa", commentText: "Hi5 sarah!! we had fun..tons of it", profilePic: require('./assets/deepa.jpg'), time: "09 January at 14.14", likeCount: 5, youLiked: false,
                    }
                    ]
                }
            ];
    return (
            <View style={styles.container}>
                <SectionList
                    sections={comments}
                    renderItem={({ item }) => <View style={styles.replyContainer}>
                        <View style={styles.picContainer}>
                            <Image style={styles.profilePic}
                                source={item.profilePic}></Image>
                        </View>
                        <View >
                            <Text style={styles.userNameTextStyle}>{item.userName}
                            </Text>
                            <Text style={styles.commentTextStyle}>{item.commentText}</Text>
                            <View style={{ marginTop: 2 }}>
                                <Text style={styles.userNameTextStyle}>Like -
                                <Text> Reply - </Text>
                                    {this.renderLikeButton(item)}
                                    <Text style={{ fontSize: 8, color: 'black' }}> {item.likeCount}   </Text>
                                    <Text style={styles.commentTextStyleGray}>{item.time}
                                    </Text>
                                </Text>
                            </View>
                        </View>
                    </View>}
                    renderSectionHeader={({ section }) => <View style={styles.commentContainer}>
                        <View style={styles.picContainer}>
                            <Image style={styles.profilePic}
                                source={section.profilePic}></Image>
                        </View>
                        <View >
                            <Text style={styles.userNameTextStyle}>{section.userName}
                            </Text>
                            <Text style={styles.commentTextStyle}>{section.commentText}</Text>
                            <View style={{ marginTop: 2 }}>
                                <Text style={styles.userNameTextStyle}>Like -
                                    <Text> Reply - </Text>
                                    {this.renderLikeButton(section)}
                                    <Text style={{ fontSize: 8, color: 'black' }}> {section.likeCount}   </Text>
                                    <Text style={styles.commentTextStyleGray}>{section.time}
                                    </Text>
                                </Text>
                            </View>
                        </View>
                    </View>}
                    keyExtractor={(item, index) => index}
                    scrollEnabled={true}
                />
            </View>
        )
    }
}

const imagePat = require('./assets/limu.jpg')
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginRight: 20,
        paddingLeft: 10,
        justifyContent: 'center',
    },
    commentContainer: {
        flex: 1,
        flexDirection: 'row',
        alignContent: 'center',
        marginTop: 5
    },
    replyContainer: {
        flexDirection: 'row',
        paddingLeft: 30,
        marginTop: 5
    },
    picContainer: {
        marginRight: 5
    },
    profilePic: {
        height: 25,
        borderRadius: 25,
        width: 25
    },
    commentSection: {
        flexDirection: 'column'
    },
    commentSectionText: {
        flexDirection: 'row'
    },
    commentLikeReplySection: {
        flexDirection: 'row'
    },
    userNameTextStyle: {
        fontSize: 8,
        fontWeight: 'bold',
        color: '#fc211a',
        alignContent: 'center',
        textAlignVertical: 'bottom',
    },
    commentTextStyle: {
        fontSize: 8,
        color: 'black',
        alignContent: 'center',
        textAlignVertical: 'bottom',
    },
    commentTextStyleGray: {
        fontSize: 8,
        color: '#787878',
        alignContent: 'center',
        textAlignVertical: 'bottom',
    },
    commentLikeIcon: {
        height: 20,
        width: 20
    }

});