import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TextInput, KeyboardAvoidingView } from 'react-native'

export default class CommentLikeShareButtonComponent extends Component {
    render() {
        return (

            <View style={styles.container}>
                <View style={styles.buttonContainer}>
                    <Image style={styles.likeIcon} source={require('./assets/like.png')} />
                    <Text style={{ fontSize: 7, fontWeight: '500', color: '#ff0508', flex: 1, paddingTop: 8 }}>  Like</Text>
                </View>
                <View style={styles.commentTextInputSection}>
                    <TextInput
                        style={{ flex: 1, fontSize: 8 }}
                        placeholder="Write Comment..."
                        returnKeyType="go"
                        underlineColorAndroid="transparent"
                    />
                    <View style={styles.uploadAndCamerabuttonSection}>
                        <View style={{ marginRight: 5 }}><Image style={styles.uploadCamera} source={require('./assets/upload.png')} /></View>
                        <View><Image style={styles.uploadCamera} source={require('./assets/camera.png')} /></View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        paddingLeft: 10,
        alignItems: 'center',
    },
    buttonContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10
    },
    commentTextInputSection: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderWidth: .5,
        borderColor: '#fff',
        height: 15,
        marginBottom: 8,
        marginRight: 10

    },
    uploadAndCamerabuttonSection: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingRight: 5
    },
    likeIcon: {
        width: 15,
        height: 15
    },
    uploadCamera: {
        width: 10,
        height: 10
    }
});