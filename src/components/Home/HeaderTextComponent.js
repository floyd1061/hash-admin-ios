import React, { Component } from 'react'
import { Text, View } from 'react-native'

export default class HeaderTextComponent extends Component {
    render() {
        return (
            <View style={{ flexDirection: 'column', marginLeft: 10, paddingTop: 2}}>
                {/* data bind later to text */}
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontSize: 18, fontWeight: '500', alignContent: 'center', textAlignVertical: 'bottom' }}>Sarah Parker
                    <Text style={{ fontSize: 8, fontWeight: 'bold', alignContent: 'center', textAlignVertical: 'bottom', }}>  Shared With
                    <Text style={{ fontSize: 8, fontWeight: 'bold', color: '#fc211a', alignContent: 'center', textAlignVertical: 'bottom', }}>  John</Text>
                        </Text>
                    </Text>
                </View>
                {/* data bind later to text */}
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontSize: 10, fontWeight: '500', alignContent: 'center', textAlignVertical: 'bottom' }}>At Amanda's party
                    <Text style={{ fontSize: 8, fontWeight: 'bold', color: '#fc211a', alignContent: 'center', textAlignVertical: 'bottom', }}>  01 January at 22:18
                        </Text>
                    </Text>
                </View>
            </View>
        )
    }
}