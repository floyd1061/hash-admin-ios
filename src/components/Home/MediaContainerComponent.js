import React, { Component } from 'react'
import { Text, View, Dimensions, StyleSheet } from 'react-native'


var width = Dimensions.get('window').width;
var widthWithoutMargin = width - 70;
const height = (widthWithoutMargin * .4);
export default class MediaContainerComponent extends Component {
    render() {
        return (
            <View style={styles.container}>
                {/* conditional load for number of content maximum for on layout */}
                <View style={styles.imageContainerOne}>
                </View>
                <View style={styles.imageContainerTwo}>
                </View>
                <View style={styles.imageContainerThreeMain}>
                    <View style={styles.imageContainerThreeSubOne}>

                    </View>
                    <View style={styles.imageContainerThreeSubTwo}>

                    </View>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
    },
    imageContainerOne: {
        width: widthWithoutMargin * .4,
        height: height,
        backgroundColor: '#fff',
        marginRight: 5
    },
    imageContainerTwo: {
        width: widthWithoutMargin * .4,
        height: height,
        backgroundColor: '#fff',
        marginRight: 5
    },
    imageContainerThreeMain: {
        width: ((widthWithoutMargin * .2)),
        height: height,
        flexDirection: 'column',
    },
    imageContainerThreeSubOne: {
        width: ((widthWithoutMargin * .2)),
        height: (height / 2) - 2.5,
        backgroundColor: '#fff',
        marginBottom: 5
    },
    imageContainerThreeSubTwo: {
        width: ((widthWithoutMargin * .2)),
        height: (height / 2) - 2.5,
        backgroundColor: '#fff',
    },
});