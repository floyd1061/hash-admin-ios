import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import NetworkItem from './NetworkItem'
import {LinearGradient, Font} from 'expo'

export default class NetworkUsage extends Component {

    state = {
        fontLoaded: true,
    };

   
    render() {
        return (

            <LinearGradient style = {styles.networkContainer} colors = { ['#E10100', '#FD0C05'] }>
                <View style = {styles.networkInfoContainer}>
                {
                    this.state.fontLoaded ? ( <Text style = {styles.header}>Network Usage</Text> ) : null
                }
                    <NetworkItem name='Total Profile Media Usage:' value='21mb'/>
                    <NetworkItem name='Total Message Sent:' value='321'/>
                    <NetworkItem name='Total Message Recieved:' value='31'/>
                    <NetworkItem name='Total Outgoing Calls:' value='4431'/>
                    <NetworkItem name='Total Incoming Calls:' value='21'/>
                    <NetworkItem name='Total Media Shared' value='551'/>
                </View>
                <TouchableOpacity> 
                    <LinearGradient colors={['#ff2002', '#df1d04']} style = {styles.buttonStyle}>
                        {
                            this.state.fontLoaded ? ( <Text style = {styles.buttonText}>Reset Statistics</Text> ) : null
                        }
                    </LinearGradient>
                </TouchableOpacity>
            </LinearGradient>
            
    
        );
    }
};

const styles = {
    networkContainer: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 40,
    },

    networkInfoContainer: {
        backgroundColor: '#2A2A2A', 
        alignSelf: "stretch", 
        height: 430,
        borderWidth: 3,
        borderColor: '#fff',
        marginBottom: 30
    },

    header: {
        paddingHorizontal: 17,
        paddingVertical: 10, 
        marginHorizontal: 3,   
        fontSize: 30,
        color: '#8F8F8F',
        fontFamily: 'coolvetica',
        borderBottomWidth: 1,
        borderColor: '#3A3A3A'
    },

    buttonStyle: {
        width: 130,
        height: 40,
        borderWidth: 1,
        borderColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 3,      
    },
    
    buttonText: {
        color: '#fff',
        fontFamily: 'coolvetica'
    }
}

