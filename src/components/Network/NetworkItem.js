import React, { Component } from 'react'
import { View, Text } from 'react-native';
import {Font} from 'expo'

export default class NetworkItem extends Component {

    state = {
        fontLoaded: true,
    };

   
    render() {
        
        return (
            <View style = {styles.container}>
                {
                    this.state.fontLoaded ? ( <Text style={styles.name}>{this.props.name}</Text> ) : null
                }
                {
                    this.state.fontLoaded ? ( <Text style={styles.value}>{this.props.value}</Text> )  : null
                }        
            </View>
        );
    }
};

const styles = {
    container: {
        paddingHorizontal: 20,
        paddingVertical: 10,  
    },

    name: {
        color: '#fff',
        fontFamily: 'coolvetica'
    },
    
    value: {
        color: '#E80B00',
        fontFamily: 'coolvetica'
    }
};