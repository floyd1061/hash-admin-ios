import React, { Component } from 'react'
import { View, Text, TextInput, Image, TouchableOpacity, StyleSheet } from 'react-native'



export default class ActivityListItem extends Component {

    renderByActivityType(item) {
        // //console.log(item);
        switch (item.activityType) {
            case 'followUser'
                : return (
                    <View style={styles.rowSec}>
                        <TouchableOpacity>
                            <Image source={require('./assets/avatar.png')} style={styles.avataricon} />
                        </TouchableOpacity>

                        <View style={styles.textsec}>
                            <Text style={styles.Text2}> You Started Following </Text>
                            <TouchableOpacity>
                                <Text style={styles.Text1}> {item.followedUser.firstName} {item.followedUser.lastName} </Text>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.box1Button}>
                            <Image source={require('./assets/load.png')} style={styles.loadicon} />
                            <Image source={require('./assets/plus.png')} style={styles.plusicon} />
                            <View style={styles.box2Button}>
                                <Text style={styles.followText}> Follow </Text>
                            </View>
                        </View>
                    </View>
                );

            case 'favoriteEvent' 
                : return (
                    <View style={styles.rowSec}>
                        <TouchableOpacity>
                            <Image source={require('./assets/avatar.png')} style={styles.avataricon} />
                        </TouchableOpacity>

                        <View style={styles.textsec}>
                            <Text style={styles.Text3}> You Favouritised an Event Day </Text>
                        </View>
    
                        {/* {//console.log(item)} */}
                        {this.styleForEventBubble(item)}
                        
                    </View>
                );

            case 'friendsUser' 
                : return (
                    <View style= { styles.rowSec }>
                        <TouchableOpacity>
                            <Image source={require('./assets/avatar.png')} style={styles.avataricon} />
                        </TouchableOpacity>

                        <View style= { styles.textsec }>
                            <Text style = {styles.Text2}> Now You Are Friend With </Text>
                            <TouchableOpacity>
                                <Text style = {styles.Text1}> {item.friendedUser.firstName} {item.friendedUser.lastName} </Text>
                            </TouchableOpacity>
                        </View>

                        <View style= { styles.box3Button }>
                            <Image source={require('./assets/likeactive.png')} style={styles.likeactiveicon} />
                            
                            <View style= { styles.box4Button }>
                                <Text style= { styles.friendText }>Friends</Text>
                            </View>
                        </View>
                    </View>
                );

            case 'friendRequestUser' 
                : return (
                    <View style= { styles.rowSec }>
                        <TouchableOpacity>
                            <Image source={require('./assets/avatar.png')} style={styles.avataricon} />
                        </TouchableOpacity>

                        <View style= { styles.textsec }>
                            <Text style = {styles.Text2}> Friend Request Sent </Text>
                            <TouchableOpacity>
                                <Text style = {styles.Text1}> {item.friendRequestedUser.firstName} {item.friendRequestedUser.lastName} </Text>
                            </TouchableOpacity>
                        </View>

                        <View style= { styles.box5Button }>
                            <Image source={require('./assets/like.png')} style={styles.likeicon} />
                            <Image source={require('./assets/plus.png')} style={styles.plusicon2} />
                            
                            <View style= { styles.box6Button }>
                                <Text style= { styles.ReqbuttonText }>Friend Request</Text>
                            </View>
                        </View>
                    </View>

                );
        }
    }

    styleForEventBubble(item){
        if(item.favouritedEvent.isHighlighted)
            return (
                <View>
                    <View style={[styles.talkBubbleSquare,{backgroundColor: '#ff0000'}]} >    
                        <Text style={{textAlign:'center', color:'#fff', fontWeight: 'bold', fontSize: 15}}>{item.favouritedEvent.eventName}</Text>
                        <Text style={{textAlign:'center', fontSize:8, color:'#fff' }}>{item.favouritedEvent.eventStartTime} - {item.favouritedEvent.eventEndTime}</Text>
                    </View>
                    <View style={styles.boxTriangle}/>
                    <View style={[styles.talkBubbleTriangle, {borderRightColor: '#ff0000'}]}/>
                </View>
            )
            return (
                <View>
                    <View style={styles.talkBubbleSquare} >    
                        <Text style={{textAlign:'center', color:'#fff', fontWeight: 'bold', fontSize: 15}}>{item.favouritedEvent.eventName}</Text>
                        <Text style={{textAlign:'center', fontSize:8, color:'#fff' }}>{item.favouritedEvent.eventStartTime} - {item.favouritedEvent.eventEndTime}</Text>
                    </View>
                    <View style={styles.boxTriangle}/>
                    <View style={styles.talkBubbleTriangle}/>
                </View>
            )
    }

    render() {
        // //console.log("new item" + this.props.item);
        const item = this.props.item;
        return (
            <View style={styles.bodyContainer}>

                <View style={styles.datetimeSec}>

                    <Text style={styles.datetimeText}> {item.dateTime } </Text>
                    <Image source={require('./assets/time.png')} style={styles.timeicon} />

                </View>

                {this.renderByActivityType(item)}

            </View>
        )
    }
}

const styles = StyleSheet.create({
    bodyContainer: {
        flex: 1,
        padding: 20
    },

    datetimeSec: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: 20,
    },

    datetimeText: {
        fontSize: 14,
        color: '#7C7C7C',
        top: 8,
        fontWeight: 'bold'
    },

    timeicon: {
        height: 25,
        width: 25,
        resizeMode: 'stretch',
    },

    rowSec: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 0,
        // backgroundColor: '#eee'
    },

    avataricon: {
        height: 80,
        width: 80,
        resizeMode: 'stretch',
    },

    textsec: {
        flex: 1,
        paddingLeft: 5,
        marginTop: 24,
        paddingRight: 5,
    },

    Text1: {
        fontSize: 12,
        color: '#FF0408',
        fontWeight: 'bold',
    },

    Text2: {
        fontSize: 12,
        color: '#000',
        fontWeight: 'bold'
    },

    box1Button: {
        width: 125,
        height: 42,
        backgroundColor: '#FFFFFF',
        borderRadius: 2,
        marginTop: 20,
    },

    box2Button: {
        width: 78,
        height: 38,
        backgroundColor: '#7C7C7C',
        bottom: 26,
        left: 45
    },

    followText: {
        fontSize: 16,
        color: '#FFFFFF',
        textAlign: 'right',
        paddingRight: 11,
        top: 6,
        fontWeight: 'bold'
    },

    loadicon: {
        height: 20,
        width: 20,
        resizeMode: 'stretch',
        left: 15,
        top: 12
    },

    plusicon: {
        height: 8,
        width: 8,
        resizeMode: 'stretch',
        left: 32,
        top: 3
    },

    Text3: {
        fontSize: 12,
        color: '#000',
        fontWeight: 'bold',
        top: 12
    },

    likeactiveicon: {
        height: 20,
        width: 20,
        resizeMode: 'stretch',
        left: 15,
        top: 10
    },

    box3Button: {
        width: 125,
        height: 42,
        backgroundColor: '#FFFFFF',
        borderRadius: 2,
        marginTop: 20,
    },

    box4Button: {
        width: 78,
        height: 36,
        backgroundColor: '#FF1103',
        bottom: 17,
        left: 44
    },

    friendText: {
        fontSize: 16,
        color: '#FFFFFF',
        textAlign: 'right',
        paddingRight: 11,
        top: 6,
        fontWeight: 'bold'
    },

    box5Button: {
        width: 125,
        height: 42,
        backgroundColor: '#FFFFFF',
        borderRadius: 2,
        marginTop: 20,
    },

    box6Button: {
        width: 78,
        height: 36,
        backgroundColor: '#7C7C7C',
        bottom: 25,
        left: 45
    },

    ReqbuttonText: {
        fontSize: 10,
        color: '#FFFFFF',
        textAlign: 'center',
        top: 9,
        fontWeight: 'bold'
    },

    likeicon: {
        height: 20,
        width: 20,
        resizeMode: 'stretch',
        left: 15,
        top: 10
    },

    plusicon2: {
        height: 8,
        width: 8,
        resizeMode: 'stretch',
        left: 32,
        bottom: 7
    },

    //eventbox style
    talkBubbleSquare: {
        width: 83,
        height: 70,
        backgroundColor: '#565656',
        borderRadius: 10,
        borderWidth: 4,
        borderColor: '#fff',
        justifyContent:'center',
        top: 8
        // lineHeight:5 
      },

      boxTriangle:{
        position: 'relative',
        left: -16,
        bottom: 37,
        width: 0,
        height: 0,     
        borderTopColor: 'transparent',
        borderTopWidth: 10,
        borderRightWidth: 20,
        borderRightColor: '#fff',
        borderBottomWidth: 10,
        borderBottomColor: 'transparent',      
      },

      talkBubbleTriangle: {
        position: 'absolute',
        left: -8,    
        top: 34,
        width: 0,
        height: 0,     
        borderTopColor: 'transparent',
        borderTopWidth: 9,
        borderRightWidth: 16,  
        borderRightColor: '#565656',
        borderBottomWidth: 9,
        borderBottomColor: 'transparent',
      },


});