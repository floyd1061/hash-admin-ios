import React, { Component } from 'react'
import { View, Text, TextInput, Image, TouchableOpacity, StyleSheet, ListView } from 'react-native'
import ActivityListItem from './ActivityListItem'
import { connect } from 'react-redux'

const messageCount = 50;

class Activity extends Component {

    componentWillMount() {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.dataSource = ds.cloneWithRows(this.props.activitydata);
    }
    renderRow(activitydata) {
        ////console.log(item);
        return <ActivityListItem activitydata={activitydata} />;
    }
  
    render() {
        // //console.log(this.props);
        return (
        <View style = {styles.Container}>
        
            <View style = {styles.HeaderSection}>


                    <View style = {styles.headeritem}>
                        <Text style = {styles.headerText1}>Hashtazapp</Text>
                        <Text style = {styles.headerText2}>My Activity</Text>
                    </View> 

                    <View style = {styles.headeritem}>
                        <TouchableOpacity >                   
                            <View style = {styles.messagearea}>
                                <Image source={require('./assets/messagenumber.png')} style={styles.messageicon} />
                                <Text style = {styles.messagecount}> {messageCount} </Text>
                            </View>
                        </TouchableOpacity>
                    
                        <TouchableOpacity>
                            <Image source={require('./assets/searchicon.png')} style={styles.searchicon} />
                        </TouchableOpacity>

                        <TouchableOpacity>
                            <Image source={require('./assets/followersnumber.png')} style={styles.followersnumber} />
                        </TouchableOpacity>

                    </View>

            </View>

            <View style= { styles.bodySection }>
                
                <ListView
                    dataSource={this.dataSource}
                    renderRow={this.renderRow}
                    initialListSize={10}
                />
                {/* <ActivityListItem/> */}
                
            </View>

        </View>

        )
    }
}


const styles = StyleSheet.create({

    Container: {
        flex: 1,
    },

    HeaderSection: {
        backgroundColor: '#FF1004',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },

    headeritem: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
        paddingBottom: 100,
        top: 35,
        left: 25,
    },

    headerText1: {
        fontSize: 23,
        color: '#fff',
        fontWeight: 'bold'
    },

    messagearea: {
        position: 'relative',
    },

    messageicon: {
        height: 37,
        width: 37,
        resizeMode: 'stretch',
        marginRight: 25  
    },

    messagecount: {
        position: 'absolute',
        top: -8,
        color: '#fff',
        backgroundColor: '#EB1406',
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#fff',
        width: 20,
        height: 20,
        fontSize: 8,
        paddingTop: 5,
        textAlign: 'center',
        left: 32
    },  

    searchicon: {
        height: 36,
        width: 36,
        resizeMode: 'stretch',
        marginRight: 20
    },

    followersnumber: {
        height: 40,
        width: 40,
        resizeMode: 'stretch',
    },

    headerText2: {
        fontSize: 23,
        color: '#fff',
        top: 60,
        right: 100,
        fontWeight: 'bold',
        padding: 10
    },

    bodySection: {
        flex: 1,
        backgroundColor: '#DEDEDE',
        // justifyContent: 'center',
        // alignItems: 'center'

    }


});


const mapStateToProps = (state) => {
    return { activitydata: state.activities };
};

export default connect(mapStateToProps)(Activity);