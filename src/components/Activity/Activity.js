import React, { Component } from 'react'
import { View, Text, TextInput, Image, TouchableOpacity, StyleSheet, ListView } from 'react-native'
import ActivityListItem from './ActivityListItem'
import { connect } from 'react-redux'
import index from '../../reducers';

const messageCount = 50;

// const x = 0;

class Activity extends Component {
    
    componentWillMount() {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.dataSource = ds.cloneWithRows(this.props.activitydata);
    }
    renderRow(item) {
        // //console.log(item);
        return <ActivityListItem item = {item} />;
    }

    /* renderText(){
        if(this.props.isHighted)
            return <Text style = {[styles.textstyle, {color: '#000'}]}>This is a simple textAlign</Text>

        return <Text style = {{color: '#ddd'}}>This is a simple textAlign</Text>        
    } */
  
    render() {
        ////console.log(this.props);
        return (
        <View style = {styles.Container}>
        
            <View style = {styles.HeaderSection}>

                    <View style = {styles.headeritem}>
                        <Text style = {styles.headerText1}>Hashtazapp</Text>
                        <Text style = {styles.headerText2}>My Activity</Text>
                    </View> 

                    <View style = {styles.headeritem}>
                        <TouchableOpacity style = {styles.messagearea}>                   
                                <Image source={require('./assets/messagenumber.png')} style={styles.messageicon} />
                                <Text style = {styles.messagecount}> {messageCount} </Text>
                        </TouchableOpacity>
                    
                        <TouchableOpacity>
                            <Image source={require('./assets/searchicon.png')} style={styles.searchicon} />
                        </TouchableOpacity>

                        <TouchableOpacity>
                            <Image source={require('./assets/followersnumber.png')} style={styles.followersnumber} />
                        </TouchableOpacity>

                    </View>

            </View>
            <View style= { styles.bodySection }>
                
                <ListView
                    dataSource={this.dataSource}
                    renderRow={this.renderRow}
                    initialListSize={10}
                />
                {/* <ActivityListItem/> */}
                
            </View>

        </View>

        )
    }
}

const styles = StyleSheet.create({

    Container: {
        flex: 1,
    },

    HeaderSection: {
        backgroundColor: '#FF1004',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: 100,
    },

    headeritem: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
        top: 35,
        left: 25,
    },

    headerText1: {
        fontSize: 23,
        color: '#fff',
        fontWeight: 'bold'
    },

    headerText2: {
        fontSize: 23,
        color: '#fff',
        top: 60,
        right: 100,
        fontWeight: 'bold',
        padding: 10
    },

    messagearea: {
        position: 'relative',
    },

    messageicon: {
        height: 37,
        width: 37,
        resizeMode: 'stretch',
        marginRight: 25  
    },

    messagecount: {
        position: 'absolute',
        top: -6,
        color: '#fff',
        backgroundColor: '#EB1406',
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#fff',
        width: 20,
        height: 20,
        fontSize: 8,
        paddingTop: 5,
        textAlign: 'center',
        left: 32,
    },  

    searchicon: {
        height: 36,
        width: 36,
        resizeMode: 'stretch',
        marginRight: 20
    },

    followersnumber: {
        height: 40,
        width: 40,
        resizeMode: 'stretch',
    },

    bodySection: {
        flex: 1,
        backgroundColor: '#DEDEDE',
    }


});

const mapStateToProps = (state) => {
    return { activitydata: state.activities };
};

export default connect (mapStateToProps)(Activity);