import React, { Component } from 'react'
import {StyleSheet} from 'react-native'
import { LinearGradient } from 'expo'
import AdSettingsForm from './AdSettingsForm'

export default class AdSettings extends Component {
  render() {
    return (
      <LinearGradient colors={['#E60300', '#FF0906']} style={ styles.container }>
        <AdSettingsForm />
      </LinearGradient>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});