import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, Text} from 'react-native'
import { LinearGradient } from 'expo'
import ToggleSwitch from 'toggle-switch-react-native'
import { Font } from 'expo'

export default class AdSettingsForm extends Component {
  
  onToggle(isOn){
    // alert('' + isOn)
  }

  state = {
    fontLoaded: true,
  };



  render() {
    return (
      <View style={styles.container}>

        <View style={styles.Box}>

          <View style={styles.SectionStyletext}>
            {
              this.state.fontLoaded ? <Text style={styles.text1Style}>Ads</Text> :null 
            }
          </View>

          <View style={styles.BoxStyle}>

            <View style={styles.BoxSection1}>

              <View style= {styles.box1}>
                {
                  this.state.fontLoaded ? <Text style={styles.textStyle1}>Disable Ads</Text> :null
                }
                {
                  this.state.fontLoaded ? <Text style={styles.textStyle2}>Do not show sponsored ads on my homepage.</Text> :null
                }
              </View>
              
              <View style= {styles.box2}>
                <ToggleSwitch onToggle={this.onToggle} onColor="#FF341C" size= 'default'/>
              </View>

            </View>

            <View style={styles.BoxSection2}>
              <View style= {styles.box3}>
              {
                this.state.fontLoaded ? <Text style={styles.textStyle3}>Block Video Share</Text> :null
              }
              {
                this.state.fontLoaded ? <Text style={styles.textStyle4}>Do not allow anonymous users to share my videos.</Text> :null
              }
              </View>

              <View style= {styles.box4}>
                <ToggleSwitch onToggle={this.onToggle} onColor="#FF341C" size= 'default'/>
              </View>
            </View>
          </View>
        </View>

        <View style={styles.buttonCenter}>
          <TouchableOpacity>
            <LinearGradient
              colors={['#ff2002', '#df1d04']}
              style={styles.SectionStyleDeleteButton}>
              {
                this.state.fontLoaded ? <Text style={styles.buttonText}>Apply Now</Text> :null
              }
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  Box: {
    alignSelf: 'stretch',
    backgroundColor: '#282828',
    borderWidth: 3,
    borderColor: 'rgb(255, 255, 255)',
    elevation: 3,
    marginHorizontal: 30
  },

  buttonCenter: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  SectionStyleDeleteButton: {
    borderWidth: 2,
    borderColor: '#fff',
    borderRadius: 3,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 12
  },

  buttonText: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 18,
    fontFamily: 'coolvetica',
  },

  SectionStyletext:{
    paddingHorizontal: 15,
    paddingVertical: 10
  },

  text1Style: {
    color: '#B2B2B2',
    fontSize: 38,
    fontFamily: 'coolvetica',
  },

  BoxStyle: {
    paddingHorizontal: 15,
  },

  BoxSection1: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    marginBottom: 10,
    borderRadius: 3,
    padding: 10
  },

  box1: {
    top: 22
  },

  box2: {
    top: 10
  },

  BoxSection2: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    marginBottom: 15,
    borderRadius: 3,
    padding: 10
  },

  box3: {
    top: 22
  },

  box4: {
    top: 10
  },

  textStyle1: {
    fontSize: 22,
    paddingTop: 8,
    color: '#919191',
    lineHeight: 28,
    bottom: 25,
    fontFamily: 'coolvetica'
  },

  textStyle2: {
    fontSize: 8,
    color: '#FB1700',
    bottom: 30,
    fontFamily: 'coolvetica'
  },

  textStyle3: {
    fontSize: 22,
    paddingTop: 8,
    color: '#919191',
    lineHeight: 28,
    bottom: 25,
    fontFamily: 'coolvetica'
  },

  textStyle4: {
    fontSize: 8,
    color: '#FB1700',
    bottom: 30,
    fontFamily: 'coolvetica'
  },
});    
