import React, { Component } from 'react';
import { StyleSheet, View, Text, TextInput } from 'react-native';
import DatePicker from 'react-native-datepicker';
import firebase from '@firebase/app'
import auth from '@firebase/auth'
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import {
    fetchUserProfileInfo,
    birthDayChange,
    updateUserBirthday,
    statusChanged,
    updateUserStatus,
    descriptionChanged,
    updateUserDescription,
    updateDisplayName
} from '../../../actions';

class ProfileBox extends Component {
    constructor(props) {
        super(props)
        this.state = { date: "01-01-1990", userName: "", userStatus: "", userDescription: "", fontLoaded: true }
    }
    statusChanged(userStatus) {
        this.props.statusChanged(userStatus);
    }
    descriptionChanged(userDescription) {
        this.props.descriptionChanged(userDescription);
    }

    updateUserBirthday(date) {
        this.setState({ date: date });
        this.props.updateUserBirthday(date);
    }
    componentWillMount() {
        // this.props.fetchUserProfileInfo();
        const { currentUser } = firebase.auth();

        if (currentUser != null) {
            this.setState({
                userName: currentUser.displayName,
            });
        }
    }

    displayNameChanged(userName) {
        this.setState({
            userName: userName
        })
    }

    render() {
        return (
            <View style={styles.textView}>
                {
                    this.state.fontLoaded ? (<TextInput style={styles.Text1Style}
                        value={this.state.userName}
                        onChangeText={this.displayNameChanged.bind(this)}
                        underlineColorAndroid="transparent"
                        onSubmitEditing={() => this.props.updateDisplayName(this.state.userName)}
                    ></TextInput>) : null
                }
                <View style={styles.DayInputStyle}>
                    {
                        this.state.fontLoaded ? (<Text style={styles.Text3Style}>Date of Birth  </Text>) : null
                    }
                    {
                        this.state.fontLoaded ? (<DatePicker
                            style={{ width: 130, paddingRight: 55, }}
                            androidMode='spinner'
                            date={this.state.date}
                            mode="date"
                            placeholder="___.___.___"
                            cancelBtnText="Cancel"
                            confirmBtnText="Done"
                            format="DD-MM-YYYY"
                            minDate="01-01-1950"
                            maxDate="30-12-2005"
                            allowFontScaling={true}
                            showIcon={false}
                            customStyles={{
                                placeholderText: {
                                    fontSize: 18,
                                    color: '#234456'
                                },
                                dateInput: {
                                    borderWidth: 0,
                                    paddingTop: 22,

                                },
                                dateText: {
                                    fontSize: 14,
                                    color: '#fff'
                                },
                                placeholderText: {
                                    fontSize: 14,
                                    color: '#fff'
                                },

                            }}
                            onDateChange={(date) => { this.updateUserBirthday(date) }}
                        />) : null
                    }

                </View>
                <View style={styles.StatusInputStyle}>
                    {
                        this.state.fontLoaded ? (<Text style={styles.Text4Style}>Status  </Text>) : null
                    }
                    {
                        this.state.fontLoaded ? (<TextInput style={[styles.textDescription, { width: 237, textAlignVertical: "top" }]}
                            value={this.props.userStatus}
                            maxLength={100}
                            returnKeyType="done"
                            underlineColorAndroid="transparent"
                            onChangeText={this.statusChanged.bind(this)}
                            onSubmitEditing={() => this.props.updateUserStatus(this.props.userStatus)}
                        />) : null
                    }
                </View>
                <View style={styles.DiscribtionInputStyle}>
                    {
                        this.state.fontLoaded ? (<Text style={styles.Text5Style}>Description  </Text>) : null
                    }
                    {
                        this.state.fontLoaded ? (<TextInput style={[styles.textDescription, { width: 210, textAlignVertical: "top" }]}
                            value={this.props.userDescription}
                            maxLength={300}
                            returnKeyType="done"
                            blurOnSubmit
                            multiline
                            underlineColorAndroid="transparent"
                            onChangeText={this.descriptionChanged.bind(this)}
                            onSubmitEditing={() => this.props.updateUserDescription(this.props.userDescription)}
                        />) : null
                    }
                </View>
                <View style={styles.styleEndText}>
                    {
                        this.state.fontLoaded ? (<Text style={styles.Text6Style}>This Name Will Be Viewable By Your
                            {
                                this.state.fontLoaded ? (<Text onPress={() => (Actions.UserProfileTimeline(), "Hashtazapp")} style={{ fontWeight: 'bold' }}>{' Hashtazapp '}</Text>) : null
                            }
                            Contacts </Text>) : null
                    }
                </View>
            </View>
        )
    }
}

const mapStateToProps = state => {
    // //console.log(state.editUserProfile);
    // //console.log(state.editUserProfileInfo);
    let userProfileData = state.editUserProfileInfo.userProfileInfoData;
    let userBirthday = (state.editUserProfile.date_of_birth != "") ? state.editUserProfile.date_of_birth
        : (userProfileData !== null) ? userProfileData.userBirthday : "01-01-1990";
    let userStatus = (state.editUserProfile.user_profile_status != "") ? state.editUserProfile.user_profile_status
        : (userProfileData !== null) ? userProfileData.userStatus : "";
    let userDescription = (state.editUserProfile.user_profile_description != "") ? state.editUserProfile.user_profile_description
        : (userProfileData !== null) ? userProfileData.userDescription : "";
    return {
        userBirthday,
        userStatus,
        userDescription
    };
}

export default connect(mapStateToProps, {
    fetchUserProfileInfo,
    updateUserBirthday,
    statusChanged,
    updateUserStatus,
    descriptionChanged,
    updateUserDescription,
    updateDisplayName
})(ProfileBox);

const styles = StyleSheet.create({

    textView: {
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'space-between',

    },
    Text1Style: {
        width: 300,
        color: '#000',
        alignItems: 'center',
        fontSize: 32,
        flexDirection: 'column',
        fontFamily: 'coolvetica'
    },
    DayInputStyle: {
        flexDirection: 'row',
        alignItems: 'flex-end',
    },

    yearInput: {
        width: 65,
    },

    textDescription: {
        fontSize: 14,
        fontFamily: 'coolvetica',
        color: '#fff',
        marginTop: 5
    },

    Text3Style: {
        fontSize: 14,
        fontFamily: 'coolvetica',
    },

    StatusInputStyle: {
        flexDirection: 'row',
    },

    Text4Style: {
        fontSize: 14,
        fontFamily: 'coolvetica',
        marginTop: 5
    },

    DiscribtionInputStyle: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        marginTop: -10
    },

    Text5Style: {
        fontSize: 14,
        fontFamily: 'coolvetica',
        marginTop: 5,
        alignSelf: 'flex-start'
    },

    styleEndText: {
        alignItems: 'center',
        top: 40,
    },

    Text6Style: {
        fontSize: 11,
        fontFamily: 'coolvetica',
        color: '#fff',
        marginTop: 10
    }
})

