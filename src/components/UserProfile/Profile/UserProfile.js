import React, { Component } from 'react';
import {
  StyleSheet, KeyboardAvoidingView, TouchableOpacity,
  View, Image, NativeModules,
  NativeEventEmitter,
  Alert
} from 'react-native';
import { ImagePicker, LinearGradient, Permissions } from 'expo';
import firebase from '@firebase/app'
import RNFS from 'react-native-fs'
import _ from 'lodash';
import { connect } from 'react-redux';
import { Ionicons } from '@expo/vector-icons';
import { Actions } from 'react-native-router-flux';
import uuid from 'uuid/v4';
import ProfileBox from './ProfileBox';
import getPermission from '../../../utils/getPermission';
import getThumbUri from '../../../utils/getThumbnailImageUrl';
import uploadAsset from '../../../utils/uploadPhoto';

const PESDK = NativeModules.PESDK

class UserProfile extends Component {
  constructor(props) {
    super(props);
    this.goBack = this.goBack.bind(this);
  }
  state = {
    image: null,
  };

  componentWillMount() {
    this.eventEmitter = new NativeEventEmitter(NativeModules.PESDK);
    this.eventEmitter.addListener('PhotoEditorDidCancel', () => {
      console.log('Cancelled')
      // The photo editor was cancelled.
      Alert.alert(
        'Profile picture upload Cancel',
        'Do you want to cancel',
        { cancelable: true }
      )
    })
    this.eventEmitter.addListener('PhotoEditorDidSave', (body) => {

      /*  var buffer = new Buffer('data:image/jpeg;base64,' + body.data, 'base64');
       var lb = new Blob([new Uint8Array(buffer)], { type: 'image/jpeg', lastModified: Date.now().valueOf() });
       console.log(lb); */



      /* const reducedImage = shrinkAssetAsync(
        'data:image/jpeg;base64,' + body.data,
      );
      console.log(body) */

      this.uploadProfilePicture({ text: 'Profile Image upload', image: 'data:image/jpeg;base64,' + body.data });
      //console.log(body.data);
      //var data = uri.substr(uri.indexOf('base64') + 7)
      /* console.log(uri);
     */
    })
    this.eventEmitter.addListener('PhotoEditorDidFailToGeneratePhoto', () => {
      // The photo editor could not create a photo.
      Alert.alert(
        'PESDK did Fail to generate a photo.',
        'Please try again.',
        { cancelable: true }
      )
    })
  }

  componentWillUnmount() {
    this.eventEmitter.removeAllListeners('PhotoEditorDidCancel')
    this.eventEmitter.removeAllListeners('PhotoEditorDidSave')
    this.eventEmitter.removeAllListeners('PhotoEditorDidFailToGeneratePhoto')
  }


  componentDidMount() {
    const { currentUser } = firebase.auth();
    this.setState({
      image: (currentUser.photoURL != null) ? currentUser.photoURL : null
    });

  }

  goBack() {
    Actions.pop();
  }
  renderImage() {
    const { currentUser } = firebase.auth();
    //let image = (currentUser.photoURL != null) ? currentUser.photoURL : this.state.image

    let image = 'https://storage.googleapis.com/hashtazappfirebase-bcf55/thumb_841d5fa9-0835-4b26-a79c-345da3b417ba.jpg'
    console.log(currentUser.photoURL);
    return (
      <TouchableOpacity onPress={this._pickImage} resizeMode='stretch' >
        <Image source={{ uri: image }} style={styles.imageStyle} />
      </TouchableOpacity>
    )
  }
  render() {
    return (
      <LinearGradient style={styles.profileContainer} colors={['#FF4200', '#E61300']} >
        <TouchableOpacity onPress={this.goBack} style={styles.BackBotton}>
          <Ionicons name="ios-arrow-back" size={40} color="#fff" />
        </TouchableOpacity>
        < KeyboardAvoidingView style={{ flex: 1, alignItems: 'center' }} behavior="position">
          <View style={styles.Imagewrappr}>
            {this.renderImage()}
            {/* <Image source={require('../assets/edit.png')} style={styles.viewimage} /> */}
          </View>
          <View style={{ marginTop: 30 }}>
            <ProfileBox style={{ felx: 1 }} />
          </View>
        </KeyboardAvoidingView>
      </LinearGradient>
    )
  }

  _pickImage = async () => {
    const status = await getPermission(Permissions.CAMERA_ROLL);
    if (status) {
      const result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: false,
        mediaTypes: 'Images'
      });

      if (!result.cancelled) {
        const image = result;
        const imagePath = RNFS.DocumentDirectoryPath + '/image.jpeg'

        RNFS.downloadFile({ fromUrl: image.uri, toFile: imagePath, progress: 0 }).promise.then(result => {

          PESDK.presentProfilePhoto(imagePath)

        })
        /*  const image = result;
         this.setState({ image: image.uri });
         this.uploadProfilePicture({ text: 'event Image upload', image }); */

      }
    }
  };

  uploadPhotoAsync = async (uri, uid) => {

    const path = `/assets/profilePicture/${uid}.jpg`;
    return uploadAsset(uri, path, (progress) => {
    });

  };

  uploadProfilePicture = async ({ text, image: localUri }) => {

    const { currentUser } = firebase.auth();
    try {
      let uid = uuid();

      let remoteUri = await this.uploadPhotoAsync(localUri, uid);
      let thumbUri = await getThumbUri(`${uid}.jpg`);
      //console.log(thumbUri);
      if (thumbUri != undefined) {
        this.setState({ image: remoteUri })
        currentUser.updateProfile({
          photoURL: remoteUri
        }).then(() => {
          var ref = firebase.database().ref(`/users/${currentUser.uid}/userProfileInfo/profilepicture`);
          ref.push({
            'URL': remoteUri,
            'originalUrl': remoteUri,
            'owner': currentUser && currentUser.uid,
            'uploadtime': new Date().getTime()
          })
        }).catch((error) => {
          console.log(error);
        })
      }
    } catch (error) {
      alert(error)
    }
  }
}

const styles = StyleSheet.create({
  profileContainer: {
    flex: 1,
  },

  Imagewrappr: {
    position: 'relative',
    marginTop: 70,
    alignItems: 'center',
  },
  BackBotton: {
    position: 'absolute',
    top: 30,
    left: 20,
    zIndex: 20
  },
  viewimage: {
    position: 'absolute',
    resizeMode: 'stretch',
    width: 130,
    height: 130,
    top: -30,
    right: -10,
  },

  imageStyle: {
    width: 260,
    height: 260,
    borderRadius: 260 / 2,
    borderWidth: 3,
    borderColor: '#FF3C00',
  },

});


export default connect(null)(UserProfile);
