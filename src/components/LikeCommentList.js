import React, { Component } from 'react'
import { Text, View, ScrollView, StyleSheet, KeyboardAvoidingView, Image } from 'react-native'
import { StackNavigator } from 'react-navigation';
import CommentItem from './CommentItem'
import ReplyItem from './ReplyItem'
import CommentTextInputFooter from './CommentTextInputFooter'


export default class LikeCommentList extends Component {
    
    renderComments() {
        const { params } = this.props.navigation.state;
        return params.story.story_comment_and_replies.map(story_comment_and_reply =>
            <CommentItem key={story_comment_and_reply.key} story_comment_and_reply={story_comment_and_reply}
            />

        );
    };
    render() {
        const { params } = this.props.navigation.state;
        return (
            <View style={styles.container}>
                <View style={styles.likeContainer}>
                    <Image source={require('./assets/likeRed.png')} style={{ height:10, width:10, marginRight: 5 }}/>
                    <Text style={styles.likeCountText}>{params.story.story_like_comment_count.likes}</Text>
                </View>
                <ScrollView>
                    {this.renderComments()}
                </ScrollView>
                <CommentTextInputFooter />
            </View>
        )
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 30,
        marginLeft: 10,
        marginRight: 10,
        flexDirection: 'column',
    },
    likeContainer: {
        height: 20,
        backgroundColor: '#f8f8f8',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative',
        paddingLeft: 10,
        padding: 5
    },
    likeCountText: {
        fontSize: 8,
        fontWeight: '500'
    }
});