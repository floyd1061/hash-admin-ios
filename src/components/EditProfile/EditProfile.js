import React, { Component } from 'react'
import { View, StyleSheet, Text, TouchableOpacity, ImageBackground } from 'react-native'
import { LinearGradient, Font } from 'expo'
import ToggleSwitch from 'toggle-switch-react-native'
import Slider from 'react-native-slider'
import { Ionicons } from '@expo/vector-icons';
import { Actions } from 'react-native-router-flux';

export default class EditProfile extends Component {

    //For Toggle
    onToggle(isOn) {
        //alert('Changed to ' + isOn)
    }

    //For Slider  
    constructor(props) {
        super(props);
        this.goBack = this.goBack.bind(this);
        this.state = {
            check: false,
            fontLoaded: true
        }
    }


    goBack() {
        Actions.pop();
    }
    render() {

        return (
            <LinearGradient colors={['#E50200', '#FF0A06']} style={styles.container}>
                <TouchableOpacity onPress={this.goBack} style={styles.BackBotton}>
                    <Ionicons name="ios-arrow-back" size={40} color="#fff" />
                </TouchableOpacity>
                <ImageBackground source={require('./assets/EditProfileBox.png')} style={styles.ViewBox}>

                    <View style={styles.BoxSection1}>
                        <View style={styles.box1}>
                            {
                                this.state.fontLoaded ? (<Text style={styles.textStyle1}>Sound</Text>) : null
                            }
                        </View>
                        <View style={styles.box2}>
                            <ToggleSwitch onToggle={this.onToggle} onColor="#FF341C" size='default' />
                        </View>
                    </View>

                    <View style={styles.BoxSection2}>
                        <View style={styles.box3}>
                            {
                                this.state.fontLoaded ? (<Text style={styles.textStyle2}>Timer Countdown</Text>) : null
                            }
                        </View>
                        <View style={styles.box4}>
                            <ToggleSwitch onToggle={this.onToggle} onColor="#FF341C" size='default' />
                        </View>
                    </View>

                    <View style={styles.BoxSection3}>

                        <View style={styles.box5}>
                            {
                                this.state.fontLoaded ? (<Text style={styles.textStyle3}>Zoom</Text>) : null
                            }
                        </View>

                        <View style={styles.box6}>
                            <Slider
                                step={1}
                                thumbStyle={{ borderWidth: 3, borderColor: '#D3D3D3' }}
                                trackStyle={{ height: 15, borderWidth: 3, borderColor: '#D3D3D3', borderRadius: 10 }}
                                minimumTrackTintColor='#FE2305'
                                maximumTrackTintColor='#DDDDDD'
                                maximumValue={30}
                                thumbTintColor='#FD1700'
                                style={{ width: 160 }}
                                value={this.state.value}
                                onValueChange={value => this.setState({ value })}
                            />
                        </View>

                    </View>

                    <View style={styles.BoxSection4}>

                        <View style={styles.box7}>
                            {
                                this.state.fontLoaded ? (<Text style={styles.textStyle4}>Clipstraw Advanced Camera Settings</Text>) : null
                            }
                        </View>

                        <View style={styles.box8}>
                            <ToggleSwitch onToggle={this.onToggle} onColor="#FF341C" size='default' />
                        </View>

                    </View>

                    <View style={styles.BoxSection5}>

                        <View style={styles.box9}>
                            {
                                this.state.fontLoaded ? (<Text style={styles.textStyle5}>Preload Videos Using Connected Wifi </Text>) : null
                            }
                        </View>

                        <View style={styles.box10}>
                            <ToggleSwitch onToggle={this.onToggle} onColor="#FF341C" size='default' />
                        </View>

                    </View>

                    <View style={styles.BoxSection6}>

                        <View style={styles.box11}>
                            {
                                this.state.fontLoaded ? (<Text style={styles.textStyle6}>Live Instant Share</Text>) : null
                            }
                            {
                                this.state.fontLoaded ? (<Text style={styles.textStyle7}>Share live pictures and videos instantly when uploaded</Text>) : null
                            }
                        </View>

                        <View style={styles.box12}>
                            <ToggleSwitch onToggle={this.onToggle} onColor="#FF341C" size='default' />
                        </View>

                    </View>

                </ImageBackground>

                <TouchableOpacity>
                    <LinearGradient colors={['#ff2002', '#df1d04']} style={styles.buttonStyle}>
                        {
                            this.state.fontLoaded ? (<Text style={styles.buttonText}>Apply Now</Text>) : null
                        }
                    </LinearGradient>
                </TouchableOpacity>

            </LinearGradient>
        )
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    BackBotton: {
        position: 'absolute',
        top: 30,
        left: 20,
        zIndex: 20
    },
    ViewBox: {
        width: 300,
        height: 460,
        paddingHorizontal: 12,
        paddingVertical: 90,
        marginBottom: 20,
    },

    buttonStyle: {
        borderWidth: 2,
        borderColor: '#fff',
        borderRadius: 3,
        paddingHorizontal: 30,
        paddingVertical: 10
    },

    buttonText: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 20,
        fontFamily: 'coolvetica'
    },

    BoxSection1: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#FFFFFF',
        borderRadius: 3,
        marginHorizontal: 10,
        marginBottom: 8,
        padding: 10,
        marginTop: 20
    },

    box1: {
        justifyContent: 'center',
    },

    box2: {
        justifyContent: 'center',
    },

    textStyle1: {
        fontSize: 18,
        color: '#919191',
        fontFamily: 'coolvetica'
    },

    BoxSection2: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#FFFFFF',
        borderRadius: 3,
        marginHorizontal: 10,
        marginBottom: 8,
        padding: 10,
    },

    box3: {
        justifyContent: 'center',
    },

    box4: {
        justifyContent: 'center',
    },

    textStyle2: {
        fontSize: 18,
        color: '#919191',
        fontFamily: 'coolvetica'
    },


    BoxSection3: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#FFFFFF',
        borderRadius: 3,
        marginHorizontal: 10,
        marginBottom: 8,
        padding: 10,
    },

    box5: {
        justifyContent: 'center',
    },

    box6: {
        justifyContent: 'center',
    },

    textStyle3: {
        fontSize: 18,
        color: '#919191',
        fontFamily: 'coolvetica'
    },


    BoxSection4: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#FFFFFF',
        borderRadius: 3,
        marginHorizontal: 10,
        marginBottom: 8,
        padding: 10,
    },

    box7: {
        justifyContent: 'center',
    },

    box8: {
        justifyContent: 'center',
    },

    textStyle4: {
        fontSize: 10,
        color: '#919191',
        fontFamily: 'coolvetica'
    },


    BoxSection5: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#FFFFFF',
        borderRadius: 3,
        marginHorizontal: 10,
        marginBottom: 8,
        padding: 10,
    },

    box9: {
        justifyContent: 'center',
    },

    box10: {
        justifyContent: 'center',
    },

    textStyle5: {
        fontSize: 10,
        color: '#919191',
        fontFamily: 'coolvetica'
    },

    BoxSection6: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#FFFFFF',
        borderRadius: 3,
        marginHorizontal: 10,
        marginBottom: 8,
        padding: 10,
    },

    box11: {
        justifyContent: 'center',
    },

    box12: {
        justifyContent: 'center',
    },

    textStyle6: {
        fontSize: 18,
        color: '#919191',
        lineHeight: 20,
        fontFamily: 'coolvetica'
    },

    textStyle7: {
        fontSize: 6,
        color: '#FF1800',
        fontFamily: 'coolvetica'
    },
});