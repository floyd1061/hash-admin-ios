import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { LinearGradient } from 'expo'
import { Actions } from 'react-native-router-flux'
export default class NavbarComponent extends Component {
    render() {
        return (
            <View>
                <LinearGradient
                    colors={['#ff4200', '#e30000']}>
                    <View style={styles.toolbar}>
                        <View style={styles.sameHWT}>
                            <TouchableOpacity onPress={() => { Actions.pop() }}>
                                <Text style={styles.toolbarTitle}>Hashtazapp</Text>
                            </TouchableOpacity>

                        </View>
                        <View style={styles.sameHW}>
                            <View>
                                <TouchableOpacity>
                                    <Image source={require('./assets/messeanger.png')} style={{ height: 25, width: 27, }} />
                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity>
                                    <Image source={require('./assets/search.png')} style={{ height: 25, width: 25, marginLeft: 10 }} />
                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity>
                                    <Image source={require('./assets/followers.png')} style={{ height: 25, width: 27, marginLeft: 10 }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </LinearGradient>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    toolbar: {
        paddingTop: 20,
        flexDirection: 'row',
        height: 65,
        paddingHorizontal: 5,
        justifyContent: 'center',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'
    },
    sameHWT: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexGrow: 2,
    },
    sameHW: {
        flex: 1,
        flexDirection: 'row',

        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    toolbarButton: {
        width: 50,
        color: '#fff',
        textAlign: 'center',
        flex: 1
    },
    toolbarTitle: {
        color: '#fff',
        textAlign: 'center',
        fontFamily: 'coolvetica',
        fontSize: 20
    }
});