import React, { Component } from 'react'
import {TouchableOpacity, Image } from 'react-native'

export default class SingleEventItem extends Component{
    render(){
        return(
            <TouchableOpacity style = {{width: 100, height: 100, marginBottom: 20}}>
                <Image source={require('./assets/Rectangle.png')} style={{flex: 1, resizeMode:'stretch'}} />                                                
            </TouchableOpacity>
        )
    }
}