import React, { Component } from 'react'
import {TouchableOpacity , View , Image , TextInput,Text } from 'react-native'
import {Font} from 'expo'
import SingleEventItem from './SingleEventItem'

export default class MostlyLikedEvents extends Component{

    constructor(props) {
        super(props);
        this.state = { showHeaderOption: false, fontLoaded: true };
    }

    
    render(){
        return(
            <View style = {styles.container}>
                <View style = {styles.headerContainer}>
                    <View style = {styles.searchBox}>
                        <View style = {{flexDirection: 'row'}}>
                            <TouchableOpacity>
                                <Image source={require('./assets/explore.png')} style={styles.exploreIcon}/>
                            </TouchableOpacity>

                            {
                                this.state.fontLoaded ? ( <TextInput
                                    style={{ flex: .8, flexDirection:'row', paddingLeft: 10, paddingRight: 10,  fontSize: 13, fontFamily: 'coolvetica' }}
                                    underlineColorAndroid="transparent"
                                    placeholder="Explor Most View Events"
                                /> ) : null
                            }
                        </View>

                        <View style = {{flexDirection: 'row', justifyContent: 'space-between', right: 18}}>
                            <TouchableOpacity>
                                <Image source={require('./assets/mic.png')} style={styles.micIcon}/>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => {this.setState({showHeaderOption: !this.state.showHeaderOption})}}>
                                <Image source={require('./assets/dropdown.png')} style={styles.dropdownIcon}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                <View style = {{paddingHorizontal: 15, flex: 1, flexDirection: 'row', flexWrap:'wrap', justifyContent: 'space-around'}}>
                    <SingleEventItem/>
                    <SingleEventItem/>
                    <SingleEventItem/>
                    <SingleEventItem/>
                </View>
                
                {this.state.showHeaderOption ?
                    <View style = {styles.headerOptionArea}>
                        <View  style = {{position: 'relative', flex: 1}}>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}>View most liked events</Text> ) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}>View most liked profiles</Text> ) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity disabled = {true}>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}>Go to most liked dates</Text> ) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}>Go to most liked years</Text> ) : null
                                }
                            </TouchableOpacity>
                        </View>                            
                    </View> : null
                }
            </View>
        )
    }
}


const styles={
    container: {
        position: 'relative',
        backgroundColor:'#DEDEDE',
        flex:1,
    },

    headerContainer: {
        backgroundColor: '#FF1303',
        height: 130,
        marginBottom:20,
    },

    searchBox: {
        flexDirection:'row',
        backgroundColor: '#fff',
        height:60, 
        alignSelf: 'stretch',
        alignItems: 'center',
        paddingHorizontal: 30,
        marginTop:60,         
    },

    exploreIcon: {
        height: 40,
        width:40, 
        resizeMode : 'stretch',
    },

    micIcon: {
        height: 40,
        width:35, 
        resizeMode : 'stretch',
        marginRight: 10,
    },

    dropdownIcon: {
        height: 35,
        width:35, 
        resizeMode : 'stretch',
    },

    headerOptionArea: {
        width:200, 
        height: 150, 
        backgroundColor: '#fff', 
        position: 'absolute',
        right: 22,
        top: 120,
        borderColor: '#FD0607',
        borderWidth: 2,
        borderRadius: 3,
        borderTopWidth: 0,
        alignItems: 'center',

    },

    optionText: {
        paddingVertical: 8,
        paddingHorizontal: 8,
        color:'#000',
        
        fontFamily: 'coolvetica'
    },
}