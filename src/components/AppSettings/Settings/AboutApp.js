import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet, Dimensions } from 'react-native'
import { LinearGradient, Font } from 'expo'
import { Ionicons } from '@expo/vector-icons';
import { Actions } from 'react-native-router-flux';
const { height, width } = Dimensions.get('window');

export default class AboutApp extends Component {
    constructor(props) {
        super(props);
        this.goBack = this.goBack.bind(this);
    }
    state = {
        fontLoaded: true,
    };


    goBack() {
        Actions.pop();
    }
    render() {
        return (

            <LinearGradient style={styles.settingContainer} colors={['#E10100', '#FD0C05']}>
                <TouchableOpacity onPress={this.goBack} style={styles.BackBotton}>
                    <Ionicons name="ios-arrow-back" size={40} color="#fff" />
                </TouchableOpacity>
                <Image source={require('../assets/setShape.png')} style={styles.viewImage} />

                <View style={styles.userContainer}>

                    {
                        this.state.fontLoaded ? (<Text style={styles.header}> About </Text>) : null
                    }

                    <View style={{ paddingHorizontal: 10 }}>
                        <TouchableOpacity style={styles.buttonStyle}>
                            {
                                this.state.fontLoaded ? (<Text style={{ color: '#E60300', fontFamily: 'coolvetica' }}> Appout Us </Text>) : null
                            }
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonStyle}>
                            {
                                this.state.fontLoaded ? (<Text style={{ color: '#E60300', fontFamily: 'coolvetica' }}> Ads </Text>) : null
                            }
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonStyle}>
                            {
                                this.state.fontLoaded ? (<Text style={{ color: '#E60300', fontFamily: 'coolvetica' }}> Terms Of Service </Text>) : null
                            }
                        </TouchableOpacity>
                    </View>

                    {
                        this.state.fontLoaded ? (<Text style={styles.header1}> Account </Text>) : null
                    }

                    <View style={{ paddingHorizontal: 10 }}>
                        <TouchableOpacity style={styles.buttonStyle}>
                            {
                                this.state.fontLoaded ? (<Text style={{ color: '#E60300', fontFamily: 'coolvetica' }}> Network Usage </Text>) : null
                            }
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonStyle}>
                            {
                                this.state.fontLoaded ? (<Text style={{ color: '#E60300', fontFamily: 'coolvetica' }}> Privacy Policy </Text>) : null
                            }
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonStyle}>
                            {
                                this.state.fontLoaded ? (<Text style={{ color: '#E60300', fontFamily: 'coolvetica' }}> Delete Account </Text>) : null
                            }
                        </TouchableOpacity>
                    </View>

                    {
                        this.state.fontLoaded ? (<Text style={styles.header2}> Help </Text>) : null
                    }

                    <View style={{ paddingHorizontal: 10 }}>
                        <TouchableOpacity style={styles.buttonStyle}>
                            {
                                this.state.fontLoaded ? (<Text style={{ color: '#E60300', fontFamily: 'coolvetica' }}> Walkthrough </Text>) : null
                            }
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonStyle}>
                            {
                                this.state.fontLoaded ? (<Text style={{ color: '#E60300', fontFamily: 'coolvetica' }}> Refresh Account Search History </Text>) : null
                            }
                        </TouchableOpacity>
                    </View>
                </View>

            </LinearGradient>
        );
    };
};

const styles = StyleSheet.create({

    settingContainer: {
        flex: 1,
        position: 'relative',
        paddingHorizontal: 30,
        paddingVertical: 5,
        backgroundColor: '#B30C09',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 50

    },
    BackBotton: {
        position: 'absolute',
        top: 30,
        left: 20,
        zIndex: 20
    },
    viewImage: {
        position: 'absolute',
        resizeMode: 'stretch',
        width: 110,
        height: 110,
        top: 35,
        zIndex: 1,
    },

    userContainer: {
        padding: 10,
        width: width - 80,
        height: height - 130,
        backgroundColor: '#282828',
        borderWidth: 3,
        borderColor: '#fff',
    },

    buttonStyle: {
        marginBottom: 10
    },

    header: {
        paddingTop: 60,
        paddingHorizontal: 10,
        paddingVertical: 10,
        fontSize: 20,
        fontFamily: 'coolvetica',
        color: '#ffff',
        borderBottomWidth: 1,
        borderColor: '#E60300',
        marginBottom: 10
    },

    header1: {
        paddingHorizontal: 10,
        paddingVertical: 10,
        fontSize: 20,
        fontFamily: 'coolvetica',
        color: '#ffff',
        borderBottomWidth: 1,
        borderColor: '#E60300',
        marginBottom: 10
    },

    header2: {
        paddingHorizontal: 10,
        paddingVertical: 10,
        fontSize: 20,
        fontFamily: 'coolvetica',
        color: '#ffff',
        borderBottomWidth: 1,
        borderColor: '#E60300',
        marginBottom: 10
    },
});

