import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import { LinearGradient } from 'expo'
import PrivacyPolicyForm from './PrivacyPolicyForm'

export default class PrivacyPolicy extends Component {
  render() {
    return (
      <LinearGradient colors={['#E60200', '#FF0707']} style={ styles.container }>
        <PrivacyPolicyForm />
      </LinearGradient>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});