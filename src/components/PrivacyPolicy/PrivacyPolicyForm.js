import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native'
import { LinearGradient } from 'expo'
import { Dropdown } from 'react-native-material-dropdown'
import { Font } from 'expo'

export default class PrivacyPolicyForm extends Component {

    state = {
        fontLoaded: true,
      };
    

    render() {

        let data = [{
            value: 'Banana',
        }, {
            value: 'Mango',
        }, {
            value: 'Pear',
        }];

        return (

            <View style={styles.container}>
                <View style={styles.Box}>

                    <View style={styles.SectionStyletext1}>
                        {
                            this.state.fontLoaded ? ( <Text style={styles.text1Style}>Privacy Policy</Text> ) :null
                        }
                    </View>

                    <View style= { styles.boxStyle }>

                        <View style={styles.SectionStyletext2}>
                            {
                                this.state.fontLoaded ? ( <Text style={styles.text2Style}>Who can see my personal information</Text> ) :null
                            }
                        </View>

                        <View style={styles.SectionStyle1}>

                            {
                                this.state.fontLoaded ? ( <Text style={ styles.text3Style }>Last Seen</Text> ) :null
                            }

                            <View style= { styles.itemBox }>
                                <Dropdown
                                    inputContainerStyle={{ borderBottomColor: 'transparent'}}
                                    dropdownOffset={{ top: 2, left: 0 }}
                                    dropdownPosition={0}
                                    selectedItemColor='#6E6E6E'
                                    pickerStyle={styles.Dropdownstyles}
                                    label='Everyone can view my last seen'
                                    fontSize={9}
                                    data={data}
                                />

                            </View>

                        </View>

                        <View style={styles.SectionStyle1}>

                            {
                                this.state.fontLoaded ? ( <Text style={ styles.text3Style }>Profile Photo</Text> ) :null
                            }

                            <View style={ styles.itemBox }>
                                <Dropdown
                                    inputContainerStyle={{ borderBottomColor: 'transparent'}}
                                    dropdownOffset={{ top: 2, left: 0 }}
                                    dropdownPosition={0}
                                    selectedItemColor='#6E6E6E'
                                    pickerStyle={styles.Dropdownstyles}
                                    label='Everyone can view my profile photo'
                                    fontSize={9}
                                    data={data}
                                />

                            </View>

                        </View>

                        <View style={styles.SectionStyle1}>

                            {
                                this.state.fontLoaded ? ( <Text style={ styles.text3Style }>Profile</Text> ) :null
                            }

                            <View style={ styles.itemBox }>
                                <Dropdown
                                    inputContainerStyle={{ borderBottomColor: 'transparent'}}
                                    dropdownOffset={{ top: 2, left: 0 }}
                                    dropdownPosition={0}
                                    selectedItemColor='#6E6E6E'
                                    pickerStyle={styles.Dropdownstyles}
                                    label='Only friends can view my profile'
                                    fontSize={9}
                                    data={data}
                                />

                            </View>

                        </View>

                        <View style={styles.SectionStyle1}>

                            {
                                this.state.fontLoaded ? ( <Text style={ styles.text3Style }>Status</Text> ) :null
                            }

                            <View style={ styles.itemBox }>
                                <Dropdown
                                    inputContainerStyle={{ borderBottomColor: 'transparent'}}
                                    dropdownOffset={{ top: 2, left: 0 }}
                                    dropdownPosition={0}
                                    selectedItemColor='#6E6E6E'
                                    pickerStyle={styles.Dropdownstyles}
                                    label='Everyone can view my status'
                                    fontSize={9}
                                    data={data}
                                />

                            </View>

                        </View>

                        <View style={styles.SectionStyle1}>

                            {
                                this.state.fontLoaded ? ( <Text style={ styles.text3Style }>Chat & Calls</Text> ) :null
                            }

                            <View style={ styles.itemBox }>
                                <Dropdown
                                    inputContainerStyle={{ borderBottomColor: 'transparent'}}
                                    dropdownOffset={{ top: 2, left: 0 }}
                                    dropdownPosition={0}
                                    selectedItemColor='#6E6E6E'
                                    pickerStyle={styles.Dropdownstyles}
                                    label='Block List'
                                    fontSize={9}
                                    data={data}
                                />

                            </View>

                        </View>

                        <View style={styles.SectionStyle1}>

                            {
                                this.state.fontLoaded ? ( <Text style={ styles.text3Style }>Event Day</Text> ) :null
                            }

                            <View style={ styles.itemBox }>
                                <Dropdown
                                    inputContainerStyle={{ borderBottomColor: 'transparent'}}
                                    dropdownOffset={{ top: 2, left: 0 }}
                                    dropdownPosition={0}
                                    selectedItemColor='#6E6E6E'
                                    pickerStyle={styles.Dropdownstyles}
                                    label='Block List'
                                    fontSize={9}
                                    data={data}
                                />

                            </View>

                        </View>

                        <View style={styles.SectionStyle1}>

                            {
                                this.state.fontLoaded ? ( <Text style={ styles.text3Style }>Sharing</Text> ) :null
                            }

                            <View style={ styles.itemBox }>
                                <Dropdown
                                    inputContainerStyle={{ borderBottomColor: 'transparent' }}
                                    dropdownOffset={{ top: 2, left: 0 }}
                                    dropdownPosition={0}
                                    selectedItemColor='#6E6E6E'
                                    pickerStyle={styles.Dropdownstyles}
                                    label='No one can share my videos'
                                    fontSize={9}
                                    data={data}
                                />

                            </View>

                        </View>

                        <View style={styles.SectionStyle1}>

                            {
                                this.state.fontLoaded ? ( <Text style={ styles.text3Style }>Read Receipt</Text> ) :null
                            }

                            <View style={ styles.itemBox }>
                                <Dropdown
                                    inputContainerStyle={{ borderBottomColor: 'transparent'}}
                                    dropdownOffset={{ top: 2, left: 0 }}
                                    dropdownPosition={0}
                                    selectedItemColor='#6E6E6E'
                                    pickerStyle={styles.Dropdownstyles}
                                    label='Yes, I want to read all my receipts'
                                    fontSize={9}
                                    data={data}
                                />

                            </View>

                        </View>

                        <View style= { styles.textGap }>

                            <View style={styles.SectionStyletext3}>
                                {
                                    this.state.fontLoaded ? ( <Text style={styles.text4Style}>If you  do not share your last seen you will not be able to view other user’s last seen. { '\n' }</Text> ) :null
                                }
                                {
                                    this.state.fontLoaded ? ( <Text style={styles.text5Style}>If you  turn off your  read receipt you will not be able to view other user’s read receipt.</Text> ) :null
                                }
                            </View>

                        </View>

                    </View>

                </View>

                <View style={styles.buttonCenter}>
                    <TouchableOpacity>
                        <LinearGradient colors={['#ff2002', '#df1d04']} style={styles.SectionStyleDeleteButton}>
                            {
                                this.state.fontLoaded ? ( <Text style={styles.buttonText}>Apply Now</Text> ) :null
                            }
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    Box: {
        alignSelf: 'stretch',
        backgroundColor: '#282828',
        borderWidth: 3,
        borderColor: '#FFFFFF',
        marginHorizontal: 30,
        marginVertical: 15
    },

    buttonCenter: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },

    SectionStyleDeleteButton: {
        borderWidth: 2,
        borderColor: '#fff',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 3,
    },

    buttonText: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 18,
        fontFamily: 'coolvetica'
    },

    SectionStyletext1: {
        borderBottomWidth: 1,
        marginHorizontal: 3,
        borderBottomColor: '#4B4B4B',
    },

    text1Style: {
        color: '#B2B2B2',
        fontSize: 30,
        padding: 12,
        fontFamily: 'coolvetica'
    },

    boxStyle: {
        padding: 15
    },

    SectionStyletext2: {
        marginBottom: 15  
    },

    text2Style: {
        color: '#BE120A',
        fontSize: 9,
        fontFamily: 'coolvetica'
    },

    SectionStyle1: {
        flexDirection: 'row',
        marginBottom: 5,
    },

    text3Style: {
        color: '#FFFFFF',
        fontSize: 11,
        flex: 1.5,
        fontFamily: 'coolvetica'
    },

    itemBox: {
        height: 30, 
        width: 190, 
        alignSelf: 'stretch',
        backgroundColor: '#D3CEC1', 
        borderColor: '#eef', 
        borderRadius: 3, 
        padding: 10,
        paddingBottom: 0,
        bottom: 5,
        justifyContent: 'center',
    },

    Dropdownstyles: {
        marginTop: 40,
        width: 200,
        height: 110,
        backgroundColor: '#D3CEC1',
        borderRadius: 3,
        justifyContent: 'center',  
    },

    textGap: {
        marginTop: 5
    },

    SectionStyletext3: {
        alignItems: 'center',
    },

    text4Style: {
        color: '#BE120A',
        fontSize: 7,
        fontFamily: 'coolvetica'  
    },

    text5Style: {
        color: '#BE120A',
        fontSize: 7,
        fontFamily: 'coolvetica' 
    }
});

