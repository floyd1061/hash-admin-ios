import React, { Component } from 'react'
import { Text, View, Image, Dimensions, TouchableOpacity } from 'react-native'
import Card from './Card'
import CardSection from './CardSection'

const widthWindow = Dimensions.get('window').width;
const ImageGalleryItem = ({ story_content, navigate }) => {
    const { id, url, likes, comments } = story_content;
    return (
        <Card key={id}>
            <CardSection >
                <Image
                    style={styles.imageStyleSingle}
                    source={url}
                />
            </CardSection>
            {RenderLikeCommentSection({ likes, comments, navigate })}
            <CardSection>
                <View style={styles.LikeCommentShareButtonContainer}>
                    <TouchableOpacity>
                        <Image style={styles.commentIconStyle} source={require('./assets/comment.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image style={styles.LikeIconStyle} source={require('./assets/like.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image style={styles.ShareIconStyle} source={require('./assets/share.png')} />
                    </TouchableOpacity>
                </View>
            </CardSection>
        </Card>
    );
};
const RenderLikeCommentSection = ({ likes, comments, navigate }) => {
    const likeCount = likes;
    const commentCount = comments.length;
    if (likeCount > 0 && commentCount > 0) {
        return (
            <CardSection>
                <View style={styles.footTextHeadContainer}>
                    <TouchableOpacity onPress={() => navigate('LikeCommentList', { likes: likes, comments: comments, origin: 'imageGallary' })}>
                        <Text style={styles.likeCommentCounterTextStyle}>{commentCount} Comment</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigate('LikeCommentList', { likes: likes, comments: comments, origin: 'imageGallary' })}>
                        <Text style={styles.likeCommentCounterTextStyle}>{likeCount} likes</Text>
                    </TouchableOpacity>
                </View>
            </CardSection>
        );
    }
    else if (likeCount === 0 && commentCount > 0) {
        return (
            <CardSection>
                <View style={styles.footTextHeadContainer}>
                    <TouchableOpacity onPress={() => navigate('ImageGallery', { story: story })}>
                        <Text style={styles.likeCommentCounterTextStyle}>{commentCount} Comment</Text>
                    </TouchableOpacity>
                </View>
            </CardSection>
        );
    }
    else if (likeCount > 0 && commentCount === 0) {
        return (
            <CardSection>
                <View style={styles.footTextHeadContainer}>
                    <TouchableOpacity>
                        <Text style={styles.likeCommentCounterTextStyle}>{likeCount} likes</Text>
                    </TouchableOpacity>
                </View>
            </CardSection>
        );
    }

}
const styles = {

    imageStyleSingle: {
        height: 200,
        width: widthWindow - 20
    },
    LikeCommentShareButtonContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5
    },
    commentIconStyle: {
        height: 15,
        width: 17,
        marginRight: 25,
        marginTop: 3
    },
    LikeIconStyle: {
        height: 15,
        width: 15,
        marginRight: 25,
    },
    ShareIconStyle: {
        height: 15,
        width: 15,
        marginTop: 1
    },
    footTextHeadContainer: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5
    },
    likeCommentCounterTextStyle: {
        fontSize: 8,
        fontWeight: '400',
        marginRight: 6
    },
};
export default ImageGalleryItem;
