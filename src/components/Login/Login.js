import React, { Component } from 'react';
import {StyleSheet, ImageBackground, TouchableOpacity, Text, KeyboardAvoidingView} from 'react-native';
import LoginForm from './LoginForm';
import { LinearGradient, Font } from 'expo';
import { Actions } from 'react-native-router-flux';

export default class Login extends Component {

  state = {
    fontLoaded: true,
  };

 

  render() {
    return (
      <ImageBackground source={require('./assets/background.png')} style={styles.container}>
        <KeyboardAvoidingView behavior="padding">
          <ImageBackground style={styles.loginBox} source={require('./assets/Login.png')}>
            <LoginForm />
          </ImageBackground>
        </KeyboardAvoidingView>
        <TouchableOpacity onPress={() => { Actions.Register() }}>
          <LinearGradient colors={['#ff2002', '#df1d04']} style={styles.SectionStyleRegisterButton}>
            {
              this.state.fontLoaded ? ( <Text style={styles.buttonText}>Register Now!</Text> ) : null
            }
          </LinearGradient>
        </TouchableOpacity>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loginBox: {
    width: 250,
    height: 370,
    paddingHorizontal: 10,
    paddingVertical: 90,
    marginBottom: 20
  },
  SectionStyleRegisterButton: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: '#fff',
    height: 35,
    borderRadius: 3,
    paddingHorizontal: 15,
    marginBottom: 5,
    width: 130
  },
  buttonText: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 12,
    fontFamily: 'coolvetica'
  }
});