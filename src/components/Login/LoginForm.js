import React, { Component } from 'react';
import { StyleSheet, View, TextInput, Image, TouchableOpacity, Text } from 'react-native';
import { connect } from 'react-redux';
import { CheckBox } from 'react-native-elements'

import { emailChanged, passwordChanged, loginUser } from '../../actions';

class LoginForm extends Component {

    onEmailChanged(email) {
        this.props.emailChanged(email);
    }

    onPasswordChange(password) {
        this.props.passwordChanged(password);
    }

    onButtonPress() {
        const { email, password } = this.props;

        this.props.loginUser({ email: email, password: password });
    }

    state = {
        fontLoaded: true,
        checked: true
    };



    render() {
        return (
            <View style={styles.container}>
                <View style={styles.SectionStyle}>
                    {
                        this.state.fontLoaded ? (<TextInput
                            style={{ flex: 1, fontFamily: 'coolvetica' }}
                            placeholder="Username/ E-mail"
                            returnKeyType="next"
                            onChangeText={this.onEmailChanged.bind(this)}
                            underlineColorAndroid="transparent"
                            keyboardType="email-address"
                            onSubmitEditing={() => this.passwordInput.focus()}
                        />) : null
                    }
                    <Image source={require('./assets/forma.png')} style={styles.ImageStyle} />
                </View>
                <View style={styles.SectionStyle}>
                    {
                        this.state.fontLoaded ? (<TextInput
                            style={{ flex: 1, fontFamily: 'coolvetica' }}
                            placeholder="Password"
                            secureTextEntry
                            onChangeText={this.onPasswordChange.bind(this)}
                            returnKeyType="go"
                            underlineColorAndroid="transparent"
                            ref={(input) => this.passwordInput = input}
                        />) : null
                    }
                    <Image source={require('./assets/padlock.png')} style={[styles.ImageStyle, { width: 18, height: 22 }]} />
                </View>
                <TouchableOpacity style={styles.SectionStyleButton} onPress={this.onButtonPress.bind(this)}>
                    {
                        this.state.fontLoaded ? (<Text style={styles.buttonText}>Sign In</Text>) : null
                    }
                </TouchableOpacity>

                <View style={styles.SectionStyleKeepLoggedIn}>
                    {
                        this.state.fontLoaded ? (
                            <CheckBox
                                center
                                textStyle={styles.CustomLabelStyle}
                                containerStyle={{ backgroundColor: 'transparent', borderWidth: 0 }}
                                size={10}
                                title='Keep me logged in'
                                checkedIcon='dot-circle-o'
                                uncheckedIcon='circle-o'
                                checkedColor='#fff'
                                uncheckedColor='#fff'
                                checked={this.state.checked}
                                onPress={()=>{
                                    this.setState({
                                        checked: !this.state.checked
                                    })
                                }}
                            />
                        ) : null
                    }
                    <TouchableOpacity>
                        {
                            this.state.fontLoaded ? (<Text style={styles.CustomLabelStyle}>Forgot Password?</Text>) : null
                        }
                    </TouchableOpacity>
                </View>
                <View style={styles.SectionStyleOrBox}>
                    <Image source={require('./assets/orbox.png')} style={styles.orImageStyle} />
                </View>
                <TouchableOpacity style={styles.SectionStyleButtonFaceBook}>
                    {
                        this.state.fontLoaded ? (<Text style={styles.buttonText}>Login with Facebook</Text>) : null
                    }
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        margin: 10
    },

    customCheckboxStyle: {
        width: 15,
        height: 15
    },
    CustomLabelStyle: {
        fontSize: 8,
        color: '#fff',
        fontFamily: 'coolvetica'
    },

    SectionStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderWidth: .5,
        borderColor: '#fff',
        height: 40,
        borderRadius: 3,
        paddingHorizontal: 10,
        marginBottom: 8
    },

    SectionStyleKeepLoggedIn: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        height: 20,
        marginBottom: 10
    },

    orImageStyle: {
        height: 25,
        width: 27,
        resizeMode: 'stretch'
    },

    SectionStyleOrBox: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 20,
        marginBottom: 15
    },

    SectionStyleButton: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#8c8c8c',
        borderWidth: 2,
        borderColor: '#fff',
        height: 40,
        borderRadius: 3,
        paddingHorizontal: 10,
        marginBottom: 5
    },

    SectionStyleButtonFaceBook: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#3B5998',
        height: 40,
        borderRadius: 3,
        paddingHorizontal: 10,
        marginBottom: 5
    },

    ImageStyle: {
        height: 20,
        width: 20,
        resizeMode: 'stretch',
        alignItems: 'center'
    },

    buttonText: {
        textAlign: 'center',
        color: '#fff',
        fontFamily: 'coolvetica'
    }

});

const mapStateToProps = state => {
    return {
        email: state.auth.email,
        password: state.auth.password
    }
}

export default connect(mapStateToProps, { emailChanged, passwordChanged, loginUser })(LoginForm);