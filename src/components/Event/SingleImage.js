import React, { Component } from 'react'
import { View, Text, TextInput, Image, TouchableOpacity, FlatList, BackHandler  } from 'react-native'
import {ImagePicker, LinearGradient} from 'expo'
import firebase from '@firebase/app'
import auth from '@firebase/auth'
import Modal from "react-native-modal";

const likeCount = 1000;
let ranges = [
    { divider: 1e18 , suffix: 'P' },
    { divider: 1e15 , suffix: 'E' },
    { divider: 1e12 , suffix: 'T' },
    { divider: 1e9 , suffix: 'G' },
    { divider: 1e6 , suffix: 'M' },
    { divider: 1e3 , suffix: 'k' }
];
export default class SingleImage extends Component{

    constructor(props) {
        super(props);
        this.state = {showHome: true, showGallery: false, isModalVisible: false};
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
   /*  componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    } */

    favourite = () => {
        alert("favourite");
    }

    like = () => {
        alert("like");
    }

    openCamera = () => {
        alert("camera");
    }

    renderLikeCount = () => {
        let formatedNumber = this.formatNumber(likeCount);
        return <Text style = {styles.likeCount}>{formatedNumber}</Text>            
    }


    formatNumber(n) {
        for (let i = 0; i < ranges.length; i++) {
            if (n >= ranges[i].divider) {
                return (n / ranges[i].divider).toString() + ranges[i].suffix;
            }
        }
        return n.toString();
    }

    _pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true,
          aspect: [4, 3],
        });

        alert();
        
       // //console.log(result);
    
        if (!result.cancelled) {
          this.uploadAsFile(result.uri, "test-image")
            .then(() => {
                alert("Success");
            })
            .catch((error) => {
                alert(error);
            });
        }
      };
    
    uploadAsFile = async (uri, progressCallback) => {

        const response = await fetch(uri);
        const blob = await response.blob();
        const { currentUser } = firebase.auth();

        var metadata = {
            contentType: 'image/jpeg',
        };
        let name = new Date().getTime() + "-media.jpg"
        const ref = firebase
            .storage()
            .ref()
            .child('eventImages/' + name);

            const task = ref.put(blob, metadata);

            return new Promise((resolve, reject) => {
              task.on(
                'state_changed',
                (snapshot) => {
                  progressCallback && progressCallback(snapshot.bytesTransferred / snapshot.totalBytes)
        
                  var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                 // //console.log('Upload is ' + progress + '% done');
                },
                (error) => reject(error), /* this is where you would put an error callback! */
                () => {
                  var downloadURL = task.snapshot.downloadURL;
                 // //console.log("_uploadAsByteArray ", task.snapshot.downloadURL)
        
                  // save a reference to the image for listing purposes
                  /* var ref = firebase.database().ref(`/users/${currentUser.uid}/userProfileInfo/profilepicture`);
                  ref.push({
                    'URL': downloadURL,
                    'name': name,
                    'owner': firebase.auth().currentUser && firebase.auth().currentUser.uid,
                    'when': new Date().getTime()
                  }).then(r => resolve(r), e => reject(e)) */
                }
              );
            });
    }

    galleryBtnPress(){
        alert();
        this.setState({showGallery: true})
    }
    renderItem({ item, index }) {
        return (
            <TouchableOpacity style={{margin: 5}} onPress={this._toggleModal}>
                <Image source={require('./assets/camera.png')} style={{width: 95,height: 95, resizeMode: 'stretch', }} />
            </TouchableOpacity>
        )
    }
    
    renderGallery(){
        return (
            <View style = {{flex: 1}}>
                <FlatList
                    contentContainerStyle={{padding: 10,backgroundColor: '#fff',justifyContent: 'center',flexDirection: 'row',flexWrap: 'wrap',}}
                    data={[{key: 'a'}, {key: 'b'},{key: 'c'},{key: 'd'}, {key: 'e'},{key: 'f'},{key: 'g'}, {key: 'h'},{key: 'i'},{key: 'j'},{key: 'k'}
                    ,{key: 'l'},{key: 'm'},{key: 'n'},{key: 'o'},{key: 'p'}]}
                    renderItem={this.renderItem}
                />
            </View>
        )
    }

    _toggleModal = () =>
        this.setState({ isModalVisible: !this.state.isModalVisible });

    handleBackButtonClick() {
        
        if(this.state.showGallery){
            this.setState({
                showGallery: false,
                showHome: true
            })
            return true;
        }

        return false;
    }

    renderHome(){
        return(
            <View style = {{flex: 1, alignItems: 'center',paddingTop: 70,}}>
                <View style = {{flexDirection: 'row', marginTop: 20}}>
                    <TouchableOpacity  onPress={this._pickImage}>
                        <Image source={require('./assets/camera.png')} style={[styles.bodyIcon,{marginRight: 20, width: 110}]} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image source={require('./assets/videotimer.png')} style={styles.bodyIcon} />
                    </TouchableOpacity>                    
                </View>
                <TouchableOpacity onPress = { () => {this.setState({showGallery: true, showHome: false})} } style={styles.galleryIconArea} >
                    <Image style = {{resizeMode: 'stretch', width: 70, height: 70}} source={require('./assets/gallery.png')} />
                </TouchableOpacity>
            </View>
        )
    }

    render(){
        return (
            <View style = {styles.boxContainer}>
                <Modal isVisible={this.state.isModalVisible} style = {{flex:1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 20}}>
                    <Text>Modal</Text>
                    <Text>Hello!</Text>
                    <TouchableOpacity onPress={this._toggleModal}>
                    <Text>Hide me!</Text>
                    </TouchableOpacity>
                </Modal>
                <View style = {styles.headerSect}>
                    <View style = {styles.headerItem}>
                        <Image source={require('./assets/locationicon.png')} style={styles.locationicon} />
                        <Text style = {styles.locationText}>154 view, Rome, Italy</Text>                    
                    </View>
                    <View style = {styles.headerItem}>
                        <TouchableOpacity onPress={this.favourite}>
                            <Image source={require('./assets/favouriteicon.png')} style={styles.favouriteicon} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.like}>
                            <View style = {styles.likeArea}>
                                <Image source={require('./assets/likeicon.png')} style={styles.likeicon} />
                                {/* <Text style = {styles.likeCount}>{likeCount}</Text> */}
                                {this.renderLikeCount()}
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style = {styles.bodySect}>
                    <TouchableOpacity>
                        <Image source={require('./assets/group.png')} style={[styles.bodyIcon]} />
                    </TouchableOpacity>                                                                       
                </View>
                <LinearGradient style = {styles.bottomSect} colors = { ['#F94101', '#EA0801'] }>
                    <TouchableOpacity style = {styles.bottomItem}>
                        <Image source={require('./assets/share.png')} style={styles.bottomIcon} />                    
                    </TouchableOpacity>
                    <TouchableOpacity style = {styles.bottomItem}>
                        <Image source={require('./assets/upload2.png')} style={[styles.bottomIcon, {width: 50}]} />                    
                    </TouchableOpacity>
                    <TouchableOpacity style = {styles.bottomItem}>
                        <Image source={require('./assets/forma3.png')} style={styles.bottomIcon} />                    
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this._toggleModal} style = {styles.bottomItem}>
                        <Image source={require('./assets/forma4.png')} style={styles.bottomIcon} />                    
                    </TouchableOpacity>
                    <TouchableOpacity disabled={true} style = {[styles.bottomItem,{borderRightWidth: 0}]}>
                        <Image source={require('./assets/forma5.png')} style={styles.bottomIcon} />                    
                    </TouchableOpacity>
                </LinearGradient>
                       
            </View>
        );
    }
};

const styles = {
    boxContainer: {
        flex: 1
    },
    headerSect: {
        backgroundColor: '#545454',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',       
        paddingBottom: 20,
        paddingHorizontal: 20,
    },
    headerItem: {
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    locationText: {
        color: '#fff',
        fontSize: 14,
    },
    locationicon: {
        height: 37,
        width: 27,
        resizeMode: 'stretch',
        marginRight: 10,
    },
    favouriteicon: {
        height: 37,
        width: 37,
        resizeMode: 'stretch',
        marginRight: 20
    },
    likeArea: {
        position: 'relative',
        marginRight: 15
    },
    likeCount: {
        position: 'absolute',
        top: 0,
        right: -15,
        color: '#fff',
        backgroundColor: '#EB1406',
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#fff',
        width: 26,
        height: 26,
        fontSize: 10,
        paddingTop: 5,
        textAlign: 'center'
    },  
    likeicon: {
        height: 37,
        width: 37,
        resizeMode: 'stretch',
    },
    bodySect: {
        backgroundColor: '#DDDDDD',
        flex: 5,
        justifyContent: 'center',
        alignItems: 'center',
        // paddingTop: 70,
        position: 'relative'           
    },
    bodyIcon: {
        width: 320,
        height: 320,
        resizeMode: 'stretch',        
    },
    galleryIconArea: {
        position: 'absolute',
        bottom: 30,
        right: 30,
    },
    bottomSect: {
        // backgroundColor: '#F81000',
        flex: 1,
        flexDirection: 'row'                
    },
    bottomItem: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRightWidth: 1,
        borderColor: '#9F624D'
    },
    bottomIcon: {
        width: 40,
        height: 40,
        resizeMode: 'stretch',
    }
}
