import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');
const numColumns = 3;


const styles = StyleSheet.create({

//gallery Styles
    container: {
        flex: 1,
        marginVertical: 20,
    },
    item: {
        //backgroundColor: '#4D243D',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        margin: 1,
        height: Dimensions.get('window').width / numColumns, // approximate a square
    },
    image: {
        //backgroundColor: '#4D243D',
        flex: 1,
        alignSelf: 'stretch',
        width: undefined,
        height: undefined
    },
    itemInvisible: {
        backgroundColor: 'transparent',
    },
    itemText: {
        color: '#fff',
    },

//gallery Styles

//Event Styles
    boxContainer: {
        flex: 1
    },

    headerSect: {
        backgroundColor: '#545454',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingBottom: 20,
        paddingHorizontal: 20,
    },
    avatar: {
        width: 55,
        height: 55,
        borderRadius: 27.5,
        borderColor: 'gray',
        borderWidth: 1,
    },
    avatarAccepted: {
        width: 55,
        height: 55,
        borderRadius: 27.5,
        borderColor: 'green',
        borderWidth: 1,
    },
    avatarPending: {
        width: 55,
        height: 55,
        borderRadius: 27.5,
        borderColor: 'green',
        borderWidth: 1,
    },
    headerItem: {
        flexDirection: 'row',
        alignItems: 'flex-end'
    },

    locationText: {
        color: '#fff',
        fontSize: 14,
        fontFamily: 'coolvetica'
    },

    locationicon: {
        height: 37,
        width: 27,
        resizeMode: 'stretch',
        marginRight: 10,
    },

    favouriteicon: {
        height: 37,
        width: 37,
        resizeMode: 'stretch',
        marginRight: 20
    },

    likeArea: {
        position: 'relative',
        marginRight: 15
    },

    likeCount: {
        position: 'absolute',
        top: 0,
        right: -15,
        color: '#fff',
        backgroundColor: '#EB1406',
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#fff',
        width: 26,
        height: 26,
        fontSize: 10,
        paddingTop: 7,
        textAlign: 'center',
        fontFamily: 'coolvetica'
    },

    likeicon: {
        height: 37,
        width: 37,
        resizeMode: 'stretch',
    },

    bodySect: {
        backgroundColor: '#DDDDDD',
        flex: 5,
        position: 'relative'
    },

    bodyIcon: {
        width: 130,
        height: 130 * .76,
        resizeMode: 'stretch',
    },

    galleryIconArea: {
        position: 'absolute',
        bottom: 30,
        right: 30,
    },

    bottomSect: {
        flex: 1,
        flexDirection: 'row'
    },

    bottomItem: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRightWidth: 1,
        borderColor: '#9F624D'
    },

    bottomIcon: {
        width: 40,
        height: 40,
        resizeMode: 'stretch',
    },

    userBox: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'stretch',
        paddingHorizontal: 15,
        paddingVertical: 10,
        justifyContent: 'space-between'

    },

    profilePhoto: {
        width: 50,
        height: 50,
        resizeMode: 'stretch',
    },

    showName: {
        justifyContent: 'space-between',
        flex: 1,
        paddingLeft: 20
    }
});

export default styles;