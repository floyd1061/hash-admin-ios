import React, { Component } from 'react'
import { PanResponder, View, Dimensions, TouchableOpacity, Text } from 'react-native'
import Svg, { Path, Circle, G } from 'react-native-svg'


export default class CircleSlider extends Component {
  constructor(props) {
    super(props)

    this.state = {
      angle: this.props.value,
    };
  }

  componentWillMount() {
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (e, gs) => true,
      onStartShouldSetPanResponderCapture: (e, gs) => true,
      onMoveShouldSetPanResponder: (e, gs) => true,
      onMoveShouldSetPanResponderCapture: (e, gs) => true,
      onPanResponderMove: (e, gs) => {
        let xOrigin = this.props.xCenter - (this.props.dialRadius + this.props.btnRadius);
        let yOrigin = this.props.yCenter - (this.props.dialRadius + this.props.btnRadius);
        let a = this.cartesianToPolar(gs.moveX - xOrigin, gs.moveY - yOrigin);

        this.setState({ angle: a });
      }
      ,
      onPanResponderRelease: (e, gestureState) => {

        this.props.prepareVideoCamera(Math.round((this.state.angle) / 6));
      },
    });
  }

  polarToCartesian(angle) {
    let r = this.props.dialRadius;
    let hC = this.props.dialRadius + this.props.btnRadius;
    let a = (angle - 90) * Math.PI / 180.0;

    let x = hC + (r * Math.cos(a));
    let y = hC + (r * Math.sin(a));
    return { x, y };
  }

  cartesianToPolar(x, y) {
    let hC = this.props.dialRadius + this.props.btnRadius;

    if (x === 0) {
      return y > hC ? 0 : 180;
    }
    else if (y === 0) {
      return x > hC ? 90 : 270;
    }
    else {
      return (Math.round((Math.atan((y - hC) / (x - hC))) * 180 / Math.PI) +
        (x > hC ? 90 : 270));
    }
  }

  render() {
    let width = 120;
    let bR = this.props.btnRadius;
    let dR = this.props.dialRadius;
    let startCoord = this.polarToCartesian(0);
    let endCoord = this.polarToCartesian(this.state.angle);

    return (
      <View style={{flex: 1}}>
        <Svg
          ref="circleslider"
          width={width}
          height={width}>

          <G>
            <Circle r={dR - this.props.dialWidth / 2}

              cx={width / 2}
              cy={width / 2}
              stroke='#eee'
              strokeWidth={5}
              fill='#EA0801' />
            {/* <Text x={dR + 10}
            y={dR + 8}
            fontSize={this.props.textSize}
            fill={this.props.textColor}
            textAnchor="middle"

          >{this.props.onValueChange(this.state.angle) + ''}</Text>
          <Text x={dR + 10}
            y={dR + 25}
            fontSize={this.props.textSize}
            fontFamily="coolvetica"
            fill={this.props.textColor}
            textAnchor="middle"
          >sets</Text> */}
          </G>

          <Circle r={dR}
            cx={width / 2}
            cy={width / 2}
            stroke='#787878'
            strokeWidth={this.props.dialWidth}
            fill='none'
          />

          <Path stroke={this.props.meterColor}
            strokeWidth={this.props.dialWidth}
            fill='none'
            d={`M${startCoord.x} ${startCoord.y} A ${dR} ${dR} 0 ${this.state.angle > 180 ? 1 : 0} 1 ${endCoord.x} ${endCoord.y}`} />

          <G x={endCoord.x - bR} y={endCoord.y - bR}>
            <Circle r={bR}

              cx={bR}
              cy={bR}
              fill={this.props.meterColor}
              {...this._panResponder.panHandlers} />

          </G>

         

        </Svg >
        <Text style={{position:'absolute', zIndex: 80, top:40, left: 54, color:'#fff', fontFamily:'coolvetica',}}>{this.props.onValueChange(this.state.angle) + ''}</Text>
        <Text style={{position:'absolute', zIndex: 100, top:55, left: 48, color:'#fff', fontFamily:'coolvetica',}}>secs</Text>
      </View>
    )
  }
}

CircleSlider.defaultProps = {
  btnRadius: 10,
  dialRadius: 50,
  dialWidth: 20,
  meterColor: '#EA0801',
  textColor: '#fff',
  textSize: 20,
  value: 0,
  xCenter: Dimensions.get('window').width / 2,
  yCenter: Dimensions.get('window').height / 2,
  onValueChange: x => Math.round(x / 6),

}