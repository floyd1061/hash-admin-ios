import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
    boxContainer: {
        flex: 1
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    headerSect: {
        backgroundColor: '#545454',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingBottom: 20,
        paddingHorizontal: 20,
    },

    headerItem: {
        flexDirection: 'row',
        alignItems: 'flex-end'
    },

    locationText: {
        color: '#fff',
        fontSize: 14,
        fontFamily: 'coolvetica'
    },

    locationicon: {
        height: 37,
        width: 27,
        resizeMode: 'stretch',
        marginRight: 10,
    },

    favouriteicon: {
        height: 37,
        width: 37,
        resizeMode: 'stretch',
        marginRight: 20
    },

    likeArea: {
        position: 'relative',
        marginRight: 15
    },

    likeCount: {
        position: 'absolute',
        top: 0,
        right: -15,
        color: '#fff',
        backgroundColor: '#EB1406',
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#fff',
        width: 26,
        height: 26,
        fontSize: 10,
        paddingTop: 5,
        textAlign: 'center',
        fontFamily: 'coolvetica'
    },

    likeicon: {
        height: 37,
        width: 37,
        resizeMode: 'stretch',
    },

    bodySect: {
        backgroundColor: '#DDDDDD',
        flex: 5,
        position: 'relative'
    },
    rowContainer: {
        marginTop: 16,
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    bodyIcon: {
        width: 85,
        height: 85,
        resizeMode: 'stretch',
    },

    galleryIconArea: {
        position: 'absolute',
        bottom: 30,
        right: 30,
    },

    bottomSect: {
        flex: 1,
        flexDirection: 'row'
    },

    bottomItem: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRightWidth: 1,
        borderColor: '#9F624D'
    },

    bottomIcon: {
        width: 40,
        height: 40,
        resizeMode: 'stretch',
    },

    userBox: {
        height: 80,
        flexDirection: 'row',
        alignItems: 'center',
        paddingRight: 15,
        paddingLeft: 25,
        justifyContent: 'space-between',
        backgroundColor: '#ccc'
    },

    profilePhoto: {
        width: 50,
        height: 50,
        resizeMode: 'stretch',
    },

    showName: {
        justifyContent: 'space-between',
        flex: 1,
        paddingLeft: 20
    }
});


export default styles;