import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity, View, FlatList, Dimensions } from 'react-native';
import ImageGallery from '../ImageGallery/ImageGallery';
import _ from 'lodash';
import { connect } from 'react-redux'
import { Ionicons } from '@expo/vector-icons';
import { fetchGalleryImages } from '../../actions';


const { width } = Dimensions.get('window');
const numColumns = 3;

class Gallery extends Component {
    state = {
        isGalleryModalVisible: false
    }
    componentDidMount() {
        this.props.fetchGalleryImages(this.props.eventKey);
    }
    formatData = (data, numColumns) => {

        const numberOfFullRows = Math.floor(data.length / numColumns);

        let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
        while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
            data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
            numberOfElementsLastRow++;
        }

        return data;
    };
    galleryImages = (galleryItems) => {
        let galleryImages = [];

        galleryItems.forEach(function (element) {
            if (element.empty) {

            } else {
                let imageObject = {};
                imageObject.source =
                    {
                        uri: element.uri,
                        uid: element.uid,
                        meta: element.meta,
                        height: element.height,
                        width: element.width
                    }

                galleryImages.push(imageObject);
            }

        })
        ////console.log(galleryImages);
        return galleryImages;
    }
    onitemPress(index) {
        this.setState({ isGalleryModalVisible: true, galleryIndex: index })
    }
    renderItem = ({ item, index }) => {

        if (item.empty === true) {
            return <View style={[styles.item, styles.itemInvisible]} />;
        } else {
            return (
                <TouchableOpacity style={styles.item} onPress={() => { this.onitemPress(index) }}>
                    {(item.meta === 'video/mp4') ? <View style={{ position: 'absolute', top: 10, zIndex: 10, right: 10 }}>
                        <Ionicons name="ios-film-outline" size={20} color="white" />
                    </View> : null}
                    <Image source={{ uri: item.thumbnail }} style={styles.image} />
                </TouchableOpacity>
            );
        }
    };

    _keyExtractor = (item, index) => item.uid;
    render() {
        //let data = galleryImages(this.props.galleryItems);
        if (this.state.isGalleryModalVisible) {
            return (
                <View style={{ flex: 1, alignItems: 'stretch', backgroundColor: 'black' }}>
                    <View style={{ position: 'absolute', top: 10, zIndex: 10, right: 20 }}>
                        <TouchableOpacity onPress={() => {
                            this.setState({
                                isGalleryModalVisible: false
                            })
                        }}>
                            <Ionicons name="ios-close" size={70} color="#EA0801" md="md-close" />
                        </TouchableOpacity>
                    </View>
                    <ImageGallery
                        style={{ flex: 1, alignItems: 'stretch', backgroundColor: 'black' }}
                        images={this.galleryImages(this.props.galleryItems)}
                        initialPage={this.state.galleryIndex}
                    />

                </View>

            )
        }
        return (
            <FlatList
                data={this.formatData(this.props.galleryItems, numColumns)}
                style={styles.container}
                renderItem={this.renderItem}
                numColumns={numColumns}
                keyExtractor={this._keyExtractor}

            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginVertical: 20,
    },
    item: {
        //backgroundColor: '#4D243D',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        margin: 1,
        height: Dimensions.get('window').width / numColumns, // approximate a square
    },
    image: {
        //backgroundColor: '#4D243D',
        flex: 1,
        alignSelf: 'stretch',
        width: undefined,
        height: undefined
    },
    itemInvisible: {
        backgroundColor: 'transparent',
    },
    itemText: {
        color: '#fff',
    },
});
const mapStateToProps = (state) => {
    const galleryItems = _.map(state.eventGallery.eventGallery, (val, uid) => {
        return { ...val, uid };
    });
    return {
        galleryItems: galleryItems,
    };
};


export default connect(mapStateToProps, { fetchGalleryImages })(Gallery)
