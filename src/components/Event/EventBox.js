import React, { Component } from 'react'
import {
    View, Text, Image, TouchableOpacity, FlatList, Dimensions, NativeModules,
    NativeEventEmitter,
    Alert
} from 'react-native'
import RNFS from 'react-native-fs'
import { ImagePicker, LinearGradient, Camera, Permissions, Video, Location, MapView, ScreenOrientation } from 'expo'
import VideoPlayer from '@expo/videoplayer';
import uuid from 'uuid/v4';
import firebase from '@firebase/app'
import Modal from "react-native-modal";
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';
import { connect } from 'react-redux'
import { Ionicons } from '@expo/vector-icons';
import styles from './EventBoxStyles';
import getPermission from '../../utils/getPermission';
import shrinkAssetAsync from '../../utils/shrinkImageAsync';
import shrinkToThumbnailAsync from '../../utils/thumbnailImageAsync';
import shrinkVideoAsync from '../../utils/shinkVideoAsync'
import uploadAsset from '../../utils/uploadPhoto';
import {
    fetchTranscript, fetchGalleryImages, getallfriends, inviteToEvent,
    setEventLocaiton, setFavouriteStatus, uploadImageToEventPhotoGallery,
    uploadVideoToEventVideoGallery
} from '../../actions';
import UserList from './Modals/UserList';
import Gallery from './Gallery';
import Camerao from './camera'

import ProgressCircle from 'react-native-progress-circle'
import CircularSlider from './videoSlider';

let ranges = [
    { divider: 1e18, suffix: 'P' },
    { divider: 1e15, suffix: 'E' },
    { divider: 1e12, suffix: 'T' },
    { divider: 1e9, suffix: 'G' },
    { divider: 1e6, suffix: 'M' },
    { divider: 1e3, suffix: 'k' }
];
const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.00622;

const PESDK = NativeModules.PESDK

class EventBox extends Component {

    constructor(props) {
        super(props);
        this.prepareVideoCamera = this.prepareVideoCamera.bind(this);
        this.toggleFavoriteEvent = this.toggleFavoriteEvent.bind(this);
        this.goBack = this.goBack.bind(this);
        this.shareEvent = this.shareEvent.bind(this);
        this.upProgress = 0;
        this.interval = null;
        this.state = {

            favoriteEvent: this.props.event.isFavourited,
            showHome: true, showGallery: false,
            isShareEventModalVisible: false,
            showCamera: false, friends: null,
            videoPlay: false, videoMuted: false,
            hasLocationPermissions: false,
            likeCount: 0, locationName: (this.props.event.location != null) ? this.props.event.location.name : "Set Event Location..",
            isMapModalVisible: false, isLikeModalVisible: false, videoLoading: false,
            uplodingVideo: false, uploadProgress: 0, showVideoCamera: false,
            mapRegion: {
                latitude: (this.props.event.location != null) ? this.props.event.location.coordinate.latitude
                    : (this.props.location != null) ? this.props.location.coords.latitude : 37.452778,
                longitude: (this.props.event.location != null) ? this.props.event.location.coordinate.longitude
                    : (this.props.location != null) ? this.props.location.coords.longitude : -122.183333,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LATITUDE_DELTA * ASPECT_RATIO,
            }, image: null, lastVideoUrl: this.props.event.coverVideo,
            firstTimerUpload: false,
            slider1: 90,
            slider2: 180,
            videoDuration: 15,
            countDownCounter: 0,
            renderGallery: false,
            poi: (this.props.event.location != null) ? this.props.event.location : null,
            newMapRegion: null
        };
        this.mapRegions = [];
        this.onPoiClick = this.onPoiClick.bind(this);
        this._handleMapRegionChange = this._handleMapRegionChange.bind(this);
    }

    componentWillMount() {
        this.eventEmitter = new NativeEventEmitter(NativeModules.PESDK);
        this.eventEmitter.addListener('PhotoEditorDidCancel', () => {
            console.log('Cancelled')
            // The photo editor was cancelled.
            Alert.alert(
                'Profile picture upload Cancel',
                'Do you want to cancel',
                { cancelable: true }
            )
        })
        this.eventEmitter.addListener('PhotoEditorDidSave', (body) => {

            //console.log(body.data);
            this.uploadEventImage({ text: 'Profile Image upload', image: 'data:image/jpeg;base64,' + body.data });
        })
        this.eventEmitter.addListener('PhotoEditorDidFailToGeneratePhoto', () => {
            // The photo editor could not create a photo.
            Alert.alert(
                'PESDK did Fail to generate a photo.',
                'Please try again.',
                { cancelable: true }
            )
        })
    }

    componentWillUnmount() {
        this.eventEmitter.removeAllListeners('PhotoEditorDidCancel')
        this.eventEmitter.removeAllListeners('PhotoEditorDidSave')
        this.eventEmitter.removeAllListeners('PhotoEditorDidFailToGeneratePhoto')
    }

    onPoiClick(e) {
        let uid = firebase.auth().currentUser.uid;
        if (uid !== this.props.event.organizer) {
            return;
        }
        const poi = e.nativeEvent;
        this.setState({
            poi: poi, locationName: poi.name
        });
        setTimeout(() => {
            this.props.setEventLocaiton(this.props.itemKey, this.props.eventKey, poi, this.props.animationLength, this.props.item);
        }, 200)
    }

    _handleMapRegionChange(e) {
        const newMapRegion = e.nativeEvent
        this.setState({ newMapRegion, });
    }

    toggleFavoriteEvent() {
        this.setState({
            favoriteEvent: !this.state.favoriteEvent
        });
        setTimeout(() => {
            this.props.setFavouriteStatus(this.props.itemKey, this.props.eventKey, this.state.favoriteEvent, this.props.animationLength, this.props.item);
        }, 300)
    }

    goBack() {
        if (this.state.renderGallery) {
            this.setState({
                renderGallery: false
            })
        } else {
            Actions.pop();
        }

    }
    // Discussion Needs to happened
    renderLikeCount = () => {
        let formatedNumber = 0;
        return <Text style={styles.likeCount}>{formatedNumber}</Text>
    }

    //media upload needs vision api implementation
    _pickImage = async () => {
        const status = await getPermission(Permissions.CAMERA_ROLL);

        if (status) {
            const result = await ImagePicker.launchImageLibraryAsync({
                allowsEditing: false,
                mediaTypes: 'All',
                quality: 0.5

            });



            if (!result.cancelled) {
                if (result.type === 'image') {
                    /* const image = result;
                    this.uploadEventImage({ text: 'event Image upload', image }); */
                    const image = result;
                    const imagePath = RNFS.DocumentDirectoryPath + '/image.jpeg'
                    /* const thumbImage = await shrinkToThumbnailAsync(
                        image,
                    ); */
                    //console.log(thumbImage)
                    RNFS.downloadFile({ fromUrl: image.uri, toFile: imagePath, progress: 0 }).promise.then(result => {

                        PESDK.present(imagePath)

                    })
                } else if (result.type === 'video') {
                    const video = result;
                    this.uploadEventVideo({ text: 'event Video upload', video })
                }

            }
        }
    };

    _pickVideo = async () => {
        const status = await getPermission(Permissions.CAMERA_ROLL);

        if (status) {
            const result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: 'Videos'
            });
            if (!result.cancelled) {
                const video = result;
                this.uploadEventVideo({ text: 'event video upload', video });
            }
        }
    };

    uploadPhotoAsync = async uri => {
        let uid = uuid();
        const path = `/eventImages/${uid}.jpg`;
        const thumbPath = `/eventImages/thumb_${uid}.jpg`;

        let url = uploadAsset(uri, path, thumbPath, (progress) => {
        });

        return url;
    };
    uploadEventImage = async ({ image: localUri }) => {
        try {


            const remoteUriOriginal = await this.uploadPhotoAsync(localUri);

            var hW = firebase.functions().httpsCallable('addMessage');
            hW({ text: this.props.eventKey }).then(function (result) {
                // Read result of the Cloud Function.
                var sanitizedMessage = result.data.text;
            }).catch(function (error) {
                // Getting the Error details.
                var code = error.code;
                var message = error.message;
                var details = error.details;
                // ...
            });
     

            /* const remoteUriThumb = await this.uploadPhotoAsync(thumbImage.uri);
            this.props.uploadImageToEventPhotoGallery(this.props.itemKey, this.props.eventKey,
                remoteUriOriginal, remoteUriThumb,
                reducedImage,
                this.props.animationLength, this.props.item) */
        } catch (error) {
            //console.log(error)
        }
    }

    uploadVideoAsync = async uri => {
        const path = `/eventVideos/${uuid()}.mp4`;

        let url = uploadAsset(uri, path, (progress) => {

            this.setState({
                uploadProgress: Math.round(progress)
            })
        });
        return url;
    };
    uploadEventVideo = async ({ video: localUri }) => {
        try {
            this.setState({
                uplodingVideo: true
            })
            const thumbImage = await shrinkVideoAsync(
                localUri,
            );
            const remoteUriThumb = await this.uploadPhotoAsync(thumbImage.uri);

            const remoteUriOriginal = await this.uploadVideoAsync(localUri.uri);
            this.props.uploadVideoToEventVideoGallery(this.props.itemKey, this.props.eventKey,
                remoteUriOriginal, remoteUriThumb,
                this.props.animationLength, this.props.item)
            this.setState({
                uploadProgress: 0,
                uplodingVideo: false,
                lastVideoUrl: remoteUriOriginal,
            })
        } catch (error) {
            //console.log(error)
        }
    }
    //media upload needs vision api implementation

    orientationChangeHandler(dims) {
        const { width, height } = dims.window;
        const isLandscape = width > height;
        this.setState({ isPortrait: !isLandscape });
        this.props.navigation.setParams({ tabBarHidden: isLandscape });
        ScreenOrientation.allow(ScreenOrientation.Orientation.ALL);
    }
    switchToLandscape() {
        ScreenOrientation.allow(ScreenOrientation.Orientation.LANDSCAPE);
    }
    switchToPortrait() {
        ScreenOrientation.allow(ScreenOrientation.Orientation.PORTRAIT);
    }
    renderUploadProgress() {
        return (
            <ProgressCircle
                percent={this.state.uploadProgress}
                radius={50}
                borderWidth={8}
                color="#ff0000"
                shadowColor="#999"
                bgColor="black"
            >
                <Text style={{ textAlign: 'center', fontSize: 16, color: '#fff', fontFamily: 'coolvetica' }}>{this.state.uploadProgress + '%'}</Text>
            </ProgressCircle>
        )
    }
    renderGallery() {
        //console.log(this.props.eventKey);
        return <Gallery itemKey={this.props.itemKey} eventKey={this.props.eventKey} />
    }
    renderHome() {
        const COLOR = '#92DCE5';
        const icon = (name, size = 36) => () =>
            <Ionicons
                name={name}
                size={size}
                color={COLOR}
                style={{ textAlign: 'center' }}
            />;


        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 0 }}>
                <View style={{ backgroundColor: 'black', height: 230, alignSelf: 'stretch' }}>
                    {(this.props.event.coverVideo != null) ?
                        <VideoPlayer
                            videoProps={{
                                shouldPlay: false,
                                resizeMode: Video.RESIZE_MODE_CONTAIN,
                                source: {
                                    uri: this.props.event.coverVideo,
                                },
                                isMuted: false,
                            }}
                            playIcon={icon('ios-play-outline')}
                            pauseIcon={icon('ios-pause-outline')}
                            fullscreenEnterIcon={icon('ios-expand-outline', 28)}
                            fullscreenExitIcon={icon('ios-contract-outline', 28)}
                            trackImage={require('./assets/track.png')}
                            thumbImage={require('./assets/thumb.png')}
                            textStyle={{
                                color: COLOR,
                                fontSize: 12,
                            }}
                            isPortrait={this.state.isPortrait}
                            switchToLandscape={this.switchToLandscape.bind(this)}
                            switchToPortrait={this.switchToPortrait.bind(this)}
                            playFromPositionMillis={0}
                        /> :
                        null
                    }
                    <View style={{
                        position: 'absolute',
                        top: 65,
                        left: (width - 100) / 2,
                        zIndex: 29
                    }}>
                        {(this.state.uplodingVideo) ? this.renderUploadProgress() : null}

                    </View>
                </View>

                <View style={{ flexDirection: 'row', marginTop: 20 }}>

                    <TouchableOpacity onPress={() => { this.setState({ showCamera: true }) }}>
                        <Image source={require('./assets/camera.png')} style={[styles.bodyIcon, { marginRight: 20, marginTop: 10, }]} />
                    </TouchableOpacity>

                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>

                        <CircularSlider
                            value={90}
                            prepareVideoCamera={this.prepareVideoCamera}
                        />

                    </View>
                </View>

                <TouchableOpacity onPress={() => {
                    this.setState({
                        renderGallery: !this.state.renderGallery
                    })
                }} style={styles.galleryIconArea} >
                    <Image style={{ resizeMode: 'stretch', width: 60, height: 60 }} source={require('./assets/gallery.png')} />
                </TouchableOpacity>

            </View>
        )
    }
    recordVideo = async () => {

        if (this.camera) {
            let video = await this.camera.recordAsync({
                quality: '4:3'
            });
            if (video) {

                this.setState({
                    showVideoCamera: false, showCamera: false
                });
                this.uploadEventVideo({ text: 'event video upload', video: video })
                    .then(() => {

                    })
                    .catch((error) => {
                        alert(error);
                    });

            }
        }
    }
    tick = () => {
        if (this.state.countDownCounter === this.state.videoDuration) {
            this.recordVideo();
        }
        this.setState({ countDownCounter: this.state.countDownCounter - 1 });
        if (this.state.countDownCounter <= 0) {
            this.camera.stopRecording();
            clearInterval(this.interval);
        }
    }
    snap = async () => {

        if (this.camera) {
            let image = await this.camera.takePictureAsync();

            if (image) {
                this.setState({
                    showVideoCamera: false, showCamera: false
                });
                this.uploadEventImage({ text: 'event Image upload', image: image })
                    .then(() => {
                    })
                    .catch((error) => {
                        alert(error);
                    });
            }
        }
    };
    prepareVideoCamera(vdoDuration) {
        this.setState({ showVideoCamera: true, videoDuration: vdoDuration, countDownCounter: vdoDuration });
        this.interval = setInterval(this.tick, 1000);
    }
    getPermissionStatus = async () => {
        let audio = await getPermission((Permissions.AUDIO_RECORDING));
        let camera = await getPermission((Permissions.CAMERA));
        if (audio && camera) {
            return true;
        }
        return false;
    }
    renderCamera() {
        const status = this.getPermissionStatus();
        if (status === null) {
            return <View />;
        }
        else if (!status) {
            return <Text style={{ fontFamily: 'coolvetica' }}>No access to camera</Text>;
        }
        else {
            return (
                <View style={{ flex: 1 }}>
                    <Camerao uploadImage={this.uploadImage} />
                </View>
            )
        }
    }
    shareEvent = () => {
        this.setState({ isShareEventModalVisible: !this.state.isShareEventModalVisible });
    }
    renderLikedUserItem({ item, index }) {
        return (
            <TouchableOpacity disabled={true} style={[styles.userBox, { backgroundColor: '#e2e2e2' }]} >
                <TouchableOpacity disabled={true}>
                    <Image source={{ uri: this.state.profilepicURL }} style={styles.profilePhoto} />
                </TouchableOpacity>
                <View style={styles.showName}>
                    <TouchableOpacity disabled={true}>
                        <Text style={{ fontSize: 16, color: '#000', fontFamily: 'coolvetica' }}>Name</Text>
                    </TouchableOpacity>
                </View>
            </TouchableOpacity>
        )
    }
    render() {
        //console.log(this.props.event);
        //console.log(this.props.event.assets);
        return (
            <View style={styles.boxContainer}>
                <Modal isVisible={this.state.isLikeModalVisible}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ backgroundColor: '#fff', width: 300, flex: 1 }}>
                            <View style={{ flex: 1 }}>
                                <FlatList
                                    contentContainerStyle={styles.list}
                                    data={[{ key: 'a' }, { key: 'b' }, { key: 'c' }, { key: 'd' }, { key: 'e' }, { key: 'f' }, { key: 'g' }, { key: 'h' }, { key: 'i' }, { key: 'j' }]}
                                    // data={this.state.friends}
                                    renderItem={this.renderLikedUserItem}
                                />
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-end', height: 40 }}>
                                <TouchableOpacity style={{ padding: 10, width: 80, }} onPress={() => {
                                    this.setState({ isLikeModalVisible: false })
                                }}>
                                    <Text style={{ color: 'red', fontFamily: 'coolvetica' }}>Cancel</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
                <Modal isVisible={this.state.isMapModalVisible}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ backgroundColor: '#fff', width: width - 5 }}>
                            <MapView
                                style={{ alignSelf: 'stretch', height: height - 100, width: width - 5 }}
                                provider='google'
                                initialRegion={this.state.mapRegion}
                                region={this.state.newMapRegion}
                                //onRegionChange={this._handleMapRegionChange}
                                onRegionChangeComplete={this._handleMapRegionChange}
                                onPoiClick={this.onPoiClick}
                            >
                                {this.state.poi && (
                                    <MapView.Marker
                                        coordinate={this.state.poi.coordinate}
                                    >
                                        <MapView.Callout>
                                            <View>
                                                <Text>Place Id: {this.state.poi.placeId}</Text>
                                                <Text>Name: {this.state.poi.name}</Text>
                                            </View>
                                        </MapView.Callout>
                                    </MapView.Marker>
                                )}

                            </MapView>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>

                                <TouchableOpacity style={{ paddingHorizontal: 10 }} onPress={() => {
                                    this.setState({ isMapModalVisible: false })
                                }}>
                                    <Text style={{ color: 'red', fontFamily: 'coolvetica', fontSize: 18 }}>Close</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
                <Modal isVisible={this.state.isShareEventModalVisible}>
                    <UserList
                        itemKey={this.props.itemKey}
                        eventKey={this.props.eventKey}
                        event={this.props.event}
                        item={this.props.item}
                        animationLength={this.props.animationLength}
                        shareEvent={this.shareEvent} />
                </Modal>

                <View style={styles.headerSect}>
                    <View style={styles.headerItem}>
                        <TouchableOpacity onPress={this.goBack}>
                            <Ionicons name="ios-arrow-back" size={40} color="#fff" style={{ paddingRight: 15 }} />
                        </TouchableOpacity>
                        <Image source={require('./assets/locationicon.png')} style={styles.locationicon} />
                        <TouchableOpacity onPress={() => { this.setState({ isMapModalVisible: !this.state.isMapModalVisible }) }}>
                            <Text style={styles.locationText}>{this.state.locationName.toString().trim()}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.headerItem}>
                        <TouchableOpacity onPress={() => {
                            this.toggleFavoriteEvent()
                        }}>
                            {
                                this.state.favoriteEvent ? <Image source={require('./assets/favouriteicon-yellow.png')} style={styles.favouriteicon} />
                                    : <Image source={require('./assets/favouriteicon.png')} style={styles.favouriteicon} />
                            }
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { this.setState({ isLikeModalVisible: !this.state.isLikeModalVisible }) }}>
                            <View style={styles.likeArea}>
                                <Image source={require('./assets/likeicon.png')} style={styles.likeicon} />
                                {this.renderLikeCount()}
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.bodySect}>
                    {this.state.renderGallery ? this.renderGallery() :
                        (this.state.showVideoCamera || this.state.showCamera) ? this.renderCamera() : this.renderHome()}

                </View>
                <LinearGradient style={styles.bottomSect} colors={['#F94101', '#EA0801']}>
                    <TouchableOpacity onPress={() => {
                        this.shareEvent();
                    }} style={styles.bottomItem}>
                        <Image source={require('./assets/share.png')} style={styles.bottomIcon} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { this._pickImage(); }} style={styles.bottomItem}>
                        <Image source={require('./assets/upload2.png')} style={[styles.bottomIcon, { width: 50 }]} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.bottomItem} >
                        <Image source={require('./assets/forma3.png')} style={styles.bottomIcon} />
                    </TouchableOpacity>
                    <TouchableOpacity disabled={true} onPress={this._toggleModal} style={styles.bottomItem}>
                        <Image source={require('./assets/forma4.png')} style={styles.bottomIcon} />
                    </TouchableOpacity>
                    <TouchableOpacity disabled={true} style={[styles.bottomItem, { borderRightWidth: 0 }]}>
                        <Image source={require('./assets/forma5.png')} style={styles.bottomIcon} />
                    </TouchableOpacity>
                </LinearGradient>

            </View>
        );
    }
};

const mapStateToProps = (state, ownProps) => {

    const location = state.auth.location;
    const userProfileData = state.editUserProfileInfo.userProfileInfoData;
    let friends = _.map(state.userPreloadData.friends, (val, uid) => {
        if (val.status === 1) {
            return { ...val, uid };
        }
    });
    friends = _.without(friends, undefined)
    let eventTranscript = state.eventData.eventTranscript;
    return {
        eventTranscript: eventTranscript, location: location,
        friends: friends, userProfileData: userProfileData
    };
};
export default connect(mapStateToProps,
    {
        fetchTranscript, inviteToEvent, setEventLocaiton, getallfriends,
        setFavouriteStatus, uploadImageToEventPhotoGallery,
        uploadVideoToEventVideoGallery
    })
    (EventBox)

