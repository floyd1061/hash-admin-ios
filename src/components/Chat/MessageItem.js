import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, TouchableOpacity } from 'react-native';
import { LinearGradient, MapView } from 'expo'
import Triangle from '../../components/Triangle';
import firebase from '@firebase/app'
import auth from '@firebase/auth'
import { MaterialIcons } from '@expo/vector-icons';

// const currentUserId = 'FN4itYPhckM890uSvICgfq4Q8HW2';

export default class MessageItem extends Component {

    timeSince(createdAt) {

        var seconds = Math.round((new Date()).getTime() / 1000) - createdAt;

        var interval = Math.floor(seconds / 31536000);

        if (interval > 1) {
            return interval + " years ago";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return interval + " months ago";
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return interval + " days ago";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return interval + " hours ago";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return interval + " minutes ago";
        }
        return Math.floor(seconds) + " seconds ago";
    }
    
    renderRight() {
        return (
            <View style={[styles.messageContainer, { flexDirection: 'row-reverse' }]}>
                <View style={[styles.profilePhotoContainer, { justifyContent: 'flex-end', flexDirection: 'row', paddingRight: 0 }]}>
                    <Image source={{ uri: this.props.item.sender.avatar }} style={styles.profilePhoto} />
                </View>
                <View style={[styles.messageBody, { paddingRight: 9, paddingLeft: 0 }]}>
                    <Triangle
                        style={styles.rightTriangle}
                        width={10}
                        height={20}
                        color={'#ff0209'}
                        direction={'right'}
                    />
                    <LinearGradient colors={['#ff0209', '#ff1800', '#e30000']} style={styles.msgRightBox}>
                        <View style={styles.message}>
                            <Text style={[styles.userName, { color: '#fff' }]}>{this.props.item.sender.name}</Text>
                            {(this.props.item.type === "image") ?

                                <Image source={{ uri: this.props.item.image.uri }} style={styles.messageImageVideo} />

                                :
                                (this.props.item.type === "video") ?

                                    <Image source={{ uri: this.props.item.video.uri }} style={styles.messageImageVideo} /> :

                                    (this.props.item.type === "map") ?
                                        <MapView
                                            style={{ alignSelf: 'stretch', height: 185, width: 272 }}
                                            provider='google'
                                            region={this.props.item.location}
                                        >
                                            <MapView.Marker
                                                coordinate={this.props.item.location}
                                                title="My Location"
                                                description="Here I am"
                                            />
                                        </MapView> :
                                        <Text style={styles.userMessage}>{this.props.item.messageBody}</Text>
                            }
                        </View>
                        <View style={styles.timebox}>
                            <Text style={styles.time}>{this.timeSince(this.props.item.createdAt)}</Text>
                        </View>
                        <View style={{
                            position: 'absolute',
                            bottom: 10,
                            right: 12,
                            zIndex: 20,

                        }}>
                            {this.props.item.received ? <MaterialIcons name='done-all' size={10} color="#fff" /> :
                                this.props.item.sent ? <MaterialIcons name='done' size={10} color="#fff" /> : null}

                        </View>
                    </LinearGradient>
                </View>

            </View>
        )
    }

    renderLeft() {
        return (
            <View style={[styles.messageContainer, this.props.style]}>
                <View style={[styles.profilePhotoContainer, this.props.profilePhotoContainerStyle]}>
                    {/* <Image source={require('./assets/avatar.png')} style={styles.profilePhoto} /> */}
                    <Image source={{ uri: this.props.item.sender.avatar }} style={styles.profilePhoto} />
                    {/* <View style = {styles.onlineSign}/> */}
                </View>
                <View style={styles.messageBody}>
                    <Triangle
                        style={styles.leftTriangle}
                        width={10}
                        height={20}
                        color={'#fff'}
                        direction={'left'}
                    />
                    <View style={{
                        backgroundColor: '#fff', flex: 1, flexDirection: 'row',
                        padding: 10,
                        borderRadius: 5
                    }}>
                        <View style={[styles.message]}>
                            <Text style={styles.userName}>{this.props.item.sender.name}</Text>
                            {(this.props.item.type === "image") ?

                                <Image source={{ uri: this.props.item.image.uri }} style={styles.messageImageVideo} /> :
                                (this.props.item.type === "video") ?

                                    <Image source={{ uri: this.props.item.video.uri }} style={styles.messageImageVideo} /> :
                                    (this.props.item.type === "map") ?
                                        <MapView
                                            style={{ alignSelf: 'stretch', height: 185, width: 272 }}
                                            provider='google'
                                            region={this.props.item.location}
                                        >
                                            <MapView.Marker
                                                coordinate={this.props.item.location}
                                                title="My Location"
                                                description="Here I am"
                                            />
                                        </MapView> :
                                        <Text style={styles.userMessage}>{this.props.item.messageBody}</Text>
                            }
                        </View>
                        <View style={styles.timebox}>
                            <Text style={styles.time}>{this.timeSince(this.props.item.createdAt)}</Text>
                        </View>
                        {/* <View style={{
                            position: 'absolute',
                            bottom: 10,
                            right: 10,
                            zIndex: 20,

                        }}>
                            {this.props.item.received ? <MaterialIcons name='done-all' size={10} color="#ff1800" /> :
                                this.props.item.sent ? <MaterialIcons name='done' size={10} color="#ff1800" /> : null}
                        </View> */}
                    </View>
                </View>
            </View>
        )
    }

    renderMessage() {
        const { currentUser } = firebase.auth();
        if (this.props.item.sender.uid == currentUser.uid)
            return this.renderRight();
        else
            return this.renderLeft();
    }

    render() {
        // //console.log("props"+this.props.x);
        // //console.log(this.props.item.user.userId);
        //console.log(this.props.item.type === "image");
        return (
            <View>
                {this.renderMessage()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    messageContainer: {
        flexDirection: 'row',
        paddingTop: 30,
    },

    profilePhotoContainer: {
        paddingRight: 10,
        position: 'relative',
        width: 70,
        // backgroundColor: '#000'
    },

    profilePhoto: {
        width: 60,
        height: 60,
        borderRadius: 100,
        resizeMode: 'stretch',
    },
    messageImageVideo: {
        paddingTop: 5,
        borderRadius: 2,
        flex: 1,
        alignSelf: 'stretch',
        width: 272,
        height: 185
    },

    onlineSign: {
        width: 20,
        height: 20,
        backgroundColor: '#FA1801',
        borderRadius: 20,
        borderWidth: 2,
        borderColor: '#fff',
        position: 'absolute',
        right: 7,
        top: 40
    },

    messageBody: {
        // backgroundColor: "#000",
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row',
        // paddingHorizontal: 15,
        paddingLeft: 10,
        // paddingVertical: 10,
        position: 'relative'
    },

    leftTriangle: {
        position: 'absolute',
        left: 0,
        top: 20,
        zIndex: 1
    },

    rightTriangle: {
        position: 'absolute',
        right: 0,
        top: 20,
        zIndex: 1
    },

    message: {
        flex: 1,
        // backgroundColor: '#ccc'
    },

    userName: {
        paddingBottom: 2,
        fontSize: 11,
        fontFamily: 'coolvetica',
        color: '#FA1801'
    },

    userMessage: {
        fontSize: 12,
        fontFamily: 'coolvetica'
    },
    timebox: {
        position: 'absolute',
        right: 10,
        top: 10,
    },
    time: {

        fontSize: 8,
        textAlign: 'right',
        color: '#ccc',
        fontFamily: 'coolvetica'
    },
    msgRightBox: {
        flex: 1, flexDirection: 'row', padding: 10, borderRadius: 5,
    }
});

