import {
    Dimensions
} from 'react-native'

const { width } = Dimensions.get('window');
const numColumns = 3;

const styles = {

    chatContainer: {
        flex: 1,
        position: 'relative'
    },

    chatHeader: {
        height: 120,
        backgroundColor: "transparent",
        flexDirection: 'row',
        paddingHorizontal: 15,
        alignItems: 'center',
        justifyContent: 'space-between'
    },

    headerOptionArea: {
        width: 200, 
        //height: 150, 
        backgroundColor: '#fff', 
        position: 'absolute',
        right: 15,
        top: 90,
        borderColor: '#FD0607',
        borderWidth: 2,
        borderRadius: 3
    },

    profilePhotoContainer: {
        width: 80,
        height: 60,
    },

    profilePhoto :{
        width: 60,
        height: 60,
        borderRadius: 60,
        resizeMode: 'stretch',
    },

    onlineSign: {
        width: 15,
        height: 15,
        backgroundColor: '#FA1801',
        borderRadius: 15,
        borderWidth: 1.5,
        borderColor: '#fff',
        position: 'absolute',
        right: 25,
        bottom: 0
    },
    offlineSign: {
        width: 15,
        height: 15,
        backgroundColor: 'gray',
        borderRadius: 15,
        borderWidth: 1.5,
        borderColor: '#fff',
        position: 'absolute',
        right: 25,
        bottom: 0
    },
    cameraSnapSign: {
        alignSelf: 'center',
        backgroundColor: 'transparent',
        position: 'absolute',
        bottom: 15
    },

    receiverName: {
        fontSize: 20,
        color: 'black',
        fontFamily: 'coolvetica'
    },

    receiverDescription: {
        fontSize: 12,
        fontFamily: 'coolvetica'
    },

    listIcon: {
        width: 30,
        height: 20
    },

    chatBody: {
        flex: 1,
        backgroundColor: "#DBDBDB",
        paddingBottom: 30
    },
    cameraBody: {
        flex: 1,
        backgroundColor: "#DBDBDB",
    },

    optionArea: {
        width: 170, 
        //height: 240, 
        backgroundColor: '#EDEDED', 
        position: 'absolute',
        left: 15,
        bottom: -2,
        borderColor: '#A3A3A3',
        borderWidth: 2, 
        borderRadius: 3
    },

    optionText: {
        paddingVertical: 8,
        paddingHorizontal: 8,
        color:'#000',
        fontFamily: 'coolvetica'
    },

    cancelContainer: {
        position: 'absolute', 
        bottom: 10, 
        right: 10
    },
    
    cancelText: { 
        fontSize:18,
        color: '#F91700',
        fontFamily: 'coolvetica'
    },

    chatBottom: {
        height: 70,
        flexDirection: 'row',
        backgroundColor: "#A8A8A8",
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 15       
    },

    uploadIcon: {
        width: 30,
        height: 30,
        resizeMode: 'stretch',
        marginRight: 5 ,
        backgroundColor: 'pink'       
    },

    cameraIcon: {
        width: 35,
        height: 30,
        resizeMode: 'stretch',
              
    },
    captureIcon: {
        width: 45,
        height: 45,       
    },
    micIcon: {
        width: 15,
        height: 30,
        resizeMode: 'stretch',        
    },

    chatBox: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: 5
    },

    chatBoxInput: {
        width: width * .57,
        height: 38,
        paddingHorizontal: 5,
        color: '#000',
        fontFamily: 'coolvetica'
    },
    chatBottomRightIconPanel: {
        width: width * .20,
        paddingLeft: 10,
    },
    chatBottomLeftIconPanel: {
        paddingRight: 5,
    },
    chatBoxButton: {

        marginHorizontal: 5,
        textAlign: 'center',
        color: '#fff',
        fontFamily: 'coolvetica'
    },  
}

export default styles;