import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity, View, FlatList, Dimensions } from 'react-native';
import ImageGallery from '../ImageGallery/ImageGallery';
import _ from 'lodash';
import { connect } from 'react-redux'
import { Ionicons } from '@expo/vector-icons';
import { fetchConversationGallery } from '../../actions';


const { width } = Dimensions.get('window');
const numColumns = 3;

class Gallery extends Component {

    /* componentDidMount() {
        this.props.fetchConversationGallery(this.props.conversationId);
    } */

    galleryImages = (galleryItems) => {
        let galleryImages = [];

        galleryItems.forEach(function (element) {
                let imageObject = {};
                imageObject.source =
                    {
                        uri: element.uri,
                        uid: element.uid,
                        meta: element.meta,
                        height: element.height,
                        width: element.width
                    }
                galleryImages.push(imageObject)
        })
        return galleryImages;
    }

    render() {

        return (
            <View style={{ flex: 1, alignItems: 'stretch', backgroundColor: 'black' }}>
                <View style={{ position: 'absolute', top: 10, zIndex: 10, right: 20 }}>
                    <TouchableOpacity onPress={() => {
                        this.props.closeGallery()
                    }}>
                        <Ionicons name="ios-close" size={70} color="#EA0801" md="md-close" />
                    </TouchableOpacity>
                </View>
                <ImageGallery
                    style={{ flex: 1, alignItems: 'stretch', backgroundColor: 'black' }}
                    images={this.galleryImages(this.props.galleryItems)}
                    initialPage={this.props.galleryIndex}
                />

            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginVertical: 20,
    },
    item: {
        //backgroundColor: '#4D243D',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        margin: 1,
        height: Dimensions.get('window').width / numColumns, // approximate a square
    },
    image: {
        //backgroundColor: '#4D243D',
        flex: 1,
        alignSelf: 'stretch',
        width: undefined,
        height: undefined
    },
    itemInvisible: {
        backgroundColor: 'transparent',
    },
    itemText: {
        color: '#fff',
    },
});
const mapStateToProps = (state, ownProps) => {
   /*  console.log(ownProps.galleryData)
    const galleryItems = _.map(state.messages.chatGallery, (val, uid) => {
        return { ...val, uid };
    }); */
    const galleryIndex = _.findIndex(ownProps.galleryData, ["uri", ownProps.indexItemUri])
    return {
        galleryItems: ownProps.galleryData,
        galleryIndex: galleryIndex
    };
};


export default connect(mapStateToProps, { fetchConversationGallery })(Gallery)
