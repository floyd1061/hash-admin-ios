import React, { PureComponent } from 'react'
import firebase from '@firebase/app'
import Modal from "react-native-modal";
import {
    View, Text, TextInput, Image,
    TouchableOpacity, TouchableWithoutFeedback, Keyboard,
    KeyboardAvoidingView, FlatList
} from 'react-native'
import uuid from 'uuid/v4';
import { connect } from 'react-redux'
import { Ionicons, Feather, Entypo, FontAwesome } from '@expo/vector-icons';
import _ from 'lodash';
import styles from './ChatContainerStyles';
import MessageItem from './MessageItem'
import { Actions } from 'react-native-router-flux';
import { ImagePicker, LinearGradient, Camera, Permissions, Video, Location, MapView, ScreenOrientation } from 'expo';
import {
    setReceived, fetchConversationById, detachChatlistListener,
    saveMessageToFirebase, saveVideoMessageToFirebase, createNewConversationWithUser, saveImageMessageToFirebase,
    saveLocationMessageToFirebase
} from '../../actions';
import getPermission from '../../utils/getPermission';
import shrinkAssetAsync from '../../utils/shrinkImageAsync';
import shrinkToThumbnailAsync from '../../utils/thumbnailImageforMesengerAsync';
import shrinkVideoAsync from '../../utils/shinkVideoAsync'
import uploadAsset from '../../utils/uploadPhoto';
import Camerao from './camera'
import Gallery from './Gallery'


class ChatListView extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            showOption: false, showHeaderOption: false, userMessage: '',
            headerName: '', profilePicUrl: '', uploadProgress: 0, showCamera: false, isGalleryModalVisible: false,
        };
        this.uploadImage = this.uploadImage.bind(this);
        this.closeGallery = this.closeGallery.bind(this);
    }
    componentDidMount() {
        if (this.props.conversation_id != null) {
            //this.props.fetchConversationById(this.props.conversation_id)
            this.props.setReceived(this.props.conversation_id);
        } else {
            //this.props.fetchConversationById(this.state.conversation_id)
            this.props.setReceived(this.state.conversation_id);
        }

    }
    /* shouldComponentUpdate(nexProps, nextState) {
        //console.log()
        return this.props.lastMessageSentAt != nexProps.lastMessageSentAt;
    } */
    goBack() {
        Actions.pop();
    }
    resizedString(msg, endIndex) {
        ////console.log(msg);
        return msg.length > endIndex ? msg.substring(0, endIndex) + '...' : msg;
    }
    sendMessage() {
        if (this.props.conversation_id != null) {
            this.props.saveMessageToFirebase(this.props.conversation_id, this.state.userMessage)
            this.setState({ userMessage: "" })
        }
        else if (this.state.conversation_id != null) {
            this.props.saveMessageToFirebase(this.state.conversation_id, this.state.userMessage)
            this.setState({ userMessage: "" })
        }
        else {
            this.props.createNewConversationWithUser(this.props.friend, this.state.userMessage, 'text', (conversationId) => {
                this.setState({ conversation_id: conversationId });
            })
            this.setState({ userMessage: "" })
        }
        //alert(this.props.conversation_id);
    }
    sendLocationMessage() {
        this.setState({ showOption: false })

        if (this.props.conversation_id != null) {
            this.props.saveLocationMessageToFirebase(this.props.conversation_id, this.props.location)

        }
        else if (this.state.conversation_id != null) {
            this.props.saveLocationMessageToFirebase(this.state.conversation_id, this.props.location)

        }
        else {
            this.props.createNewConversationWithUser(this.props.friend, this.props.location, 'location', (conversationId) => {
                this.setState({ conversation_id: conversationId });

            })
            this.setState({ userMessage: "" })
        }
    }
    uploadImage = async ({ image: localUri }) => {

        try {
            this.setState({
                showCamera: false,
                uploding: true,
                isGalleryModalVisible: false,
                indexItemUri: ""
            })
            const reducedImage = await shrinkAssetAsync(
                localUri,
            );
            //console.log(reducedImage);
            const thumbImage = await shrinkToThumbnailAsync(
                localUri,
            );
            //console.log(thumbImage);
            const remoteUriOriginal = await this.uploadPhotoAsync(reducedImage.uri);
            //const remoteUriThumb = await this.uploadPhotoAsync(thumbImage.uri);

            const remoteUriThumb = "";
            if (this.props.conversation_id != null) {
                this.props.saveImageMessageToFirebase(this.props.conversation_id, remoteUriOriginal, remoteUriThumb,
                    reducedImage)
            }
            else if (this.state.conversation_id != null) {
                this.props.saveImageMessageToFirebase(this.state.conversation_id, remoteUriOriginal, remoteUriThumb,
                    reducedImage)
            }
            else {

                /* this.props.saveImageMessageToFirebase(this.props.conversation_id, remoteUriOriginal, remoteUriThumb,
                    reducedImage) */

                let userMessage = {
                    remoteUriOriginal: remoteUriOriginal,
                    remoteUriThumb: remoteUriThumb,
                    reducedImage: reducedImage
                }

                this.props.createNewConversationWithUser(this.props.friend, userMessage, 'image', (conversationId) => {
                    this.setState({ conversation_id: conversationId });

                })
                this.setState({ userMessage: "" })
            }

        } catch (error) {
            console.log(error)
        }
        finally {
            this.setState({
                uploadProgress: 0,
                uploding: false,
            })
        }
    }
    uploadVideo = async ({ video: localUri }) => {
        try {
            this.setState({
                uploding: true
            })
            const thumbImage = await shrinkVideoAsync(
                localUri,
            );
            //const remoteThumb = await this.uploadPhotoAsync(thumbImage.uri);
            const remoteThumb = "";
            const remoteUri = await this.uploadVideoAsync(localUri.uri);
            if (this.props.conversation_id != null) {
                this.props.saveVideoMessageToFirebase(this.props.conversation_id, remoteUri, remoteThumb)
            }
            else if (this.state.conversation_id != null) {
                this.props.saveVideoMessageToFirebase(this.state.conversation_id, remoteUri, remoteThumb)
            }
            else {

                let userMessage = {
                    remoteUri: remoteUri,
                    remoteThumb: remoteThumb,
                }

                this.props.createNewConversationWithUser(this.props.friend, userMessage, 'video', (conversationId) => {
                    this.setState({ conversation_id: conversationId });

                })
                this.setState({ userMessage: "" })
            }


        } catch (error) {
            console.log(error)
        } finally {
            this.setState({
                uploadProgress: 0,
                uploding: false,
            })
        }
    }

    closeGallery = () => {
        this.setState({
            isGalleryModalVisible: !this.state.isGalleryModalVisible,
        })
    }
    showgallery = (item) => {
        //this.closeGallery();
        let indexItemUri = item.type === "image" ? item.image.uri : item.video.uri
        this.setState({
            indexItemUri: indexItemUri,
            isGalleryModalVisible: !this.state.isGalleryModalVisible,
        })


    }
    renderItem = ({ item, index }) => {

        if (item.type === "image" || item.type === "video") {
            return (
                <TouchableOpacity onPress={() => {
                    this.showgallery(item)
                    /* this.setState({
                        isGalleryModalVisible: true,
                        indexItemUri: item.type === "image" ? item.image.uri : item.video.uri
                    }) */
                }}>
                    <MessageItem item={item} />
                </TouchableOpacity>
            );
        } else {
            return <MessageItem item={item} />;
        }


    }
    isObject(value) {
        return value && typeof value === 'object' && value.constructor === Object;
    }
    getProfilePic(profilepictures) {

        if (this.isObject(profilepictures)) {
            let pps = _.map(profilepictures, (val, uid) => {
                return { ...val, uid }
            })
            if (pps.length > 0) {
                return pps[pps.length - 1].URL
            }

        } else {
            return profilepictures;
        }
        return '../assets/avatar.png';
    }

    uploadPhotoAsync = async uri => {
        //console.log(uri);
        const path = `/messengerImages/${uuid()}.jpg`;
        let url = uploadAsset(uri, path, (progress) => {
            this.setState({
                uploadProgress: Math.round(progress)
            })
        });

        return url;
    };


    uploadVideoAsync = async uri => {

        const path = `/messengerVideos/${uuid()}.mp4`;

        let url = uploadAsset(uri, path, (progress) => {

            this.setState({
                uploadProgress: Math.round(progress)
            })
        });
        return url;
    };


    _pickImage = async () => {
        const status = await getPermission(Permissions.CAMERA_ROLL);

        if (status) {
            const result = await ImagePicker.launchImageLibraryAsync({
                allowsEditing: true,
                mediaTypes: 'All',
                quality: 0.5

            });

            if (!result.cancelled) {
                this.setState({ showOption: false })
                if (result.type === 'image') {
                    const image = result;
                    this.uploadImage({ text: 'event Image upload', image });
                } else if (result.type === 'video') {
                    const video = result;
                    this.uploadVideo({ text: 'event Video upload', video })
                }

            }
        }
    };

    getUsersOnlineStatus(uid) {
        let status = _.findLast(this.props.appUsers, ["uid", uid])

        if (status.status.state != null && status.status.state === 'online') {
            return true;
        }
        return false;
    }

    snap = async () => {

        if (this.camera) {
            let image = await this.camera.takePictureAsync();
            this.setState({
                showCamera: false
            });
            if (image) {

                this.uploadImage({ text: 'chat Image upload', image: image })
                    .then(() => {

                    })
                    .catch((error) => {
                        alert(error);
                    });
            }
        }
    };

    getPermissionStatus = async () => {
        let audio = await getPermission((Permissions.AUDIO_RECORDING));
        let camera = await getPermission((Permissions.CAMERA));
        if (audio && camera) {
            return true;
        }
        return false;
    }

    renderCamera() {
        const status = this.getPermissionStatus();
        if (status === null) {
            return <View />;
        }
        else if (!status) {
            return <Text style={{ fontFamily: 'coolvetica' }}>No access to camera</Text>;
        }
        else {
            return (
                <View style={{ flex: 1 }}>
                    <Camerao uploadImage={this.uploadImage} />
                </View>
            )
        }
    }


    getProfilePicAndStatus(reversedArray) {
        const { currentUser } = firebase.auth();
        let status = false;

        let pp = '../assets/avatar.png';
        let displayNameStr = "";
        let userStatus = "";
        if (this.props.friend != null || this.props.friend != undefined) {
            let pps = _.map(this.props.friend.profilepicture, (val, uid) => { return { ...val, uid } })
            pp = pps[pps.length - 1].URL;
            let onOff = _.findLast(this.props.appUsers, ["uid", this.props.friend.uid])
            if (onOff.status.state != null && onOff.status.state === 'online') {
                status = true;
            }
            displayNameStr = this.props.friend.displayName;
            userStatus = this.props.friend.userStatus
        }
        if (this.props.withWhom.length > 0) {
            let user = _.findLast(this.props.friends, ["uid", this.props.withWhom[0].uid]);
            //console.log(this.props.withWhom,this.props.friends);
            let pps = _.map(user.profilepicture, (val, uid) => { return { ...val, uid } })
            //console.log(pps);
            pp = pps[pps.length - 1].URL;
            let onOff = _.findLast(this.props.appUsers, ["uid", user.uid])
            if (onOff.status.state != null && onOff.status.state === 'online') {
                status = true;
            }
            displayNameStr = user.displayName;
            userStatus = user.userStatus
        }

        for (let i = 0; i <= reversedArray.length - 1; i++) {
            //console.log(reversedArray[i].sender.uid, currentUser.uid);
            if (reversedArray[i].sender.uid === currentUser.uid) {

            } else {
                break;
                let user = _.findLast(this.props.friends, ["uid", reversedArray[i].sender.uid]);
                let pps = _.map(user.profilepicture, (val, uid) => { return { ...val, uid } })
                //console.log(pps);
                pp = pps[pps.length - 1].URL;
                let onOff = _.findLast(this.props.appUsers, ["uid", user.uid])
                if (onOff.status.state != null && onOff.status.state === 'online') {
                    status = true;
                }
                displayNameStr = user.displayName;
                userStatus = user.userStatus
            }

            /* pp = reversedArray[i].sender.avatar;
            let onOff = _.findLast(this.props.appUsers, ["uid", reversedArray[i].sender.uid])
            if (onOff.status.state != null && onOff.status.state === 'online') {
              status = true;
            } */
        }

        return { profilePicture: pp, status: status, displayName: displayNameStr, userStatus: userStatus };
    }
    _keyExtractor = (item, index) => item.uid;
    _getGalleryDataFormatted = () => {

        if (this.props.chatData != null) {
            let xx = _.find(this.props.chatData, ["uid", this.state.conversation_id]);
            if (xx != null || xx != undefined) {
                return _.map(xx.conversation, (val, uid) => { return { ...val, uid } }).reverse();
            }

        }
        return;

    }
    _getChatDataFormatted = () => {


        if (this.props.chatData != null) {
            let xx = _.find(this.props.chatData, ["uid", this.state.conversation_id]);
            if (xx != null || xx != undefined) {
                return _.map(xx.conversationAssets, (val, uid) => { return { ...val, uid } }).reverse();
            }

        }
        return;

    }
    render() {
        var data = [];
        var galleryData = [];
        if (this.props.conversation_id != null) {
            data = this.props.messageData;
            galleryData = this.props.galleryData
        } else if (this.state.conversation_id != null) {
            data = this._getChatDataFormatted()
            galleryData = this._getGalleryDataFormatted()
        }

        return (
            <View style={styles.chatContainer}>

                <LinearGradient colors={['#ff0209', '#ff1800', '#e30000']} style={styles.chatHeader}>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TouchableOpacity onPress={this.goBack}>
                            <Ionicons name="ios-arrow-back" size={40} color="#fff" style={{ paddingRight: 15 }} />
                        </TouchableOpacity>
                        <View style={styles.profilePhotoContainer}>
                            <Image style={styles.profilePhoto} source={{
                                uri: this.getProfilePicAndStatus(this.props.messageData).profilePicture

                            }}
                            />
                            {this.getProfilePicAndStatus(this.props.messageData).status ?
                                <View style={styles.onlineSign}></View>
                                : <View style={styles.offlineSign}></View>
                            }

                        </View>
                        <View style={styles.receiverInfo}>
                            <Text style={styles.receiverName}>{this.resizedString(
                                this.getProfilePicAndStatus(this.props.messageData).displayName
                                , 16)}</Text>
                            {<Text style={styles.receiverDescription}>
                                {this.getProfilePicAndStatus(this.props.messageData).userStatus}
                            </Text>}
                        </View>
                    </View>
                    <TouchableOpacity onPress={() => { this.setState({ showHeaderOption: !this.state.showHeaderOption }) }}>
                        <Image source={require('./assets/list.png')} style={styles.listIcon} />
                    </TouchableOpacity>
                </LinearGradient>

                {<KeyboardAvoidingView behavior="padding" style={{ flex: 1 }}>
                    <View style={this.state.showCamera ? styles.cameraBody : styles.chatBody}>
                        <FlatList
                            inverted={true}
                            style={{ paddingHorizontal: 20, }}
                            data={data}
                            //data={this.props.messageData}
                            renderItem={this.renderItem}
                            keyExtractor={this._keyExtractor}

                        />
                        {/* {
                            !this.state.showCamera ?  : this.renderCamera()} */}
                        {this.state.showOption ?
                            <View style={styles.optionArea}>
                                <View style={{ position: 'relative', flex: 1 }}>

                                    <TouchableOpacity onPress={() => { this._pickImage(); }}>
                                        <Text style={styles.optionText}>Photo/Video Library</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.sendLocationMessage()}>
                                        <Text style={[styles.optionText, { paddingBottom: 30 }]}>Share Location</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => { this.setState({ showOption: !this.state.showOption }) }} style={styles.cancelContainer}>
                                        <Text style={styles.cancelText}>Cancel</Text>
                                    </TouchableOpacity>
                                </View>
                            </View> : null
                        }
                        <Modal ref={(ref) => this.galleryModal = ref} isVisible={this.state.isGalleryModalVisible}>
                            <Gallery galleryData={this.props.galleryData} closeGallery={this.closeGallery} indexItemUri={this.state.indexItemUri} conversationId={this.props.conversation_id} />
                        </Modal>
                    </View>
                    <View style={styles.chatBottom}>
                        <TouchableOpacity style={styles.chatBottomLeftIconPanel} onPress={() => { this.setState({ showOption: !this.state.showOption }) }}>
                            <Feather name='upload' size={30} color='#ff1800' />
                        </TouchableOpacity>
                        <View style={styles.chatBox}>
                            <TextInput
                                style={styles.chatBoxInput}
                                //onSubmitEditing={Keyboard.dismiss}
                                onFocus={() => { this.props.setReceived(this.props.conversation_id); }}
                                multiline={true}
                                editable={true}
                                onChangeText={(userMessage) => { this.setState({ userMessage }) }}
                                placeholder="Write a message"
                                value={this.state.userMessage}
                                returnKeyType="next"
                                underlineColorAndroid="transparent"
                            />
                            <TouchableOpacity onPress={() => this.sendMessage()} style={{
                                justifyContent: 'center', alignItems: 'center', backgroundColor: '#ff1800', borderTopRightRadius: 5, borderBottomRightRadius: 5
                            }}>
                                <Text style={styles.chatBoxButton}>Send</Text>
                                {/* <MaterialIcons name='send' size={30} color="#fff" /> */}
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.chatBottomRightIconPanel, { flexDirection: 'row' }]}>
                            <TouchableOpacity onPress={() => {
                                this.setState({ showCamera: !this.state.showCamera })
                            }}>
                                <Image source={require('./assets/camera.png')} style={styles.cameraIcon} />
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Entypo name='mic' size={28} color='#ff0209' />
                            </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAvoidingView>

                }
                {this.state.showHeaderOption ?
                    <View style={styles.headerOptionArea}>
                        <View style={{ position: 'relative', flex: 1 }}>
                            <TouchableOpacity>
                                <Text style={styles.optionText}>Invite friend to chat group</Text>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Text style={styles.optionText}>View contact</Text>
                            </TouchableOpacity>

                        </View>
                    </View> : null
                }
                {this.state.uploding ? <View style={{ position: 'absolute', bottom: 75, left: 10 }}>
                    <Text style={{
                        fontSize: 8,
                        fontFamily: 'coolvetica',
                        color: '#ff1800'
                    }}>{'Uploading ' + this.state.uploadProgress + '%'}</Text>
                </View> : null}
            </View>
        )
    }
}

const mapStateToProps = (state, ownProps) => {

    let messageData = [];
    let galleryData = [];

    const location = state.auth.location;
    const appUsers = _.map(state.userPreloadData.applicationUsers, (val, uid) => {
        return { ...val, uid }
    })
    const { currentUser } = firebase.auth();
    const friends = _.map(state.userPreloadData.friends, (val, uid) => { return { ...val, uid } })

    let withWhom = [];

    let conversationWithWhom = null;

    const chatData = _.map(state.allMessages.conversations, (val, uid) => { return { ...val, uid } });
    if (ownProps.conversation_id != null) {
        galleryData = _.map(_.find(chatData, ["uid", ownProps.conversation_id]).conversationAssets, (val, uid) => { return { ...val, uid } });
        messageData = _.map(_.find(chatData, ["uid", ownProps.conversation_id]).conversation, (val, uid) => { return { ...val, uid } });
        messageData.reverse();
        conversationWithWhom = _.map(ownProps.conversationMetaData.withWhom)

        conversationWithWhom.forEach(element => {
            if (element.uid !== currentUser.uid) {
                withWhom.push(element);
            }
        });

    } else {

    }
    // console.log(messageData);
    /* const chatData = _.map(state.allMessages.conversations, (data, uid) => {
        return { ...data, uid };
    }); */
    return {
        messageData: messageData,//ownProps.messageData,
        chatData: chatData,
        galleryData: galleryData,
        appUsers: appUsers,
        location: location,
        friends: friends,
        withWhom: withWhom,

    };

};

export default connect(mapStateToProps, {
    setReceived,
    saveMessageToFirebase, createNewConversationWithUser, saveImageMessageToFirebase, saveVideoMessageToFirebase, fetchConversationById, detachChatlistListener,
    saveLocationMessageToFirebase
})(ChatListView);

