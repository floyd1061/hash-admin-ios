import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, StyleSheet } from 'react-native'
import Card from './Card'
import CardSection from './CardSection'

const ReplyItem = ({ reply, navigate }) => {
    return (
            <View style={{paddingLeft: 30,marginTop: 5}}>
                <View style={styles.replyContainerStyle}>
                    <View style={styles.thumbnailContainerStyle}>
                        <Image
                            style={styles.thumbnailStyle}
                            source={reply.user.profile_pic}
                        />
                    </View>
                    <View style={styles.headerContentStyle}>
                        <Text style={styles.headerUserTextStyle}>{reply.user.user_name}</Text>
                        <Text style={styles.headerUserCommentStyle}>{reply.comment_text}</Text>
                    </View>
                </View>
                <View style={styles.replyContainerStyle}>
                    <View style={styles.likeCommentButtonContainer}>
                        <TouchableOpacity>
                            <Text style={styles.likeCommentButtonTextStyle}>Like</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
    )
}

const styles = {
    replyContainerStyle: {
        borderBottomWidth: 0,
        paddingTop: 5,
        paddingBottom: 3,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        position: 'relative'
    },
    headerContentStyle: {
        flexDirection: 'column',
        justifyContent: 'center',
        paddingBottom: -3,
    },
    thumbnailStyle: {
        height: 20,
        width: 20,
        borderRadius: 20,
    },
    thumbnailContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    headerTextContentStyle: {
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    likeCommentButtonContainer: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        marginLeft: 40,
        marginRight: 40,
    },
    likeCommentButtonTextStyle: {
        fontSize: 6,
        fontWeight: '500',
        marginRight: 6,
        color: '#4d4f51'
    },
    headerUserTextStyle: {
        fontSize: 8,
        fontWeight: '500',
        marginRight: 6,
        color: '#4d4f51'
    },
    headerUserCommentStyle: {
        fontSize: 8,
        fontWeight: '200',
        marginRight: 6
    },
}

export default ReplyItem;
