import React, { Component } from 'react';
import { View, Image, TouchableOpacity, Text } from 'react-native';
import {Font} from 'expo'

export default class FriendSingleEvent extends Component{

    state = {
        fontLoaded: true,
    };

  
    
    render(){
        return(
            <View style = {{paddingHorizontal: 15, marginBottom: 20, alignSelf: 'stretch'}}>
                <View style = {styles.topArea}>
                    <TouchableOpacity>
                        <Image source={require('./assets/avatar.png')} style={{width: 50, height: 50}} />
                    </TouchableOpacity>
                    <View style = {{marginLeft: 10}}>
                        <View style = {{flexDirection: 'row', alignItems: 'flex-end'}}>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {{fontSize: 20, marginRight: 5, fontFamily: 'coolvetica', color: 'red'}}>Sarah Parker</Text> ) : null
                                }
                            </TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {{marginBottom: 2, fontSize: 12, fontFamily: 'coolvetica'}}>Shared With</Text> ) : null
                                }
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {{color: '#FF0408', marginLeft: 5, fontSize: 12,  marginBottom: 2, fontFamily: 'coolvetica'}}>Jhon Smith</Text> ) : null
                                }
                            </TouchableOpacity>
                        </View>
                        <View style = {{flexDirection: 'row'}}>
                            {
                                this.state.fontLoaded ? ( <Text style = {{fontSize: 13, fontFamily: 'coolvetica'}}>The One at Amanda's Party</Text> ) : null
                            }
                            {
                                this.state.fontLoaded ? ( <Text style = {{color: '#FF0408', fontSize: 13, marginLeft: 10, fontFamily: 'coolvetica'}}>01 January at 22:18</Text> ) : null
                            }
                        </View>
                    </View>
                </View>

                <View style = {{flexDirection: 'row', marginTop: 20}}>
                    <Image source={require('./assets/image.png')} style={{flex: 2, height: 130, marginRight: 10}} />
                    <Image source={require('./assets/image.png')} style={{flex: 2, height: 130}} />
                    <View style = {{marginLeft: 10, backgroundColor: '#DDDDDD', flex: 1}}>
                        <Image source={require('./assets/image.png')} style={{flex: 1}} />
                        <Image source={require('./assets/image.png')} style={{flex: 1, marginTop: 10}} />
                    </View>
                </View>
                <View style = {{flexDirection: 'row', height: 30, alignItems: 'flex-end', alignSelf: 'stretch', justifyContent: 'space-between'}}>
                    <View>
                        {
                            this.state.fontLoaded ? ( <Text style = {{fontSize: 12, color: '#FF0408',  fontFamily: 'coolvetica'}}>Night At Amanda's party</Text> ) : null
                        }
                    </View>
                    <View style = {{flexDirection: 'row', alignItems: 'flex-end' }}>
                        <Image source={require('./assets/Location.png')} style={{width: 15, height: 23, resizeMode: 'stretch', marginRight: 3}} />
                        {
                            this.state.fontLoaded ? ( <Text style = {{fontSize: 12, color: '#FF0408',  fontFamily: 'coolvetica'}}>Elha Nightclub</Text> ) : null
                        }                                                       
                    </View>
                    <View style = {{flexDirection: 'row'}}>
                        <TouchableOpacity>
                            {
                                this.state.fontLoaded ? ( <Text style = {{fontSize: 12, marginRight: 5,  fontFamily: 'coolvetica'}}>1 Comments</Text> ) : null
                            }
                        </TouchableOpacity>
                        <TouchableOpacity>
                            {
                                this.state.fontLoaded ? ( <Text style = {{fontSize: 12,  fontFamily: 'coolvetica'}}>54 Likes</Text> ) : null
                            }
                        </TouchableOpacity> 
                    </View>                     
                </View>

                <View style = {{marginTop: 12}}>
                {
                    this.state.fontLoaded ? ( <Text style = {{fontFamily: 'coolvetica', textAlign: 'justify'}}>Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been 
                    the industry's standard dummy text ever since the 1500s ... </Text> ) : null
                }
                </View>
                <View style = {{marginTop: 12, flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('./assets/eye.png')} style={{width: 30, height: 20,resizeMode: 'stretch',marginRight: 3}} />
                    <TouchableOpacity>
                        {
                            this.state.fontLoaded ? ( <Text style = {{fontSize: 15, marginLeft: 5, color: '#808B96',  fontFamily: 'coolvetica'}}>View More</Text> ) : null
                        }
                    </TouchableOpacity>              
                </View>
            </View>
        )
    }
}

const styles = {
    
    topArea: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'stretch'
    }
}