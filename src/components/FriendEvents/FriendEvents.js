import React, { Component } from 'react';
import { View, TextInput, Image, TouchableOpacity, Text } from 'react-native';
import {Font} from 'expo'
import FriendSingleEvent from './FriendSingleEvent'

export default class FriendEvents extends Component{

    constructor(props) {
        super(props);
        this.state = { showHeaderOption: false, fontLoaded: true };
    }

  

    render(){
        return(
            <View style = {styles.container}>
                <View style = {styles.headerContainer}>
                    <View style = {styles.searchBox}>

                        <View style = {{flexDirection: 'row'}}>
                            <TouchableOpacity>
                                <Image source={require('./assets/explore.png')} style={styles.exploreIcon}/>
                            </TouchableOpacity>

                            {
                                this.state.fontLoaded ? ( <TextInput
                                    style={{ flex: .8, flexDirection:'row', paddingLeft: 10, paddingRight: 10,  fontSize: 13, fontFamily: 'coolvetica' }}
                                    underlineColorAndroid="transparent"
                                    placeholder="Explor Most View Events"
                                /> ) : null
                            }
                        </View>

                        <View style = {{flexDirection: 'row', justifyContent: 'space-between', right: 35}}>
                            <TouchableOpacity>
                                <Image source={require('./assets/mic.png')} style={styles.micIcon}/>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => {this.setState({showHeaderOption: !this.state.showHeaderOption})}}>
                                <Image source={require('./assets/dropdown.png')} style={styles.dropdownIcon}/>
                            </TouchableOpacity>
                        </View>

                    </View>
                </View>

                <FriendSingleEvent/>
                
                {this.state.showHeaderOption ?
                    <View style = {styles.headerOptionArea}>
                        <View  style = {{position: 'relative', flex: 1}}>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}>Invite friend to chat group</Text> ) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}>View contact</Text> ) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity disabled = {true}>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}>Call frined</Text> ) : null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity>
                                {
                                    this.state.fontLoaded ? ( <Text style = {styles.optionText}>Delete chat</Text> ) : null
                                }
                            </TouchableOpacity>
                        </View>                            
                    </View> : null
                }
            </View>
        )
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#DDDDDD',
    },
    headerContainer: {
        backgroundColor: '#FF1303',
        height: 150,
        marginBottom: 20
    },

    searchBox: {
        flexDirection:'row',
        backgroundColor: '#fff',
        height:60, 
        alignSelf: 'stretch',
        alignItems: 'center',
        paddingLeft: 15,
        marginTop:80,           
    },

    exploreIcon: {
        height: 40,
        width:40, 
        resizeMode : 'stretch',
    },

    micIcon: {
        height: 40,
        width:35, 
        resizeMode : 'stretch',
        marginRight: 10,
    },

    dropdownIcon: {
        height: 35,
        width:35, 
        resizeMode : 'stretch',
    },

    headerOptionArea: {
        width:200, 
        height: 150, 
        backgroundColor: '#fff', 
        position: 'absolute',
        right: 22,
        top: 140,
        borderColor: '#FD0607',
        borderWidth: 2,
        borderRadius: 3,
        borderTopWidth: 0
    },

    optionText: {
        paddingVertical: 8,
        paddingHorizontal: 8,
        color:'#000',
        
        fontFamily: 'coolvetica'
    }
}