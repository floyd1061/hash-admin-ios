import React, { Component, PureComponent } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import { Card, CardSection } from './common'
import Svg, {
    Circle,
    Ellipse,
    G,
    LinearGradient,
    RadialGradient,
    Line,
    Path,
    Polygon,
    Polyline,
    Rect,
    Symbol,
    Use,
    Defs,
    Stop
} from 'react-native-svg'
import { evenPoints, oddPoints, oddEvenOddPoints, evenOddEvenPoints } from './points'

export default class ListItem extends React.PureComponent {
    renderFrdColor(item){
        const { colorFrd, colorFrdN, isHighlightedDay } = item;
        if (isHighlightedDay) {
            return colorFrd;
        }
        return colorFrdN;
    }
    renderBcrdColor(item){
        const { colorBcrd, colorBcrdN, isHighlightedDay } = item;
        var colorArr = new Array();
        if (isHighlightedDay) {
            return colorBcrd;
        }
        return colorBcrdN;
    }
    renderEvenPoints(item) {
        return (
            <Svg
                height='50'
                width='100'
            >
                <Polyline
                    points={evenPoints}
                    fill="none"
                    stroke={this.renderBcrdColor(item)}
                    strokeWidth="18"
                />
                <Polyline
                    points={evenPoints}
                    fill="none"
                    stroke={this.renderFrdColor(item)}
                    strokeWidth="14"
                />

            </Svg>
        );
    }
    renderOddPoints(item) {
        return (
            <Svg
                height='50'
                width='100'
            >
                <Polyline
                    points={oddPoints}
                    fill="none"
                    stroke={this.renderBcrdColor(item)}
                    strokeWidth="18"
                />
                <Polyline
                    points={oddPoints}
                    fill="none"
                    stroke={this.renderFrdColor(item)}
                    strokeWidth="14"
                />

            </Svg>
        );
    }
    renderSvg(item) {
        const { name, points } = item;
        if (points == 1) {
            return (
                <View>
                    {this.renderOddPoints(item)}
                </View>);
        } else if (points == 2) {
            return (
                <View>
                    {this.renderEvenPoints(item)}
                </View>);
        }
        else if (points == 121) {
            return (
                <View>
                    {this.renderOddPoints(item)}
                    {this.renderEvenPoints(item)}
                    {this.renderOddPoints(item)}
                </View>
            );
        }
        else if (points == 212) {
            return (
                <View>
                    {this.renderEvenPoints(item)}
                    {this.renderOddPoints(item)}
                    {this.renderEvenPoints(item)}
                </View>
            );
        }
    }
    render() {
        return (
            <TouchableOpacity
            >
                <View>
                    <CardSection style={styles.profileCardStyle}>
                        {this.renderSvg(this.props.item)}
                        {/* <Text style={{ color: 'black' }}>{name}</Text> */}
                    </CardSection>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = {
    profileCardStyle: {
        borderBottomWidth: 0,
        padding: 0,
        justifyContent: 'center',
    }
}