import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'

export default class Header extends Component {
  render(props) {
    return (
      <View style={styles.container}>
        <Text style={{fontSize: this.props.headerFontSize, color: this.props.headerTextColor}}> {this.props.headerText} </Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
        height: 60,
        paddingTop: 20,
        backgroundColor: '#f8f8f8',
        justifyContent: 'center',
        alignItems: 'center',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'
    },
    fontStyling: {
        fontSize: 20,
        //color: this.props.headerColor,
    }
});