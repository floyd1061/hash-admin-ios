import React from 'react'
import { Text, View } from 'react-native'

const CardSection = (props) => {
    return (
        <View style={[styles.containerStyle, this.props.styles]}>
            {props.children}
        </View>
    );
};

const styles = {
    containerStyle: {
        padding: 0,
        backgroundColor: '#fff',
        justifyContent: 'center',
        flexDirection: 'row',
        position: 'relative',
    },
};

export default CardSection;