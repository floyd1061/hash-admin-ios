import React, { Component } from 'react'
import {
    LinearGradient,
    Defs,
    Stop
} from 'react-native-svg'


export const d1 = "M109.2,0c0.1,7.3-1.8,14.4-5.6,20.6c-4,6.5-10.4,12.6-19.9,18.7S67.9,51.4,64,57.5c-3.7,5.6-5.6,12.2-5.5,19c0,6.6,3.4,13.1,9.6,18.9s15.2,10.9,26.3,14.9c14.9,5.4,26.1,10.1,33.7,15.3l0,0c7.7,4.8,11.3,11,11.3,18.4s-4.6,13.6-12,19.4s-17.5,11-28.5,16.5c-8.1,4-15.8,8.9-21.5,14.4S68,206,68,212.8s3.4,12.8,8.3,18.3s11.2,10.5,17,15c8,6.3,14.3,11.6,18.6,16.6s6.6,10.6,6.6,16.3";
export const d2 = "M118.6-0.6c0,6.8-4.2,15.9-10.9,21.5S92,31,82.3,35.6c-11.9,5.7-21.2,11.2-27.4,17s-9.6,11.9-9.6,18.8c0,10.6,5,18.6,12.9,25.1S76.7,108.2,88,113c10.6,4.5,18.8,8.5,24.5,13.3s8.7,10.3,8.7,17.9c0,7-2.2,13.8-6.2,19.5c-3.9,5.6-9.3,10.4-15.5,15c-8.7,6.4-15.4,11.6-19.8,17s-6.8,10.8-6.8,17.7s4.9,13.2,12.6,19.2s18.2,11.5,29.3,16.8c12.9,6.1,22.2,10.6,28.2,15.4s8.8,9.8,8.8,16.8";
export const d3 = "M151.6,0c0,7-3.2,13.5-9.7,19.8s-16.4,12.5-29.7,18.8c-15,7.1-24.9,12.8-31.2,18.5s-9.1,11.1-8.8,18.4l0,0c0,7,3.5,13.6,8.8,19.5s12.7,10.9,20.4,15c9.5,5,15.8,8.9,19.8,13.6c4,4.7,5.6,10.4,5.6,18.8s-2.1,15.5-7.8,21.7c-5.7,6.2-15,11.5-29.5,16.1c-9.3,3-18,6.9-24.3,11.9l0,0c-4.3,3.9-5.9,7.9-6.3,9.7c-0.4,1.5-0.9,3.8-0.9,5.8c0,0.4,0,0.8,0,1.2l0,0c0,5.8,2.8,11.4,6.8,16.5c1.3,1.6,2.8,3.1,4.4,4.4l4.7,3.8l0,0c0,0,3,1.9,3.8,2.4c6.1,3.6,13.9,7.9,20.4,13.1c7,5.5,11.1,11.9,11.1,19.3";


export const oneEvOne = 535;
export const oneEvTwo = 420;
export const oneEvThree = 315;

export const twoEvOne = 435;
export const twoEvTwo = 325;
export const twoEvThree = 235;

export const threeEvOne = 275;
export const threeEvTwo = 175;
export const threeEvThree = 80;

export const svgOneHeight = 278.1393;
export const svgTwoHeight = 281.4884;
export const svgThreeHeight = 268.2128;

export const lgDefTwo = (
    <Defs>
        <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c0" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#eaeaea" />
            <Stop offset="1" stopColor="#eaeaea" />
        </LinearGradient>
        <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#cecfd0" />
            <Stop offset="1" stopColor="#e4e4e4" />

        </LinearGradient>
        <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c01" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4C23" />
            <Stop offset="1" stopColor="#EF4923" />

        </LinearGradient>
        <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc1" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#E21F26" />
            <Stop offset="1" stopColor="#EF4323" />
        </LinearGradient>

    </Defs>
);
export const lgDefTwoOne = (
    <Defs>
        <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c0" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#EF4C23" />
            <Stop offset="25%" stopColor="#EF4923" />
            <Stop offset="26%" stopColor="#eaeaea" />
            <Stop offset="100%" stopColor="#eaeaea" />
        </LinearGradient>
        <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#E21F26" />
            <Stop offset="25%" stopColor="#EF4323" />
            <Stop offset="26%" stopColor="#cecfd0" />
            <Stop offset="100%" stopColor="#e4e4e4" />

        </LinearGradient>
        <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c01" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4C23" />
            <Stop offset="1" stopColor="#EF4923" />

        </LinearGradient>
        <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc1" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#E21F26" />
            <Stop offset="1" stopColor="#EF4323" />
        </LinearGradient>

    </Defs>
);
export const lgDefTwoTwo = (
    <Defs>
        <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c0" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#EF4C23" />
            <Stop offset="50%" stopColor="#EF4923" />
            <Stop offset="51%" stopColor="#eaeaea" />
            <Stop offset="100%" stopColor="#eaeaea" />
        </LinearGradient>
        <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#E21F26" />
            <Stop offset="50%" stopColor="#EF4323" />
            <Stop offset="51%" stopColor="#cecfd0" />
            <Stop offset="100%" stopColor="#e4e4e4" />

        </LinearGradient>
        <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c01" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4C23" />
            <Stop offset="1" stopColor="#EF4923" />

        </LinearGradient>
        <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc1" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#E21F26" />
            <Stop offset="1" stopColor="#EF4323" />
        </LinearGradient>

    </Defs>
);
export const lgDefTwoThree = (
    <Defs>
        <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c0" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4C23" />
            <Stop offset="1" stopColor="#EF4923" />
        </LinearGradient>
        <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#E21F26" />
            <Stop offset="1" stopColor="#EF4323" />

        </LinearGradient>
        <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c01" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4C23" />
            <Stop offset="1" stopColor="#EF4923" />

        </LinearGradient>
        <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc1" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#E21F26" />
            <Stop offset="1" stopColor="#EF4323" />
        </LinearGradient>

    </Defs>
);

export const lgDefThree = (
    <Defs>
        <LinearGradient id="f5bfdeee-16ad-4168-9edd-48c395e44d65" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#eaeaea" />
            <Stop offset="1" stopColor="#cecfd0" />

        </LinearGradient>
        <LinearGradient id="686a64e9-a46d-4086-98cb-9ddc3a5017df" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#e4e4e4" />
            <Stop offset="1" stopColor="#eaeaea" />

        </LinearGradient>

        <LinearGradient id="f5bfdeee-16ad-4168-9edd-48c395e44d66" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4723" />
            <Stop offset="1" stopColor="#EF4923" />
        </LinearGradient>
        <LinearGradient id="686a64e9-a46d-4086-98cb-9ddc3a5017dg" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#F04023" />
            <Stop offset="1" stopColor="#F04E23" />
        </LinearGradient>

    </Defs>
);

export const lgDefThreeOne = (
    <Defs>
        <LinearGradient id="f5bfdeee-16ad-4168-9edd-48c395e44d65" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">

            <Stop offset="0%" stopColor="#EF4723" />
            <Stop offset="25%" stopColor="#EF4923" />
            <Stop offset="26%" stopColor="#eaeaea" />
            <Stop offset="100%" stopColor="#cecfd0" />

        </LinearGradient>
        <LinearGradient id="686a64e9-a46d-4086-98cb-9ddc3a5017df" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#F04023" />
            <Stop offset="25%" stopColor="#F04E23" />
            <Stop offset="26%" stopColor="#e4e4e4" />
            <Stop offset="100%" stopColor="#eaeaea" />

        </LinearGradient>

        <LinearGradient id="f5bfdeee-16ad-4168-9edd-48c395e44d66" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4723" />
            <Stop offset="1" stopColor="#EF4923" />
        </LinearGradient>
        <LinearGradient id="686a64e9-a46d-4086-98cb-9ddc3a5017dg" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#F04023" />
            <Stop offset="1" stopColor="#F04E23" />
        </LinearGradient>

    </Defs>
);
export const lgDefThreeTwo = (
    <Defs>
        <LinearGradient id="f5bfdeee-16ad-4168-9edd-48c395e44d65" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">

            <Stop offset="0%" stopColor="#EF4723" />
            <Stop offset="50%" stopColor="#EF4923" />
            <Stop offset="51%" stopColor="#eaeaea" />
            <Stop offset="100%" stopColor="#cecfd0" />

        </LinearGradient>
        <LinearGradient id="686a64e9-a46d-4086-98cb-9ddc3a5017df" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#F04023" />
            <Stop offset="50%" stopColor="#F04E23" />
            <Stop offset="51%" stopColor="#e4e4e4" />
            <Stop offset="100%" stopColor="#eaeaea" />
        </LinearGradient>

        <LinearGradient id="f5bfdeee-16ad-4168-9edd-48c395e44d66" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4723" />
            <Stop offset="1" stopColor="#EF4923" />
        </LinearGradient>
        <LinearGradient id="686a64e9-a46d-4086-98cb-9ddc3a5017dg" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#F04023" />
            <Stop offset="1" stopColor="#F04E23" />
        </LinearGradient>

    </Defs>
);
export const lgDefThreeThree = (
    <Defs>
        <LinearGradient id="f5bfdeee-16ad-4168-9edd-48c395e44d65" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4723" />
            <Stop offset="1" stopColor="#EF4923" />

        </LinearGradient>
        <LinearGradient id="686a64e9-a46d-4086-98cb-9ddc3a5017df" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#F04023" />
            <Stop offset="1" stopColor="#F04E23" />

        </LinearGradient>

        <LinearGradient id="f5bfdeee-16ad-4168-9edd-48c395e44d66" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4723" />
            <Stop offset="1" stopColor="#EF4923" />
        </LinearGradient>
        <LinearGradient id="686a64e9-a46d-4086-98cb-9ddc3a5017dg" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#F04023" />
            <Stop offset="1" stopColor="#F04E23" />
        </LinearGradient>

    </Defs>
);
export const lgDefFour = (
    <Defs>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b87" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">

            <Stop offset="0" stopColor="#ccc" />
            <Stop offset="1" stopColor="#ccc" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b88" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#eaeaea" />
            <Stop offset="1" stopColor="#eaeaea" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b89" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#ED4323" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b90" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#F04E23" />
            <Stop offset="1" stopColor="#EF4923" />
        </LinearGradient>
    </Defs>
);
export const lgDefFourOne = (
    <Defs>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b87" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">

            <Stop offset="0%" stopColor="#EF4923" />
            <Stop offset="25%" stopColor="#ED4323" />
            <Stop offset="26%" stopColor="#ccc" />
            <Stop offset="100%" stopColor="#ccc" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b88" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#F04E23" />
            <Stop offset="25%" stopColor="#EF4923" />
            <Stop offset="26%" stopColor="#eaeaea" />
            <Stop offset="100%" stopColor="#eaeaea" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b89" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#ED4323" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b90" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#F04E23" />
            <Stop offset="1" stopColor="#EF4923" />
        </LinearGradient>
    </Defs>
);
export const lgDefFourTwo = (
    <Defs>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b87" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">

            <Stop offset="0%" stopColor="#EF4923" />
            <Stop offset="50%" stopColor="#ED4323" />
            <Stop offset="51%" stopColor="#ccc" />
            <Stop offset="100%" stopColor="#ccc" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b88" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#F04E23" />
            <Stop offset="50%" stopColor="#EF4923" />
            <Stop offset="51%" stopColor="#eaeaea" />
            <Stop offset="100%" stopColor="#eaeaea" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b89" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#ED4323" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b90" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#F04E23" />
            <Stop offset="1" stopColor="#EF4923" />
        </LinearGradient>
    </Defs>
);
export const lgDefFourThree = (
    <Defs>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b87" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#ED4323" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b88" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#F04E23" />
            <Stop offset="1" stopColor="#EF4923" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b89" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#ED4323" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b90" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#F04E23" />
            <Stop offset="1" stopColor="#EF4923" />
        </LinearGradient>
    </Defs>
);
export const lgDefTwoR = (
    <Defs>
        <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c0" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#ccc" />
            <Stop offset="1" stopColor="#ccc" />
        </LinearGradient>
        <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#eaeaea" />
            <Stop offset="1" stopColor="#eaeaea" />

        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b89" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#ED4323" />
            <Stop offset="1" stopColor="#E32626" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b90" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#EF4723" />
        </LinearGradient>

    </Defs>
);
export const lgDefTwoROne = (
    <Defs>
        <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c0" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#ED4323" />
            <Stop offset="25%" stopColor="#E32626" />
            <Stop offset="26%" stopColor="#ccc" />
            <Stop offset="100%" stopColor="#ccc" />
        </LinearGradient>
        <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#EF4923" />
            <Stop offset="25%" stopColor="#EF4723" />
            <Stop offset="26%" stopColor="#eaeaea" />
            <Stop offset="100%" stopColor="#eaeaea" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b89" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#ED4323" />
            <Stop offset="1" stopColor="#E32626" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b90" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#EF4723" />
        </LinearGradient>

    </Defs>
);
export const lgDefTwoRTwo = (
    <Defs>
        <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c0" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#ED4323" />
            <Stop offset="50%" stopColor="#E32626" />
            <Stop offset="51%" stopColor="#ccc" />
            <Stop offset="100%" stopColor="#ccc" />
        </LinearGradient>
        <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#EF4923" />
            <Stop offset="50%" stopColor="#EF4723" />
            <Stop offset="51%" stopColor="#eaeaea" />
            <Stop offset="100%" stopColor="#eaeaea" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b89" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#ED4323" />
            <Stop offset="1" stopColor="#E32626" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b90" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#EF4723" />
        </LinearGradient>

    </Defs>
);
export const lgDefTwoRThree = (
    <Defs>
        <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c0" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#ED4323" />
            <Stop offset="1" stopColor="#E32626" />
        </LinearGradient>
        <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#EF4723" />

        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b89" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#ED4323" />
            <Stop offset="1" stopColor="#E32626" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b90" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#EF4723" />
        </LinearGradient>

    </Defs>
);
export const lgDefThreeR = (
    <Defs>
        <LinearGradient id="f5bfdeee-16ad-4168-9edd-48c395e44d65" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#cecfd0" />
            <Stop offset="1" stopColor="#eaeaea" />
        </LinearGradient>
        <LinearGradient id="686a64e9-a46d-4086-98cb-9ddc3a5017df" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#eaeaea" />
            <Stop offset="1" stopColor="#e4e4e4" />

        </LinearGradient>
        <LinearGradient id="f5bfdeee-16ad-4168-9edd-48c395e44d66" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#E32626" />
            <Stop offset="1" stopColor="#EF4923" />
        </LinearGradient>
        <LinearGradient id="686a64e9-a46d-4086-98cb-9ddc3a5017dg" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#F04E23" />
        </LinearGradient>
    </Defs>
);
export const lgDefThreeROne = (
    <Defs>
        <LinearGradient id="f5bfdeee-16ad-4168-9edd-48c395e44d65" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#E32626" />
            <Stop offset="25%" stopColor="#EF4923" />
            <Stop offset="26%" stopColor="#cecfd0" />
            <Stop offset="100%" stopColor="#eaeaea" />
           
        </LinearGradient>
        <LinearGradient id="686a64e9-a46d-4086-98cb-9ddc3a5017df" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#EF4923" />
            <Stop offset="25%" stopColor="#F04E23" />
            <Stop offset="26%" stopColor="#eaeaea" />
            <Stop offset="100%" stopColor="#e4e4e4" />

        </LinearGradient>
        <LinearGradient id="f5bfdeee-16ad-4168-9edd-48c395e44d66" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#E32626" />
            <Stop offset="1" stopColor="#EF4923" />
        </LinearGradient>
        <LinearGradient id="686a64e9-a46d-4086-98cb-9ddc3a5017dg" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#F04E23" />
        </LinearGradient>
    </Defs>
);
export const lgDefThreeRTwo = (
    <Defs>
        <LinearGradient id="f5bfdeee-16ad-4168-9edd-48c395e44d65" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#E32626" />
            <Stop offset="50%" stopColor="#EF4923" />
            <Stop offset="51%" stopColor="#cecfd0" />
            <Stop offset="100%" stopColor="#eaeaea" />
           
        </LinearGradient>
        <LinearGradient id="686a64e9-a46d-4086-98cb-9ddc3a5017df" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#EF4923" />
            <Stop offset="50%" stopColor="#F04E23" />
            <Stop offset="51%" stopColor="#eaeaea" />
            <Stop offset="100%" stopColor="#e4e4e4" />

        </LinearGradient>
        <LinearGradient id="f5bfdeee-16ad-4168-9edd-48c395e44d66" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#E32626" />
            <Stop offset="1" stopColor="#EF4923" />
        </LinearGradient>
        <LinearGradient id="686a64e9-a46d-4086-98cb-9ddc3a5017dg" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#F04E23" />
        </LinearGradient>
    </Defs>
);
export const lgDefThreeRThree = (
    <Defs>
        <LinearGradient id="f5bfdeee-16ad-4168-9edd-48c395e44d65" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#E32626" />
            <Stop offset="1" stopColor="#EF4923" />
        </LinearGradient>
        <LinearGradient id="686a64e9-a46d-4086-98cb-9ddc3a5017df" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#F04E23" />

        </LinearGradient>
        <LinearGradient id="f5bfdeee-16ad-4168-9edd-48c395e44d66" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#E32626" />
            <Stop offset="1" stopColor="#EF4923" />
        </LinearGradient>
        <LinearGradient id="686a64e9-a46d-4086-98cb-9ddc3a5017dg" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#F04E23" />
        </LinearGradient>
    </Defs>
);
export const lgDefFourR = (
    <Defs>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b87" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#eaeaea" />
            <Stop offset="1" stopColor="#eaeaea" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b88" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#e4e4e4" />
            <Stop offset="1" stopColor="#cecfd0" />

        </LinearGradient>
        <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c01" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#EF4C23" />
        </LinearGradient>
        <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc1" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#F04E23" />
            <Stop offset="1" stopColor="#E21F26" />
        </LinearGradient>
    </Defs>
);
export const lgDefFourROne = (
    <Defs>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b87" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#EF4923" />
            <Stop offset="25%" stopColor="#EF4C23" />
            <Stop offset="26%" stopColor="#eaeaea" />
            <Stop offset="100%" stopColor="#eaeaea" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b88" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#F04E23" />
            <Stop offset="25%" stopColor="#E21F26" />
            <Stop offset="26%" stopColor="#e4e4e4" />
            <Stop offset="100%" stopColor="#cecfd0" />

        </LinearGradient>
        <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c01" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#EF4C23" />
        </LinearGradient>
        <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc1" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#F04E23" />
            <Stop offset="1" stopColor="#E21F26" />
        </LinearGradient>
    </Defs>
);

export const lgDefFourRTwo = (
    <Defs>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b87" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#EF4923" />
            <Stop offset="50%" stopColor="#EF4C23" />
            <Stop offset="51%" stopColor="#eaeaea" />
            <Stop offset="100%" stopColor="#eaeaea" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b88" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#F04E23" />
            <Stop offset="50%" stopColor="#E21F26" />
            <Stop offset="51%" stopColor="#e4e4e4" />
            <Stop offset="100%" stopColor="#cecfd0" />

        </LinearGradient>
        <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c01" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#EF4C23" />
        </LinearGradient>
        <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc1" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#F04E23" />
            <Stop offset="1" stopColor="#E21F26" />
        </LinearGradient>
    </Defs>
);

export const lgDefFourRThree = (
    <Defs>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b87" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#EF4C23" />
        </LinearGradient>
        <LinearGradient id="551a812d-0e06-4ac7-abb0-e4d828b82b88" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#F04E23" />
            <Stop offset="1" stopColor="#E21F26" />

        </LinearGradient>
        <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c01" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#EF4C23" />
        </LinearGradient>
        <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc1" x2="0%" y2="100%" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#F04E23" />
            <Stop offset="1" stopColor="#E21F26" />
        </LinearGradient>
    </Defs>
);

