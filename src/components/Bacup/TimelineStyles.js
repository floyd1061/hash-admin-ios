import { StyleSheet,Dimensions } from "react-native";


const {width} = Dimensions.get("window");

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'row',
        position: 'relative',
    },
    svgBox: {
        flex: 1,
        justifyContent: 'center',
        position: 'relative',
    },
    lheBox: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end',
        backgroundColor: 'red'
    },
    rheBox: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    rheBoxEvName: {
        height: 50,
        justifyContent: 'center'
    },
    rheBoxEvBox: {
        flex: 1,
        backgroundColor: 'red'
    },
    eventNameBox: {
        borderColor: 'green',
        borderWidth: 0,
        zIndex: 10,
        
        top: 5,
        right: 50,
        position: 'absolute',
        alignItems: 'flex-start',
        ///backgroundColor: 'red'

    },
    evNameSegOne: {
        right: 90
    },
    evNameSegTwo: {
        right: 80
    },
    evNameSegThree: {
        right: 55
    },
    eventOne: {
        borderColor: 'red',
        paddingLeft: 80,
        alignSelf: 'stretch',
        zIndex: 20,
        // height: 140,
        left: 0,
        top: -19,
        width: 200,
        position: 'absolute',
        alignItems: 'flex-start',
        justifyContent: 'center',
        marginBottom: 1,
        paddingVertical: 10,
        // backgroundColor: 'red'

    },
    eventOneSegOne: {
        borderColor: 'red',
        paddingLeft: 80,
        alignSelf: 'stretch',
        zIndex: 20,
        // height: 140,
        left: 0,
        top: 0,
        width: 200,
        position: 'absolute',
        alignItems: 'flex-start',
        justifyContent: 'center',
        marginBottom: 1,
        paddingVertical: 10,
        // backgroundColor: 'red'

    },
    eventOneSegTwo: {
        borderColor: 'red',
        paddingLeft: 80,
        alignSelf: 'stretch',
        zIndex: 20,
        // height: 140,
        left: -15,
        top: 0,
        width: 200,
        position: 'absolute',
        alignItems: 'flex-start',
        justifyContent: 'center',
        marginBottom: 1,
        paddingVertical: 10,
        // backgroundColor: 'red'

    },
    eventOneSegThree: {
        borderColor: 'red',
        paddingLeft: 80,
        alignSelf: 'stretch',
        zIndex: 20,
        // height: 140,
        left: 10,
        top: 0,
        width: 200,
        position: 'absolute',
        alignItems: 'flex-start',
        justifyContent: 'center',
        marginBottom: 1,
        paddingVertical: 10,
        // backgroundColor: 'red'

    },
    eventTwo: {
        borderColor: 'gray',
        paddingLeft: 20,
        alignSelf: 'stretch',
        // height: 140,
        zIndex: 40,
        width: 190,
        top: 70,
        right: 0,
        position: 'absolute',
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingVertical: 10,
        // backgroundColor: 'green'

    },
    eventTwoSegOne: {
        borderColor: 'gray',
        paddingLeft: 20,
        alignSelf: 'stretch',
        // height: 140,
        zIndex: 40,
        width: 190,
        top: 90,
        right: -13,
        position: 'absolute',
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingVertical: 10,
        // backgroundColor: 'green'

    },
    eventTwoSegTwo: {
        borderColor: 'gray',
        paddingLeft: 20,
        alignSelf: 'stretch',
        // height: 140,
        zIndex: 40,
        width: 190,
        top: 90,
        right: 10,
        position: 'absolute',
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingVertical: 10,
        // backgroundColor: 'green'

    },
    eventTwoSegThree: {
        borderColor: 'gray',
        paddingLeft: 20,
        alignSelf: 'stretch',
        // height: 140,
        zIndex: 40,
        width: 190,
        top: 90,
        right: 3,
        position: 'absolute',
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingVertical: 10,
        // backgroundColor: 'green'

    },

    eventThree: {
        borderColor: 'blue',
        alignSelf: 'stretch',
        // height: 140,
        paddingLeft: 80,
        zIndex: 20,
        left: 0,
        top: 120,
        width: 200,
        position: 'absolute',
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingVertical: 10,
        // backgroundColor: 'pink'

    },
    eventThreeSegOne: {
        borderColor: 'blue',
        alignSelf: 'stretch',
        // height: 140,
        paddingLeft: 80,
        zIndex: 20,
        left: 10,
        top: 140,
        width: 200,
        position: 'absolute',
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingVertical: 10,
        // backgroundColor: 'pink'

    },
    eventThreeSegTwo: {
        borderColor: 'blue',
        alignSelf: 'stretch',
        // height: 140,
        paddingLeft: 80,
        zIndex: 20,
        left: 15,
        top: 140,
        width: 200,
        position: 'absolute',
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingVertical: 10,
        // backgroundColor: 'pink'

    },
    eventThreeSegThree: {
        borderColor: 'blue',
        alignSelf: 'stretch',
        // height: 140,
        paddingLeft: 80,
        zIndex: 20,
        left: -5,
        top: 140,
        width: 200,
        position: 'absolute',
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingVertical: 10,
        // backgroundColor: 'pink'

    },

    profileCardStyle: {
        borderBottomWidth: 0,
        padding: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 15,
    },
    eventCreate: {
        alignSelf: 'stretch',
        height: 150,
        width: width-100,
        borderWidth: 1,
        borderColor: '#B7B7B7',
        borderRadius: 5
    },

    headerTitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 35,
        alignSelf: 'stretch',
        paddingHorizontal: 5,
        borderRadius: 3
    },

    headerTextTitle: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#fff',
        fontFamily: 'coolvetica'
    },

    timeSelectorStart: {
        height: 25,
        width: 70,
        backgroundColor: '#fff',
        borderRadius: 2,
    },

    timeSelectorStartText: {
        fontSize: 9,
        fontWeight: 'bold',
        color: '#FF1105',
        textAlign: 'center',
        marginTop: 6,
        fontFamily: 'coolvetica'
    },

    timeSelectorEnd: {
        height: 25,
        width: 70,
        backgroundColor: '#fff',
        borderRadius: 2,
        marginLeft: 5
    },

    timeSelectorEndText: {
        fontSize: 9,
        fontWeight: 'bold',
        color: '#FF1105',
        textAlign: 'center',
        marginTop: 6,
        fontFamily: 'coolvetica'
    },

    eventBody: {
        flexDirection: 'row',
        alignSelf: 'stretch',
        marginHorizontal: 10,
        marginBottom: 2,
        justifyContent: 'space-between'
    },

    eventBodyText: {
        fontSize: 10,
        fontWeight: 'bold',
        color: '#544D48',
        marginTop: 18,
        fontFamily: 'coolvetica'
    },

    textInputStyle: {
        alignSelf: 'stretch'
    },
    bottomPanelContainer: {
        flexDirection: 'row',

        paddingVertical: 5
    },
    errorTextStyle: {
        fontSize: 8,
        color: '#F30F00',
        fontFamily: 'coolvetica'
    },
    errorStyle: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        paddingVertical: 5,
        paddingLeft: 10
    },
    buttonStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        paddingVertical: 5
    },

    button1Style: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#F30F00',
        marginRight: 5,
        fontFamily: 'coolvetica'
    },

    button2Style: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#F30F00',
        marginRight: 8,
        fontFamily: 'coolvetica'
    },

    animatedStyle: {
        transform: [{ scale: 1 }]
    }
});

export default styles;

