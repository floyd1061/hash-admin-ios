import React, { Component } from 'react'
import { View, Text, TextInput, Image, TouchableOpacity, StyleSheet } from 'react-native'
import {LinearGradient} from 'expo'
import Modal from "react-native-modal";


export default class ShareEvent extends Component {

    state = {
        isModalVisible: false,
    };
     
    _toggleModal = () =>
        this.setState({ isModalVisible: !this.state.isModalVisible });
     
    render() {
        return (
          <View style={{ flex: 1, paddingTop: 100 }}>
            <TouchableOpacity onPress={this._toggleModal}>
              <Text>Show Modal</Text>
            </TouchableOpacity>
            <Modal onSwipe={() => this.setState({ isVisible: false })} swipeDirection="right" swipeThreshold = {10} isVisible={this.state.isModalVisible} style = {{flex:1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 20}}>
              
            <LinearGradient style = {styles.shareEventCreate} colors = { ['#F0F0F0', '#E4E4E4'] }>

                <LinearGradient style = {styles.shareEventheaderTitle} colors = { ['#EC0900', '#FF0D05'] }>
                    <Text style = {styles.shareEventheaderTextTitle}> Shares </Text>
                </LinearGradient>

                <View style = {styles.shareEventBody}>

                    <View style = {styles.shareEventBody1}>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                    </View>

                    <View style = {styles.shareEventBody1}>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                    </View>

                    <View style = {styles.shareEventBody1}>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                    </View>

                </View>

                <View style = {styles.buttonStyle}>
                    <TouchableOpacity onPress={this._toggleModal}>
                        <Text style = {styles.button1Style}> Close </Text>
                    </TouchableOpacity>
                </View>

                </LinearGradient>
            
            
            </Modal>
          </View>
        );
    }
}

const styles = StyleSheet.create({
    shareEventCreate: {
        alignSelf: 'stretch', 
        height: 270,
        borderWidth: 1,
        borderColor: '#B7B7B7',
        borderRadius: 5
    },

    shareEventheaderTitle: {
        alignSelf: 'stretch', 
        flexDirection: 'row',
        height: 40,
        borderRadius: 3
    },

    shareEventheaderTextTitle: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#fff',
        marginLeft: 5,
        marginTop: 10
    },

    shareEventBody: {
        alignSelf: 'stretch', 
        height: 195,
        padding: 5,
        // backgroundColor: '#ccc'
    },

    shareEventBody1: {
        flexDirection: 'row',
        padding: 3,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: '#000'
    },

    shareEventUser: {
        height: 50,
        width: 50,
        // backgroundColor: '#ccc',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 1
    },

    shareEventDayheaderTitle: {
        flexDirection: 'row',
        height: 40,
        width: 268,
        borderRadius: 3
    },

    shareEventDayheaderTextTitle: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#fff',
        marginLeft: 5,
        marginTop: 11
    },

    likearea: {
        position: 'relative',
    },

    likeCount: {
        position: 'absolute',
        top: 8,
        color: '#fff',
        backgroundColor: '#EB1406',
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#fff',
        width: 13,
        height: 13,
        fontSize: 5,
        paddingTop: 3,
        textAlign: 'center',
        left: 18,
    },

    buttonStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        // backgroundColor: '#000',    
    },

    button1Style: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#F30F00',
        paddingVertical: 5,
        marginRight: 5   
    },
});
