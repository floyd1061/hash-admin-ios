import React, { PureComponent } from "react";
import { View, Text, FlatList, ActivityIndicator, TouchableOpacity } from "react-native";
import { List, ListItem, SearchBar } from "react-native-elements";
import _ from 'lodash';
import { fetchAttendee, inviteToEvent } from '../../actions'
import { connect } from 'react-redux'
class UserList extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      //data: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false
    };
  }

  componentDidMount() {
    this.props.fetchAttendee(this.props.eventKey);
    this.makeRemoteRequest();
  }

  makeRemoteRequest = () => {

    this.setState({ loading: true });
    this.setState({
      error: null,
      loading: false,
      refreshing: false
    });
  };

  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        seed: this.state.seed + 1,
        refreshing: true
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  handleLoadMore = () => {
    this.setState(
      {
        page: this.state.page + 1
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 0.4,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    );
  };

  renderHeader = () => {
    return <SearchBar fontFamily='coolvetica' placeholder="Type Here..." lightTheme round />;
  };

  renderFooter = () => {
    if (!this.state.loading) return (
      <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
        <TouchableOpacity style={{ padding: 10, }} onPress={this.props.shareEvent}>
          <Text style={{ color: 'red', fontFamily: 'coolvetica' }}>Cancel</Text>
        </TouchableOpacity>
      </View>
    );

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
  inviteFriends(userData) {
    this.props.inviteToEvent(userData, this.props.userProfileData, this.props.eventKey);
  }
  getProfilePic(profilepictures) {
    let pps = _.map(profilepictures, (val, uid) => {
      return { ...val, uid }
    })
    if (pps.length > 0) {
      return pps[pps.length-1].URL
    }
    return '../assets/avatar.png';
  }
  render() {

    return (
      <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
        <FlatList
          data={this.props.data}
          renderItem={({ item }) => (

            < ListItem
              disabled={(item.invitationStatus == 0) ? true : (item.invitationStatus == 1) ? true : false}
              fontFamily='coolvetica'
              roundAvatar
              rightIcon={(item.invitationStatus == 1) ? { name: 'done-all', color: '#F94101' } :
                (item.invitationStatus == 0) ? { name: 'done', color: '#228B22' } :
                  { name: 'person-add' }}
              onPressRightIcon={() => this.inviteFriends(item.userId)}
              title={`${item.displayName}`}
              subtitle={`${item.email}`}
              avatar={{ uri: this.getProfilePic(item.profilepicture) }}
              containerStyle={{ borderBottomWidth: 0 }}

            />
          )}
          keyExtractor={item => item.userId}
          ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={this.renderHeader}
          ListFooterComponent={this.renderFooter}
          onRefresh={this.handleRefresh}
          refreshing={this.state.refreshing}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={50}
        />

      </List>
    );
  }
}
const mapStateToProps = (state) => {
  const userProfileData = state.editUserProfileInfo.userProfileInfoData;
  let friends = _.map(state.userPreloadData.friends, (val, userId) => {
    return { ...val, userId };
  });
////console.log(friends);
  friends = _.without(friends, undefined);
  friends = _.uniqBy(friends, "userId");

  let attendee = _.map(state.eventData.attendee, (val, uid) => {
    return { ...val, uid };
  });
  ////console.log(attendee);
  let comparedAttendeeFriends = attendee.length > 0 ?
    _.unionBy(attendee, friends, "userId")
    : friends;
  ////console.log(comparedAttendeeFriends);
  return { userProfileData: userProfileData, attendee: attendee, friends: friends, data: comparedAttendeeFriends };
};
export default connect(mapStateToProps, { fetchAttendee, inviteToEvent })(UserList);