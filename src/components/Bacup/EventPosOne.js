import React, { PureComponent } from 'react'
import { Text, View } from 'react-native'

export default class EventPosOne extends PureComponent {
    render() {
        return (
            <View style={styles.eventOne}>
                <Text>{this.props.event.title}</Text>
            </View>
        )
    }
}

const styles = {
    eventOne: {
        borderColor: 'green',
        borderWidth: 2,
        height: 30,
        zIndex: 20,
        right: 20,
        top: 50,
        width: 90,
        position: 'absolute',
        alignItems: 'center'

    },
};