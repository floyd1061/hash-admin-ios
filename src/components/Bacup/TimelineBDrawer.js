import React, { Component } from 'react'
import { Image, TouchableOpacity, StyleSheet, Dimensions } from 'react-native'
import { LinearGradient } from 'expo'
const {width} = Dimensions.get('window');
import { Actions } from 'react-native-router-flux';

export default class TimelineBDrawer extends Component {

    constructor(props) {
        super(props);

    }
    render() {
        return (
            <LinearGradient style={styles.bottomSection} colors={['#2B2B2B', '#272727']}>
                <TouchableOpacity style={styles.bottomItem} onPress={() => Actions.HomeStoryList()}>
                    <Image source={require('./assets/home.png')} style={styles.bottomIcon} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.bottomItem}>
                    <Image source={require('./assets/mostpopulartimeline.png')} style={[styles.bottomIcon, { width: 45 }]} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.bottomItem}>
                    <Image source={require('./assets/mostlikedevent.png')} style={styles.bottomIcon} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.bottomItem}>
                    <Image source={require('./assets/populareventbox.png')} style={[styles.bottomIcon, { width: 45, height: 35 }]} />
                </TouchableOpacity>
            </LinearGradient>
        )
    }
}

const styles = StyleSheet.create({
    bottomSection: {
        flex: 1,
        flexDirection: 'row',
        height: 70,
        width: width,
        alignSelf: 'stretch',
        position: 'absolute',
        bottom: 0
    },

    bottomItem: {
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center',
        borderRightWidth: 1,
        borderColor: '#7E7E7E',
    },

    bottomIcon: {
        width: 40,
        height: 40,
        resizeMode: 'stretch',
    }
});
