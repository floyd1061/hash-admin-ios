import React, { PureComponent } from 'react'
import { Text, View, TouchableOpacity, StyleSheet, Image, FlatList } from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import { fetchTranscript } from '../../actions';
import Modal from "react-native-modal";
import { LinearGradient, Font } from 'expo'
import firebase from '@firebase/app'
import auth from '@firebase/auth'
import uuid from 'uuid/v4';
import UserList from './UserList'
const likeCount = 45;

class TranscriptBoxTopRight extends PureComponent {

    /* componentDidMount() {
        var key = this.props.eventKey;
        this.props.fetchTranscript(this.props.eventKey);
        ////console.log(this.props)
    } */

    state = {
        isInviteModalVisible: false,
        isShareEventDayModalVisible: false,
        isMayBeModalVisible: false,
        isSharesModalVisible: false,
        attendee: null,
        fontLoaded: true
    }

    renderUserList(shareEvent) {
        return <UserList key={uuid()} eventKey={this.props.eventKey} shareEvent={shareEvent} />

    }

    toggleModal1 = () =>
        this.setState({
            isInviteModalVisible: !this.state.isInviteModalVisible
        });

    toggleModal2 = () =>
        this.setState({
            isShareEventDayModalVisible: !this.state.isShareEventDayModalVisible
        });

    toggleModal3 = () =>
        this.setState({
            isMayBeModalVisible: !this.state.isMayBeModalVisible
        });

    toggleModal4 = () =>
        this.setState({
            isSharesModalVisible: !this.state.isSharesModalVisible
        });

    getAttendee = (eventId) => {
        const { currentUser } = firebase.auth();
        firebase.database().ref(`/users/pYhwvwSvZvZaxilFhfMTJRu9iG73/timelinedata/2018/-LGYhnZNF2dz9Ppn0Qpp/events/${eventId}/attendee`)
            .once('value', snapshot => {

                let fetch_attendee = _.map(snapshot.val(), (val, uid) => {
                    return { ...val, uid };
                });
                ////console.log("fetching"+fetch_attendee);
                this.setState({ attendee: fetch_attendee });

            });
    }

    renderInvitedItem({ item }) {
        return (
            <TouchableOpacity style={styles.inviteEventUser}>
                <Image source={require('./assets/avatar.png')} style={{ position: 'relative', width: 55, height: 55, resizeMode: 'stretch' }} />
                <Image source={require('./assets/HostAdd.png')} style={{ position: 'absolute', width: 25, height: 25, resizeMode: 'stretch', top: 5, right: 3 }} />
                {
                    this.state.fontLoaded ? <Text style={{ fontSize: 8, color: '#fff', color: '#9F9F9F', textAlign: 'center', fontFamily: 'coolvetica' }}> {item.name} </Text> : null
                }
            </TouchableOpacity>
        )
    }

    renderSharesEventDayItem({ item }) {
        return (
            <TouchableOpacity style={styles.shareEventUser}>
                <Image source={require('./assets/avatar.png')} style={{ position: 'relative', width: 40, height: 40, resizeMode: 'stretch' }} />
            </TouchableOpacity>
        )
    }

    renderMaybeItem({ item }) {
        return (
            <TouchableOpacity style={styles.maybeEventUser}>
                <Image source={require('./assets/avatar.png')} style={{ position: 'relative', width: 40, height: 40, resizeMode: 'stretch' }} />
            </TouchableOpacity>
        )
    }

    renderSharesItem({ item }) {
        return (
            <TouchableOpacity style={styles.shareEventUser}>
                <Image source={require('./assets/avatar.png')} style={{ position: 'relative', width: 40, height: 40, resizeMode: 'stretch' }} />
            </TouchableOpacity>
        )
    }

    renderInviteModal() {
        return (

            <LinearGradient style={styles.inviteEventCreate} colors={['#F0F0F0', '#E4E4E4']}>

                <LinearGradient style={styles.inviteEventheaderTitle} colors={['#EC0900', '#FF0D05']}>
                    <View>
                        {
                            this.state.fontLoaded ? <Text style={styles.inviteEventheaderTextTitle}> Invite Friends to the Event </Text> : null
                        }
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity>
                            <Image source={require('./assets/fb.png')} style={{ width: 25, height: 25, resizeMode: 'stretch' }} />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Image source={require('./assets/twitter.png')} style={{ width: 25, height: 25, resizeMode: 'stretch', marginLeft: 10 }} />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Image source={require('./assets/instagram.png')} style={{ width: 25, height: 25, resizeMode: 'stretch', marginLeft: 10 }} />
                        </TouchableOpacity>
                    </View>
                </LinearGradient>

                {/*  <UserList eventKey={this.props.eventKey} shareEvent={this.toggleModal1}/> */}
                {this.renderUserList(this.toggleModal1)}
                {/* <View style={styles.buttonStyle}>
                    <TouchableOpacity>
                        {
                            this.state.fontLoaded ? <Text style={styles.button1Style}> Send Invitation </Text> : null
                        }
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.toggleModal1}>
                        {
                            this.state.fontLoaded ? <Text style={styles.button2Style}> Cancel </Text> : null
                        }
                    </TouchableOpacity>
                </View> */}

            </LinearGradient>

        )
    }

    renderSharesEventDayModal() {
        return (

            <LinearGradient style={styles.shareEventCreate} colors={['#F0F0F0', '#E4E4E4']}>

                <LinearGradient style={styles.shareEventDayheaderTitle} colors={['#EC0900', '#FF0D05']}>

                    <View style={{ flexDirection: 'row' }}>
                        {
                            this.state.fontLoaded ? <Text style={styles.shareEventDayheaderTextTitle}> Share Event Day </Text> : null
                        }

                        <TouchableOpacity>
                            <Image source={require('./assets/favouriteicon.png')} style={{ width: 18, height: 18, resizeMode: 'stretch', marginLeft: 5 }} />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={this.like} style={styles.likearea}>
                            <Image source={require('./assets/likeicon.png')} style={{ width: 18, height: 18, resizeMode: 'stretch', marginLeft: 5, marginRight: 10 }} />
                            {
                                this.state.fontLoaded ? <Text style={styles.likeCount}>{likeCount}</Text> : null
                            }
                        </TouchableOpacity>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity>
                            <Image source={require('./assets/fb.png')} style={{ width: 25, height: 25, resizeMode: 'stretch', marginLeft: 20 }} />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Image source={require('./assets/twitter.png')} style={{ width: 25, height: 25, resizeMode: 'stretch', marginLeft: 5 }} />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Image source={require('./assets/instagram.png')} style={{ width: 25, height: 25, resizeMode: 'stretch', marginLeft: 5 }} />
                        </TouchableOpacity>
                    </View>

                </LinearGradient>
                {this.renderUserList(this.toggleModal2)}
                {/* <View style={styles.shareEventBody}>

                    <FlatList
                        style={{ flexDirection: 'row', justifyContent: 'center', flexWrap: 'wrap', margin: 1 }}
                        // data={[{key: 'a'}, {key: 'b'},{key: 'c'},{key: 'd'}, {key: 'e'},{key: 'f'},{key: 'g'}, {key: 'h'},{key: 'i'},{key: 'j'}]}
                        // data={this.state.attendee}
                        renderItem={this.renderSharesEventDayItem}
                    />

                </View>

                <View style={styles.buttonStyle}>
                    <TouchableOpacity>
                        {
                            this.state.fontLoaded ? <Text style={styles.button1Style}> Send Event </Text> : null
                        }
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.toggleModal2}>
                        {
                            this.state.fontLoaded ? <Text style={styles.button2Style}> Cancel </Text> : null
                        }
                    </TouchableOpacity>
                </View> */}

            </LinearGradient>

        )
    }

    renderMayBeModal() {
        return (

            <LinearGradient style={styles.maybeEventCreate} colors={['#F0F0F0', '#E4E4E4']}>

                <LinearGradient style={styles.maybeEventheaderTitle} colors={['#EC0900', '#FF0D05']}>
                    {
                        this.state.fontLoaded ? <Text style={styles.maybeEventheaderTextTitle}> Maybe </Text> : null
                    }
                </LinearGradient>

                {/* <View style={styles.maybeEventBody}>
                    <FlatList
                        style={{ flexDirection: 'row', justifyContent: 'center', flexWrap: 'wrap', margin: 1 }}
                        // data={[{key: 'a'}, {key: 'b'},{key: 'c'},{key: 'd'}, {key: 'e'},{key: 'f'},{key: 'g'}, {key: 'h'},{key: 'i'},{key: 'j'}]}
                        // data={this.state.attendee}
                        renderItem={this.renderMaybeItem}
                    />
                </View>

                <View style={styles.buttonStyle}>
                    <TouchableOpacity onPress={this.toggleModal3}>
                        {
                            this.state.fontLoaded ? <Text style={styles.button1Style}> Close </Text> : null
                        }
                    </TouchableOpacity>
                </View> */}
                {this.renderUserList(this.toggleModal3)}

            </LinearGradient>
        )
    }

    renderSharesModal() {
        return (

            <LinearGradient style={styles.shareEventCreate} colors={['#F0F0F0', '#E4E4E4']}>

                <LinearGradient style={styles.shareEventheaderTitle} colors={['#EC0900', '#FF0D05']}>
                    {
                        this.state.fontLoaded ? <Text style={styles.shareEventheaderTextTitle}> Shares </Text> : null
                    }
                </LinearGradient>

                {/* <View style={styles.shareEventBody}>
                    <FlatList
                        style={{ flexDirection: 'row', justifyContent: 'center', flexWrap: 'wrap', margin: 1 }}
                        // data={[{key: 'a'}, {key: 'b'},{key: 'c'},{key: 'd'}, {key: 'e'},{key: 'f'},{key: 'g'}, {key: 'h'},{key: 'i'},{key: 'j'}]}
                        // data={this.state.attendee}
                        renderItem={this.renderSharesItem}
                    />
                </View>

                <View style={styles.buttonStyle}>
                    <TouchableOpacity onPress={this.toggleModal4}>
                        {
                            this.state.fontLoaded ? <Text style={styles.button1Style}> Close </Text> : null
                        }
                    </TouchableOpacity>
                </View> */}
                {this.renderUserList(this.toggleModal4)}

            </LinearGradient>
        )
    }

    render() {
        return (
            <View style={{
                alignSelf: 'stretch', alignItems: 'center', justifyContent: 'center', padding: 2, flexDirection: 'row',
                height: 30, width: 140, backgroundColor: '#EBEBEB', borderWidth: 1, borderColor: '#B2AFA4', borderRadius: 3, top: 5, right: 20
            }}>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity onPress={() => { this.toggleModal1(); this.getAttendee("-LGYj8BK7cN42mDUmzpH") }}>
                        <View style={{ height: 30, width: 35, alignItems: 'center', justifyContent: 'center' }}>
                            {
                                this.state.fontLoaded ? <Text style={{ fontSize: 7, color: 'red', fontFamily: 'coolvetica' }}> {this.props.invited} </Text> : null
                            }
                            {
                                this.state.fontLoaded ? <Text style={{ fontSize: 6, color: '#494949', fontFamily: 'coolvetica' }}> Invited </Text> : null
                            }
                        </View>
                        <View >
                            <Modal isVisible={this.state.isInviteModalVisible}>
                                <View>
                                    {this.renderInviteModal()}
                                </View>
                            </Modal>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { this.toggleModal2() }}>
                        <View style={{ height: 30, width: 35, alignItems: 'center', justifyContent: 'center' }}>
                            {
                                this.state.fontLoaded ? <Text style={{ fontSize: 7, color: 'red', fontFamily: 'coolvetica' }}> {this.props.attended} </Text> : null
                            }
                            {
                                this.state.fontLoaded ? <Text style={{ fontSize: 6, color: '#494949', fontFamily: 'coolvetica' }}> Attended </Text> : null
                            }
                        </View>
                        <View >
                            <Modal isVisible={this.state.isShareEventDayModalVisible}>
                                <View>
                                    {this.renderSharesEventDayModal()}
                                </View>
                            </Modal>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { this.toggleModal3() }}>
                        <View style={{ height: 30, width: 35, alignItems: 'center', justifyContent: 'center' }}>
                            {
                                this.state.fontLoaded ? <Text style={{ fontSize: 7, color: 'red', fontFamily: 'coolvetica' }}> {this.props.liked} </Text> : null
                            }
                            {
                                this.state.fontLoaded ? <Text style={{ fontSize: 6, color: '#494949', fontFamily: 'coolvetica' }}> Maybe </Text> : null
                            }
                        </View>
                        <View >
                            <Modal isVisible={this.state.isMayBeModalVisible}>
                                <View>
                                    {this.renderMayBeModal()}
                                </View>
                            </Modal>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { this.toggleModal4() }}>
                        <View style={{ height: 30, width: 35, alignItems: 'center', justifyContent: 'center' }}>
                            {
                                this.state.fontLoaded ? <Text style={{ fontSize: 7, color: 'red', fontFamily: 'coolvetica' }}> {this.props.shares} </Text> : null
                            }
                            {
                                this.state.fontLoaded ? <Text style={{ fontSize: 6, color: '#494949', fontFamily: 'coolvetica' }}> Shares </Text> : null
                            }
                        </View>
                        <View >
                            <Modal isVisible={this.state.isSharesModalVisible}>
                                <View>
                                    {this.renderSharesModal()}
                                </View>
                            </Modal>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    inviteEventCreate: {
        alignSelf: 'stretch',
        paddingBottom: 10,
        borderWidth: 1,
        borderColor: '#B7B7B7',
        borderRadius: 5,
        marginHorizontal: 25,
    },

    inviteEventheaderTitle: {
        alignSelf: 'stretch',
        flexDirection: 'row',
        height: 40,
        borderRadius: 3,
        paddingHorizontal: 5,
        justifyContent: 'space-between',
        alignItems: 'center'
    },

    inviteEventheaderTextTitle: {
        fontSize: 12,

        color: '#fff',
        fontFamily: 'coolvetica'
    },

    inviteEventBody: {
        height: 225,
        padding: 5,
    },

    inviteEventBody1: {
        flexDirection: 'row',
        padding: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },

    inviteEventUser: {
        height: 90,
        width: 75,
        alignItems: 'center',
        justifyContent: 'center',
    },



    maybeEventCreate: {
        alignSelf: 'stretch',
         paddingBottom: 10,
        borderWidth: 1,
        borderColor: '#B7B7B7',
        borderRadius: 5,
        marginHorizontal: 25,
    },

    maybeEventheaderTitle: {
        alignSelf: 'stretch',
        flexDirection: 'row',
        height: 40,
        borderRadius: 3,
        paddingHorizontal: 5,
        justifyContent: 'space-between',
        alignItems: 'center'
    },

    maybeEventheaderTextTitle: {
        fontSize: 15,

        color: '#fff',
        fontFamily: 'coolvetica'
    },

    maybeEventBody: {
        alignSelf: 'stretch',
        height: 195,
        padding: 5,
    },

    maybeEventBody1: {
        flexDirection: 'row',
        padding: 3,
        alignItems: 'center',
        justifyContent: 'center',
    },

    maybeEventUser: {
        height: 50,
        width: 50,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 1
    },



    shareEventCreate: {
        alignSelf: 'stretch',
        paddingBottom: 10,
        borderWidth: 1,
        borderColor: '#B7B7B7',
        borderRadius: 5,
        marginHorizontal: 25,
    },

    shareEventheaderTitle: {
        alignSelf: 'stretch',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 5,
        height: 40,
        borderRadius: 3,
        justifyContent: 'space-between',
        alignItems: 'center'
    },

    shareEventheaderTextTitle: {
        fontSize: 15,

        color: '#fff',
        fontFamily: 'coolvetica'
    },

    shareEventBody: {
        alignSelf: 'stretch',
        height: 195,
        padding: 5,
    },

    shareEventBody1: {
        flexDirection: 'row',
        padding: 3,
        alignItems: 'center',
        justifyContent: 'center',
    },

    shareEventUser: {
        height: 50,
        width: 50,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 1
    },


    shareEventCreate: {
         paddingBottom: 10,
        alignSelf: 'stretch',
        borderWidth: 1,
        borderColor: '#B7B7B7',
        borderRadius: 5,
        marginHorizontal: 25,
    },

    shareEventDayheaderTitle: {
        flexDirection: 'row',
        alignSelf: 'stretch',
        height: 40,
        borderRadius: 3,
        paddingHorizontal: 5,
        justifyContent: 'space-between',
        alignItems: 'center'
    },

    shareEventDayheaderTextTitle: {
        fontSize: 12,
        top: 3,
        color: '#fff',
        fontFamily: 'coolvetica'
    },

    shareEventBody: {
        alignSelf: 'stretch',
        height: 195,
        padding: 5,
    },

    shareEventBody1: {
        flexDirection: 'row',
        padding: 3,
        alignItems: 'center',
        justifyContent: 'center',
    },

    shareEventUser: {
        height: 50,
        width: 50,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 1
    },

    likearea: {
        position: 'relative',
    },

    likeCount: {
        position: 'absolute',
        top: -1,
        color: '#fff',
        backgroundColor: '#EB1406',
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#fff',
        width: 13,
        height: 13,
        fontSize: 5,
        paddingTop: 4,
        textAlign: 'center',
        left: 18,
        fontFamily: 'coolvetica',
        marginTop: 1
    },



    buttonStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },

    button1Style: {
        fontSize: 15,
        color: '#F30F00',
        paddingVertical: 5,
        marginRight: 5,
        fontFamily: 'coolvetica'
    },

    button2Style: {
        fontSize: 15,

        color: '#F30F00',
        paddingVertical: 5,
        marginRight: 10,
        fontFamily: 'coolvetica'
    },

})

const transcriptStatetoProps = (state, ownProps) => {
    console.log(ownProps.eventKey);
    let treanscriptMetrics = _.findLast(state.eventData.eventTranscript, ['eventKey', ownProps.eventKey]);
    //let treanscriptMetrics = state.eventData.eventTranscript;
    //console.log(treanscriptMetrics);
    return {
        invited: treanscriptMetrics ? treanscriptMetrics.invited : 0,
        attended: treanscriptMetrics ? treanscriptMetrics.attended : 0,
        liked: treanscriptMetrics ? treanscriptMetrics.liked : 0,
        shares: treanscriptMetrics ? treanscriptMetrics.shares : 0

    }
}

export default connect(transcriptStatetoProps, { })(TranscriptBoxTopRight);