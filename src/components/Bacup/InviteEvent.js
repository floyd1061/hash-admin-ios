import React, { Component } from 'react'
import { View, Text, TextInput, Image, TouchableOpacity, StyleSheet } from 'react-native'
import {LinearGradient} from 'expo'
import Modal from "react-native-modal";


export default class InviteEvent extends Component {

    state = {
        isModalVisible: false,
    };
     
    _toggleModal = () =>
        this.setState({ isModalVisible: !this.state.isModalVisible });
     
    render() {
        return (
          <View style={{ flex: 1, paddingTop: 100 }}>
            <TouchableOpacity onPress={this._toggleModal}>
              <Text>Show Modal</Text>
            </TouchableOpacity>
            <Modal isVisible={this.state.isModalVisible} style = {{flex:1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 20}}>
                <LinearGradient style = {styles.inviteEventCreate} colors = { ['#F0F0F0', '#E4E4E4'] }>

                        <LinearGradient style = {styles.inviteEventheaderTitle} colors = { ['#EC0900', '#FF0D05'] }>
                            <Text style = {styles.inviteEventheaderTextTitle}> Invite Friends to the Event </Text>
                            <TouchableOpacity>
                                <Image source={require('./assets/fb.png')} style={{width: 25, height: 25, resizeMode: 'stretch', marginTop: 7, marginLeft: 15}}/>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Image source={require('./assets/twitter.png')} style={{width: 25, height: 25, resizeMode: 'stretch', marginTop: 7, marginLeft: 5}}/>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Image source={require('./assets/instagram.png')} style={{width: 25, height: 25, resizeMode: 'stretch', marginTop: 7, marginLeft: 5}}/>
                            </TouchableOpacity>
                        </LinearGradient>


                        <View style = {styles.inviteEventBody}>

                            <View style = {styles.inviteEventBody1}>
                                <TouchableOpacity style = {styles.inviteEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 55, height: 55, resizeMode: 'stretch'}}/>
                                    <Image source={require('./assets/HostAdd.png')} style={{position: 'absolute' ,width: 25, height: 25, resizeMode: 'stretch', top: 5, right: 3}}/>
                                    <Text style = {{fontSize: 8, fontWeight: 'bold', color: '#fff', color: '#9F9F9F', textAlign: 'center'}}> John Smith </Text>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.inviteEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 55, height: 55, resizeMode: 'stretch'}}/>
                                    <Image source={require('./assets/HostAdd.png')} style={{position: 'absolute' ,width: 25, height: 25, resizeMode: 'stretch', top: 5, right: 3}}/>
                                    <Text style = {{fontSize: 8, fontWeight: 'bold', color: '#fff', color: '#9F9F9F', textAlign: 'center'}}> Heyley Burn </Text>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.inviteEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 55, height: 55, resizeMode: 'stretch'}}/>
                                    <Image source={require('./assets/HostAdd.png')} style={{position: 'absolute' ,width: 25, height: 25, resizeMode: 'stretch', top: 5, right: 3}}/>
                                    <Text style = {{fontSize: 8, fontWeight: 'bold', color: '#fff', color: '#9F9F9F', textAlign: 'center'}}> Joey Rice </Text>
                                </TouchableOpacity>
                            </View>

                            <View style = {styles.inviteEventBody1}>
                                <TouchableOpacity style = {styles.inviteEventUser}>
                                    <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 55, height: 55, resizeMode: 'stretch'}}/>
                                    <Image source={require('./assets/HostAdd1.png')} style={{position: 'absolute' ,width: 25, height: 25, resizeMode: 'stretch', top: 5, right: 3}}/>
                                    <Text style = {{fontSize: 8, fontWeight: 'bold', color: '#fff', color: '#9F9F9F', textAlign: 'center'}}> David Corn </Text>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.inviteEventUser}>
                                    <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 55, height: 55, resizeMode: 'stretch'}}/>
                                    <Image source={require('./assets/HostAdd1.png')} style={{position: 'absolute' ,width: 25, height: 25, resizeMode: 'stretch', top: 5, right: 3}}/>
                                    <Text style = {{fontSize: 8, fontWeight: 'bold', color: '#fff', color: '#9F9F9F', textAlign: 'center'}}> Stephen Cain </Text>
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.inviteEventUser}>
                                    <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 55, height: 55, resizeMode: 'stretch'}}/>
                                    <Image source={require('./assets/HostAdd.png')} style={{position: 'absolute' ,width: 25, height: 25, resizeMode: 'stretch', top: 5, right: 3}}/>
                                    <Text style = {{fontSize: 8, fontWeight: 'bold', color: '#fff', color: '#9F9F9F', textAlign: 'center'}}> Joey Rice </Text>
                                </TouchableOpacity>
                            </View>

                        </View>

                        <View style = {styles.buttonStyle}>
                            <TouchableOpacity>
                                <Text style = {styles.button1Style}> Send Invitation </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this._toggleModal}>
                                <Text style = {styles.button2Style}> Cancel </Text>
                            </TouchableOpacity>
                        </View>

                    </LinearGradient>
            </Modal>
          </View>
        );
    }
}

const styles = StyleSheet.create({
    inviteEventCreate: {
        alignSelf: 'stretch',        
        height: 320,
        // backgroundColor: '#F1F1F1',
        borderWidth: 1,
        borderColor: '#B7B7B7',
        borderRadius: 5
    },

    inviteEventheaderTitle: {
        alignSelf: 'stretch',   
        flexDirection: 'row',
        height: 40,
        borderRadius: 3
    },

    inviteEventheaderTextTitle: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#fff',
        marginLeft: 5,
        marginTop: 11
    },

    inviteEventBody: {
        alignSelf: 'stretch', 
        height: 225,
        padding: 5,
        // backgroundColor: '#ccc'
    },

    inviteEventBody1: {
        flexDirection: 'row',
        padding: 5,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: '#000'
    },

    inviteEventUser: {
        height: 90,
        width: 75,
        // backgroundColor: '#ccc',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 5
    },

    buttonStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        // backgroundColor: '#000',    
    },

    button1Style: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#F30F00',
        paddingVertical: 5,
        marginRight: 5   
    },

    button2Style: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#F30F00',
        paddingVertical: 5,
        marginRight: 10   
    },
});
