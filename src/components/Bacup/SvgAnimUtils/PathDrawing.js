import React, { PureComponent } from 'react'
import { View, Animated, Easing } from 'react-native';
import { svgPathProperties } from 'svg-path-properties';
import Svg, {
    LinearGradient,
    Path,
    Defs,
    Stop
} from 'react-native-svg'
import svgOne from '../SvgPoints/VivusHi';

const AnimatedPath = Animated.createAnimatedComponent(Path);

export default class helperSvgPoints extends PureComponent {
    constructor(props) {
        super(props);
        
        this.def = defOne
        this.d = svgOne
        this.svgAnimationLength = this.props.svgAnimationLength;

        const properties = svgPathProperties(this.d);
        this.length = properties.getTotalLength();
        this.strokeDashoffset = new Animated.Value(this.length);

        
    }


    componentDidMount() {
        ////console.log(this.props);
        this.animate();
    }

    animate = () => {
        this.strokeDashoffset.setValue(this.length);
        Animated.sequence([
            Animated.delay(1000),
            Animated.timing(this.strokeDashoffset, {
                toValue: this.svgAnimationLength,
                easing: Easing.bounce,
                duration: 4000
            })
        ]).start();
    };
    
    render() {
        return (
            <Svg
                height={278.1393}
                width={170.4683}
            >
                {this.def}
                <AnimatedPath fill="none"
                    stroke="url(#df0272b4-af27-4498-a012-8e6b05c8c6c0)"
                   
                    strokeWidth={28.125}
                    strokeDashoffset={this.strokeDashoffset}
                    strokeDasharray={[this.length, this.length]}
                    d={this.d}
                />
                <AnimatedPath fill="none"
                    stroke="url(#0629d1b3-c525-43f9-b7a3-ad319f4252fc)"
                    
                    strokeWidth={19.6875}
                    strokeDashoffset={this.strokeDashoffset}
                    strokeDasharray={[this.length, this.length]}
                    d={this.d}
                />
            </Svg>
        )
    }

}

const defOne = (
    <Defs>
        <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c0" x1="99.005" x2="99.005" y2="279.1393" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4C23" />
            <Stop offset="1" stopColor="#EF4923" />
        </LinearGradient>
        <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc" x1="99.005" x2="99.005" y2="279.1393" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#E21F26" />
            <Stop offset="1" stopColor="#EF4323" />
        </LinearGradient>

    </Defs>
);

const defTwo = (
    <Defs>
          <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c0" x1="98.4425" y1="281.4884" x2="98.4425" y2="-0.5703" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4723" />
            <Stop offset="1" stopColor="#EF4923" />
          </LinearGradient>
          <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc" x1="98.4425" y1="281.4884" x2="98.4425" y2="-0.5703" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#F04023" />
            <Stop offset="1" stopColor="#F04E23" />
          </LinearGradient>

        </Defs>
)

const defThree = (
    <Defs>
          <LinearGradient id="df0272b4-af27-4498-a012-8e6b05c8c6c0" x1="48.1024" y1="134.1382" x2="161.4439" y2="134.1382" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#ED4323" />
            <Stop offset="1" stopColor="#E32626" />
          </LinearGradient>
          <LinearGradient id="0629d1b3-c525-43f9-b7a3-ad319f4252fc" x1="48.1024" y1="134.1382" x2="161.4439" y2="134.1382" gradientUnits="userSpaceOnUse">
            <Stop offset="0" stopColor="#EF4923" />
            <Stop offset="1" stopColor="#EF4723" />
          </LinearGradient>

        </Defs>
)