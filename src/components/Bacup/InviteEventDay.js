import React, { Component } from 'react'
import { View, Text, TextInput, Image, TouchableOpacity, StyleSheet } from 'react-native'
import {LinearGradient} from 'expo'
import Modal from "react-native-modal";

const likeCount = 45;

export default class InviteEventDay extends Component {

    state = {
        isModalVisible: false,
    };
     
    _toggleModal = () =>
        this.setState({ isModalVisible: !this.state.isModalVisible });
     
    render() {
        return (
          <View style={{ flex: 1, paddingTop: 100 }}>
            <TouchableOpacity onPress={this._toggleModal}>
              <Text>Show Modal</Text>
            </TouchableOpacity>
            <Modal isVisible={this.state.isModalVisible} style = {{flex:1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 20}}>
              
            <LinearGradient style = {styles.shareEventCreate} colors = { ['#F0F0F0', '#E4E4E4'] }>

                <LinearGradient style = {styles.shareEventDayheaderTitle} colors = { ['#EC0900', '#FF0D05'] }>
                    <Text style = {styles.shareEventDayheaderTextTitle}> Share Event Day </Text>

                    <TouchableOpacity>
                        <Image source={require('./assets/favouriteicon.png')} style={{width: 18, height: 18, resizeMode: 'stretch', marginTop: 10, marginLeft: 5}}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.like} style = {styles.likearea}>
                        <Image source={require('./assets/likeicon.png')} style={{width: 18, height: 18, resizeMode: 'stretch', marginTop: 10, marginLeft: 5, marginRight: 10}}/>
                        <Text style = {styles.likeCount}>{likeCount}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <Image source={require('./assets/fb.png')} style={{width: 25, height: 25, resizeMode: 'stretch', marginTop: 7, marginLeft: 20}}/>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image source={require('./assets/twitter.png')} style={{width: 25, height: 25, resizeMode: 'stretch', marginTop: 7, marginLeft: 5}}/>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image source={require('./assets/instagram.png')} style={{width: 25, height: 25, resizeMode: 'stretch', marginTop: 7, marginLeft: 5}}/>
                    </TouchableOpacity>
                </LinearGradient>

                <View style = {styles.shareEventBody}>

                    <View style = {styles.shareEventBody1}>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                    </View>

                    <View style = {styles.shareEventBody1}>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                    </View>

                    <View style = {styles.shareEventBody1}>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.shareEventUser}>
                            <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                    </View>

                </View>

                <View style = {styles.buttonStyle}>
                    <TouchableOpacity>
                        <Text style = {styles.button1Style}> Send Event </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this._toggleModal}>
                        <Text style = {styles.button2Style}> Cancel </Text>
                    </TouchableOpacity>
                </View>

            </LinearGradient>
    
            
            </Modal>
          </View>
        );
    }
}

const styles = StyleSheet.create({
    shareEventCreate: {
        height: 270,
        alignSelf: 'stretch', 
        borderWidth: 1,
        borderColor: '#B7B7B7',
        borderRadius: 5
    },

    shareEventheaderTitle: {
        alignSelf: 'stretch', 
        flexDirection: 'row',
        height: 40,
        borderRadius: 3
    },

    shareEventheaderTextTitle: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#fff',
        marginLeft: 5,
        marginTop: 10
    },

    shareEventBody: {
        alignSelf: 'stretch', 
        height: 195,
        padding: 5,
        // backgroundColor: '#ccc'
    },

    shareEventBody1: {
        flexDirection: 'row',
        padding: 3,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: '#000'
    },

    shareEventUser: {
        height: 50,
        width: 50,
        // backgroundColor: '#ccc',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 1
    },

    shareEventDayheaderTitle: {
        flexDirection: 'row',
        alignSelf: 'stretch', 
        height: 40,
        borderRadius: 3
    },

    shareEventDayheaderTextTitle: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#fff',
        marginLeft: 5,
        marginTop: 11
    },

    likearea: {
        position: 'relative',
    },

    likeCount: {
        position: 'absolute',
        top: 8,
        color: '#fff',
        backgroundColor: '#EB1406',
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#fff',
        width: 13,
        height: 13,
        fontSize: 5,
        paddingTop: 3,
        textAlign: 'center',
        left: 18,
    },

    buttonStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        // backgroundColor: '#000',    
    },

    button1Style: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#F30F00',
        paddingVertical: 5,
        marginRight: 5   
    },

    button2Style: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#F30F00',
        paddingVertical: 5,
        marginRight: 10   
    },

});
