import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet, DrawerLayoutAndroid, Image } from 'react-native'
import { LinearGradient } from 'expo'

const notificationCount = 2;

export default class NotificationDrawer extends Component {


    render() {

        var navigationView = (

            <View style={{
                alignSelf: 'stretch', alignItems: 'center', justifyContent: 'center', flexDirection: 'column', position: 'absolute',
                height: 270, width: 100, backgroundColor: '#EBEBEB', borderWidth: 1, borderColor: '#B2AFA4', borderRadius: 3, top: -2, right: -2, paddingTop: 25
            }}>

                <TouchableOpacity style = {{position: 'absolute'}}>
                        <Text style={styles.messageCount}>{notificationCount}</Text>
                </TouchableOpacity>


                <View style={{ flexDirection: 'column' }}>
                    <TouchableOpacity>
                        <View style={{ height: 50, width: 100, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'red', }}> 65 </Text>
                            <Text style={{ fontSize: 10, fontWeight: 'bold', color: '#494949', }}> Notifications </Text>
                        </View>
                    
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <View style={{ height: 50, width: 100, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'red', }}> 80 </Text>
                            <Text style={{ fontSize: 10, fontWeight: 'bold', color: '#494949', }}> Events </Text>
                        </View>
                        
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <View style={{ height: 50, width: 100, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'red', }}> 400 </Text>
                            <Text style={{ fontSize: 10, fontWeight: 'bold', color: '#494949', }}> Friends </Text>
                        </View>
                        
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <View style={{ height: 50, width: 100, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'red', }}> 1000 </Text>
                            <Text style={{ fontSize: 10, fontWeight: 'bold', color: '#494949', }}> Followers </Text>
                        </View>
                        
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <View style={{ height: 50, width: 100, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'red', }}> 55 </Text>
                            <Text style={{ fontSize: 10, fontWeight: 'bold', color: '#494949', }}> Favourites </Text>
                        </View>
                        
                    </TouchableOpacity>
                </View>

            </View>

        );


        return (
            <DrawerLayoutAndroid 
                drawerWidth={100}
                drawerBackgroundColor = 'transparent'
                drawerPosition={DrawerLayoutAndroid.positions.Right}
                renderNavigationView={() => navigationView}>

            </DrawerLayoutAndroid>
            
        )
    }

}


const styles = StyleSheet.create({

    messageCount: {
        // position: 'absolute',
        top: 0,
        color: '#fff',
        backgroundColor: '#FE2800',
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#fff',
        width: 30,
        height: 30,
        fontSize: 10,
        paddingTop: 7,
        textAlign: 'center',
        marginRight: 100
        
    },
});

