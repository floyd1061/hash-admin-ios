import React, { Component } from 'react'
import { View, Text, TextInput, Image, TouchableOpacity, StyleSheet } from 'react-native'
import {LinearGradient} from 'expo'
import Modal from "react-native-modal";


export default class MayBeEvent extends Component {

    state = {
        isModalVisible: false,
    };
     
    _toggleModal = () =>
        this.setState({ isModalVisible: !this.state.isModalVisible });
     
    render() {
        return (
          <View style={{ flex: 1, paddingTop: 100 }}>
            <TouchableOpacity onPress={this._toggleModal}>
              <Text>Show Modal</Text>
            </TouchableOpacity>
            <Modal isVisible={this.state.isModalVisible} style = {{flex:1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 20}}>
            
            <LinearGradient style = {styles.maybeEventCreate} colors = { ['#F0F0F0', '#E4E4E4'] }>

                <LinearGradient style = {styles.maybeEventheaderTitle} colors = { ['#EC0900', '#FF0D05'] }>
                    <Text style = {styles.maybeEventheaderTextTitle}> Maybe </Text>
                </LinearGradient>

                <View style = {styles.maybeEventBody}>

                    <View style = {styles.maybeEventBody1}>
                        <TouchableOpacity style = {styles.maybeEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.maybeEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.maybeEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.maybeEventUser}>
                            <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.maybeEventUser}>
                            <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                    </View>

                    <View style = {styles.maybeEventBody1}>
                        <TouchableOpacity style = {styles.maybeEventUser}>
                            <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.maybeEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.maybeEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.maybeEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.maybeEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                    </View>

                    <View style = {styles.maybeEventBody1}>
                        <TouchableOpacity style = {styles.maybeEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.maybeEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.maybeEventUser}>
                            <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.maybeEventUser}>
                            <Image source={require('./assets/avatar.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.maybeEventUser}>
                            <Image source={require('./assets/avater.png')} style={{position: 'relative' ,width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                    </View>

                </View>

                <View style = {styles.buttonStyle}>
                    <TouchableOpacity onPress={this._toggleModal}>
                        <Text style = {styles.button1Style}> Close </Text>
                    </TouchableOpacity>
                </View>


                </LinearGradient>
            </Modal>
          </View>
        );
    }
}

const styles = StyleSheet.create({
    maybeEventCreate: {
        alignSelf: 'stretch', 
        height: 270,
        borderWidth: 1,
        borderColor: '#B7B7B7',
        borderRadius: 5
    },

    maybeEventheaderTitle: {
        alignSelf: 'stretch', 
        flexDirection: 'row',
        height: 40,
        borderRadius: 3,
        paddingHorizontal: 5,
        justifyContent: 'space-between',
        alignItems: 'center'
    },

    maybeEventheaderTextTitle: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#fff',
    },

    maybeEventBody: {
        alignSelf: 'stretch', 
        height: 195,
        padding: 5,
        // backgroundColor: '#ccc'
    },

    maybeEventBody1: {
        flexDirection: 'row',
        padding: 3,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: '#000'
    },

    maybeEventUser: {
        height: 50,
        width: 50,
        // backgroundColor: '#ccc',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 1
    },

    buttonStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        // backgroundColor: '#000',    
    },

    button1Style: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#F30F00',
        paddingVertical: 5,
        marginRight: 5   
    },
});
