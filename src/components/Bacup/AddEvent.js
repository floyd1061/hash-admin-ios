import React, { Component } from 'react'
import { View, Text, TextInput, Image, TouchableOpacity, StyleSheet } from 'react-native'
import {LinearGradient} from 'expo'
import Slider from 'react-native-slider' 
import DatePicker from 'react-native-datepicker';



export default class AddEvent extends Component {

    state = {
        startingtime: "00:00",
        endingtime: "00:00"
    };
    render() {
        return (
              <LinearGradient style = {styles.eventCreate} colors = { ['#F0F0F0', '#E4E4E4'] }>
                <LinearGradient style = {styles.headerTitle} colors = { ['#EC0900', '#FF0D05'] }>
                    <Text style = {styles.headerTextTitle}> Add Event Title </Text>

                    <TouchableOpacity style = {styles.timeSelectorStart}>
                        <Text style = {styles.timeSelectorStartText}>Starts {this.state.startingtime}</Text>
                        <DatePicker
                            // style={{width: 100, height: 30}}

                            mode="time"
                            format="HH:mm"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            minuteInterval={10}
                            customStyles={{
                                dateInput: {
                                    borderWidth:0,
                                    
                                }}
                            }
                            onDateChange={(startingtime) => {this.setState({startingtime: startingtime});}}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity style = {styles.timeSelectorEnd}>
                        <Text style = {styles.timeSelectorEndText}>Ends {this.state.endingtime} </Text>
                        <DatePicker
                            // style={{width: 100, height: 30}}
                            mode="time"
                            format="HH:mm"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            minuteInterval={10}
                            customStyles={{
                                dateInput: {
                                    borderWidth:0,
                                    
                                }}
                            }
                            onDateChange={(endingtime) => {this.setState({endingtime: endingtime});}}
                        />
                    </TouchableOpacity>
                </LinearGradient>
                <View style = {styles.eventBody}>
                    <Text style = {styles.eventBodyText}> Update your event priority... </Text>
                    <Slider 
                        step= {1}
                        thumbStyle = {{borderWidth: 3, borderColor: '#FFFFFF'}}
                        trackStyle = {{ height: 15, borderWidth: 3, borderColor: '#726358', borderRadius: 10 }}
                        minimumTrackTintColor= '#F50D00'
                        maximumTrackTintColor= '#5C534F'
                        thumbTintColor= '#FD1700'
                        // maximumValue= '100'
                        style= {{ width: 135, marginLeft: 10, marginTop: 6 }}
                        value={this.state.value}
                        onValueChange={value => this.setState({ value })}
                    />
                </View>
                <View style = {styles.textInputStyle}>
                    <TextInput
                        style={{ flex: 1 }}
                        placeholder="Enter event name"
                        underlineColorAndroid="transparent"
                        style = {{height: 30, width: 250, borderRadius: 2, borderWidth: 2, borderColor: '#E90600', marginLeft: 8, paddingLeft: 10}}   
                    />
                </View>
                <View style = {styles.buttonStyle}>
                    <TouchableOpacity onPress={this._toggleModal}>
                        <Text style = {styles.button1Style}> Cancel </Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style = {styles.button2Style}> OK </Text>
                    </TouchableOpacity>
                </View>
                </LinearGradient>
        );
    }
}

const styles = StyleSheet.create({
    eventCreate: {
        alignSelf: 'stretch',
        height: 150,
        borderWidth: 1,
        borderColor: '#B7B7B7',
        borderRadius: 5    
    },

    headerTitle: {
        flexDirection: 'row',
        height: 35,
        alignSelf: 'stretch',
        // width: 268,
        borderRadius: 3
    },

    headerTextTitle: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#fff',
        marginLeft: 10,
        marginTop: 5
    },

    timeSelectorStart: {
        height: 25,
        width: 72,
        backgroundColor: '#fff',
        borderRadius: 2,
        marginTop: 5,
        marginLeft: 30
    },

    timeSelectorStartText: {
        fontSize: 9,
        fontWeight: 'bold',
        color: '#FF1105',
        textAlign: 'center',
        marginTop: 5
    },

    timeSelectorEnd: {
        height: 25,
        width: 72,
        backgroundColor: '#fff',
        borderRadius: 2,
        marginTop: 5,
        marginLeft: 10
    },

    timeSelectorEndText: {
        fontSize: 9,
        fontWeight: 'bold',
        color: '#FF1105',
        textAlign: 'center',
        marginTop: 5
    },

    eventBody: {
        flexDirection: 'row', 
        alignSelf: 'stretch',
    },

    eventBodyText: {
        fontSize: 9,
        fontWeight: 'bold',
        color: '#544D48',
        marginLeft: 3,
        marginTop: 20
    },

    textInputStyle: {

    },

    buttonStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        // backgroundColor: '#000',    
    },

    button1Style: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#F30F00',
        paddingVertical: 5,
        marginRight: 5   
    },

    button2Style: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#F30F00',
        paddingVertical: 5,
        marginRight: 10   
    },
});
