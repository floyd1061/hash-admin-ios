import React, { Component } from 'react'
import { Text, View, StyleSheet, TextInput, Image } from 'react-native'

export default class CommentTextInputFooter extends Component {
    render(props) {
        return (
            <View style={styles.container}>
                <View style={styles.commentTextInputSection}>
                    <TextInput
                        style={{ flex: 1, fontSize: 8 }}
                        placeholder="Write a comment..."
                        returnKeyType="go"
                        underlineColorAndroid="transparent"
                    />
                    <View style={styles.uploadAndCamerabuttonSection}>
                        <View style={{ marginRight: 5 }}><Image style={styles.uploadCamera} source={require('./assets/upload.png')} /></View>
                        <View><Image style={styles.uploadCamera} source={require('./assets/camera.png')} /></View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 30,
        paddingTop: 10,
        backgroundColor: '#f8f8f8',
        justifyContent: 'center',
        alignItems: 'center',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'
    },
    fontStyling: {
        fontSize: 20,
        //color: this.props.headerColor,
    },
    buttonContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10
    },
    commentTextInputSection: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#f8f8f8',
        borderWidth: .5,
        borderColor: '#f8f8f8',
        height: 15,
        marginBottom: 8,
        marginRight: 10,
        paddingLeft: 10

    },
    uploadAndCamerabuttonSection: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingRight: 5
    },
    likeIcon: {
        width: 15,
        height: 15
    },
    uploadCamera: {
        width: 10,
        height: 10
    }
});